<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\LogManager Language Lines
    |--------------------------------------------------------------------------
    */

      'log_manager'                 => '日誌管理',
      'log_manager_description'     => 'Preview, download and delete logs.',
      'actions'                     => '動作',
      'delete_confirm'              => '確定要刪除此日誌?',
      'back_to_all_logs'            => 'Back to all logs',
      'create_a_new_log'            => '創建一個新日誌',
      'date'                        => '日期',
      'delete'                      => '刪除',
      'delete_confirmation_title'   => '完成',
      'download'                    => '下載',
      'delete_error_title'          => 'Error',
      'existing_logs'               => '已存在的日誌',
      'file_size'                   => '檔案大小',
      'delete_cancel_title'         => 'It&#039;s ok',
      'last_modified'               => '最後修改',
      'logs'                        => 'Logs',
      'preview'                     => '預覽',
      'delete_cancel_message'       => '此日誌無法刪除.',
      'delete_error_message'        => '此日誌無法刪除.',
      'delete_confirmation_message' => '此日誌已刪除.',
      'log_file_doesnt_exist'       => "此日誌不存在.",

];
