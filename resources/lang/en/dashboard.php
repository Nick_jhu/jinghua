<?php 

return [
    "order" => "Order Completion",
    "error" => "Error times",
    "rate" => "Order with the rate",
    "eta" => "ETA",
    "map" => "Map",
    "onlineTruck" => "Online Truck",
    "battery" => "Battery",
    "speed" => "Speed",
    "updated" => "Updated",
    "monthly" => "Monthly Recap Report",
    "action" => "Action",
    "anotherAction" => "Another Action",
    "somethingElseHere" => "Something else here",
    "separatedLink" => "Separated link",
    "goalCompletion" => "Goal Completion",
    "addProducts" => "Add Products to Cart",
    "completePurchase" => "Complete Purchase",
    "visitPremium" => "Visit Premium Page",
    "sendInquiries" => "Send Inquiries",
    "totalRevenue" => "TOTAL REVENUE",
    "totalCost" => "TOTAL COST",
    "totalProfit" => "TOTAL PROFIT",
    "goalCompletions" => "GOAL COMPLETIONS",
    "customerService" => "Customer Service",
    ];