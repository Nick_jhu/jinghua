<?php 

return [
    "id" => "id",
    "cntryCd" => "Country Code",
    "cntryNm" => "Country Name",
    "gKey" => "Group",
    "cKey" => "Company",
    "sKey" => "Station",
    "dKey" => "Department",
    "createdAt" => "Created At",
    "updatedAt" => "Updated At",
    "updatedBy" => "Updated By",
    "createdBy" => "Created By",
    "titleName" => "Country Profile",
    ];