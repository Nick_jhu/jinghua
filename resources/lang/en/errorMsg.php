<?php 

return [
    "id" => "id",
    "reason" => "reason",
    "description" => "description",
    "type" => "type",
    "isRelieve" => "isRelieve",
    "gKey" => "Group",
    "cKey" => "Company",
    "sKey" => "Station",
    "dKey" => "Department",
    ];