@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      {{ trans('sysCustomers.titleAddName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix'),'customerProfile') }}">{{ trans('sysCustomers.titleName') }}</a></li>
		<li class="active">{{ trans('sysCustomers.titleAddName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('backpack::template.toolbar')
<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">        
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('sysCustomers.baseInfo') }}</a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('sysCustomers.custNo') }}</label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cname">{{ trans('sysCustomers.cname') }}</label>
                                        <input type="text" class="form-control" id="cname" name="cname">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ename">{{ trans('sysCustomers.ename') }}</label>
                                        <input type="text" class="form-control" id="ename" name="ename">
                                    </div>    
                                    <div class="form-group col-md-3">
                                        <label for="tax_id">{{ trans('sysCustomers.taxId') }}</label>
                                        <input type="text" class="form-control" id="tax_id" name="tax_id">
                                    </div>                                  
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="email">{{ trans('sysCustomers.email') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="eamil" class="form-control" id="email" name="email" >
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cmp_abbr">{{ trans('sysCustomers.cmpAbbr') }}</label>
                                        <input type="text" class="form-control" id="cmp_abbr" name="cmp_abbr" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('sysCustomers.status') }}</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A">{{ trans('sysCustomers.STATUS_A') }}</option>
                                            <option value="B">{{ trans('sysCustomers.STATUS_B') }}</option>
                                        </select>
                                    </div>                                                                       
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="contact">{{ trans('sysCustomers.contact') }}</label>
                                        <input type="text" class="form-control" id="contact" name="contact">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('sysCustomers.phone') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fax">{{ trans('sysCustomers.fax') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fax"></i>
                                            </div>
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>{{ trans('sysCustomers.custType') }}</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" name="cust_type[]">
                                        </select>
                                    </div>                                                                       
                                </div>                               
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="zip">{{ trans('sysCustomers.zip') }}</label>
                                        <input type="text" class="form-control" id="zip" name="zip">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="city_nm">{{ trans('sysCustomers.cityNm') }}</label>
                                        <input type="text" class="form-control" id="city_nm" name="city_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="area_nm">{{ trans('sysCustomers.areaNm') }}</label>
                                        <input type="hidden" class="form-control" id="area_id" name="area_id">
                                        <input type="text" class="form-control" id="area_nm" name="area_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="def">{{ trans('sysCustomers.def') }}</label>
                                        <select type="text" class="form-control" id="def" name="def">
                                            <option value="0">否</option>
                                            <option value="1">是</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="address">{{ trans('sysCustomers.address') }}</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sysCustomers.remark') }}</label>
                                            <textarea class="form-control" rows="3" id="remark" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>

                                {{--  <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('sysCustomers.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('sysCustomers.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('sysCustomers.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('sysCustomers.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>  --}}
                                                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>   

@endsection


@include('backpack::template.lookup')


@section('after_scripts')

<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}";

    var fieldData = null;
    var fieldObj = null;

    @if(isset($crud->create_fields))
        fieldData = '{{!! json_encode($crud->create_fields) !!}}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
    @endif


    @if(isset($id))
        mainId = "{{$id}}";
        editData = '{{!! $entry !!}}';

        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        editObj = JSON.parse(objJson);
        cust_no= editObj.cust_no;
    @endif


    $(function(){
        
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}";
        //formOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_customers') }}";
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/sys_customers') }}/" + mainId;
        formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}";
        
        formOpt.initFieldCustomFunc = function (){
            $("select[name='state[]']").select2({tags: true});
        };

        formOpt.addFunc = function() {
            $("#status").val("B");
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);      
    })
</script>    

@endsection
