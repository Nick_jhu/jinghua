@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		一般商品訂單明細總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">一般商品訂單明細</li>
	</ol>
</section>
@endsection 
@section('before_scripts')


<script>
    var gridOpt = {};
    gridOpt.pageId        = "modOrderDetailView";
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_detail_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_detail_view') }}?dbRaw=" + "{{$qUrl}}&";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create') }}";
    gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = function(row) {
        var data = $("#jqxGrid").jqxGrid('getrowdata', row);
        if(data.status == "出貨") {
            swal("已出貨商品，無法再修改", "", "warning");
            return;
        }
        if(data.pick_way != "宅配") {
            swal("只有宅配可以輸入物流單號", "", "warning");
            return;
        }
        if(data.ship_flag == "Z大") {
            swal("訂單號：" + data.ord_no + "，這筆是出給z大的！！", "", "warning");
            return;
        }

        $("#myModal").modal("show");
    };
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
            //$("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modOrder.titleName") }}');
            $("#jqxGrid").jqxGrid('exportdata', 'json', '一般商品訂單明細', true, null, false, BASE_API_URL+'/admin/export/data');
            }
        },
        {
            btnId: "btnCreateLogisticsList",
            btnIcon: "fa fa-cloud-download",
            btnText: "產生物流單",
            btnFunc: function () {   
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var CVSValidationNo = new Array();
                var status = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(row.pick_way == '宅配' || row.pick_way == '機場') {
                        swal("只有超商取貨能產生物流單", "", "warning");
                        return false;
                    }
                    if(typeof row != "undefined") {
                        if(ids.indexOf(row.id)  == -1)
                            ids.push(row.id);
                    }
                    if(row.CVSPaymentNo!= null) {
                        CVSValidationNo.push(row.ord_no)
                    }
                    if(row.status== "未付款") {
                        status.push(row.ord_no)
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                if(status.length !=0) {
                    swal(status+"未付款不能產生物流單", "", "warning");
                    return;
                }
                if(CVSValidationNo.length>0) {
                    swal(CVSValidationNo+"已產生過物流單", "", "warning");
                    return;
                }
                $("#cssLoading").show();
                $.post(BASE_URL + '/CreateLogisticsList', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                    $("#cssLoading").hide();
                });
                
            }
        },
        {
            btnId:"btnPrintBill",
            btnIcon:"fa fa-print",
            btnText:"列印小白單",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var id = "";
                if(rows.length > 1) {
                    swal("列印作業一次只能操作一筆資料", "", "warning");
                    return;
                }

                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        id = row.id;

                        if(row.pick_way == "宅配") {
                            swal("只有超取才能列印", "", "warning");
                            return;
                        }

                        if(row.CVSPaymentNo == "" || row.CVSPaymentNo == null) {
                            swal("尚未產生物流單號", "", "warning");
                            return;
                        }

                        if(row.status == "未付款") {
                            swal("已付款才能列印", "", "warning");
                            return;
                        }
                    }
                }

                if(id.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                window.open(BASE_URL + '/PrintC2CBill/' + id);

                
            }
        },
        {
            btnId:"btnProdSend",
            btnIcon:"fa fa-truck",
            btnText:"商品出貨",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        if(row.ship_flag == "Z大" || row.ship_flag == "已給Z大") {
                            swal("給Z大的貨，請按「出給Z大」", "", "warning");
                            return;
                        }
                        if(ids.indexOf(row.id) == -1)
                            ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
            
                $.post(BASE_URL + '/prodShipping', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },
        {
            btnId:"btnSendToZ",
            btnIcon:"fa fa-send",
            btnText:"出給Z大",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        if(ids.indexOf(row.id) == -1)
                            ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
            
                $.post(BASE_URL + '/sendToZ', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },
    ];

    $(function() {
        $("#submitBtn").on("click", function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    ids.push(row.id);
                }
            }

            var shipper = $("#shipper").val();
            var shipNo = $("#shipNo").val();

            if(shipNo == "") {
                swal("請填寫物流單號", "", "warning");
            }

            $.post(BASE_URL + '/vendorUpdateShipmentNo', {'ids': ids, 'shipper': shipper, 'shipNo': shipNo}, function(data){
                if(data.msg == "success") {
                    swal("操作成功", "", "success");
                    $("#jqxGrid").jqxGrid('updatebounddata');
                    $('#jqxGrid').jqxGrid('clearselection');
                    $("#myModal").modal('hide');
                }
                else{
                    swal("操作失敗", "", "error");
                }
                $("#cssLoading").hide();
            });
        });
    });
</script>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">出貨確認-訂單號：<span id="ordNo"></span></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="shipper">物流業者</label>
                    <select class="form-control" id="shipper" name="shipper">
                        @foreach($shipper as $row)
                        <option value="{{$row->cd}}">{{$row->cd_descp}}</option>
                        @endforeach
                        <option value="OTHER">其它</option>
                    </select>
                    <input type="hidden" name="ordId" id="ordId">
                </div>
                <div class="form-group">
                    <label for="shipNo">物流單號</label>
                    <input type="text" class="form-control" id="shipNo" placeholder="" name="shipNo">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                <button type="button" class="btn btn-primary" id="submitBtn">送出</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection 
@include('backpack::template.search')