<!doctype html>

<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>簽收照片</title>

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>

</head>

<body>
    @if(count($imgData) > 0)
    @foreach($imgData as $row)
        <img src="data:image/jpeg;base64,{{base64_encode(Storage::get($row->guid))}}" class="img-fluid" alt="Responsive image" width="300">
    @endforeach
    @else
        <p>無照片</p>
    @endif
    
</body>

</html>