@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a7d0/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            {{--  <a href="#" id="onlineStatus"><i class="fa fa-circle text-success"></i> Online</a>  --}}
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->

          <!-- @can('MemberMgmt')
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/MemberMgmt') }}"><i class="fa fa-user"></i> <span>會員總覽</span></a></li>
          @endcan -->


          @can('OrderMgmt')
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}"><i class="fa fa-truck"></i> <span>訂單總覽</span></a></li>
          @endcan
          
          @can('setting')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.sysSetting') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/language') }}"><i class="fa fa-flag-o"></i> <span>{{ trans('menu.langSetting') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/language/texts') }}"><i class="fa fa-language"></i> <span>{{ trans('menu.langFile') }}</span></a></li>
              <li><a href="{{ url('admin/menu-item') }}"><i class="fa fa-list"></i> <span>Menu</span></a></li>              
            </ul>
          </li>
          @endcan

          <!-- Users, Roles Permissions -->
          @can('Permissions')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.permissions') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i class="fa fa-user"></i> <span>{{ trans('menu.users') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i class="fa fa-group"></i> <span>{{ trans('menu.roles') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i class="fa fa-key"></i> <span>{{ trans('menu.permission') }}</span></a></li>              
            </ul>
          </li>
          @endcan
          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
