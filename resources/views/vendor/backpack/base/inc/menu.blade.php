<div class="navbar-custom-menu pull-left">
    <ul class="nav navbar-nav">
        <!-- =================================================== -->
        <!-- ========== Top menu items (ordered left) ========== -->
        <!-- =================================================== -->

        <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Home</span></a></li> -->

        <!-- ========== End of top menu left items ========== -->
    </ul>
</div>


<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- ========================================================= -->
      <!-- ========== Top menu right items (ordered left) ========== -->
      <!-- ========================================================= -->

      <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Home</span></a></li> -->
        @if (Auth::guest())
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/login') }}">{{ trans('backpack::base.login') }}</a></li>
            @if (config('backpack.base.registration_open'))
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/register') }}">{{ trans('backpack::base.register') }}</a></li>
            @endif
        @else
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <img src="https://placehold.it/160x160/5cb85c/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image" width="12" height="12" id="userPhoto">
                    <span class="hidden-xs">{{Auth::user()->name}} ({{Auth::user()->c_key}})</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <button class="btn btn-success btn-sm switch-online" status="1">線上</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button class="btn btn-warning btn-sm switch-online" status="2">離開</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button class="btn btn-muted btn-sm switch-online" status="0">離線</button>
                            </div>
                        </div>
                    <!-- /.row -->
                    </li>
                </ul>
            </li>
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <span class="hidden-xs">修改密碼</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="password" class="form-control" id="originalPassword" name="originalPassword" placeholder="原來密碼">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="password" class="form-control" id="userPassword" name="userPassword" placeholder="新密碼">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="password" class="form-control" id="userConfirmPassword" name="userConfirmPassword" placeholder="確認密碼">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" style="text-align: center">
                                <button class="btn btn-sm btn-primary" id="modifyPwdBtn">修改確認</button>
                            </div>
                        </div>
                        <!-- /.row -->
                    </li>
                </ul>
            </li>
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-btn fa-sign-out"></i> {{ trans('backpack::base.logout') }}</a></li>
        @endif

       <!-- ========== End of top menu right items ========== -->
    </ul>
</div>