<!-- field_type_name -->
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
  {{-- FIELD EXTRA CSS  --}}
  {{-- push things in the after_styles section --}}

      @push('crud_fields_styles')
          <!-- no styles -->
      @endpush


  {{-- FIELD EXTRA JS --}}
  {{-- push things in the after_scripts section --}}

      @push('crud_fields_scripts')
        <script>
            $(function(){
                $("#amt").prop("required", true);
                var discountMode = $("select[name='discount_mode']").val();

                if(discountMode == "B") {
                    $("select[name='prod_id[]']").parent(".form-group").show();
                    $("label[for='limit_amt']").parent(".form-group").hide();
                }
                else {
                    $("select[name='prod_id[]']").parent(".form-group").hide();
                    $("label[for='limit_amt']").parent(".form-group").show();
                }
                
                $("select[name='discount_mode']").on("change", function(){
                    var val = $(this).val();

                    if(val == "A") {
                        $("select[name='prod_id[]']").parent(".form-group").hide();
                        $("label[for='limit_amt']").parent(".form-group").show();
                        $("select[name='prod_id[]']").val("").trigger('change');
                    }
                    else {
                        $("select[name='prod_id[]']").parent(".form-group").show();
                        $("label[for='limit_amt']").parent(".form-group").hide();
                        $("input[name='limit_amt']").val("");
                    }
                });

                $("select[name='discount_type']").on("change", function(){
                    var val = $(this).val();

                    if(val == "B") {
                        $("#amt").prop("required", true);
                    }
                    else {
                        $("#amt").prop("required", false);
                    }
                });

                $("select[name='discount_way']").on("change", function(){
                    var val = $(this).val();

                    if(val == "C") {
                        $("#discount").siblings("div.input-group-addon").text("元");
                    }
                    else {
                        $("#discount").siblings("div.input-group-addon").text("%");
                    }
                });
            });
        </script>
      @endpush
@endif

{{-- Note: most of the times you'll want to use @if ($crud->checkIfFieldIsFirstOfItsType($field, $fields)) to only load CSS/JS once, even though there are multiple instances of it. --}}