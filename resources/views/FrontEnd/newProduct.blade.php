@extends('FrontEnd.newlayout',[
	"seo_title" => "商品列表",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("商品列表")),
	"seo_img" => null
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/fonts/FontAwesome/font-awesome.css')}}">
    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="{{url('assets/css/bootsnav.css?')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/newstyle.css')}}" type="text/css" media="screen">
    <link href="{{url('assets/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('assets/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('assets/css/rwd.css')}}" type="text/css" media="screen">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/am-pagination.js')}}"></script>
    @if(Session::get('message'))
    <script>
        alert("{{Session::get('message')}}");
    </script>
    @endif

    @if($errors->any())
    <script>
        var errorMsg = "{{$errors->all()[0]}}";
        alert(errorMsg);
    </script>
    @endif
@endsection

@section('header')
    @include('FrontEnd.layouts.newHeader')
@endsection

@section('content')
<div class="cont">
    <div class="pagebanner">
        <img src="{{url('assets/images/product-banner.jpg')}}">
    </div>
    <section class="page">
        <div class="container">
            <div class="product-list-header">
                <div class="breadcrumbs">
                    <a href="{{url('/')}}">ZI 你愛的生活品味 </a> &gt; 
                    @if(!empty($cateData))
                        <a href="{{url('product')}}">全部商品</a> &gt;
                        @foreach($cateData as $key => $row)
                            @if($key+1 != count($cateData))
                            <a href="{{url('product')}}?cateId={{$row->id}}">{{$row->name}} </a> &gt;
                            @else
                            <span>{{$row->name}}</span> 
                            @endif
                        @endforeach
                    @else
                    <span>全部產品</span> 
                    @endif
                </div>
                <div class="collection_filters">
                    <div class="styled-select">
                        <select id="brand">
                            <option value="all">全部品牌</option>
                            @foreach($brandData as $row)
                            <option value="{{$row->cd}}" @if($brand == $row->cd)selected="selected"@endif>{{$row->cd_descp}}</option>
                            @endforeach
                        </select>
                        <select id="sort-by">
                            <option value="created-descending" @if($sorting == 'created-descending')selected="selected"@endif>最新商品在前</option>
                            <option value="created-ascending" @if($sorting == 'created-ascending')selected="selected"@endif>最早商品在前</option>
                            <option value="price-ascending" @if($sorting == 'price-ascending')selected="selected"@endif>價格：由低至高</option>
                            <option value="price-descending" @if($sorting == 'price-descending')selected="selected"@endif>價格：由高至低</option>
                            <option value="best-selling" @if($sorting == 'best-selling')selected="selected"@endif>熱賣商品</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="product-list">
                <div class="row">
                    @foreach($prodData as $row)
                    <div class="col-md-3 col-sm-4 col-xs-6 product-item">
                        <a href="{{url('productDetail/'.$row->id)}}">
                            <div class="pro-img-box">
                                <div class="cover"></div>
                                <img src="{{(isset($row->img1)?Storage::url($row->img1):url('assets/images/no_image.png'))}}">
                            </div>
                            <h6 class="pro-list-name">{{$row->title}}</h6>
                            @if(isset($row->f_price))
                            <h6 class="pro-money"><span>TWD</span>{{number_format($row->f_price)}}</h6>
                            @else
                            <h6 class="pro-money"><span>TWD</span>{{number_format($row->t_price)}}</h6>
                            @endif
                        </a>
                    </div>
                    @endforeach
                    <div class="col-xs-12 text-center" id="pagination">
                        {{ $prodData->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.newfooter')
@endsection

@section('after_scripts')
    <script type="text/javascript" src="{{url('assets/js/prodslist.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/imagefill.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript">
        $('.responsive-slider-nav').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
        
        $('.responsive-slider-01').slick({
            dots: false,
            infinite: true,
            speed: 600,
            autoplay: true,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
        $('.pro-img-box').imagefill();
    </script>
@endsection
