@extends('FrontEnd.layout',[
	"seo_title" => "最新消息",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("最新消息")),
	"seo_img" => null
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/qa.css')}}" type="text/css" media="screen">
    
    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')
<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">最新消息</div>
		<div class="breadcrumbs">
			<a href="{{url('/')}}">美z.人生</a>  &gt; 最新消息
		</div>
	</div>
</div>
<div id="content-box">
    <div class="left-box">
        <div class="title"><i class="fas fa-square"></i>消息類別</div>
        <ul>
            @foreach($cateData as $key=>$row)
            <a href="{{url('news?cateId='.$row->id)}}">
                <li @if($key == 0) class="active" @endif>{{$row->name}}</li>
            </a>
            @endforeach
        </ul>
    </div>
    <div class="right-box">
        @if(isset($newsData))
        <h2>
        {{$newsData->title}}
        </h2>
        <p><span class="fas fa-clock"></span> 發表於 {{$newsData->created_at}}</p>
        <hr>
        <img class="img-responsive" src="{{Storage::url('public/'.$newsData->image)}}" alt="">
        <hr>
        <p>{!! $newsData->content !!}</p>
        <hr>
        @endif
    </div>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
  <script type="text/javascript" src="{{url('assets/js/qa.js')}}"></script>
@endsection

