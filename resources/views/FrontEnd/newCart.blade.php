<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-TW">
<head>
	<meta charset="UTF-8">
	<title>請下滑填寫 抽獎 / 付款 資訊</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name='description' content=""/>
	<meta name='keywords' content=""/>
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{url('assets/css/style.css?')}}">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="{{url('assets/js/goto.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	
</head>
<body>
	<header class="header-box">
		<div class="content-box inside-box clearfix">
			<a class="home arr" href="{{url('/')}}" title="回首頁"></a>
			<h3 class="title">抽獎序號 / 中獎資訊 查詢</h3>
		</div>
	</header>
	<main class="main-box">
		<form class="shoppingcar_form_box" action="{{url('pay')}}" method="post" id="payForm">
		{{ csrf_field() }}
			<section class="info-box form-box">
				<div class="shopping-cart-list">
					<h1 class="title">499元福袋送開運招財貔貅手環一組及晶華養生會館200元抵用券</h1>
					<table class="buy-list" cellpadding="0" cellspacing="0" sborder="0">
						<tr>
							<th width="50%"><p>品項</p></th>
							<th><p>數量</p></th>
							<th align="right"><p>小計</p></th>
						</tr>
						<tr>
							<td><p>{{$prod_nm}}</p></td>
							<td align="center"><p>{{$num}}</p></td>
							<td align="right"><p class="c-red">{{number_format($sumamt)}}</p></td>
						</tr>
						<tr>
							<td><p>運費 <span class="c-red">+ ${{number_format($shipfee)}}</span></p></td>
							<td colspan="2" align="right"><p>福袋總計 <span class="c-red f-size-m">{{number_format($ttlamt)}}</span></p></td>
						</tr>
					</table>
				</div>
				<label  style="color:red;font-size:12px" for="buy">凡購買兩組以上者，請擇一選擇數量，按下【前往結帳】後，裡面的備註欄註明男女款各要幾條。</label>
				<label  style="color:red;font-size:12px" for="buy">若無說明則視選擇出貨。</label>
				<div class="content-box">
					<input type="hidden" name="ttlAmt" value="{{$ttlamt}}">
					<input type="hidden" name="shipfee" value= "{{$shipfee}}" />
					<input type="hidden" name="num" value ="{{$num}}" />
					<input type="hidden" name="prod_nm" value ="{{$prod_nm}}" />
					<input type="hidden" name="agreePayRemeber" />
					
						<h3 class="from-title">付款方式</h3>
						<ul class="fillin pay">
							<li>
								<input id="pay-card" type="radio" name="payWay" value="Credit">
								<label for="pay-card">信用卡-快速付款</label>
							</li>
							<li>
								<input id="pay-atm" type="radio" name="payWay" value="WEBATM" checked>
								<label for="pay-atm">ATM付款</label>
							</li>
							<li>
								<input id="pay-store" type="radio" name="payWay" value="CVS">
								<label for="pay-store">超商條碼付款</label>
							</li>
						</ul>
						<br>
						<h3 class="from-title">抽獎資訊</h3>
						<ul class="fillin">
							<li>
								<label for="name">姓名</label>
								<input required id="name" type="text" name="dlv_nm" value="" placeholder="請輸入姓名">
							</li>
							<li>
								<label for="phone">手機號碼</label>
								<input required id="phone" type="text" name="dlv_phone" value="" placeholder="請輸入購買人手機號碼 例: 0912345678">
							</li>
							<li>
								<label for="phone">電子信箱</label>
								<input required id="phone" type="text" name="email" value="" placeholder="請輸入購買人電子信箱">
							</li>
							<li>
								<label for="code">福袋收件地址</label>
								<input required id="code" type="text" name="dlv_addr" value="" placeholder="請輸入福袋完整收件地址(含縣市)">
							</li>
							<li>
								<label for="cust_remark">備註</label>
								<input type="text" name="cust_remark" value="" placeholder="請輸入備註">
								<p style="color:red;font-size: 12px;">凡購買兩組以上者，請擇一選擇數量，按下【前往結帳】後，裡面的備註欄註明男女款各要幾條，若無說明則視選擇出貨。</p>
							</li>
							<li class="the_terms">
								<input required id="terms" type="radio" name="field" value="option">
								<label for="terms">同意隱私權條款</label>
								<a href="#" onclick="showrule()" class="link">隱私權條款</a>
							</li>
						</ul>
				</div>
			</section>
			<footer class="footer_box">
				<ul class="link clearfix">
					<li><a href="{{url('search')}}"><p class="icon_01">抽獎序號/<br>中獎查詢</p></a></li>
					<li><a href="https://www.facebook.com/%E6%99%B6%E8%8F%AF%E9%A4%8A%E7%94%9F%E6%9C%83%E9%A4%A8-348842531964050/?__tn__=kC-R&eid=ARCLVy2JVRLEqld-VCgaq2BKWer4N5cnKHJ2fdVsIvTPUN3Qy14R0EtWIz-aD5eTs7NN4OaNTPIDusUg&hc_ref=ARQGaf0hVNpKUbAs6VfmEtKQ-S6PVdP11d7GXt1dPB7L5NaehZ0QN87d1QnRL9WOiGc&fref=tag&__xts__%5B0%5D=68.ARBZlFscLop11qD0bjFn8_1RmiCfMUrS7C3-HmxP0gm4VsC8S-cze9gDYtMgQkxxjDyrw8z38fs_BRLJHyqQ338eeMsDU_iMOCw-xxVcFOC_WOxtKl7KiNAhCCRkr_ttNpt_NkicxP4C6HIoHcEm4RyYpuKNFt1TSErdtDoPEhd6MKvURp8mFn3cT15pzYbzjr8X2DX4y2P2NBYBGivpY11q7LT0407vihRavzb_Qmh5P7o5KO11l_xyulK1_WE9S1mmR3Cq04UWs1rO8xeIMocbVNbdhriFNLyW" target="_blank"><p class="icon_02">facebook<br>粉絲團</p></a></li>
					<li><a href="{{url('/')}}#ruleinfo"><p class="icon_03">隱私權條款</p></a></li>
				</ul>
				<p class="Copyright">本活動由林懿君 律師 為第三方公證人<br>
					© 2019 Copyright      客服電話 03-357-9199#510
				</p>
				<div class="btn_buy"><p><a onclick="document.getElementById('send').click();"><span>付款</span></a></p></div>
				<button id ="send" style="display:none" type="submit"></button>
			</footer>
		</form>
	</main>
</body>
<script>
	  function showrule() {

		Swal.fire({
		title: "隱私權條款",
		width: '1000px',
		html: "<ul style ='text-align:left;'><li><p><b>蒐集之⽬的：晶華瘦身美妍社(下稱本公司)，於舉辦【499福袋】福袋販及抽獎活動(下稱本活動)的特定⽬的範圍內將蒐集您個人資料。</b></p></li>\
			<li><p><b>1.蒐集個人之資料之類別：包括您的姓名、⾝分證字號、出生年月日、電話、地址、電子郵件等個人資料及您的信⽤卡或金融機構帳戶資訊等個人資料。</b></p></li>\
			<li><p><b>2.個⼈資料利用之期間、地區、對象及⽅式：</b><br>\
				期間：本公司依業務需求及依法令規定應為保存之期間或經您提出書⾯要求本公司刪除⽌。<br>\
				地區：臺灣。<br>\
				對象：本公司及本公司合作之公司。<br>\
				⽅式：您的個⼈資料將用於本活動商品或獎品之 通知及寄送或其他本活動目的範圍內之利利⽤⽅式。\
				</p>\
			</li>\
			<li><p><b>3.針對您的個⼈資料，您可依法向本公司行使下述之權利：</b><br>\
				查詢或請求閱覽您的個⼈資料。<br>\
				請求製給複製本。<br>\
				請求補充或更正。<br>\
				請求停⽌蒐集、處理或利用。<br>\
				請求刪除。<br>\
				如您欲行使上述權利時，您可撥打本公司之消費者服務電話或以電⼦郵件⽅式聯聯絡本公司。\
				</p>\
			</li>\
			<li><p><b>4.特別告知，您得⾃由選擇是否提供您的個⼈資料，惟若您拒絕提供或提供不完全時，將影響本活動商品、獎品之通知、寄送或您可能無法參加本活動。</b></p></li>\
		</ul>"
		});
	}
</script>
</html>



