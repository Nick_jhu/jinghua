<link href="{{url('assets/css/slide-menu.css')}}" rel="stylesheet">
<!-- Start Navigation -->
<nav class="navbar navbar-default navbar-mobile  bootsnav">
    <div class="nav-header-fixed text-center responsive-slider-01">
        @foreach($marqueeData as $row)
        <div class="col-xs-12"><a href="{{$row->link}}" style="color:#868d96">{{$row->descp}}</a></div>
        @endforeach
    </div>
    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <form action="{{url('/product')}}" method="GET">
            <div class="input-group">
                
                    <input type="text" class="form-control" placeholder="輸入關鍵字" name="search">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                
            </div>
            </form>
        </div>
    </div>
    <!-- End Top Search -->

    <div class="navcontainer">            
        <!-- Start Atribute Navigation -->
        <div class="attr-nav">
            <ul>
                <li class="search"><a href="#"><i class="fa fa-search" style="font-size: 18px;" title="搜尋"></i></a></li>
                <li class="member-icon @if(Auth::guard('member')->check()) is-active @endif"> <a href="{{url('center')}}"><i class="fa fa-user" style="font-size: 18px;" @if(Auth::guard('member')->check()) title="會員專區" @endif  @if(!Auth::guard('member')->check()) title="登入" @endif"></i></a></li>

                <li>
                    <a href="{{url('cart')}}">
                        <i class="fa fa-shopping-bag" style="font-size: 18px;" title="購物車"></i>
                        <span class="badge header-num-box">0</span>
                    </a>
                </li>
                @if(Auth::guard('member')->check())
                <li><a href="{{url('member/logout')}}"><i class="fa fa-sign-out" style="font-size: 18px;" title="登出"></i></a></li>
                @endif
            </ul>
        </div>
        <!-- End Atribute Navigation -->

        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle slide-menu-control" data-action="toggle" data-target="navbar-menu-mobile">
                <i class="fa fa-bars" style="font-size: 30px;"></i>
            </button>
            <a class="navbar-brand"  href="{{url('/')}}"><img src="{{url('assets/images/zizi_logo.png')}}" class="logo" alt=""></a>
        </div>
        <!-- End Header Navigation -->

        <nav class="slide-menu" id="navbar-menu-mobile">
            <div class="controls">
                <button type="button" class="btn slide-menu-control" data-action="close"><i class="fa fa-times"></i></button>
            </div>
            <div class="container text-center">
                <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
                    @foreach($menuData as $key=>$row)
                    <li class="dropdown megamenu-fw mobile-menu" cate-id="{{$row->id}}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$row->name}}</a>
                        <ul class="nav navbar-nav navbar-left">
                            <li class="dropdown megamenu-fw"><a href="{{url('product')}}?cateId={{$row->id}}">所有{{$row->name}}</a></li>
                            @foreach($mobileMenu[$row->id] as $row1) 
                                <li class="dropdown megamenu-fw">
                                    <a href="@if(empty($row1->children)) {{url('product')}}?cateId={{$row1->id}} @endif">{{$row1->name}}</a>
                                    @if(!empty($row1->children))
                                    <ul class="nav navbar-nav navbar-left">
                                        <li class="dropdown megamenu-fw"><a href="{{url('product')}}?cateId={{$row1->id}}">所有{{$row1->name}}</a></li>
                                        @foreach($row1->children as $row2) 
                                        <li class="dropdown megamenu-fw">
                                            <a href="@if(empty($row2->children)) {{url('product')}}?cateId={{$row2->id}} @endif">{{$row2->name}}</a>
                                            @if(!empty($row2->children))
                                            <ul class="nav navbar-nav navbar-left">
                                                <li class="dropdown megamenu-fw"><a href="{{url('product')}}?cateId={{$row2->id}}">所有{{$row2->name}}</a></li>
                                                @foreach($row2->children as $row3) 
                                                <li class="dropdown megamenu-fw">
                                                    <a href="@if(empty($row3->children)) {{url('product')}}?cateId={{$row3->id}} @endif">{{$row3->name}}</a>
                                                    @if(!empty($row3->children))
                                                    <ul class="nav navbar-nav navbar-left">
                                                        <li class="dropdown megamenu-fw"><a href="{{url('product')}}?cateId={{$row3->id}}">所有{{$row3->name}}</a></li>
                                                        @foreach($row3->children as $row4) 
                                                        <li class="dropdown megamenu-fw">
                                                            <a href="@if(empty($row4->children)) {{url('product')}}?cateId={{$row4->id}} @endif">{{$row4->name}}</a>
                                                            @if(!empty($row4->children))
                                                            <ul class="nav navbar-nav navbar-left">
                                                                <li class="dropdown megamenu-fw"><a href="{{url('product')}}?cateId={{$row4->id}}">所有{{$row4->name}}</a></li>
                                                                @foreach($row4->children as $row5) 
                                                                <li class="dropdown megamenu-fw">
                                                                    <a href="@if(empty($row5->children)) {{url('product')}}?cateId={{$row5->id}} @endif">{{$row5->name}}</a>
                                                                    @if(!empty($row5->children))
                                                                    <ul class="nav navbar-nav navbar-left">
                                                                        <li class="dropdown megamenu-fw"><a href="{{url('product')}}?cateId={{$row5->id}}">所有{{$row5->name}}</a></li>
                                                                        @foreach($row5->children as $row6) 
                                                                        <li class="dropdown megamenu-fw">
                                                                            <a href="@if(empty($row6->children)) {{url('product')}}?cateId={{$row6->id}} @endif">{{$row6->name}}</a>
                                                                            @if(!empty($row6->children))
                                                                            <ul class="nav navbar-nav navbar-left">
                                                                                <li class="dropdown megamenu-fw"><a href="{{url('product')}}?cateId={{$row6->id}}">所有{{$row6->name}}</a></li>
                                                                                @foreach($row6->children as $row7) 
                                                                                <li class="dropdown megamenu-fw">
                                                                                    <a href="@if(empty($row7->children)) {{url('product')}}?cateId={{$row7->id}} @endif">{{$row7->name}}</a>
                                                                                </li>
                                                                                @endforeach
                                                                            </ul>
                                                                            @endif
                                                                        </li>
                                                                        @endforeach
                                                                    </ul>
                                                                    @endif
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                
                            @endforeach
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </div>
        </nav>

            <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <div class="container text-center">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    @foreach($menuData as $key=>$row)
                    <li class="dropdown megamenu-fw" cate-id="{{$row->id}}" level="first">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$row->name}}</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li class="next-menu-{{$row->id}}">
                                <div class="row menubox">
                                    <div class="col-menu col-md-9 menurec">
                                        <h6 class="title">熱門推薦</h6>
                                        <div class="content">
                                            <ul class="menu-col ">
                                                <li class="nav-pro hot-prod">
                                                    
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end col-3 -->
                                    <div class="col-menu col-md-3 show" id="menu-{{$row->id}}">
                                        <div class="content">
                                            <ul class="menu-col next-menu">
                                                <li class=""><a href="{{url('product')}}?cateId={{$row->id}}">{{$row->name}}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div> <!-- /.navbar-collapse -->
    </div>   
</nav>
<!-- End Navigation -->
<div class="clearfix"></div>
<script src="{{url('assets/js/slide-menu.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/menu.js')}}?v={{Config::get('app.version')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    $(function(){
        var cartNum = $.cookie('cartNum');
        var cartId  = $.cookie('cartId');
        var isLogin = "{{Auth::guard('member')->check()}}";
        if((typeof cartNum == "undefined" || cartNum == "null" || cartId == 0 || typeof cartId == "undefined") && isLogin != "1") {
            cartNum = 0;
        }

        $(".header-num-box").text(cartNum);

        $('.responsive-slider-01').slick({
            dots: false,
            infinite: true,
            speed: 600,
            autoplay: true,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
    });
</script>