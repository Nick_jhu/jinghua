<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-TW">
<head>
	<meta charset="UTF-8">
	<title>晶華養生會館</title>
	<meta name='description' content="晶華養生會館25週年生日快樂活動網站"/>
	<meta name='keywords' content=""/>
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- You can use open graph tags to customize link previews.
    Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
    <meta property="og:url"           content="https://www.your-domain.com/your-page.html" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Your Website Title" />
    <meta property="og:description"   content="Your description" />
    <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />

	<link rel="stylesheet" href="{{url('assets/css/style.css?')}}">
    <script type="text/javascript" src="{{url('assets/js/jquery-1.11.1.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/js/goto.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/js/jquery-3.3.1.min.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/jquery.flipcountdown.css?')}}" />
	<script type="text/javascript" src="{{url('assets/js/jquery.flipcountdown.js')}}"></script>
	<script>
		$(function(){
		
			var NY = Math.round((new Date('7/20/2019 19:00:00')).getTime()/1000);
			$('#retroclockbox1').flipcountdown({
				tick:function(){
					var nol = function(h){
						return h>9?h:'0'+h;
					}
					var	range  	= NY-Math.round((new Date()).getTime()/1000),
						secday = 86400, sechour = 3600,
						days 	= parseInt(range/secday),
						hours	= parseInt((range%secday)/sechour),
						min		= parseInt(((range%secday)%sechour)/60),
						sec		= ((range%secday)%sechour)%60;
					return nol(days)+' '+nol(hours)+' '+nol(min)+' '+nol(sec);
				}
			});
		});
	</script>

@section('after_style')
    @if($errors->any())
    <script>
        var errorMsg = "{{$errors->all()[0]}}";
        alert(errorMsg);
    </script>
    @endif
@endsection

</head>
<body>

	<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) return;
	    js = d.createElement(s); js.id = id;
	    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
	    fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));
	  function add_a() {
		  var num  = parseInt($("#num_a").val());
		  num+=1;
		  $("#num_a").val(num);
		}
		function add_b() {
			var num  = parseInt($("#num_b").val());
		  num+=1;
		  $("#num_b").val(num);
		}
		function remove_a() {
			var num  = parseInt($("#num_a").val());
			if(num==1){
				return;
			}else{
			num-=1;
			$("#num_a").val(num);
			}
		}
		function remove_b() {
			var num  = parseInt($("#num_b").val());
			if(num==1){
				return;
			}else{
			num-=1;
			$("#num_b").val(num);
			}
		}
		function addcart() {
			var num = 0;
			var type = $("input[name='pay-list']:checked").val();
			if(type=="a"){
				num  = parseInt($("#num_a").val());
			}
			else{
				num  = parseInt($("#num_b").val());
			}
			$.post(BASE_URL+'/cart', {"num": num,"prodId": type
			}, function(data){
                    if(data.status == "success") {
                        $(".header-num-box").text(cartNum);
                        Swal.fire({
                            type: 'success',
                            text: '商品已加入購物車',
                        })
                        //alert('商品已加入購物車');
                    }
                    else {
                        alert(data.message);
                    }
                    
                }, 'JSON');
		}
	</script>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.2&appId=227613341408803&autoLogAppEvents=1"></script>

	<header class="header-box" id="header-box">
		<div class="content-box clearfix">
			<h3 class="title">抽獎序號/中獎查詢</h3>
			<p class="btn"><a href="{{url('search')}}">查詢</a></p>
		</div>
	</header>
	<main class="main-box">
		<section class="info-box" id="info-box">
			<img src="{{url('assets/images/img_800x1000.jpg')}}"  alt="" width="100%">
			<div class="content-box">
				<h1 class="manin-title">499元晶華養生會館福袋，內附開運招財貔貅</h1>
				<h1 class="manin-title">⼿環(男款女款擇一)及晶華養生會館200元抵⽤券</h1>
				<h2 class="sub-title c-org">再抽<br>勞⼒⼠50週年紅字紀念款鬼王<br>LV LOCKY BB經典包</h2>
				<h3 class="limited c-blue">每袋只要499，限量販售中</h3>
				<div class="freight-box">
					<p>⼀袋運費80元！ <br>
						兩袋以上免運費！<br>
						<span>{{($snData)}} 已出售</span>
					</p>
				</div>
				<div class="share-box clearfix">
					<div class="fb-likebox">
						<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="" data-layout="box_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
					</div>
					<ul class="social_media">
						<li><a href="https://www.facebook.com/%E6%99%B6%E8%8F%AF%E9%A4%8A%E7%94%9F%E6%9C%83%E9%A4%A8-348842531964050/?__tn__=kC-R&eid=ARCLVy2JVRLEqld-VCgaq2BKWer4N5cnKHJ2fdVsIvTPUN3Qy14R0EtWIz-aD5eTs7NN4OaNTPIDusUg&hc_ref=ARQGaf0hVNpKUbAs6VfmEtKQ-S6PVdP11d7GXt1dPB7L5NaehZ0QN87d1QnRL9WOiGc&fref=tag&__xts__%5B0%5D=68.ARBZlFscLop11qD0bjFn8_1RmiCfMUrS7C3-HmxP0gm4VsC8S-cze9gDYtMgQkxxjDyrw8z38fs_BRLJHyqQ338eeMsDU_iMOCw-xxVcFOC_WOxtKl7KiNAhCCRkr_ttNpt_NkicxP4C6HIoHcEm4RyYpuKNFt1TSErdtDoPEhd6MKvURp8mFn3cT15pzYbzjr8X2DX4y2P2NBYBGivpY11q7LT0407vihRavzb_Qmh5P7o5KO11l_xyulK1_WE9S1mmR3Cq04UWs1rO8xeIMocbVNbdhriFNLyW" target="_blank" class="fb"></a></li>
						<li><a href="https://www.facebook.com/messages/t/348842531964050" class="messenger"></a></li>
					</ul>
				</div>
				<ul class="gift-list">
					<li><p class="c-org">(壹獎) 限定款ROLEX目前抽獎率1/{{($snData)}}</p></li>
					<li><p>(貳獎) LV包目前抽獎率1/{{($snData)}} </p></li>
					<li><p>(參獎) PHILIPS 50吋電視目前抽獎率1/{{($snData)}}</p></li>
					<li><p>(肆獎) IPAD⽬前抽獎率1/{{($snData)}}</p></li>
					<li><p>(伍獎) SWITCH⽬前抽獎率1/{{($snData)}}</p></li>
					<li><p>(伍獎) PS4目前抽獎率1/{{($snData)}}</p></li>
					<li><p>(陸獎) WINIX空氣清淨機目前抽獎率1/{{($snData)}}</p></li>
					<li><p>(柒獎) GHD吹風機⽬前抽獎率1/{{($snData)}}</p></li>
					<li><p>(捌獎) 雀巢咖啡機目前抽獎率3/{{($snData)}}</p></li>
				</ul>
				<p class="p-info">超高中獎率！<br>錯過不再！名錶、名包屬於你！</p>
				<div class="time-box"><!-- 時間倒數欄位 -->
					<table cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto;">
						<tr>
							<td colspan="4" align="center">
								<div id="retroclockbox1" style="display: inline-block;"></div>
							</td>
						</tr>
						<tr>
							<td style="width:80px;text-align:center;">天</td>
							<td style="width:80px;text-align:center;">時</td>
							<td style="width:80px;text-align:center;">分</td>
							<td style="width:80px;text-align:center;">秒</td>
						</tr>
					</table>
					
				</div>
				<p class="p-info">
					<span class="c-org f-size-l">晶華養生會館</span><br>
					<span class="c-org f-size-l">25週年⽣⽇快樂！</span><br>
					<span class="c-blue">好康好禮大獎送不完！</span>
				</p>
				<p class="p-info f-size-s c-gray">委託林懿君 律師 為第三方公證人<br>
					全程直播<br>
					公開、公正、公平
				</p>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x458.jpg')}}"  alt="" width="100%">
				</div>
				<h3 class="product-name">福袋商品</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_01.jpg')}}"  alt="" width="100%">
				</div>
				<ul class="product-info">
					<li><p class="left-p">商品名稱：</p><p>開運招財貔貅手環</p></li>
					<li><p class="left-p">商品顏色：</p><p>男款</li>
					<li><p class="left-p">商品市值：</p><p>約499元/個</p></li>
					<li><p class="left-p">使用⽅法：</p><p>男生建議配戴於左手、女⽣配戴於右⼿。</p></li>
				</ul>
				<h3 class="product-name">福袋商品</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_02.jpg')}}"  alt="" width="100%">
				</div>
				<ul class="product-info">
					<li><p class="left-p">商品名稱：</p><p>開運招財貔貅手環</p></li>
					<li><p class="left-p">商品顏色：</p><p>女款</li>
					<li><p class="left-p">商品市值：</p><p>約499元/個</p></li>
					<li><p class="left-p">使用⽅法：</p><p>男生建議配戴於左手、女⽣配戴於右⼿。</p></li>
				</ul>
				<h3 class="product-name">福袋商品</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_03.jpg')}}"  alt="" width="100%">
				</div>
				<ul class="product-info">
					<li><p class="left-p" width="100%">貔貅是風水上的三大聖獸之一，貔貅的發音為「⽪休」，是一種頭帶⾓、嘴巴⼤大、渾身及尾巴有卷毛，模樣有點像「四不像」的神獸，北⽅⼈叫它做「辟邪」，南⽅人則稱之為「貔貅」，它除了可鎮宅避邪，更能招財旺財，尤其對招偏財更更具奇效，是風水文物中催財化煞的至寶。</li>
				</ul>
				<h3 class="product-name">福袋商品</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_13.jpg')}}"  alt="" width="100%">
				</div>
				<ul class="product-info">
					<li><p class="left-p" width="100%">貔貅的⼀些常識，比如公獸有獨角、頭朝左、左腳在前、有翅膀和鬃⽑，⽽⺟獸正好相反，但其實單隻貔貅是不分雌雄。依照民間的說法是，公獸代表財運，⽽母獸代表財庫，一次收藏一對代表有財有庫，才能真正招財進寶，而配戴在手腕上最好⼀手戴一隻，以免兩獸打架。</li>
				</ul>
				<!-- <div class="youtube-box">
					<iframe src="https://www.youtube.com/embed/iBncuiDvPG4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div> -->
				<h3 class="product-name">福袋商品</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_14.jpg')}}"  alt="" width="100%">
				</div>
				<ul class="product-info">
					<li><p class="left-p">商品名稱：</p><p>晶華養生會館200元抵⽤券</p></li>
					<li><p class="left-p">商品顏色：</p><p>無</li>
					<li><p class="left-p">商品市值：</p><p>200元/張</p></li>
					<li><p class="left-p">使用⽅法：</p><p>使用期限內於店家消費使用之。</p></li>
				</ul>
				<h3 class="product-name">福袋獎項</h3>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_04.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_05.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_06.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_07.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_08.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_09.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_10.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_11.jpg')}}"  alt="" width="100%">
				</div>
				<div class="bn-box">
					<img src="{{url('assets/images/img_800x800_12.jpg')}}"  alt="" width="100%">
				</div>
				<div class="rule-info">
					<h5>獎  項</h5>
					<ul>
						<li><p>壹獎⼀名：限定款50週年ROLEX紅字⿁王(機率最低1/8000)</p></li>
						<li><p>貳獎⼀名：限定款LV LOCKY BB經典包(機率最低1/8000)</p></li>
						<li><p>參獎一名：PHILIPS 50吋電視(機率最低1/8000)</p></li>
						<li><p>肆獎⼀名：APPLE IPAD(機率最低1/8000)</p></li>
						<li><p>伍獎⼀名：任天堂SWITCH(機率最低1/8000)</p></li>
						<li><p>伍獎⼀名：SONY PS4(機率最低1/8000)</p></li>
						<li><p>陸獎⼀名：WINIX空氣清淨機(機率最低1/8000) </p></li>
						<li><p>柒獎⼀名：GHD吹風機(機率最低1/8000)</p></li>
						<li><p>捌獎三名：雀巢咖啡機(機率最低3/8000)</p></li>
					</ul>
				</div>
				<div class="rule-info">
					<h5>活動規則</h5>
					<ul>
						<li><p>活動期間：2019/05/20~07/20，晶華週年慶之【499元福袋】單⼀售價：新台幣499元整。</p></li>
						<li><p>【499元福袋】限量銷售八千份，售完為⽌。</p></li>
						<li><p>【499元福袋】分別有兩款，內容說明如下。<br>
						   	一.499元福袋送開運招財貔貅手環一組(男款)及晶華養生會館200元抵用券。<br>
						    二.499元福袋送開運招財貔貅手環一組(女款)及晶華養生會館200元抵用券。</p>
						</li>
						<li><p>【活動期間於本活動網站完成【499元福袋】線上購買，即可獲得⼀組序號(網站上查詢)，並獲得【網站上壹～捌獎】抽獎資格，獎項請見以下獎項說明。 </p></li>
						<li><p>所有抽獎資格為獨立擁有，不限個人名額，故同一消費者，若完成購買複數【499福袋】，即代表獲得複數個抽獎資格，更多數量則以此類推。 </p></li>
						<li><p>所有獎項不限銷售數量，皆會抽出。</p></li>
						<li><p>本活動所有獎項為限量抽獎之獎項，數量與詳細內容請⾒獎項說明。 </p></li>
						<li><p>特別提醒：所有商品，恕不退貨，若商品有瑕疵，請以網站上各種聯絡⽅式聯繫客服換貨。</p></li>
						<li><p>至桃園市桃園區經國路515號晶華養⽣會館自行領取。</p></li>
						<li><p>中獎者若未滿20歲，須獲得法定代理人同意及代為領取。依中華⺠國所得法規定，舉凡中獎⾦額或獎項年度累計價值超過1,001元（含）以上，將列列入本年度之個人 綜合所得稅申報。另中獎獎品價值超過新台幣20,000元（含）以上者，應⾃行負擔機會中獎所得稅，依法須預先扣繳10%稅金（稅額以獎品市價計算），請⾃行報稅及繳稅，未能如期依法繳納應繳稅額者，視同⾃願放棄中獎權利。 </p></li>
						<li><p>如遇不可抗力之因素，主辦單位保留隨時修改活動辦法及更換等值獎品之權利。</p></li>
						<li><p>本活動獎項寄送僅限台灣本島。</p></li>
						<li><p>若因提供資料填寫不完整、不正確，或其他非不可抗拒因素，導致主辦單位無法取得聯繫者，則視同放棄兌換資格，且不另⾏通知。</p></li>
						<li><p>獎項或獎品⼀旦寄出後，若有遺失、遭冒領或偷竊等喪失佔有之情形，主辦單位恕不負責補發。</p></li>
						<li><p>若中獎者不符合、不同意或違反本活動規定者，主辦單位保有取消中獎資格及參與本活動的權利，並對任何破壞本活動⾏為者保留法律追訴權。</p></li>
						<li><p>獎項內容及服務品質應參照⽣產商或服務商提供之標準，如因使⽤該獎品服務產生任何爭議，由該獎項或服務之提供廠商依法承擔責任。 </p></li>
					</ul>
				</div>
				<div class="rule-info">
					<h5>補充說明</h5>
						<ul>
							<li><p>【壹獎】至【捌獎】中獎人資格，限定合法擁有中華民國身分證或中華民國護照之民眾，故購買【499元晶華養生會館福袋】消費者，請確認合法擁有中華民國身分證或中華民國護照，若無則視同放棄後續領獎資格。</p></li>
							<li><p>本活動網站銷售期間至2019/07/20晚上23:59:59，抽獎將於2019/07/28上午12:00或8千份【499元晶華養生會館福袋】完售7天後於暫定晶華養生會館舉行，並同步進行網路直播。</p></li>
							<li><p>所有完成【499元晶華養生會館福袋】線上購買流程之消費者，主辦單位將進行個人資料之蒐集及利用，供；後續各項獎項寄送時之使用，詳見隱私權條款。</p></li>
							<li><p>【壹獎】至【捌獎】若獎項有超過新台幣兩萬元整，該中獎人須於中獎後依照中華民國稅法繳納10%所得稅金，此部分由中獎人自行承擔。 </p></li>
							<li><p>本活動主辦單位：晶華瘦身美妍社，負責人:張金來，消費者服務MAIL：jinhuaasdfghjkl@gmail.com，消費者服務專線:03-357-9199#510，消費者服務地址:桃園市桃園區經國路515號</p></li>
							<li><p>收到本活動福袋後若有瑕疵，請勿拆封，並請撥打消費者服務電話辦理退貨或換貨事宜。</p></li>
						</ul>
				</div>
				<a name = "ruleinfo"></a>
				<div class="rule-info">
					<h5 >隱私權政策</h5>
					<ul>
						<li><p><b>蒐集之⽬的：晶華瘦身美妍社(下稱本公司)，於舉辦【499福袋】福袋販售及抽獎活動(下稱本活動)的特定⽬的範圍內將蒐集您個人資料。</b></p></li>
						<li><p><b>蒐集個人之資料之類別：包括您的姓名、⾝分證字號、出生年月日、電話、地址、電子郵件等個人資料及您的信⽤卡或金融機構帳戶資訊等個人資料。</b></p></li>
						<li><p><b>個⼈資料利用之期間、地區、對象及⽅式：</b><br>
							期間：本公司依業務需求及依法令規定應為保存之期間或經您提出書⾯要求本公司刪除⽌。<br>
							地區：臺灣。<br>
							對象：本公司及本公司合作之公司。<br>
							⽅式：您的個⼈資料將用於本活動商品或獎品之 通知及寄送或其他本活動目的範圍內之利⽤⽅式。
							</p>
						</li>
						<li><p><b>針對您的個⼈資料，您可依法向本公司行使下述之權利：</b><br>
							查詢或請求閱覽您的個⼈資料。<br>
							請求製給複製本。<br>
							請求補充或更正。<br>
							請求停⽌蒐集、處理或利用。<br>
							請求刪除。<br>
							如您欲行使上述權利時，您可撥打本公司之消費者服務電話或以電⼦郵件⽅式聯絡本公司。
							</p>
						</li>
						<li><p><b>特別告知，您得⾃由選擇是否提供您的個⼈資料，惟若您拒絕提供或提供不完全時，將影響本活動商品、獎品之通知、寄送或您可能無法參加本活動。</b></p></li>
					</ul>
				</div>
				<div class="fb-box">
					<div class="fb-page" data-href="https://www.facebook.com/%E6%99%B6%E8%8F%AF%E9%A4%8A%E7%94%9F%E6%9C%83%E9%A4%A8-348842531964050/?__tn__=kC-R&amp;eid=ARCLVy2JVRLEqld-VCgaq2BKWer4N5cnKHJ2fdVsIvTPUN3Qy14R0EtWIz-aD5eTs7NN4OaNTPIDusUg&amp;hc_ref=ARQGaf0hVNpKUbAs6VfmEtKQ-S6PVdP11d7GXt1dPB7L5NaehZ0QN87d1QnRL9WOiGc&amp;fref=tag&amp;__xts__%5B0%5D=68.ARBZlFscLop11qD0bjFn8_1RmiCfMUrS7C3-HmxP0gm4VsC8S-cze9gDYtMgQkxxjDyrw8z38fs_BRLJHyqQ338eeMsDU_iMOCw-xxVcFOC_WOxtKl7KiNAhCCRkr_ttNpt_NkicxP4C6HIoHcEm4RyYpuKNFt1TSErdtDoPEhd6MKvURp8mFn3cT15pzYbzjr8X2DX4y2P2NBYBGivpY11q7LT0407vihRavzb_Qmh5P7o5KO11l_xyulK1_WE9S1mmR3Cq04UWs1rO8xeIMocbVNbdhriFNLyW" data-tabs="timeline" data-width="100%" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/%E6%99%B6%E8%8F%AF%E9%A4%8A%E7%94%9F%E6%9C%83%E9%A4%A8-348842531964050/?__tn__=kC-R&amp;eid=ARCLVy2JVRLEqld-VCgaq2BKWer4N5cnKHJ2fdVsIvTPUN3Qy14R0EtWIz-aD5eTs7NN4OaNTPIDusUg&amp;hc_ref=ARQGaf0hVNpKUbAs6VfmEtKQ-S6PVdP11d7GXt1dPB7L5NaehZ0QN87d1QnRL9WOiGc&amp;fref=tag&amp;__xts__%5B0%5D=68.ARBZlFscLop11qD0bjFn8_1RmiCfMUrS7C3-HmxP0gm4VsC8S-cze9gDYtMgQkxxjDyrw8z38fs_BRLJHyqQ338eeMsDU_iMOCw-xxVcFOC_WOxtKl7KiNAhCCRkr_ttNpt_NkicxP4C6HIoHcEm4RyYpuKNFt1TSErdtDoPEhd6MKvURp8mFn3cT15pzYbzjr8X2DX4y2P2NBYBGivpY11q7LT0407vihRavzb_Qmh5P7o5KO11l_xyulK1_WE9S1mmR3Cq04UWs1rO8xeIMocbVNbdhriFNLyW" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/%E6%99%B6%E8%8F%AF%E9%A4%8A%E7%94%9F%E6%9C%83%E9%A4%A8-348842531964050/?__tn__=kC-R&amp;eid=ARCLVy2JVRLEqld-VCgaq2BKWer4N5cnKHJ2fdVsIvTPUN3Qy14R0EtWIz-aD5eTs7NN4OaNTPIDusUg&amp;hc_ref=ARQGaf0hVNpKUbAs6VfmEtKQ-S6PVdP11d7GXt1dPB7L5NaehZ0QN87d1QnRL9WOiGc&amp;fref=tag&amp;__xts__%5B0%5D=68.ARBZlFscLop11qD0bjFn8_1RmiCfMUrS7C3-HmxP0gm4VsC8S-cze9gDYtMgQkxxjDyrw8z38fs_BRLJHyqQ338eeMsDU_iMOCw-xxVcFOC_WOxtKl7KiNAhCCRkr_ttNpt_NkicxP4C6HIoHcEm4RyYpuKNFt1TSErdtDoPEhd6MKvURp8mFn3cT15pzYbzjr8X2DX4y2P2NBYBGivpY11q7LT0407vihRavzb_Qmh5P7o5KO11l_xyulK1_WE9S1mmR3Cq04UWs1rO8xeIMocbVNbdhriFNLyW">晶華養生會館</a></blockquote></div>
				</div>
			</div>
		</section>
		<footer class="footer_box">
			<ul class="link clearfix">
				<li><a href="{{url('search')}}"><p class="icon_01">抽獎序號/<br>中獎查詢</p></a></li>
				<li><a href="https://www.facebook.com/%E6%99%B6%E8%8F%AF%E9%A4%8A%E7%94%9F%E6%9C%83%E9%A4%A8-348842531964050/?__tn__=kC-R&eid=ARCLVy2JVRLEqld-VCgaq2BKWer4N5cnKHJ2fdVsIvTPUN3Qy14R0EtWIz-aD5eTs7NN4OaNTPIDusUg&hc_ref=ARQGaf0hVNpKUbAs6VfmEtKQ-S6PVdP11d7GXt1dPB7L5NaehZ0QN87d1QnRL9WOiGc&fref=tag&__xts__%5B0%5D=68.ARBZlFscLop11qD0bjFn8_1RmiCfMUrS7C3-HmxP0gm4VsC8S-cze9gDYtMgQkxxjDyrw8z38fs_BRLJHyqQ338eeMsDU_iMOCw-xxVcFOC_WOxtKl7KiNAhCCRkr_ttNpt_NkicxP4C6HIoHcEm4RyYpuKNFt1TSErdtDoPEhd6MKvURp8mFn3cT15pzYbzjr8X2DX4y2P2NBYBGivpY11q7LT0407vihRavzb_Qmh5P7o5KO11l_xyulK1_WE9S1mmR3Cq04UWs1rO8xeIMocbVNbdhriFNLyW" target="_blank"><p class="icon_02">facebook<br>粉絲團</p></a></li>
				<li><a href="#ruleinfo"><p class="icon_03">隱私權條款</p></a></li>
			</ul>
			<p class="Copyright">本活動由林懿君 律師 為第三方公證人<br>
				© 2019 Copyright      客服電話 03-357-9199#510
			</p>
			<a class="go_top"></a>
			<div class="btn_buy" id="show_order"><p><a><span class="icon_cart">我要購買499福袋</span></a></p></div>
			<div class="btn_buy go_order" id="go_order">
			<form class="form" role="form" id ="myForm" method="post" action="{{ url('cartsub') }}">
			{{ csrf_field() }}
				<div class="fix">
					<div class="buy_list">
						<a class="btn_close"></a>
						<label  style="color:red;font-size:12px" for="buy">備註：凡購買兩組以上者，請擇一選擇數量</label>
						<label  style="color:red;font-size:12px" for="buy">按下【前往結帳】後，裡面的備註欄註明男女款各要幾條，若無說明則視選擇出貨。</label>
						<ul>
							<li class="clearfix">
								<div class="title">
								<input id="buy-a" type="radio" name="type" value="a" checked="">
									<label for="buy-a">開運招財貔貅手環(男款)及晶華養生會館200元抵用券</label>
								</div>
								<div class="right_nb">
									<div class="input_quantity">
										<button type="button" onclick="remove_a()">–</button>
			                            <input type="text" name='num_a' id="num_a" value="1">
			                            <button type="button" onclick="add_a()">+</button>
			                        </div>
								</div>
							</li>
							<li class="clearfix">
								<div class="title">
									<input id="buy-b" type="radio" name="type" value="b" checked="">
									<label for="buy-b">開運招財貔貅手環(女款)及晶華養生會館200元抵用券</label>
								</div>
								<div class="right_nb">
									<div class="input_quantity">
										<button type="button" onclick="remove_b()">–</button>
			                            <input type="text" name='num_b' id="num_b" value="1">
			                            <button type="button" onclick="add_b()">+</button>
			                        </div>
								</div>
							</li>
						</ul>
					</div>
				<p><a href="#" onclick="document.getElementById('myForm').submit();">前往結帳</a></p>
				</div>
				</form>
			</div>
		</footer>
	</main>
</body>
</html>



