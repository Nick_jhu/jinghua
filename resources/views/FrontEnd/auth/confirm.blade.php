@extends('FrontEnd.layout',[
	"seo_title" => "註冊驗證",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("註冊驗證")),
	"seo_img" => null
])


@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/verificationcode.css')}}" type="text/css" media="screen">    
    
    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/js/tether.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

    @if($errors->has('message'))
    <script>
        var errorMsg = "{{$errors->get('message')[0]}}";
        alert(errorMsg);
    </script>
    @endif
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')
    <div class="breadcrumbs-box">
        <div class="content-box">
            <div class="name">會員專區</div>
            <div class="breadcrumbs">
                StartIt &gt; <a href="#">會員專區</a> &gt; <a href="#">輸入驗證碼</a>
            </div>
        </div>            
    </div>

    <div id="content-box">
        <div class="logo">
            <img src="{{url('assets/images/logo-black.png')}}" width="200" />
        </div>
        
        <div class="box">
            <div class="title">輸入驗證碼</div>
            <form role="form" method="POST" action="{{ route('member.do.confirm') }}"> 
                {{ csrf_field() }}
                <div class="verificationcode">
                    <input type="text" class="form-control" id="validation_code" name="validation_code" placeholder="請輸入四位數驗證碼">
                    <input type="hidden" name="phone" value="{{ Session::get('cellphone') }}">
                    <p class="note top">你輸入的號碼是 {{ Session::get('cellphone') }}</p>
                    <!-- <p class="note bottom">剩餘 <span>2</span>次 可發送驗證碼</p> -->
                </div>
                <button type="submit" class="btn btn-primary btn-block">送出</button>
                <button type="button" class="btn btn-primary btn-block" id="reSendBtn" style="display:none">重新發送驗證碼</button>
                <p class="note"><span id="sec">26</span> 秒後可重發驗證信</p>
                <a href="#">&lt; 重新輸入電話號碼</a>
            </form>
        </div>
    </div>
    
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
<script>

//設定倒數秒數
var t = 60;
var timer = null;
//顯示倒數秒收
function showTime()
{
    t -= 1;
    $("#sec").text(t);
    
    if(t==0)
    {
        $("#reSendBtn").show();
        clearTimeout(timer);
    }
    else {
        //每秒執行一次,showTime()
        timer = setTimeout("showTime()",1000);
    }
    
    
}

showTime();

$(function(){
    $("#reSendBtn").on("click", function(){
        
        $.post(BASE_URL + "/reSendCode", {}, function(data){
            if(data.message == "success") {
                $(this).hide();
                t = 60;
                showTime();
            }
        }, "JSON");

    });
});
</script>
<script type="text/javascript" src="{{url('assets/js/verificationcode.js')}}"></script>
@endsection

