@extends('FrontEnd.layout',[
	"seo_title" => "會員登入/註冊",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("會員登入/註冊")),
	"seo_img" => null
])

@section('after_style')
<link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
<link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{url('assets/css/sign.css')}}" type="text/css" media="screen">

<script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
	@if($errors->has('message'))
    <script>
        var errorMsg = "{{$errors->get('message')[0]}}";
        alert(errorMsg);
	</script>
    @endif
    <script type='text/javascript'>
		var captchaContainer = null;
		var loadCaptcha = function() {
			captchaContainer = grecaptcha.render('captcha_container', {
			'sitekey' : '6LfaRlIUAAAAAH06_yVHG1W2PC488GrHqoFPz7wf',
			'callback' : function(response) {
				//alert(response);
			}
			});
		};
	</script>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')
<div class="breadcrumbs-box">
	<div class="content-box">
		<div class="name">會員專區</div>
		<div class="breadcrumbs">
			<a href="{{url('/')}}">美z.人生</a> &gt; 發送驗證碼
		</div>
	</div>            
</div>
<div id="content-box">
	<div class="logo">
		<img src="{{url('assets/images/logo-black.png')}}" width="200" />
	</div>
	<div class="form tabs-box box">
		<div class="btn-box">
		</div>
		<div class="content-box">
			<!-- 註冊 -->
			<div class="signup-box box active">
				<form class="form" role="form" method="POST" action="{{ url('reSendVcode') }}">
					{{ csrf_field() }}
					<div class="input-group">
                        <div class="input-group-addon"><i class="fas fa-user"></i></div>
                        <input type="text" class="form-control" name="phone"  placeholder="請輸入手機號碼" required>
                    </div>
                    <div class="verificationcode-box">
                        <!-- <div class="top-box">
                            <div class="verificationcode"></div>
                            <div class="reloadbtn"><a href="#">更換一組驗證碼</a></div>
                            </div>
                            <input type="text" class="form-control"  placeholder="請輸入顯示的文字(英文可不區分大小寫)"> -->
                            <!-- <div class="g-recaptcha" data-sitekey="6LfaRlIUAAAAAH06_yVHG1W2PC488GrHqoFPz7wf"></div> -->

                            <div class="form-group">
                                <small>你是機器人嗎?</small>
                                <div id="captcha_container"></div>
                            </div>
                    </div>
					<button type="submit" class="btn btn-primary btn-block">發送</button>
					<!-- <button type="button" class="btn btn-light btn-block">使用電子信箱註冊</button> -->
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>
    <script type="text/javascript" src="{{url('assets/js/sign.js')}}"></script>
@endsection
