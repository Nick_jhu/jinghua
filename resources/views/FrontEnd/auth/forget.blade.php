@extends('FrontEnd.newlayout',[
	"seo_title" => "忘記密碼",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("忘記密碼")),
	"seo_img" => null
])

@section('after_style')
<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{url('assets/fonts/FontAwesome/font-awesome.css')}}">
<link rel="stylesheet" href="{{url('assets/css/sign.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{url('assets/css/bootsnav.css?')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{url('assets/css/newstyle.css')}}" type="text/css" media="screen">


<link href="{{url('assets/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('assets/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{url('assets/css/rwd.css')}}" type="text/css" media="screen">

	@if($errors->has('message'))
    <script>
        var errorMsg = "{{$errors->get('message')[0]}}";
        alert(errorMsg);
	</script>
	@endif

	<script type='text/javascript'>
		// var captchaContainer = null;
		// var loadCaptcha = function() {
		// 	captchaContainer = grecaptcha.render('captcha_container', {
		// 	'sitekey' : '6LfaRlIUAAAAAH06_yVHG1W2PC488GrHqoFPz7wf',
		// 	'callback' : function(response) {
		// 		//alert(response);
		// 	}
		// 	});
		// };
	</script>
<script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/imagefill.js')}}"></script>
@endsection

@section('header')
    @include('FrontEnd.layouts.newHeader')
@endsection

@section('content')
<div class="main-container" id="main-container" style="margin-bottom: 45px;">
<div id="content-box">
	<div class="logo">
		<img src="{{url('assets/images/logo-black.png')}}" width="200" />
	</div>
	<div class="form tabs-box box">
        <h4>忘記密碼</h4>
		<div class="content-box">
			<!-- 登入 -->
			<div class="signin-box box active">
				<form class="form" role="form" id="sendForm" method="POST" action="{{ route('member.forget.submit') }}">
					{{ csrf_field() }}
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-user"></i></div>
						<input type="text" class="form-control" name="phone"  placeholder="手機號碼/E-MAIL" required>
					</div>
					<div class="verificationcode-box">
							<div class="form-group">
								<small>你是機器人嗎?</small>
								<!-- <div id="captcha_container"></div> -->
								<div class="g-recaptcha" data-sitekey="6Ldkv5oUAAAAADzDzOOQashDs0vY1FPEMlLU4mTQ"></div>
							</div>
					</div>
					<button type="submit" id="sendBtn" class="btn btn-primary btn-block" style="background-color:#c9302c">送出</button>
					<p class="note" style="display:none"><span id="sec">10</span> 秒後可重新發送</p>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.newfooter')
@endsection

@section('after_scripts')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<script>
		//設定倒數秒數
		var t = 10;
		var timer = null;
		//顯示倒數秒收
		function showTime()
		{
			t -= 1;
			$("#sec").text(t);
			
			if(t==0)
			{
				//$("#reSendBtn").show();
				$("#sendBtn").prop('disabled', false);
				clearTimeout(timer);
			}
			else {
				//每秒執行一次,showTime()
				timer = setTimeout("showTime()",1000);
			}
			
			
		}

		$(function(){

			$("#sendForm").on("submit", function(){
				$.post("{{ route('member.forget.submit') }}", $(this).serialize(), function(data){
					if(data.msg == "success") {
						$("p.note").show();
						alert(data.log);
						$("#sendBtn").prop('disabled', true);
						t = 10;
						showTime();
					}
					else {
						alert(data.log);
					}
				}, "JSON");
				return false;
			});

		});
	</script>
	<!-- <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script> -->
    <script type="text/javascript" src="{{url('assets/js/sign.js')}}"></script>
@endsection
