@extends('FrontEnd.newlayout',[
	"seo_title" => $prodData->title,
	"seo_desc" => preg_replace("/\n+/","",strip_tags($prodData->slogan)),
	"seo_img" => (isset($prodData->img1)?url(Storage::url($prodData->img1)):url('assets/images/no_image.png'))
])
@section('after_style')
    <link rel="stylesheet" href="{{url('assets/fonts/FontAwesome/font-awesome.css')}}">
    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="{{url('assets/css/bootsnav.css?')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/newstyle.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/rwd.css')}}" type="text/css" media="screen">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <style>
        a.disabled{pointer-events: none;}
    </style>
@endsection
@section('header')
    @include('FrontEnd.layouts.newHeader')
@endsection
@section('content')
<div class="cont">
    <section class="page product-detail">
        <div class="container container2">
            <div class="product-detail_header">
                <div class="row">
                    <div class="col-md-6">
                        <div class="pro-img-box">
                            <img src="{{(isset($prodData->img1)?Storage::url($prodData->img1):url('assets/images/no_image.png'))}}" id="prodImg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product-detail_header_info">
                            <div class="product_maininfo">
                                <h3 class="pro-title">{{$prodData->title}}</h3>
                                <div class="product_price">
                                    @if($prodData->t_price == 0)
                                    <h6 class="pro-money">
                                    <span>$</span>  <b class="price">{{number_format($prodData->f_price)}}</b>
                                    </h6> 
                                    <del class="o-price">NTD {{number_format($prodData->t_price)}}</del> 
                                    @else
                                        <h6 class="pro-money">
                                        <span>$</span><b class="price">{{number_format($prodData->f_price)}} ~ <span>$</span> {{number_format($prodData->t_price)}}</b>
                                        </h6> 
                                        <del class="o-price">NTD {{number_format($prodData->t_price)}}</del> 
                                    @endif
                                </div>
                            </div>
                            <div class="product_info">
                                <!-- <div class="product_inventory_policy">
                                    <span>供貨狀況: </span><span class="val">尚有庫存 </span>
                                </div> -->
                                <div class="product_brief">
                                    {!!$prodData->slogan!!}
                                </div>
                                @if(!empty($instalment))
                                <div class="product_brief">
                                    ・分期資訊：{{$instalment[$instalmentLastKey]['showTxt']}}  <a href="#demo5" data-toggle="collapse">每期 <span> ${{$instalment[$instalmentLastKey]['amt']}} NTD </span><span class="fa fa-chevron-circle-down"></span></a>
                                    <div class="collapse staging" id="demo5">
                                        <p>
                                            @foreach($instalment as $row)
                                            <span>{{$row['showTxt']}}</span>，每期 <span>${{$row['amt']}}</span> NTD <a href="#bank" bank="{{$row['value3']}}">合作銀行</a><br>
                                            @endforeach
                                        </p>
                                        <div class="bank">
                                            <!-- <h5>合作銀行</h5>
                                            <span>xxx</span> -->
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if(isset($prodData->ship_way))
                                <div class="product_brief">
                                    ・運送方式：<br>
                                    <div class="howtopay">
                                        @foreach(explode(',', $prodData->ship_way) as $row)
                                        <img src="{{url('assets/images/shipping/'.$row.'.png')}}">
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                                <div class="product_brief">
                                    ・付款方式：<br>
                                    <div class="howtopay">
                                        <!-- <img src="{{url('assets/images/icon/pay-line.jpg')}}"> -->
                                        <img src="{{url('assets/images/icon/pay-apple.jpg')}}">
                                        <!-- <img src="{{url('assets/images/icon/pay-samsung.jpg')}}">
                                        <img src="{{url('assets/images/icon/pay-g.jpg')}}"> -->
                                        <img src="{{url('assets/images/icon/pay-card.jpg')}}">
                                        <!-- <img src="{{url('assets/images/icon/pay-c.jpg')}}"> -->
                                    </div>
                                </div>
                                @if(isset($giftData) && count($giftData) > 0)
                                <div class="product_brief">
                                    ・贈品：<br>
                                    <div class="product_brief product_gift">
                                        @foreach($giftData as $row)
                                        <a href="javascript:void(0)" onclick="window.open('{{$row->link}}', '', 'width=500,height=500');" >{{$row->descp}}</a><br>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            </div>
                            @if($prodData->sell_type == "S")
                            <div class="product_select01">
                                <div class="product_brief">
                                    ・請選擇商品：
                                </div>
                                @foreach($prodDetailData as $key=>$dRow)
                                    <a href="#prod" style="margin:5px;" class="prodDetailLink @if($dRow->stock <= 0) disabled @endif @if($key == 0 && $dRow->stock > 0) is-active @endif" 
                                    o-price="{{$dRow->o_price}}" 
                                    d-price="{{$dRow->d_price}}" 
                                    prod-id="{{$prodData->id}}"
                                    prod-no="{{$dRow->prod_no}}"
                                    prod-detail-id="{{$dRow->id}}"
                                    sell-type="{{$prodData->sell_type}}"
                                    prod-nm="{{$prodData->title}}({{$dRow->title}})"
                                    prod-type="{{$prodData->prod_type}}"
                                    prod-img = "{{(isset($dRow->img1)?Storage::url($dRow->img1):'')}}"
                                    >@if($dRow->stock > 0)
                                    {{$dRow->title}}
                                    @else
                                    {{$dRow->title}}(售完)
                                    @endif</a>
                                @endforeach
                            </div>
                            @endif
                            @if($prodData->sell_type == "R")
                            <div class="product_select02">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" id="date" class="form-control" placeholder="選擇旅遊期間">
                                    </div>
                                    <div class="col-sm-6">
                                        <select>
                                            @foreach($prodDetailData as $dRow)
                                            <option value="{{$dRow->id}}">{{$dRow->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="product_button">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <span class="product_quantity">
                                            <div class="btn-group" role="group" aria-label="First group">
                                                <button class="btn btn-secondary add-down" type="button"><svg class="svg-inline--fa fa-minus fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="minus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M424 318.2c13.3 0 24-10.7 24-24v-76.4c0-13.3-10.7-24-24-24H24c-13.3 0-24 10.7-24 24v76.4c0 13.3 10.7 24 24 24h400z"></path></svg><!-- <i class="fas fa-minus"></i> --></button>
                                                <input class="btn btn-secondary item-quantity" type="text" name="quantity" value="1" style="height:30px;" id="num">
                                                <button class="btn btn-secondary add-up" type="button"><svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M448 294.2v-76.4c0-13.3-10.7-24-24-24H286.2V56c0-13.3-10.7-24-24-24h-76.4c-13.3 0-24 10.7-24 24v137.8H24c-13.3 0-24 10.7-24 24v76.4c0 13.3 10.7 24 24 24h137.8V456c0 13.3 10.7 24 24 24h76.4c13.3 0 24-10.7 24-24V318.2H424c13.3 0 24-10.7 24-24z"></path></svg><!-- <i class="fas fa-plus"></i> --></button>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-xs-6">  
                                        <a class="btn" href="#" id="addCart">加入購物車</a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-detail_body">
                <!-- <div class="product-detail_body-s1">
                    <div class="product-detail_body-title"><h4>加購商品</h4></div>
                    <div class="responsive-slider2">
                        <div class="col-sm-4 col-xs-6 product-item product-item_more">
                            <div class="row">
                                <div class="pro-img-box col-md-4 col-sm-5 col-xs-4">
                                    <img src="https://cdn.shopify.com/s/files/1/0254/0393/products/3925d3dfbd8717cc9b325bd9e2eaad82_600x.jpg?v=1549874679">
                                </div>
                                <div class="pro-info col-md-8 col-sm-7 col-xs-8">
                                    <h6 class="pro-list-name">紅手指雲手機序號(30天3人同行包)</h6>
                                    <h6 class="pro-money"><span>加購價</span>$897</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6 product-item product-item_more is-active">
                            <div class="row">
                                <div class="pro-img-box col-md-4 col-sm-5 col-xs-4">
                                    <img src="https://cdn.shopify.com/s/files/1/0254/0393/products/3925d3dfbd8717cc9b325bd9e2eaad82_600x.jpg?v=1549874679">
                                </div>
                                <div class="pro-info col-md-8 col-sm-7 col-xs-8">
                                    <h6 class="pro-list-name">紅手指雲手機序號(30天3人同行包)</h6>
                                    <h6 class="pro-money"><span>加購價</span>$897</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6 product-item product-item_more">
                            <div class="row">
                                <div class="pro-img-box col-md-4 col-sm-5 col-xs-4">
                                    <img src="https://cdn.shopify.com/s/files/1/0254/0393/products/3925d3dfbd8717cc9b325bd9e2eaad82_600x.jpg?v=1549874679">
                                </div>
                                <div class="pro-info col-md-8 col-sm-7 col-xs-8">
                                    <h6 class="pro-list-name">紅手指雲手機序號(30天3人同行包)</h6>
                                    <h6 class="pro-money"><span>加購價</span>$897</h6>
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-4 col-xs-6 product-item product-item_more">
                            <div class="row">
                                <div class="pro-img-box col-md-4 col-sm-5 col-xs-4">
                                    <img src="https://cdn.shopify.com/s/files/1/0254/0393/products/3925d3dfbd8717cc9b325bd9e2eaad82_600x.jpg?v=1549874679">
                                </div>
                                <div class="pro-info col-md-8 col-sm-7 col-xs-8">
                                    <h6 class="pro-list-name">紅手指雲手機序號(30天3人同行包)</h6>
                                    <h6 class="pro-money"><span>加購價</span>$897</h6>
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-4 col-xs-6 product-item product-item_more">
                            <div class="row">
                                <div class="pro-img-box col-md-4 col-sm-5 col-xs-4">
                                    <img src="https://cdn.shopify.com/s/files/1/0254/0393/products/3925d3dfbd8717cc9b325bd9e2eaad82_600x.jpg?v=1549874679">
                                </div>
                                <div class="pro-info col-md-8 col-sm-7 col-xs-8">
                                    <h6 class="pro-list-name">紅手指雲手機序號(30天3人同行包)</h6>
                                    <h6 class="pro-money"><span>加購價</span>$897</h6>
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-4 col-xs-6 product-item product-item_more">
                            <div class="row">
                                <div class="pro-img-box col-md-4 col-sm-5 col-xs-4">
                                    <img src="https://cdn.shopify.com/s/files/1/0254/0393/products/3925d3dfbd8717cc9b325bd9e2eaad82_600x.jpg?v=1549874679">
                                </div>
                                <div class="pro-info col-md-8 col-sm-7 col-xs-8">
                                    <h6 class="pro-list-name">紅手指雲手機序號(30天3人同行包)</h6>
                                    <h6 class="pro-money"><span>加購價</span>$897</h6>
                                </div>
                            </div>
                        </div>
                </div> -->
                <div class="product-detail_body-s2">
                    <div class="tab">
                        <ul class="tabs">
                            <li><a href="#">商品詳情</a></li>
                            <li><a href="#">支援地區</a></li>
                        </ul> <!-- / tabs -->

                        <div class="tab_content">

                            <div class="tabs_item tabs_item-main text-center">
                                {!! $prodData->descp !!}
                            </div>
                            <div class="tabs_item">
                                {!! $prodData->other_content !!}
                            </div> 
                        </div>
                    </div> 
                </div>
                <div class="product-detail_body-s3">
                    <div class="product-detail_body-title"><h4>相關商品</h4></div>
                    <div class="responsive-slider">
                        @foreach($relateData as $row)
                        <div class="col-md-3 col-sm-4 col-xs-6 product-item">
                            <a href="{{url('productDetail/'.$row->id)}}">
                                <div class="pro-img-box">
                                    <div class="cover"></div>
                                    <img src="{{(isset($row->img1)?Storage::url($row->img1):url('assets/images/no_image.png'))}}">
                                </div>
                                <h6 class="pro-list-name">{{$row->title}}</h6>
                                <h6 class="pro-money"><span>TWD</span> {{number_format($row->f_price)}}</h6>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('footer')
    @include('FrontEnd.layouts.newfooter')
@endsection

@section('after_scripts')

<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {  
        setTimeout(() => {
            $('a[href="#prod"]').eq(0).click();
        }, 300);
    (function ($) { 
        
        $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
        
        $('.tab ul.tabs li a').click(function (g) { 
            var tab = $(this).closest('.tab'), 
                index = $(this).closest('li').index();
            
            tab.find('ul.tabs > li').removeClass('current');
            $(this).closest('li').addClass('current');
            
            tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
            tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
            
            g.preventDefault();
        } );
    })(jQuery);

});
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
<script type="text/javascript">
    $('.responsive-slider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            }
        },
        {
            breakpoint: 600,
            settings: {
            slidesToShow: 2,
            slidesToScroll: 2
            }
        }
        ]
    });
    $('.responsive-slider2').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
        {
            breakpoint: 600,
            settings: {
            slidesToShow: 2,
            slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1
            }
        }
        ]
    });

    $(function(){
        $('a[href="#bank"]').on('click', function(){
            const bank = $(this).attr('bank');

            Swal.fire({
                type: 'info',
                title: '合作銀行',
                html: bank,
                showConfirmButton: true,
                closeOnClickOutside: true,
                allowOutsideClick: true
            });
        });
    });
</script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/newprod.js?v=')}}{{Config::get('app.version')}}"></script>
<script type="text/javascript" src="{{url('assets/js/imagefill.js')}}"></script>
<script type="text/javascript">
    $("body").addClass("product-detail-page");
    $('#date').daterangepicker({
        "startDate": "02/26/2019",
        "endDate": "03/04/2019"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
    $('.pro-img-box').imagefill();
</script>

@endsection
