@extends('FrontEnd.layout',[
	"seo_title" => "會員專區",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("會員專區")),
	"seo_img" => null
])

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/member.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/newcart.css')}}">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
    <style>
        input {padding: 5px !important;}
    </style>

    @if(Session::get('message'))
    <script>
        alert("{{Session::get('message')}}");
    </script>
    @endif

    @if($errors->any())
    <script>
        var errorMsg = "{{$errors->all()[0]}}";
        alert(errorMsg);
    </script>
    @endif
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')
<div class="breadcrumbs-box">
    <div class="content-box">
        <div class="name">美Z商城</div>
        <div class="breadcrumbs">
            <a href="{{url('/')}}">美z.人生</a>  &gt; 會員專區
        </div>
    </div>
</div>
<div id="content-box">
    <div class="tabs-box">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-content-1">未付款訂單({{count($orderData)}})</a></li>
            <li><a data-toggle="tab" href="#tab-content-4">處理中訂單({{count($processOrderData)}})</a></li>
            <li><a data-toggle="tab" href="#tab-content-3">歷史訂單({{count($orderHistoryData)}})</a></li>
            <li><a data-toggle="tab" href="#tab-content-2">會員資料</a></li>
        </ul>
        <div class="tab-content">
            <!-- /// -->
            <div id="tab-content-1" class="tab-pane fade in active">
                <h3>訂單列表</h3>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="col-md-3 text-center">訂單編號</th>
                            <th scope="col" class="col-md-2 text-center">訂單狀態</th>
                            <th scope="col" class="col-md-2 text-center">訂單日期</th>
                            <th scope="col" class="col-md-2 text-center">訂單金額</th>
                            <th scope="col" class="col-md-1 text-center">付款</th>
                            <th scope="col" class="col-md-1 text-center">取消</th>
                            <th scope="col" class="col-md-2 text-center">查看明細</th>
                        </tr>
                    </thead>
                </table>
                @foreach($orderData as $key=>$row)
                <div class="panel-group" id="accordion-{{$key}}">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th scope="col" class="col-md-3 text-center">
                                                {{$row->ord_no}}
                                            </th>
                                            <th scope="col" class="col-md-2 text-center" id="orderStatus">
                                                @if($row->status == 'A')
                                                    未付款
                                                @elseif($row->status == 'B')
                                                    已付款
                                                @elseif($row->status == 'C' || $row->status == 'I')
                                                    運送中
                                                @elseif($row->status == 'D')
                                                    已送達
                                                @elseif($row->status == 'E')
                                                    已歸還
                                                @elseif($row->status == 'F')
                                                    通知取消
                                                @elseif($row->status == 'G')
                                                    已取消
                                                @elseif($row->status == 'H')
                                                    已刷退
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-2 text-center">{{substr($row->created_at, 0, 16)}}</th>
                                            <th scope="col" class="col-md-2 text-center">$ {{number_format($row->d_price)}}</th>
                                            <th scope="col" class="col-md-1 text-center">
                                                @if($row->status == 'A')
                                                <button class="btn btn-link" name="payBtn" url="{{url('rePay')}}" data-toggle="modal" data-target="#myModal" ordNo="{{$row->ord_no}}" ordId="{{$row->id}}" ttlAmt="{{$row->d_price}}"><i class="fas fa-credit-card"></i></button>
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-1 text-center">
                                                @if($row->status == 'A')
                                                <button class="btn btn-link" name="cancelBtn" url="{{url('cancelOrder/'.$row->id)}}"><i class="fas fa-trash-alt"></i></button>
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-2 text-center">
                                                <a class="toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" 
                                                    href="#collapse-1-{{$key}}">
                                                <span class="open-txt">展開</span>
                                                <span class="close-txt">收合</span>
                                                </a>
                                            </th>
                                        </tr>
                                        @if($row->CVSPaymentNo != " NULL" && isset($row->CVSPaymentNo) && ($row->status == 'C' || $row->status == 'I'))
                                        <tr>
                                            <th colspan="7" class="text-center">
                                            @if($row->pick_way == 'UNIMARTC2C')
                                            物流查詢單號：<a href="https://eservice.7-11.com.tw/e-tracking/search.aspx" target="_blank">{{$row->CVSPaymentNo.$row->CVSValidationNo}}</a>
                                            @elseif($row->pick_way == 'FAMIC2C')
                                            物流查詢單號：<a href="https://www.famiport.com.tw/distribution_search.asp?page=4" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @elseif($row->pick_way == 'HILIFEC2C')
                                            物流查詢單號：<a href="http://www.hilife.com.tw/serviceInfo_search.aspx" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @else
                                            物流查詢單號：<a href="http://query2.e-can.com.tw/%E5%A4%9A%E7%AD%86%E6%9F%A5%E4%BB%B6A.htm" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @endif
                                            </th> 
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </h4>
                        </div>
                        <div id="collapse-1-{{$key}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-2 bs-wizard-step @if($row->status == 'A') active @elseif($row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已下單</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step  @if($row->status == 'B') active @elseif($row->status == 'A' || $row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已付款</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-3 bs-wizard-step @if($row->status == 'C' || $row->status == 'I') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">運送中</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step @if($row->status == 'D') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'F' || $row->status == 'G' || $row->status == 'I') disabled @else complete @endif">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已送達</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    @if(count($orderRentData[$key]) >  0)
                                    <div class="col-xs-3 bs-wizard-step @if($row->status == 'E') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'D' || $row->status == 'F' || $row->status == 'G' || $row->status == 'I') disabled @else complete @endif">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已歸還</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    @endif
                                </div>
                                @if(count($orderRentData[$key]) >  0)
                                <div class="title"><i class="fas fa-square red"></i> 租賃列表</div>
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th scope="col">國家</th> -->
                                            <th scope="col">商品</th>
                                            <th scope="col">日期(起)</th>
                                            <th scope="col">日期(迄)</th>
                                            <th scope="col">顯示天數</th>
                                            <!-- <th scope="col">結算天數</th> -->
                                            <th scope="col">單價</th>
                                            <th scope="col">備註</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderRentData[$key] as $val)
                                        <tr class="table-red">
                                            <!-- <th scope="row">日本</th> -->
                                            <td>{{$val->prod_nm}}</td>
                                            <td>{{substr($val->f_day, 0, 10)}}</td>
                                            <td>{{substr($val->e_day, 0, 10)}}</td>
                                            <td>{{$val->use_day}}</td>
                                            <!-- <td>100</td> -->
                                            <td>{{number_format($val->amt)}}</td>
                                            <td>
                                                {!! $row->cust_remark !!}
                                                <br />領機航廈：@if($row->departure_terminal == "T1") 桃1 @endif @if($row->departure_terminal == "T2") 桃2 @endif
                                                <br />還機航廈：@if($row->return_terminal == "T1") 桃1 @endif  @if($row->return_terminal == "T2") 桃2 @endif 
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                                <br>
                                @if(count($orderDetailData[$key]) > 0)
                                <div class="title"><i class="fas fa-square orange"></i> 購買列表</div>
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">商品</th>
                                            <th scope="col">單價</th>
                                            <th scope="col">數量</th>
                                            <th scope="col">金額</th>
                                            <th scope="col">動作</th>
                                            <th scope="col">備註</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderDetailData[$key] as $val)
                                        <tr class="table-orange">
                                            <th scope="row">{{$val->prod_nm}}</th>
                                            <td>{{number_format($val->unit_price)}}</td>
                                            <td>{{$val->num}}</td>
                                            <td>{{number_format($val->amt)}}</td>
                                            <td>
                                                @if($val->prod_type == 'S' && $row->status != 'A')
                                                    <button type="button" class="btn btn-link reSendSn" order-detail-id="{{$val->id}}" data-target="#snModal" ordNo="{{$row->ord_no}}">查看序號</button>
                                                @endif
                                            </td>
                                            <td>
                                                {!! $row->cust_remark !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- 分頁 -->
                <br>
                <!-- <nav aria-label="Page navigation example" class="text-center">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav> -->
            </div>
            <!-- /// -->
            <div id="tab-content-2" class="tab-pane fade">
                <h3>會員資料</h3>
                <form class="form" role="form" method="POST" action="{{ url('member/update') }}">
                    {{ csrf_field() }}
                    <table class="table">
                        <tr>
                            <td class="col-md-2">會員手機</td>
                            <td>
                                <div class="form-group col-md-5">                                        
                                    <input type="text" class="form-control" name="cellphone" value="{{$userData->cellphone}}" placeholder="電話">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">會員姓名</td>
                            <td>
                                <div class="form-group col-md-5">                                        
                                    <input type="text" class="form-control" name="name" value="{{$userData->name}}" placeholder="姓名">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">會員EMAIL</td>
                            <td>
                                <div class="form-group col-md-5"> 
                                    {{$userData->email}}                                      
                                    {{-- <input type="email" class="form-control" name="email" value="{{$userData->email}}" placeholder="Email"> --}}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">修改密碼</td>
                            <td>
                                <div class="col-md-5">                                        
                                    <input type="password" class="form-control" name="password"  placeholder="密碼">
                                    <br>
                                    <input type="password" class="form-control" name="confirm_password"  placeholder="確認密碼">
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-2">收貨資料</td>
                            <td>
                                <div class="row">
                                    <div class="form-group col-md-2 col-xs-2">                                        
                                            <input type="text" class="form-control" id="dlv_zip" name="dlv_zip" value="{{$userData->dlv_zip}}" placeholder="郵地區號" readonly>
                                    </div>
                                    <div class="form-group col-md-3 col-xs-4" style="padding-left:20px;">       
                                        <select class="form-control" name="dlv_city" id="dlv_city">
                                            <option value="" selected>請選擇縣市</option>
                                            @foreach($cityData as $row)
                                            <option value="{{$row->city_nm}}" @if($userData->dlv_city == $row->city_nm) selected @endif>{{$row->city_nm}}</option>
                                            @endforeach
                                        </select>                                 
                                        {{--<input type="text" class="form-control" name="dlv_city" value="{{$userData->dlv_city}}" placeholder="城市">--}}
                                    </div>
                                    <div class="form-group col-md-3 col-xs-4" style="padding-left:20px;"> 
                                        <select class="form-control" name="dlv_area" id="dlv_area">
                                            <option value="" selected>請選擇鄉鎮市區</option>
                                            @foreach($areaData as $row)
                                            <option zip="{{$row->zip_f}}" value="{{$row->dist_nm}}" @if($userData->dlv_area == $row->dist_nm) selected @endif>{{$row->dist_nm}}</option>
                                            @endforeach
                                        </select>                                                 
                                        <!-- <input type="text" class="form-control" name="dlv_area" value="{{$userData->dlv_area}}" placeholder="區域"> -->
                                    </div>
                                </div>
                                <div class="row" style="margin-top:5px;">
                                    <div class="form-group col-md-8">                                        
                                        <input type="text" class="form-control" name="dlv_addr" value="{{$userData->dlv_addr}}" placeholder="地址">
                                    </div>
                                </div>
                            </td>
                        </tr>

                        {{-- <tr>
                            <td class="col-md-2">取貨方式</td>
                            <td>
                                <div class="form-group col-md-5">
                                    <select class="form-control" id="pick_way" name="pick_way">
                                        <option value="" selected>請選擇取貨方式</option>
                                        @foreach($pickWay as $row)
                                            <option value="{{$row->cd}}" @if($userData->pick_way == $row->cd) selected @endif>{{$row->cd_descp}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">還貨方式</td>
                            <td>
                                <div class="form-group col-md-5">
                                    <select class="form-control" id="return_way" name="return_way">
                                        <option value="" selected>請選擇還貨方式</option>
                                        @foreach($returnWay as $row)
                                            <option value="{{$row->cd}}" @if($userData->return_way == $row->cd) selected @endif>{{$row->cd_descp}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                        </tr> --}}
                    </table>
                    <br>
                    <button class="btn btn-lg btn-primary btn-block">送出</button>
                </form>
            </div>
            <!-- /// -->
            <div id="tab-content-3" class="tab-pane fade">
                <h3>歷史訂單</h3>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="col-md-3 text-center">訂單編號</th>
                            <th scope="col" class="col-md-2 text-center">訂單狀態</th>
                            <th scope="col" class="col-md-2 text-center">訂單日期</th>
                            <th scope="col" class="col-md-2 text-center">訂單金額</th>
                            <th scope="col" class="col-md-1 text-center">付款</th>
                            <th scope="col" class="col-md-1 text-center">取消</th>
                            <th scope="col" class="col-md-2 text-center">查看明細</th>
                            </tr>
                    </thead>
                </table>

                @foreach($orderHistoryData as $key=>$row)
                <div class="panel-group" id="accordion-{{$key}}">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th scope="col" class="col-md-3 text-center">
                                                {{$row->ord_no}}
                                            </th>
                                            <th scope="col" class="col-md-2 text-center" id="orderStatus">
                                                @if($row->status == 'A')
                                                    未付款
                                                @elseif($row->status == 'B')
                                                    已付款
                                                @elseif($row->status == 'C' || $row->status == 'I')
                                                    運送中
                                                @elseif($row->status == 'D')
                                                    已送達
                                                @elseif($row->status == 'E')
                                                    已歸還
                                                @elseif($row->status == 'F')
                                                    通知取消
                                                @elseif($row->status == 'G')
                                                    已取消
                                                @elseif($row->status == 'H')
                                                    已刷退
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-2 text-center">{{substr($row->created_at,0,16)}}</th>
                                            <th scope="col" class="col-md-2 text-center">$ {{number_format($row->d_price)}}</th>
                                            <th scope="col" class="col-md-1 text-center">
                                                @if($row->status == 'A')
                                                <button class="btn btn-link" name="payBtn" url="{{url('rePay')}}" data-toggle="modal" data-target="#myModal" ordNo="{{$row->ord_no}}" ordId="{{$row->id}}"><i class="fas fa-credit-card"></i></button>
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-1 text-center">
                                                @if($row->status == 'A')
                                                <button class="btn btn-link" name="cancelBtn" url="{{url('cancelOrder/'.$row->id)}}"><i class="fas fa-trash-alt"></i></button>
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-2 text-center">
                                                <a class="toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" 
                                                    href="#collapse-3-{{$key}}">
                                                <span class="open-txt">展開</span>
                                                <span class="close-txt">收合</span>
                                                </a>
                                            </th>
                                        </tr>
                                        @if($row->CVSPaymentNo != " NULL" && isset($row->CVSPaymentNo) && ($row->status == 'C' || $row->status == 'I' || $row->status == 'D'))
                                        <tr>
                                            <th colspan="7" class="text-center">
                                            @if($row->pick_way == 'UNIMARTC2C')
                                            物流查詢單號：<a href="https://eservice.7-11.com.tw/e-tracking/search.aspx" target="_blank">{{$row->CVSPaymentNo.$row->CVSValidationNo}}</a>
                                            @elseif($row->pick_way == 'FAMIC2C')
                                            物流查詢單號：<a href="https://www.famiport.com.tw/distribution_search.asp?page=4" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @elseif($row->pick_way == 'HILIFEC2C')
                                            物流查詢單號：<a href="http://www.hilife.com.tw/serviceInfo_search.aspx" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @else
                                            物流查詢單號：{{$row->CVSPaymentNo}}
                                            @endif
                                            </th> 
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </h4>
                        </div>
                        <div id="collapse-3-{{$key}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-2 bs-wizard-step @if($row->status == 'A') active @elseif($row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已下單</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step  @if($row->status == 'B') active @elseif($row->status == 'A' || $row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已付款</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-3 bs-wizard-step @if($row->status == 'C' || $row->status == 'I') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">運送中</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step @if($row->status == 'D') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'F' || $row->status == 'G' || $row->status == 'I') disabled @else complete @endif">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已送達</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    @if(count($orderHistoryRentData[$key]) >  0)
                                    <div class="col-xs-3 bs-wizard-step @if($row->status == 'E') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'D' || $row->status == 'F' || $row->status == 'G' || $row->status == 'I') disabled @else complete @endif">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已歸還</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    @endif
                                </div>
                                @if(count($orderHistoryRentData[$key]) > 0)
                                <div class="title"><i class="fas fa-square red"></i> 租賃列表</div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th scope="col">國家</th> -->
                                            <th scope="col">商品</th>
                                            <th scope="col">日期(起)</th>
                                            <th scope="col">日期(迄)</th>
                                            <th scope="col">顯示天數</th>
                                            <!-- <th scope="col">結算天數</th> -->
                                            <th scope="col">單價</th>
                                            <th scope="col">備註</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderHistoryRentData[$key] as $val)
                                        <tr class="table-red">
                                            <!-- <th scope="row">日本</th> -->
                                            <td>{{$val->prod_nm}}</td>
                                            <td>{{substr($val->f_day, 0, 10)}}</td>
                                            <td>{{substr($val->e_day, 0, 10)}}</td>
                                            <td>{{((strtotime($val->e_day) - strtotime($val->f_day))/3600)/24}}</td>
                                            <td>{{number_format($val->amt)}}</td>
                                            <td>
                                                {!! $row->cust_remark !!}
                                                <br />領機航廈：@if($row->departure_terminal == "T1") 桃1 @endif @if($row->departure_terminal == "T2") 桃2 @endif
                                                <br />還機航廈：@if($row->return_terminal == "T1") 桃1 @endif  @if($row->return_terminal == "T2") 桃2 @endif 
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                                <br>
                                @if(count($orderHistoryDetailData[$key]) > 0)
                                <div class="title"><i class="fas fa-square orange"></i> 購買列表</div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">商品</th>
                                            <th scope="col">單價</th>
                                            <th scope="col">數量</th>
                                            <th scope="col">金額</th>
                                            <th scope="col">動作</th>
                                            <th scope="col">備註</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderHistoryDetailData[$key] as $val)
                                        <tr class="table-orange">
                                            <th scope="row">{{$val->prod_nm}}</th>
                                            <td>{{number_format($val->unit_price)}}</td>
                                            <td>{{$val->num}}</td>
                                            <td>{{number_format($val->amt)}}</td>
                                            <td>
                                                @if($val->prod_type == 'S' && $row->status != 'A')
                                                    <button type="button" class="btn btn-link reSendSn" order-detail-id="{{$val->id}}" data-target="#snModal" ordNo="{{$row->ord_no}}">查看序號</button>
                                                @endif
                                            </td>
                                            <td>
                                                {!! $row->cust_remark !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- 分頁 -->
                <br>
                <!-- <nav aria-label="Page navigation example" class="text-center">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav> -->
            </div>

            <div id="tab-content-4" class="tab-pane fade in">
                <h3>訂單列表</h3>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="col-md-3 text-center">訂單編號</th>
                            <th scope="col" class="col-md-2 text-center">訂單狀態</th>
                            <th scope="col" class="col-md-2 text-center">訂單日期</th>
                            <th scope="col" class="col-md-2 text-center">訂單金額</th>
                            <th scope="col" class="col-md-1 text-center">付款</th>
                            <th scope="col" class="col-md-1 text-center">取消</th>
                            <th scope="col" class="col-md-2 text-center">查看明細</th>
                        </tr>
                    </thead>
                </table>
                @foreach($processOrderData as $key=>$row)
                <div class="panel-group" id="accordion-{{$key}}">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th scope="col" class="col-md-3 text-center">
                                                {{$row->ord_no}}
                                            </th>
                                            <th scope="col" class="col-md-2 text-center" id="orderStatus">
                                                @if($row->status == 'A')
                                                    未付款
                                                @elseif($row->status == 'B')
                                                    已付款
                                                @elseif($row->status == 'C' || $row->status == 'I')
                                                    運送中
                                                @elseif($row->status == 'D')
                                                    已送達
                                                @elseif($row->status == 'E')
                                                    已歸還
                                                @elseif($row->status == 'F')
                                                    通知取消
                                                @elseif($row->status == 'G')
                                                    已取消
                                                @elseif($row->status == 'H')
                                                    已刷退
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-2 text-center">{{substr($row->created_at, 0, 16)}}</th>
                                            <th scope="col" class="col-md-2 text-center">$ {{number_format($row->d_price)}}</th>
                                            <th scope="col" class="col-md-1 text-center">
                                                @if($row->status == 'A')
                                                <button class="btn btn-link" name="payBtn" url="{{url('rePay')}}" data-toggle="modal" data-target="#myModal" ordNo="{{$row->ord_no}}" ordId="{{$row->id}}"><i class="fas fa-credit-card"></i></button>
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-1 text-center">
                                                @if($row->status == 'A')
                                                <button class="btn btn-link" name="cancelBtn" url="{{url('cancelOrder/'.$row->id)}}"><i class="fas fa-trash-alt"></i></button>
                                                @endif
                                            </th>
                                            <th scope="col" class="col-md-2 text-center">
                                                <a class="toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" 
                                                    href="#collapse-2-{{$key}}">
                                                <span class="open-txt">展開</span>
                                                <span class="close-txt">收合</span>
                                                </a>
                                            </th>
                                        </tr>
                                        @if($row->CVSPaymentNo != " NULL" && isset($row->CVSPaymentNo) && ($row->status == 'C' || $row->status == 'I'))
                                        <tr>
                                            <th colspan="7" class="text-center">
                                            @if($row->pick_way == 'UNIMARTC2C')
                                            物流查詢單號：<a href="https://eservice.7-11.com.tw/e-tracking/search.aspx" target="_blank">{{$row->CVSPaymentNo.$row->CVSValidationNo}}</a>
                                            @elseif($row->pick_way == 'FAMIC2C')
                                            物流查詢單號：<a href="https://www.famiport.com.tw/distribution_search.asp?page=4" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @elseif($row->pick_way == 'HILIFEC2C')
                                            物流查詢單號：<a href="http://www.hilife.com.tw/serviceInfo_search.aspx" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @else
                                            物流查詢單號：<a href="http://query2.e-can.com.tw/%E5%A4%9A%E7%AD%86%E6%9F%A5%E4%BB%B6A.htm" target="_blank">{{$row->CVSPaymentNo}}</a>
                                            @endif
                                            </th> 
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </h4>
                        </div>
                        <div id="collapse-2-{{$key}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-2 bs-wizard-step @if($row->status == 'A') active @elseif($row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已下單</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step  @if($row->status == 'B') active @elseif($row->status == 'A' || $row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已付款</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-3 bs-wizard-step @if($row->status == 'C' || $row->status == 'I') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'F' || $row->status == 'G') disabled @else complete @endif">
                                        <!-- complete -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">運送中</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    <div class="col-xs-2 bs-wizard-step @if($row->status == 'D') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'F' || $row->status == 'G' || $row->status == 'I') disabled @else complete @endif">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已送達</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    @if(isset($orderProcessRentDetail[$key]) && count($orderProcessRentDetail[$key]) >  0)
                                    <div class="col-xs-3 bs-wizard-step @if($row->status == 'E') active @endif @if($row->status == 'A' || $row->status == 'B' || $row->status == 'C' || $row->status == 'D' || $row->status == 'F' || $row->status == 'G' || $row->status == 'I') disabled @else complete @endif">
                                        <!-- active -->
                                        <div class="text-center bs-wizard-stepnum" style="font-size:12px">已歸還</div>
                                        <div class="progress">
                                            <div class="progress-bar"></div>
                                        </div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"></div>
                                    </div>
                                    @endif
                                </div>
                                @if(isset($orderProcessRentDetail[$key]) && count($orderProcessRentDetail[$key]) >  0)
                                <div class="title"><i class="fas fa-square red"></i> 租賃列表</div>
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th scope="col">國家</th> -->
                                            <th scope="col">商品</th>
                                            <th scope="col">日期(起)</th>
                                            <th scope="col">日期(迄)</th>
                                            <th scope="col">顯示天數</th>
                                            <!-- <th scope="col">結算天數</th> -->
                                            <th scope="col">單價</th>
                                            <th scope="col">備註</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderProcessRentDetail[$key] as $val)
                                        <tr class="table-red">
                                            <!-- <th scope="row">日本</th> -->
                                            <td>{{$val->prod_nm}}</td>
                                            <td>{{substr($val->f_day, 0, 10)}}</td>
                                            <td>{{substr($val->e_day, 0, 10)}}</td>
                                            <td>{{$val->use_day}}</td>
                                            <!-- <td>100</td> -->
                                            <td>{{number_format($val->amt)}}</td>
                                            <td>
                                                {!! $row->cust_remark !!}
                                                <br />領機航廈：@if($row->departure_terminal == "T1") 桃1 @endif @if($row->departure_terminal == "T2") 桃2 @endif
                                                <br />還機航廈：@if($row->return_terminal == "T1") 桃1 @endif  @if($row->return_terminal == "T2") 桃2 @endif 
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                                <br>
                                @if(isset($orderProcessDetail[$key]) && count($orderProcessDetail[$key]) > 0)
                                <div class="title"><i class="fas fa-square orange"></i> 購買列表</div>
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">商品</th>
                                            <th scope="col">單價</th>
                                            <th scope="col">數量</th>
                                            <th scope="col">金額</th>
                                            <th scope="col">動作</th>
                                            <th scope="col">備註</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orderProcessDetail[$key] as $val)
                                        <tr class="table-orange">
                                            <th scope="row">{{$val->prod_nm}}</th>
                                            <td>{{number_format($val->unit_price)}}</td>
                                            <td>{{$val->num}}</td>
                                            <td>{{number_format($val->amt)}}</td>
                                            <td>
                                                @if($val->prod_type == 'S' && $row->status != 'A')
                                                    <button type="button" class="btn btn-link reSendSn" order-detail-id="{{$val->id}}" data-target="#snModal" ordNo="{{$row->ord_no}}">查看序號</button>
                                                @endif
                                            </td>
                                            <td>
                                                {!! $row->cust_remark !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- 分頁 -->
                <br>
                <!-- <nav aria-label="Page navigation example" class="text-center">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav> -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">付款確認-訂單號：<span id="ordNo"></span></h4>
        </div>
        <main class="main_shoppingcar" style="min-height:auto;padding:0;">
        <form class="shoppingcar_form_box" action="{{url('rePay')}}" method="POST" id="payForm" style="padding:0;">
            {{ csrf_field() }}
            <input type="hidden" name="ttlAmt" value="">
            <input type="hidden" name="prime" />
            <input type="hidden" name="agreePayRemeber" />
            @if(isset($userData->card_token))
            <input type="hidden" name="oldQuickPay" value="YES"/>
            <input type="hidden" name="useQuickPay" value="YES"/>
            @endif
            <input type="hidden" name="ordId" id="ordId">
            <div class="modal-body">
                <div class="group clearfix">
                    <label class="left_label">付款方式</label>
                    <div class="right_fillbox">
                        <div class="clearfix">
                            <div class="styled-select w_01">
                                <select class="md-input form-control" id="payWay" name="payWay">
                                    <option value="Credit">信用卡(一次付清)</option>
                                    <!-- <option value="Installment">信用卡(分期付款)</option> -->
                                    <!-- <option value="GooglePay">Google Pay</option> -->
                                    <option value="ApplePay">Apple Pay</option>
                                    <!-- <option value="LinePay">Line Pay</option>
                                    <option value="SamsungPay">Samsung Pay</option> -->
                                    <option value="WEBATM">Web ATM</option>
                                    <option value="CVS">超商付款</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="group clearfix creditCardArea" @if(isset($userData->card_token)) style="display:none" @endif>
                    <label class="left_label">信用卡卡號</label>
                    <div class="right_fillbox for_w100">
                        <div id="tappay-iframe"></div>
                        <!-- <p class="p_note p_k"><input type="checkbox" name="creditRemember" value="YES"> 記錄本次付款資訊，並設定為快速結帳</p> -->
                        <div class="redbox-checkbox clearfix">
                            <input id="iknow7" type="checkbox" name="creditRemember" value="YES" checked="">
                            <label class="checkbox_style" for="iknow7"></label>
                            <span> 記錄本次付款資訊，並設定為快速結帳</span>
                        </div>
                    </div>
                </div>

                @if(isset($userData->card_token))
                <div class="group clearfix" id="quickPayment">
                    <label class="left_label">信用卡卡號</label>
                    <div class="right_fillbox for_w100">
                        <input type="text" class="fbox" value="{{'**** **** **** '.$userData->last_four}}" disabled>
                        <button type="button" class="btn btn-sm btn-primary" id="btnChangeCard">換一張信用卡</button>
                    </div>
                </div>
                @endif

                <div class="group clearfix">
                    <label class="left_label">發票類型</label>
                    <div class="right_fillbox">
                        <div class="styled-select w_01">
                            <select class="md-input form-control" name="invoiceType" id="invoiceType">
                                <option value="a2" selected="selected">二聯電子發票</option>
                                <option value="a1">捐贈發票</option>
                                <option value="a3">三聯電子發票</option>
                            </select>
                        </div>
                        <p class="p_note p_gray">配合國稅局勸止二聯換開三聯之政策，本公司保留換開發票的權利</p>
                    </div>
                </div>
                <div class="group clearfix a3" style="display: none">
                    <label class="left_label">統一編號</label>
                    <div class="right_fillbox">
                        <input class="w_01 fbox" type="text" name="customerIdentifier" placeholder="限數字">
                    </div>
                </div>
                <div class="group clearfix a3" style="display: none">
                    <label class="left_label">發票抬頭</label>
                    <div class="right_fillbox">
                        <input class="fbox" type="text" name="customerIdentifier" placeholder="限22個全形文字">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                <button type="submit" class="btn btn-primary">送出</button>
            </div>
        </form>
        </main>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="snModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">訂單號：<span id="snOrdNo"></span></h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="customerIdentifier">序號</label>
                <textarea type="text" class="form-control" id="sn" rows="10" readonly></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
    <script src="https://pay.google.com/gp/p/js/pay.js"></script>
    <script src="https://js.tappaysdk.com/tpdirect/v4"></script>
    @if(env('APP_ENV') == 'production')
    <script> TPDirect.setupSDK(13155, 'app_4R0lByKiPVDYQOqemZ8ERcmsIn5TJB16B8G5R7BUQPmGESOjjdLlwl00zGT8', 'production') </script>
    @else
    <script> TPDirect.setupSDK(13155, 'app_4R0lByKiPVDYQOqemZ8ERcmsIn5TJB16B8G5R7BUQPmGESOjjdLlwl00zGT8', 'sandbox') </script>
    @endif
    
    <script type="text/javascript" src="{{url('assets/js/member.js')}}?v={{Config::get('app.version')}}"></script>
@endsection