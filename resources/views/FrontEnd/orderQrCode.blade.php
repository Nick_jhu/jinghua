<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>美z人生旅遊</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
</head>
<body>
    
    <div class="container h-100" style="margin-top: 10px;">
        <div class="visible-print text-center">
            <img src="{{url('assets/images/logo-black.png')}}" alt="" width="300"/>
        </div>
        @if(isset($ordData))
        <div class="row h-100 justify-content-center align-items-center">
            <div class="visible-print text-center">
                {!! QrCode::size(300)->generate($ordData->dlv_phone); !!}
            </div>
        </div>  
        @foreach($rentData as $row)
        <table class="table">
            <tbody>
                <tr>
                    <td>訂單號</td>
                    <td>{{$ordData->ord_no}}</td>
                </tr>
                <tr>
                    <td>產品名稱</td>
                    <td>{{$row->prod_nm}}</td>
                </tr>
                <tr>
                    <td>收件人</td>
                    <td>{{$ordData->dlv_nm}}</td>
                </tr>
                <tr>
                    <td>電話</td>
                    <td>{{$ordData->dlv_phone}}</td>
                </tr>
                <tr>
                    <td>取機航廈</td>
                    <td>
                        @if($ordData->departure_terminal == "T1")
                            桃園國際機場第一航廈
                        @elseif($ordData->departure_terminal == "T2")
                            桃園國際機場第二航廈
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>還機航廈</td>
                    <td>
                        @if($ordData->return_terminal == "T1")
                            桃園國際機場第一航廈
                        @elseif($ordData->return_terminal == "T2")
                            桃園國際機場第二航廈
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>電話</td>
                    <td>{{$ordData->dlv_phone}}</td>
                </tr>
                <tr class="table-success">
                    <td>出國日</td>
                    <td>{{substr($row->f_day, 0, 10)}}</td>
                </tr>
                <tr class="table-danger">
                    <td>歸國日</td>
                    <td>{{substr($row->e_day, 0, 10)}}</td>
                </tr>
            </tbody>
        </table>
        @endforeach
        @else
        <div class="row h-100 justify-content-center align-items-center">
            <p>訂單號錯誤</p>
        </div> 
        @endif
    </div>
</body>
</html>