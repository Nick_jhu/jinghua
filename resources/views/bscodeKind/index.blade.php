@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      {{ trans('bscodeKind.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">
      <li class="active">{{ trans('bscodeKind.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "bscodeKind";
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/bscode_kind') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/bscode_kind') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind') }}" + "/{id}/edit";

var btnGroup = [
  {
    btnId: "btnSearchWindow",
    btnIcon: "fa fa-search",
    btnText: "{{ trans('common.search') }}",
    btnFunc: function () {
        $('#searchWindow').jqxWindow('open');
    }
  },
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("bscodeKind.titleName") }}');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
      alert("Delete");
    }
  }
];
</script>
@endsection

@include('backpack::template.search')
