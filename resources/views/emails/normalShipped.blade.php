<p>{{$order['name']}}&nbsp; 您好,</p>
<p>您訂購的商品已經出貨</p>
<p>訂單號：<a href="https://z-trip.com/center" target="_blank">{{$order['ordNo']}}</a></p>
<p>商品名細如下：</p>
<p>@foreach($order['prodDetail'] as $val){{$val['prod_nm']}} * {{$val['num']}}<br>@endforeach</p>
<p>@foreach($order['wifiDetail'] as $val){{$val['prod_nm']}} * {{$val['num']}} @endforeach</p>
<p>@if($order['ShipmentNo'] != '')貨物追蹤查詢單號：<a href="{{$order['ShipmentUrl']}}">{{$order['ShipmentNo']}}</a>@endif</p>
<p>@if($order['sendprod_remark'] != '')出貨備註：{{$order['sendprod_remark']}}@endif</p>
<p>※若為宅配通配送，每日宅配通查詢貨況時間為超過晚上7點後，敬請留意，謝謝。<br></p>
<p>美z.人生 感謝您的使用，謝謝。</p>