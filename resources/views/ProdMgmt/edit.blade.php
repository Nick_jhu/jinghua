@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	商品編輯<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'OrderMgmt') }}">商品總覽</a></li>
		<li class="active">商品編輯</li>
	</ol>
</section>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>
						<!-- <li><a href="#tab_2" data-toggle="tab" aria-expanded="true">圖片管理</a></li> -->
						<li><a href="#tab_3" data-toggle="tab" aria-expanded="true">詳細資料</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
										<label for="title">商品編號</label>
										<input type="text" class="form-control" id="prod_no" name="prod_no" required>
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="prod_type">商品大類</label>
										<select class="form-control" id="prod_type" name="prod_type" @if(isset($id)) switch="off" disabled @endif) >
                                            <option value="N">一般商品</option>
                                            <option value="W">WIFI</option>
                                            <option value="S">軟體</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="sell_type">銷售方式</label>
										<select class="form-control" id="sell_type" name="sell_type">
                                            <option value="S" selected>買</option>
                                            <option value="R">租</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="brand">品牌</label>
                                        <!-- <select class="form-control select2" data-placeholder="請選擇" style="width: 100%;" name="brand[]"></select> -->
                                        <input type="text" class="form-control" id="brand_nm" name="brand_nm" />
                                        <input type="hidden" class="form-control" id="brand" name="brand" />
                                    </div>
                                </div>
								<div class="row">
									<div class="form-group col-md-3">
										<label for="cate_id">商品類別</label>
										<select class="form-control select2"  name="cate_id[]"></select>
									</div>
                                    <div class="form-group col-md-3" id="countryArea">
										<label for="country">適用地區</label>
                                        {{-- <input type="text" class="form-control" id="country" name="country"> --}}
                                        <select class="form-control select2" multiple="multiple" data-placeholder="請選擇" style="width: 100%;" name="country[]"></select>
									</div>
									<div class="form-group col-md-3">
										<label for="title">商品名稱</label>
										<input type="text" class="form-control" id="title" name="title">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="sub_title">副標題</label>
										<input type="text" class="form-control" id="sub_title" name="sub_title">
									</div>
								</div>

                                <div class="row">
									<div class="form-group col-md-3">
										<label for="f_price">價格區間(起)</label>
										<input type="number" class="form-control" id="f_price" name="f_price">
									</div>
									<div class="form-group col-md-3" id="tPriceArea">
										<label for="t_price">價格區間(迄)</label>
										<input type="number" class="form-control" id="t_price" name="t_price">
									</div>
                                    <!-- <div class="form-group col-md-3">
										<label for="pay_way">付款方式</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="請選擇" style="width: 100%;" name="pay_way[]"></select>
									</div> -->
                                    <div class="form-group col-md-3">
										<label for="ship_way">送貨方式</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="請選擇" style="width: 100%;" name="ship_way[]"></select>
									</div>
                                    <div class="form-group col-md-3">
										<label for="number_of_periods">分期</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="請選擇" style="width: 100%;" name="number_of_periods[]"></select>
									</div>
								</div>


                                <div class="row">
									<div class="form-group col-md-3">
										<label for="d_percent">折扣%</label>
										<input type="number" class="form-control" id="d_percent" name="d_percent">
									</div>
									<div class="form-group col-md-3" id="action_date">
										<label for="action_date">活動日期</label>
										<input type="text" class="form-control" id="action_date" name="action_date">
									</div>
                                    <div class="form-group col-md-3">
										<label for="is_hot">熱門商品</label>
										<select class="form-control" id="is_hot" name="is_hot">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
									</div>
                                    <div class="form-group col-md-3">
										<label for="is_focus">精選商品</label>
										<select class="form-control" id="is_focus" name="is_focus">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
									</div>
								</div>

                                @if(Auth::user()->identity == 'A')
                                <div class="row">
                                    
									<div class="form-group col-md-3">
										<label for="is_added">上架</label>
										<select class="form-control" id="is_added" name="is_added">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>
                                    
									<div class="form-group col-md-3">
										<label for="added_on">上架時間</label>
										<input type="text" class="form-control" id="added_on" name="added_on">
                                    </div>
                                    <div class="form-group col-md-3">
										<label for="sort">順序</label>
                                        <input type="number" class="form-control" id="sort" name="sort">
                                    </div>
                                </div>
                                @endif
                                
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="img1">圖片1</label>
                                        <input type="file" name="img1" id="img1"> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="img_descp1">描述</label>
                                        <input type="text" class="form-control" id="img_descp1" name="img_descp1">
                                    </div>
                                    <div class="form-group col-md-3">

                                        @if(isset($id) && $crud->entry['original']['img1'])
                                        <a name="img1" href="{{Storage::url($crud->entry['original']['img1'])}}" target="_blank">
                                            <img name="img1" src="{{Storage::url($crud->entry['original']['img1'])}}" alt="" height="100">
                                        </a>
                                        @else
                                            <a name="img1" href="#" target="_blank">
                                                <img name="img1" src="#" alt="" height="100" style="display:none">
                                            </a>
                                        @endif
                                    </div>
                                </div>

								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif

							</div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            {{--<div class="row">
                                <div class="form-group col-md-3">
                                    <label for="img2">圖片2</label>
                                    <input type="file" name="img2" id="img2"> 
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="img_descp2">描述</label>
                                    <input type="text" class="form-control" id="img_descp2" name="img_descp2">
                                </div>
                                <div class="form-group col-md-3">
                                    @if(isset($id) && $crud->entry['original']['img2'])
                                    <a name="img2" href="{{Storage::url($crud->entry['original']['img2'])}}" target="_blank">
                                        <img name="img2" src="{{Storage::url($crud->entry['original']['img2'])}}" alt="" height="100">
                                    </a>
                                    @else
                                        <a name="img2" href="#" target="_blank">
                                            <img name="img2" src="#" alt="" height="100" style="display:none">
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="img3">圖片3</label>
                                    <input type="file" name="img3" id="img3"> 
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="img_descp3">描述</label>
                                    <input type="text" class="form-control" id="img_descp3" name="img_descp3">
                                </div>
                                <div class="form-group col-md-3">
                                    @if(isset($id) && $crud->entry['original']['img3'])
                                    <a name="img3" href="{{Storage::url($crud->entry['original']['img3'])}}" target="_blank">
                                        <img name="img3" src="{{Storage::url($crud->entry['original']['img3'])}}" alt="" height="100">
                                    </a>
                                    @else
                                        <a name="img3" href="#" target="_blank">
                                            <img name="img3" src="#" alt="" height="100" style="display:none">
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="img4">圖片4</label>
                                    <input type="file" name="img4" id="img4"> 
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="img_descp4">描述</label>
                                    <input type="text" class="form-control" id="img_descp4" name="img_descp4">
                                </div>
                                <div class="form-group col-md-3">
                                    @if(isset($id) && $crud->entry['original']['img4'])
                                    <a name="img4" href="{{Storage::url($crud->entry['original']['img4'])}}" target="_blank">
                                        <img name="img4" src="{{Storage::url($crud->entry['original']['img4'])}}" alt="" height="100">
                                    </a>
                                    @else
                                        <a name="img4" href="#" target="_blank">
                                            <img name="img4" src="#" alt="" height="100" style="display:none">
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="img5">圖片5</label>
                                    <input type="file" name="img5" id="img5"> 
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="img_descp5">描述</label>
                                    <input type="text" class="form-control" id="img_descp5" name="img_descp5">
                                </div>
                                <div class="form-group col-md-3">
                                    @if(isset($id) && isset($crud->entry['original']['img5']))
                                    <a name="img5" href="{{Storage::url($crud->entry['original']['img5'])}}" target="_blank">
                                        <img name="img5" src="{{Storage::url($crud->entry['original']['img5'])}}" alt="" height="100">
                                    </a>
                                    @else
                                        <a name="img5" href="#" target="_blank">
                                            <img name="img5" alt="" height="100" style="display:none">
                                        </a>
                                    @endif
                                </div>
                            </div> --}}
                        </div>
                        
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="slogan">商品簡述</label>
                                    <textarea class="form-control iEditor" id="slogan" name="slogan"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="descp">商品描述</label>
                                    <textarea class="form-control iEditor" id="descp" name="descp"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="other_content">支援地區</label>
                                    <textarea class="form-contro iEditor" id="other_content" name="other_content"></textarea>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</form>
		</div>	
    </div>
    
    <div class="row">

        <div class="col-md-12">
            <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
                <ul class="nav nav-tabs">
                    <li name="prodDetail" class="active">
                        <a href="#tab_4" data-toggle="tab" aria-expanded="false">商品型號</a>
                    </li>
                    <li name="snDetail">
                        <a href="#tab_5" data-toggle="tab" aria-expanded="false">序號</a>
                    </li>
                    <li name="prodGift">
                        <a href="#tab_6" data-toggle="tab" aria-expanded="false">贈品</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- /.tab-pane -->
                    <div class="tab-pane active" id="tab_4" name="prodDetail">
                        <div class="box box-primary" id="subBox" style="display:none">
                            <div class="box-header with-border">
                            <h3 class="box-title">商品型號</h3>
                            </div>
                            <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="country">適用地區</label>
                                        <select type="text" class="form-control input-sm" name="country" grid="true" >
                                            @isset($crud->create_fields['country']['options'])
                                                @foreach($crud->create_fields['country']['options'] as $row)
                                                    <option value="{{$row->code}}">{{$row->descp}}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="title">名稱</label>
                                        <input type="text" class="form-control input-sm" name="title" grid="true" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="stock">庫存</label>
                                        <input type="number" class="form-control input-sm" name="stock" grid="true" >
                                    </div>  
                                    <div class="form-group col-md-3">
                                        <label for="seq">順序</label>
                                        <input type="number" class="form-control input-sm" name="seq" grid="true" >
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <!-- <div class="form-group col-md-3">
                                        <label for="c_price">成本價</label>
                                        <input type="number" class="form-control input-sm" name="c_price" grid="true" >
                                    </div>   -->
                                    <div class="form-group col-md-3">
                                        <label for="o_price">原價</label>
                                        <input type="number" class="form-control input-sm" name="o_price" grid="true" >
                                    </div>  
                                    <div class="form-group col-md-3">
                                        <label for="d_price">特價</label>
                                        <input type="number" class="form-control input-sm" name="d_price" grid="true" >
                                    </div>                              
                                    <div class="form-group col-md-3">
                                        <label for="d_price">活動結束價格</label>
                                        <input type="number" class="form-control input-sm" name="end_price" grid="true" >
                                    </div>   
                                </div>
                                <!-- <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="slogan">商品簡述</label>
                                        <textarea class="form-control" name="slogan" grid="true"></textarea>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="img1">圖片1</label>
                                        <input type="file" name="img1" grid="true"> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="img_descp1">描述1</label>
                                        <input type="text" class="form-control" name="img_descp1" grid="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <a name="grid_img1" href="#" target="_blank">
                                            <img name="grid_img1" src="#" alt="" height="100" style="display:none">
                                        </a>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="img2">圖片2</label>
                                        <input type="file" name="img2" grid="true"> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="img_descp2">描述2</label>
                                        <input type="text" class="form-control" name="img_descp2" grid="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <a name="grid_img2" href="#" target="_blank">
                                            <img name="grid_img2" src="#" alt="" height="100" style="display:none">
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="img3">圖片3</label>
                                        <input type="file" name="img3" grid="true"> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="img_descp3">描述3</label>
                                        <input type="text" class="form-control" name="img_descp3" grid="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <a name="grid_img3" href="#" target="_blank">
                                            <img name="grid_img3" src="#" alt="" height="100" style="display:none">
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="img4">圖片4</label>
                                        <input type="file" name="img4" grid="true"> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="img_descp4">描述4</label>
                                        <input type="text" class="form-control" name="img_descp4" grid="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <a name="grid_img4" href="#" target="_blank">
                                            <img name="grid_img4" src="#" alt="" height="100" style="display:none">
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="img5">圖片5</label>
                                            <input type="file" name="img5" grid="true"> 
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="img_descp5">描述5</label>
                                            <input type="text" class="form-control" name="img_descp5" grid="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <a name="grid_img5" href="#" target="_blank">
                                                <img name="grid_img1" src="#" alt="" height="100" style="display:none">
                                            </a>
                                        </div>
                                    </div> -->
                            </div>
                            <!-- /.box-body -->
    
                            <div class="box-footer">
                                <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                            </div>
                            </form>
                        </div>
                        
                        <div id="jqxGrid"></div>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane active" id="tab_5" name="snDetail">
                        <div class="box box-primary" id="subBox1" style="display:none">
                            <form method="POST"  accept-charset="UTF-8" id="subForm1" enctype="multipart/form-data">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="sn">序號</label>
                                            <input type="text" class="form-control input-sm" name="sn" grid="true" >
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="is_sell">已使用</label>
                                            <select class="form-control" id="is_sell" name="is_sell" grid="true">
                                                <option value="Y">是</option>
                                                <option value="N">否</option>
                                            </select>
                                        </div>                            
                                    </div>
                                </div>
                                <!-- /.box-body -->
        
                                <div class="box-footer">
                                    <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                    <button type="button" class="btn btn-sm btn-primary" id="Save1">{{ trans('common.save') }}</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="Cancel1">{{ trans('common.cancel') }}</button>
                                </div>
                            </form>
                        </div>
                        
                        <div id="snGrid"></div>
                    </div>

                    <div class="tab-pane" id="tab_6" name="prodGift">
                        <div class="box box-primary" id="subBox2" style="display:none">
                            <form method="POST"  accept-charset="UTF-8" id="subForm2" enctype="multipart/form-data">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="prod_id">贈品商品名稱</label>
                                            <input type="text" class="form-control input-sm" name="descp" grid="true" >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="link">連結</label>
                                            <input type="text" class="form-control input-sm" name="link" grid="true" >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="img1">圖片1</label>
                                            <input type="file" name="img1" grid="true"> 
                                        </div>                            
                                    </div>
                                </div>
                                <!-- /.box-body -->
        
                                <div class="box-footer">
                                    <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                    <button type="button" class="btn btn-sm btn-primary" id="Save2">{{ trans('common.save') }}</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="Cancel2">{{ trans('common.cance2') }}</button>
                                </div>
                            </form>
                        </div>
                        
                        <div id="GiftGrid"></div>
                    </div>
    
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
        
    </div>
@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
	<script>
        var mainId = "";
        var prodNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/ProdMgmt') }}";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "{{Storage::url('/')}}";

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        console.log(editData);
		//editObj  = JSON.parse(objJson);
		@endif

		$(function () {

            function prodTypeSwitch(prodType) {
                if(prodType == "S") {
                    $("#countryArea").hide();
                    $("#tPriceArea").hide();
                }
                else {
                    $("#countryArea").show();
                    $("#tPriceArea").show();
                }
            }

            function chkProdNo(prodNo) {
                var result = true;
                $.ajax({
                    method: "GET",
                    async: false,
                    url: BASE_URL + "/chkProdNoExist/"  + prodNo,
                })
                .done(function( data ) {
                    if(data.status == "error") {
                        swal(data.msg, "", "error");
                        result = false;
                    }
                });

                return result;
            }

            $("#prod_no").on("change", function(){
                var chk = chkProdNo($(this).val());

                if(chk == false) {
                    $(this).val(prodNo);
                }
                else {
                    prodNo = $(this).val();
                }
            });

            $("#prod_type").on("change", function(){
                prodTypeSwitch($(this).val());
            });

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/ProdMgmt') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_product') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/ProdMgmt') }}";
			formOpt.afterInit = function() {
				if(mainId != null) {
                    $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                    setTimeout(function(){
                        //CKEDITOR.instances['descp'].setReadOnly(true);
                        //CKEDITOR.instances['slogan'].setReadOnly(true);
                        //CKEDITOR.instances['other_content'].setReadOnly(true);
                        
                    }, 500);                    
                }                    
                
                var prodType = $("#prod_type").val();
                prodNo = $("#prod_no").val();

                if(prodType == "S") {
                    $("[name='snDetail']").show();
                    $("[name='prodDetail']").hide();
                    $("[name='prodGift']").hide();
                    $("[href='#tab_5']").click();
                }
                else {
                    $("[name='snDetail']").hide();
                    $("[name='prodDetail']").show();
                    $("[href='#tab_4']").click();
                }
                
                getDetailGrid();
                genSnGrid();
                genGiftGrid();
                prodTypeSwitch(prodType);
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                setTimeout(function(){
                    CKEDITOR.instances['descp'].setReadOnly(false);
                    CKEDITOR.instances['slogan'].setReadOnly(false);
                    CKEDITOR.instances['other_content'].setReadOnly(false);

                    CKEDITOR.instances['descp'].setData("");
                    CKEDITOR.instances['slogan'].setData("");
                    CKEDITOR.instances['other_content'].setData("");
                }, 500);
                
                @if(isset($prodType))
                $("#prod_type").val("{{$prodType}}");
                $("#prod_type").trigger("change");
                @endif
			}

			formOpt.editFunc = function() {
                setTimeout(function(){
                    CKEDITOR.instances['descp'].setReadOnly(false);
                    CKEDITOR.instances['slogan'].setReadOnly(false);
                    CKEDITOR.instances['other_content'].setReadOnly(false);
                }, 500);
                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                setTimeout(function(){
                    CKEDITOR.instances['descp'].setReadOnly(false);
                    CKEDITOR.instances['slogan'].setReadOnly(false);
                    CKEDITOR.instances['other_content'].setReadOnly(false);

                    CKEDITOR.instances['descp'].setData(editObj.descp);
                    CKEDITOR.instances['slogan'].setData(editObj.slogan);
                    CKEDITOR.instances['other_content'].setData(editObj.other_content);
                }, 500);
            }

            formOpt.beforeSave = function() {
                return chkProdNo(row["prod_no"]);
            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;

                if(result.img1 != null) {
                    $("img[name='img1']").attr("src", result.img1).show();
                    $("a[name='img1']").attr("href", result.img1);
                }

                if(result.img2 != null) {
                    $("img[name='img2']").attr("src", result.img2).show();
                    $("a[name='img2']").attr("href", result.img2);
                }

                if(result.img3 != null) {
                    $("img[name='img3']").attr("src", result.img3).show();
                    $("a[name='img3']").attr("href", result.img3);
                }

                if(result.img4 != null) {
                    $("img[name='img4']").attr("src", result.img4).show();
                    $("a[name='img4']").attr("href", result.img4);
                }

                if(result.img5 != null) {
                    $("img[name='img5']").attr("src", result.img5).show();
                    $("a[name='img5']").attr("href", result.img5);
                }

                CKEDITOR.instances['descp'].setReadOnly(true);
                CKEDITOR.instances['slogan'].setReadOnly(true);
                CKEDITOR.instances['other_content'].setReadOnly(true);
                $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                $("input[type='file']").val("");
			}

			var btnGroup = [

			];

            initBtn(btnGroup);


            $('a[href="#tab_3"]').on("click", function(){
                var descp = CKEDITOR.instances['descp'].getData();
                var slogan = CKEDITOR.instances['slogan'].getData();
                var other_content = CKEDITOR.instances['other_content'].getData();

                if(descp == "") {
                    CKEDITOR.instances['descp'].setData(editObj.descp);
                }

                if(slogan == "") {
                    CKEDITOR.instances['slogan'].setData(editObj.slogan);
                }

                if(other_content == "") {
                    CKEDITOR.instances['other_content'].setData(editObj.other_content);
                }
                
            });
            
        });

        function getDetailGrid() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/storage/' + value.replace('public/', '') + '"/>';
                }

                return "";
            }
            var countryCode = []

            if(fieldObj != null) {
                for(i in fieldObj["country"]["options"]) {
                    var obj = {value: fieldObj["country"]["options"][i]["code"], label: fieldObj["country"]["options"][i]["descp"]};
                    countryCode.push(obj);
                }
            }

            var countrySource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: countryCode
            };

            var countryAdapter = new $.jqx.dataAdapter(countrySource, {
                autoBind: true
            });
            //console.log(countryAdapter.records);
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "seq", type: "number"},
                    {name: "title", type: "string"},
                    {name: "country", type: "string", values: { source: countryAdapter.records, value: 'value', name: 'label' }},
                    {name: "c_price", type: "number"},
                    {name: "o_price", type: "number"},
                    {name: "d_price", type: "number"},
                    {name: "stock", type: "number"},
                    {name: "sell_amt", type: "number"},
                    {name: "end_price", type: "number"},
                    {name: "slogan", type: "string"},
                    {name: "img1", type: "string"},
                    {name: "img_descp1", type: "string"},
                    {name: "img2", type: "string"},
                    {name: "img_descp2", type: "string"},
                    {name: "img3", type: "string"},
                    {name: "img_descp3", type: "string"},
                    {name: "img4", type: "string"},
                    {name: "img_descp4", type: "string"},
                    {name: "img5", type: "string"},
                    {name: "img_descp5", type: "string"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "順序", datafield: "seq", width: 100},
                    {text: "名稱", datafield: "title", width: 130},
                    {text: "適用地區", datafield: "country", width: 150},
                    {text: "成本價", datafield: "c_price", width: 150},
                    {text: "原價", datafield: "o_price", width: 150},
                    {text: "特價", datafield: "d_price", width: 150},
                    {text: "庫存", datafield: "stock", width: 150},
                    {text: "銷量", datafield: "sell_amt", width: 150},
                    {text: "活動結束價格", datafield: "end_price", width: 150},
                    {text: "商品簡述", datafield: "slogan", width: 150},
                    {text: "圖片1", datafield: "img1", width: 150, cellsrenderer: imagerenderer},
                    {text: "描述1", datafield: "img_descp1", width: 150},
                    {text: "圖片2", datafield: "img2", width: 150, cellsrenderer: imagerenderer},
                    {text: "描述2", datafield: "img_descp2", width: 150},
                    {text: "圖片3", datafield: "img3", width: 150, cellsrenderer: imagerenderer},
                    {text: "描述3", datafield: "img_descp3", width: 150},
                    {text: "圖片4", datafield: "img4", width: 150, cellsrenderer: imagerenderer},
                    {text: "描述4", datafield: "img_descp4", width: 150},
                    {text: "圖片5", datafield: "img5", width: 150, cellsrenderer: imagerenderer},
                    {text: "描述5", datafield: "img_descp5", width: 150},
                ]
            ];
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/prodDetail') }}" + '/' + prodNo;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/prodDetail') }}" + "/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/prodDetail') }}" + "/update/"+ prodNo;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/prodDetail') }}" + "/delete/";
            opt.defaultKey = {'prod_no': prodNo};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.adapterRecords = {
                "country": countryAdapter.records
            };
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            genDetailGrid(opt);
        }
        
        function genSnGrid() {
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "sn", type: "string"},
                    {name: "is_sell", type: "string"},
                    {name: "email", type: "string"}
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "序號", datafield: "sn", width: 300},
                    {text: "已使用", datafield: "is_sell", width: 130},
                    {text: "email", datafield: "email", width: 130},
                ]
            ];
            var opt = {};
            opt.gridId = "snGrid";
            opt.fieldData = col;
            opt.formId = "subForm1";
            opt.saveId = "Save1";
            opt.cancelId = "Cancel1";
            opt.showBoxId = "subBox1";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/snDetail') }}" + '/' + prodNo;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/snDetail') }}" + "/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/snDetail') }}" + "/update/"+ prodNo;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/snDetail') }}" + "/delete/";
            opt.defaultKey = {'prod_no': prodNo};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.adapterRecords = {};
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                
            }
            genDetailGrid(opt);

            $("#prod_type").on("change", function(){
                if($(this).val() == "S") {
                    $("[name='snDetail']").show();
                    $("[name='prodDetail']").hide();
                    $("[href='#tab_5']").click();
                }
                else {
                    $("[name='snDetail']").hide();
                    $("[name='prodDetail']").show();
                    $("[href='#tab_4']").click();
                }
            });
        }
        function genGiftGrid() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/storage/' + value.replace('public/', '') + '"/>';
                }

                return "";
            }
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "descp", type: "string"},
                    {name: "img1", type: "string"},
                    {name: "link", type: "string"}
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "贈品商品名稱", datafield: "descp", width: 300},
                    {text: "圖片1", datafield: "img1", width: 150, cellsrenderer: imagerenderer},
                    {text: "連結", datafield: "link", width: 130},
                ]
            ];
            var opt = {};
            opt.gridId = "GiftGrid";
            opt.fieldData = col;
            opt.formId = "subForm2";
            opt.saveId = "Save2";
            opt.cancelId = "Cancel2";
            opt.showBoxId = "subBox2";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/prodGift') }}" + '/' + mainId;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/prodGift') }}" + "/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/prodGift') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/prodGift') }}" + "/delete/";

            opt.defaultKey = {'prod_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.adapterRecords = {};
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
          
            }
            genDetailGrid(opt);
            $("#prod_type").on("change", function(){
                if($(this).val() == "S") {
                    $("[name='snDetail']").show();
                    $("[name='prodDetail']").hide();
                    $("[href='#tab_5']").click();
                }
                else {
                    $("[name='snDetail']").hide();
                    $("[name='prodDetail']").hide();
                    $("[name='prodGift']").show();
                    $("[href='#tab_4']").click();
                }
            });
			}
	</script>
@endsection