@extends('backpack::layout')

@section('header')
<section class="content-header">
    <h1>
    {{ trans('dashboard.titleName') }}<small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{ trans('dashboard.titleName') }}</li>
    </ol>
</section>
@endsection

@section('content')

@endsection

@section('after_scripts')


@endsection