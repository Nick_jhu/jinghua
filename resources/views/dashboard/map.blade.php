<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ trans('menu.map') }}</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="{{url('/css/materialize.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
        body,
        html {
            height: 100%;
            width: 100%;
        }

        #map_canvas {
            height: 100%;
            width: 100%
        }

        #map_canvas img {
            max-width: none;
        }

        #map_canvas div {
            -webkit-transform: translate3d(0, 0, 0);
        }
    </style>
</head>

<body>
    <div class="fixed-action-btn" style="top: 45px; left: 24px;width:56px;height:56px;">
        <a class="btn-floating btn-large button-collapse" data-activates="nav-mobile" id="menuBtn">
            <i class="material-icons">menu</i>
        </a>
    </div>
    <ul id="nav-mobile" class="side-nav">
        <li>
            <div class="user-view">
                <div class="background">
                    <img src="https://images.unsplash.com/photo-1493679349286-c570804c71ec?auto=format&fit=crop&w=1351&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D"
                        width="800">
                </div>
                <a href="#!user">
                    <!-- <img class="circle" src="images/yuna.jpg"> -->
                    <img src="https://placehold.it/160x160/26a69a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="circle" alt="User Image">
                </a>
                <a href="#!name">
                    <span class="white-text">{{ Auth::user()->name }}</span>
                </a>
                <a href="#!email">
                    <span class="white-text email">{{ Auth::user()->email }}</span>
                </a>
            </div>
        </li>
        <li style="padding: 10px;">
            <div class="input-field">
                <input id="car_no" id="car_no" type="text" class="validate">
                <label for="car_no">車號：</label>
            </div>
        </li>
        <li style="padding: 10px;">
            <div class="input-field">
                <input id="s_date" id="s_date" type="text" class="validate">
                <label for="s_date">開始時間：</label>
            </div>
        </li>
        <li style="padding: 10px;">
            <div class="input-field">
                <input id="e_date" id="e_date" type="text" class="validate">
                <label for="e_date">結束時間：</label>
            </div>
        </li>
        <li>
            <a class="waves-effect waves-light btn" id="searchPathBtn">卡車路線</a>
        </li>
        <li>
            <a class="waves-effect waves-light btn" id="searchCar">卡車目前狀態</a>
        </li>
        <li>
            <div class="divider"></div>
        </li>
    </ul>
    <div id="map_canvas" style="position: relative; overflow: hidden;"></div>
</body>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w&libraries=geometry,places&ext=.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.0/firebase.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script>
    $('#s_date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $('#e_date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $("#s_date").click(function(){
        $("div.bootstrap-datetimepicker-widget").find("a.btn").removeClass("btn");
    });

    $("#e_date").click(function(){
        $("div.bootstrap-datetimepicker-widget").find("a.btn").removeClass("btn");
    });

    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    var m = (date.getMonth()+1);
    var d = date.getDate();

    if(m < 10) {
        m = '0' + m;
    }

    if(d < 10) {
        d = '0' + d;
    }
    $('#s_date').val(date.getFullYear() + '-' + m + '-' + d + ' 00:00:00');
    $('#e_date').val(date.getFullYear() + '-' + m + '-' + d + ' 23:59:59');

    var markers = [];
    
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: new google.maps.LatLng(25.041591, 121.538248),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });

    //Associate the styled map with the MapTypeId and set it to display.
    //map.mapTypes.set('styled_map', styledMapType);
    //map.setMapTypeId('styled_map');

    
    var flightPath = null;
    var snappedPolyline = null;
    var lastOpenedInfoWindow;
    $(function () {
        $(".button-collapse").sideNav({
            menuWidth: 300
        });

        $("#searchPathBtn").click(function () {
            
            $.get("{{ url(config('backpack.base.route_prefix', 'admin') . '/carPath/get') }}", {'s_date': $("#s_date").val(), 'e_date': $('#e_date').val(), 'car_no': $('#car_no').val()}, function (data) {
                if(data.length == 0) {
                    swal("查無結果", "", "warning");
                    return;
                }
                if(flightPath != null) {
                    flightPath.setMap(null);
                }
                
                clearMarkers();

                /*
                var flightPlanCoordinates = data.gpsData;
                for(i in flightPlanCoordinates) {
                    flightPlanCoordinates[i].lng = parseFloat(flightPlanCoordinates[i].lng);
                    flightPlanCoordinates[i].lat = parseFloat(flightPlanCoordinates[i].lat);
                }
                flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#ec407a ',
                    strokeOpacity: 1.0,
                    strokeWeight: 8
                });

                flightPath.setMap(null);
                flightPath.setMap(map);
                map.setCenter({
                    lat: data[0].lat,
                    lng: data[0].lng
                });
                */
                
                
                for(i in data.marksData) {
                    
                    var marker1 = new google.maps.Marker({
                        position: new google.maps.LatLng(data.marksData[i].lat, data.marksData[i].lng),
                        map: map,
                        icon: 'https://cdn1.iconfinder.com/data/icons/web-and-application-1/512/home_house-48.png'
                    });
                    markers.push(marker1);
                    var sort = parseInt(i) + 1;
                    setMarkWindow(marker1, data.marksData[i], sort);
                }

                for(i in data.setOffData) {
                    
                    var marker1 = new google.maps.Marker({
                        position: new google.maps.LatLng(25.060439, 121.574060),
                        map: map,
                        icon: 'https://cdn2.iconfinder.com/data/icons/officeicons/PNG/48/Flag.png'
                    });
                    markers.push(marker1);
                    setSetOffWindow(marker1, data.setOffData[i], i);
                }
                

                setMapOnAll(map);
                
                //runSnapToRoad(data);
                runSnapToRoad(data.gpsData);
            });
        });
    });

    function setSetOffWindow(thisMarker, data, sort) {
        var custNm = (data.cust_nm == null)?"":data.cust_nm;
        var addr = (data.addr == null)?"":data.addr;
        var infowindow = new google.maps.InfoWindow({
                        content: '出發時間：'+data.created_at//+'<br />經度：'+data.lat+'<br />緯度：'+data.lng
                        });
        google.maps.event.clearListeners(thisMarker, 'click');
        thisMarker.addListener('click', function() {
            closeLastOpenedInfoWindow();
            infowindow.open(map, thisMarker);
            lastOpenedInfoWindow = infowindow;
        });
    }

    function setMarkWindow(thisMarker, data, sort) {
        var custNm = (data.cust_nm == null)?"":data.cust_nm;
        var addr = (data.addr == null)?"":data.addr;
        var infowindow = new google.maps.InfoWindow({
                        content: '順序：'+sort+'<br />客戶名稱：'+custNm+'<br />地址：'+addr+'<br />司機：'+data.driver_nm+'<br />完成時間：'+data.created_at+'<br />經度：'+data.lat+'<br />緯度：'+data.lng
                        });
        google.maps.event.clearListeners(thisMarker, 'click');
        thisMarker.addListener('click', function() {
            closeLastOpenedInfoWindow();
            infowindow.open(map, thisMarker);
            lastOpenedInfoWindow = infowindow;
        });
    }

    function closeLastOpenedInfoWindow() {
        if (lastOpenedInfoWindow) {
            lastOpenedInfoWindow.close();
        }
    }


    $(function(){
        $("#searchCar").click(function(){
            $.get("{{ url(config('backpack.base.route_prefix', 'admin') . '/car/get') }}/" + $("#car_no").val(), {}, function(data){
                if(flightPath != null) {
                    flightPath.setMap(null);
                }
                
                clearMarkers()
                map.setCenter({
                            lat: data.lat,
                            lng: data.lng
                        });
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(data.lat,data.lng),
                    map: map,
                    icon: "https://cdn4.iconfinder.com/data/icons/shopping-colorful-flat-long-shadow/136/Shopping_icons-31-24-48.png"
                });

                markers.push(marker);
            });
        });

        $("#menuBtn").click();
    });
    

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
        markers = [];
    }

    var polylines = [];
    var snappedCoordinates = [];

    // Snap a user-created polyline to roads and draw the snapped path
    function runSnapToRoad(path) {
        var pathValues = [];
        var pathArray = {snappedPoints:[]};
        //console.log(path);
        for (var i = 0; i < path.length; i++) {
            pathValues.push(path[i].lat+","+path[i].lng);
            if(i % 98 == 0 && i >= 98) {

                $.ajax({
                    url: 'https://roads.googleapis.com/v1/snapToRoads',                        // url位置
                    type: 'get',                   // post/get
                    async: false, //啟用同步請求
                    data: {
                        interpolate: true,
                        key: 'AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w',
                        path: pathValues.join('|')
                    },       // 輸入的資料
                    error: function (xhr) { },      // 錯誤後執行的函數
                    success: function (data) { 
                        console.log(data);
                        for (var i = 0; i < data.snappedPoints.length; i++) {
                            pathArray.snappedPoints.push(data.snappedPoints[i]);
                        }
                        
                        
                    }// 成功後要執行的函數
                });
                pathValues = [];
            }
        }
        if(path.length < 98) {
            $.ajax({
                url: 'https://roads.googleapis.com/v1/snapToRoads',                        // url位置
                type: 'get',                   // post/get
                async: false, //啟用同步請求
                data: {
                    interpolate: true,
                    key: 'AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w',
                    path: pathValues.join('|')
                },       // 輸入的資料
                error: function (xhr) { },      // 錯誤後執行的函數
                success: function (data) { 
                    for (var i = 0; i < data.snappedPoints.length; i++) {
                        pathArray.snappedPoints.push(data.snappedPoints[i]);
                    }
                    
                    
                }// 成功後要執行的函數
            });
            pathValues = [];
        }
        console.log(pathArray);
        processSnapToRoadResponse(pathArray);
        drawSnappedPolyline();
        getAndDrawSpeedLimits();
    }

    // Store snapped polyline returned by the snap-to-road service.
    function processSnapToRoadResponse(data) {
        snappedCoordinates = [];
        placeIdArray = [];
        for (var i = 0; i < data.snappedPoints.length; i++) {
            var latlng = new google.maps.LatLng(
                data.snappedPoints[i].location.latitude,
                data.snappedPoints[i].location.longitude);
            snappedCoordinates.push(latlng);
            placeIdArray.push(data.snappedPoints[i].placeId);
        }
    }

    // Draws the snapped polyline (after processing snap-to-road response).
    function drawSnappedPolyline() {
        if(snappedPolyline != null) {
            snappedPolyline.setMap(null);
        }
        snappedPolyline = new google.maps.Polyline({
            path: snappedCoordinates,
            strokeColor: 'black',
            strokeWeight: 6
        });

        snappedPolyline.setMap(map);
        polylines.push(snappedPolyline);
    }

    // Gets speed limits (for 100 segments at a time) and draws a polyline
    // color-coded by speed limit. Must be called after processing snap-to-road
    // response.
    function getAndDrawSpeedLimits() {
        for (var i = 0; i <= placeIdArray.length / 100; i++) {
            // Ensure that no query exceeds the max 100 placeID limit.
            var start = i * 100;
            var end = Math.min((i + 1) * 100 - 1, placeIdArray.length);

            //drawSpeedLimits(start, end);
        }
    }

    // Gets speed limits for a 100-segment path and draws a polyline color-coded by
    // speed limit. Must be called after processing snap-to-road response.
    function drawSpeedLimits(start, end) {
        var placeIdQuery = '';
        for (var i = start; i < end; i++) {
            placeIdQuery += '&placeId=' + placeIdArray[i];
        }

        processSpeedLimitResponse(placeIdArray, start);
    }

    // Draw a polyline segment (up to 100 road segments) color-coded by speed limit.
    function processSpeedLimitResponse(speedData, start) {
        var end = start + speedData.length;
        for (var i = 0; i < speedData.length - 1; i++) {
            //var speedLimit = speedData[i].speedLimit;
            //var color = getColorForSpeed(speedLimit);

            // Take two points for a single-segment polyline.
            var coords = snappedCoordinates.slice(start + i, start + i + 2);

            var snappedPolyline = new google.maps.Polyline({
            path: coords,
            strokeColor: 'blue',
            strokeWeight: 7
            });
            snappedPolyline.setMap(map);
            polylines.push(snappedPolyline);
        }
    }
</script>

</html>