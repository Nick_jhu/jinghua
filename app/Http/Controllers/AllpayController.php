<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;

use App\Models\MemberMgmtModel;
use App\Models\OrderMgmtModel;


class allPayController extends Controller
{
    public function allPayReply() {
        try {

			// $AL->encryptType = ECPay_EncryptType::ENC_MD5; // MD5
			Allpay::i()->EncryptType = 1; // SHA256
            $data = Allpay::i()->CheckOutFeedback();
            \Log::info($data);
			// 以付款結果訊息進行相對應的處理
			/*
			回傳的綠界科技的付款結果訊息如下:
			Array
			(
				[MerchantID] =>
				[MerchantTradeNo] =>
				[StoreID] =>
				[RtnCode] =>
				[RtnMsg] =>
				[TradeNo] =>
				[TradeAmt] =>
				[PaymentDate] =>
				[PaymentType] =>
				[PaymentTypeChargeFee] =>
				[TradeDate] =>
				[SimulatePaid] =>
				[CustomField1] =>
				[CustomField2] =>
				[CustomField3] =>
				[CustomField4] =>
				[CheckMacValue] =>
			)
			*/
			// 在網頁端回應 1|OK
			echo '1|OK';
		} catch(Exception $e) {
			echo '0|' . $e->getMessage();
		}
    }
}