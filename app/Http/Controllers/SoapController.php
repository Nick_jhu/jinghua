<?php

namespace App\Http\Controllers;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;
use SoapClient;

class SoapController
{
  /**
   * @var SoapWrapper
   */
  protected $soapWrapper;

  /**
   * SoapController constructor.
   *
   * @param SoapWrapper $soapWrapper
   */
  public function __construct(SoapWrapper $soapWrapper)
  {
    $this->soapWrapper = $soapWrapper;
  }

  /**
   * Use the SoapWrapper
   */
  public function show() 
  {

    $this->soapWrapper->add('Currency', function ($service) {
      $service
        ->wsdl('http://currencyconverter.kowabunga.net/converter.asmx?WSDL')
        ->trace(true)
        ->classmap([
          GetConversionAmount::class,
          GetConversionAmountResponse::class,
        ]);
    });

    // Without classmap
    $response = $this->soapWrapper->call('Currency.GetConversionAmount', [
      'CurrencyFrom' => 'USD', 
      'CurrencyTo'   => 'EUR', 
      'RateDate'     => '2014-06-05', 
      'Amount'       => '1000',
    ]);

    var_dump($response);

    // With classmap
    $response = $this->soapWrapper->call('Currency.GetConversionAmount', [
      new GetConversionAmount('USD', 'EUR', '2014-06-05', '1000')
    ]);

    var_dump($response);
    exit;
  }

  public function createOrder($order_arr) 
  {
    /*
    $order_arr = array();
    array_push($order_arr,array(
        "ORDERNO"=>"2018090096",
        "NAME"=>"KingWang",
        "TEL"=>"0932111111",
        "QTY"=>"2",
        "DATE1"=>"2018/05/01",
        "TERMINAL1"=>"T1", //T1 or T2
        "TIME1"=>"00:00",
        "DATE2"=>"2018/05/10",
        "TERMINAL2"=>"T1", //T1 or T2
        "TIME2"=>"12:00",
        "EMAIL"=>"kingwang317@gmail.com",
        "COUNTRY"=>"大陸",
        "REMARKS"=>"測試用",
    ));
    array_push($order_arr,array(
        "ORDERNO"=>"2018090198",
        "NAME"=>"KingWang",
        "TEL"=>"0932111111",
        "QTY"=>"2",
        "DATE1"=>"2018/05/01",
        "TERMINAL1"=>"T1", //T1 or T2
        "TIME1"=>"00:00",
        "DATE2"=>"2018/05/10",
        "TERMINAL2"=>"T1", //T1 or T2
        "TIME2"=>"12:00",
        "EMAIL"=>"kingwang317@gmail.com",
        "COUNTRY"=>"大陸",
        "REMARKS"=>"測試用",
    ));*/

    $client = new SoapClient(config('ac2000soap.ServiceURL'));
    $params = array(
        'sNO'            => config('ac2000soap.Account'), 
        'sPASS'          => config('ac2000soap.Password'), 
        'ORDERString'    => json_encode($order_arr)
    
    );
    $return_msg = "";
    $response = $client->__soapCall("IMPORTORDER", array($params));
    /*foreach(json_decode($response->IMPORTORDERResult,true) as $status){
        if($status["ERRORCode"] == 0){
            $return_msg.=$status["ORDERNO"]."錯誤(".$status["DESC"]."),";
        }
    }*/
    return json_decode($response->IMPORTORDERResult,true);

    //TODO InsertSerialNo
  

  }

  public function deleteOrder($order_arr) 
  {
    
    //$order_arr = array("2018090096","2018090198");
    

    $client = new SoapClient(config('ac2000soap.ServiceURL'));
    $params = array(
        'sNO' => config('ac2000soap.Account'), 
        'sPASS'   => config('ac2000soap.Password'), 
        'ORDERString'     => json_encode($order_arr)
    
    );
    $response = $client->__soapCall("DELETEOrder", array($params));
    
    return json_decode($response->DELETEOrderResult,true);
  

  }
  public function getOrderStatus($order_arr) 
  {
    /*
    $sample_arr = array("O180400129","O180400128");
    */
    $client = new SoapClient(config('ac2000soap.ServiceURL'));
    $params = array(
        'sNO' => config('ac2000soap.Account'), 
        'sPASS'   => config('ac2000soap.Password'), 
        'SEARCHString'     => json_encode($order_arr)
    );
    $response = $client->__soapCall("GetDATA2", array($params));

    return json_decode($response->GetDATA2Result,true);;
  
  }

  public function getStock() 
  {
    $client = new SoapClient(config('ac2000soap.ServiceURL'));
    $params = array(
        'sNO' => config('ac2000soap.Account'), 
        'sPASS'   => config('ac2000soap.Password'), 
        'STATUS'     => 1
    );
    $response = $client->__soapCall("GetSTOCK", array($params));
    var_dump($response);
  

  }

}