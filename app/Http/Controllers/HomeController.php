<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use Allpay;
use App\Models\CommonModel;
use App\Models\OrderMgmtModel;
use App\Models\Discount;
use App\Models\ModSnModel;

use Crypt;
use Shorty;
use MPG;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:member');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user = Auth::guard('member')->user();

        $areaData = DB::table('mod_cate')->select('id', 'name')->where('parent_id', 6)->orderBy('id', 'asc')->get();
        $bannerProd = array();

        $today      = new \DateTime();
        $str_date    = $today->format('Y-m-d H:i:s');
        $flashingProdData = DB::table('mod_product')
                            ->whereNotNull('d_percent')
                            ->where('is_added', 'Y')
                            ->where('action_date', '>=', $str_date)
                            ->get();
        

        $focusProdData = DB::table('mod_product')
                            ->where('is_focus', 'Y')
                            ->where('is_added', 'Y')
                            ->orderBy('sort', 'asc')
                            ->orderBy('created_at', 'desc')
                            // ->where(function($query) use ($str_date){
                            //     $query->whereRaw("action_date is null or action_date >= "."'".$str_date."'");
                            // })
                            ->get();

        $snData = DB::table('sys_sn')->count();
        $viewData = array(
            'viewName'  => 'index',
            'areaData'  => $areaData,
            'flashingProdData' => $flashingProdData,
            'focusProdData' => $focusProdData,
            'snData' => $snData,
        );
        
        return view('FrontEnd.homePage')->with($viewData);
    }

    public function about() {
        $viewData = array(
            'viewName'  => 'about'
        );
        
        return view('FrontEnd.about')->with($viewData);
    }

    public function chkAuth() {
        $isLogin = false;
        if(Auth::guard('member')->check()) {
            $isLogin = true;
        }

        return response()->json(['status' => $isLogin]);
    }

    public function allPayReply() {
        $ordNo = array();
        try {
            //$user = Auth::guard('member')->user();
            \Log::info('all pay reply');
            $allpay = new Allpay();
            $allpay->i()->Send['HashKey']    = config('ecpay.HashKey');
            $allpay->i()->Send['HashIV']     = config('ecpay.HashIV');
            $allpay->i()->Send['MerchantID'] = config('ecpay.MerchantID');
            $allpay->i()->Send['EncryptType'] = 1;
            $data = $allpay->i()->CheckOutFeedback();

            if($data['RtnCode'] == 1) {
                $ordModel = new OrderMgmtModel();
                $ordModel->proccessEcpCallBak($data);            
            }
		} catch(\Exception $e) {
            DB::table('mod_order')
                ->where('ord_no', $ordNo[0])
                ->update([
                    'status'   => 'I'
                ]);
            \Log::error($e->getMessage());
        }
        
        return redirect()->to('center')->with(['message' => '訂購完成']);
    }

    public function allPayReplySchedule() {
        $ordNo = array();
        try {
            //$user = Auth::guard('member')->user();
            $allpay = new Allpay();
            \Log::info('all pay reply');
            $allpay->i()->Send['HashKey']    = config('ecpay.HashKey');
            $allpay->i()->Send['HashIV']     = config('ecpay.HashIV');
            $allpay->i()->Send['MerchantID'] = config('ecpay.MerchantID');
            $allpay->i()->Send['EncryptType'] = 1;
            $data = $allpay->i()->CheckOutFeedback();

            if($data['RtnCode'] == 1) {
                $ordModel = new OrderMgmtModel();
                $ordModel->proccessEcpCallBak($data);
                echo '1|OK';
            }
            else {
                echo '0|ERROR';
            }
		} catch(\Exception $e) {
            DB::table('mod_order')
                ->where('ord_no', $ordNo[0])
                ->update([
                    'status'   => 'J'
                ]);
            echo '0|'.$e->getMessage();
            \Log::error($e->getMessage());
        }
    }

    public function ecpCallback(Request $request) {
        \Log::info('ec pay call back start');
        \Log::info($request->all());

        return;
    }

    public function orderQrCode($ordNo=null) {
        try {
            $ordNo = Crypt::decrypt($ordNo);
            if(isset($ordNo)) {
                $ordData = DB::table('mod_order')->where('ord_no', $ordNo)->first();
    
                if(isset($ordData)) {
                    $rentData = DB::table('mod_order_rent_detail')->where('ord_id', $ordData->id)->get();
    
                    return view('FrontEnd.orderQrCode')->with(['rentData' => $rentData, 'ordData' => $ordData]);
                }
            }
        } catch (\Exception $e) {
            return view('FrontEnd.orderQrCode');
        }

        return view('FrontEnd.orderQrCode');
    }

    public function genShorty() {
        $url = "http://google.com.tw";
        dd(Shorty::shorten($url));
    }

    public function spgCallBack() {
        $tradeInfo = MPG::parse(request()->TradeInfo);

        //dd($tradeInfo);
        $result = '';
        try {
            \Log::info('spg reply');

            $orderMgmtModel = new OrderMgmtModel();

            $result = $orderMgmtModel->spgCallBack($tradeInfo);

		} catch(\Exception $e) {
            \Log::error($e->getMessage());
            return redirect()->to('search')->with(['message' => '訂購發生問題，請聯絡客服']);
        }

        if($result != 'success') {
            return redirect()->to('search')->with(['message' => '訂購發生問題，請聯絡客服']);
        }
        
        return redirect()->to('search')->with(['message' => '訂購完成']);
    }

    public function spgCallBackSchedule() {
        \Log::info('spg Schedule reply');
        try {
            $tradeInfo = MPG::parse(request()->TradeInfo);
            $result = '';
            

            $orderMgmtModel = new OrderMgmtModel();

            $result = $orderMgmtModel->spgCallBack($tradeInfo);

            \Log::info('SPG Pay '.$result);

		} catch(\Exception $e) {
            \Log::error('SPG Pay fail');
            \Log::error("The exception was created on line: " . $e->getLine());
            \Log::error($e->getMessage());
        }

        
    }

    public function spgCustomerCallBack() {
        $result = '';
        try {
            \Log::info('spg customer reply');

            $tradeInfo = MPG::parse(request()->TradeInfo);

            $orderMgmtModel = new OrderMgmtModel();

            $result = $orderMgmtModel->spgCallBack($tradeInfo);

		} catch(\Exception $e) {
            \Log::error($e->getMessage());

            return redirect()->to('center')->with(['message' => '訂購發生問題，請聯絡客服']);
        }

        if($result != 'success') {
            return redirect()->to('center')->with(['message' => '訂購發生問題，請聯絡客服']);
        }
        
        return redirect()->to('center')->with(['message' => '訂購完成']);
    }

    public function trackingReply(Request $request) {
        \Log::info('tracking reply');
        \Log::info($request->all());
        $res = $request->all();

        if(isset($res->RtnCode) && ($res->RtnCode == '2073' || $res->RtnCode == '3018' || $res->RtnCode == '2063')) {
            $CVSPaymentNo = $res->CVSPaymentNo;

            DB::table('mod_order')->where('CVSPaymentNo', $CVSPaymentNo)->update(['status' => 'D']);
            \Log::info('tracking update');
        }
        else if(isset($res['RtnCode']) && ($res['RtnCode'] == '2073' || $res['RtnCode'] == '3018' || $res['RtnCode'] == '2063')) {
            $CVSPaymentNo = $res['CVSPaymentNo'];

            DB::table('mod_order')->where('CVSPaymentNo', $CVSPaymentNo)->update(['status' => 'D']);
            \Log::info('tracking update');
        }

        echo '1|OK';
        
    }
}