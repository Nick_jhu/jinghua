<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\NewsCrudRequest as StoreRequest;
use App\Http\Requests\NewsCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class UnboxingCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\News');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modUnboxing');
        $this->crud->setEntityNameStrings('Unboxing', 'Unboxing');

        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => "簡述",
                'type'  => 'text'
            ],
            [
                'name' => 'link',
                'label' => "連結",
                'type'  => 'text'
            ],
            [
                'name' => 'created_by',
                'label' => "建立者",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addField([
            'name' => 'title',
            'label' => '簡述',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([  // Select
            'label' => "類別",
            'type' => 'select_from_array',
            'name' => 'cate_id', // the db column for the foreign key
            'attribute' => 'name', // foreign key attribute that is shown to user
            'options' => DB::table('mod_cate')->where('id', 71)->pluck('descp', 'id'),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'link',
            'label' => '連結',
            'type' => 'text'
        ]);
        
        $this->crud->addField([   // Upload
            'name' => 'image',
            'label' => 'Image',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'public' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
        ]);
        

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        $this->crud->addClause('where', 'cate_id', 71); 

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

}