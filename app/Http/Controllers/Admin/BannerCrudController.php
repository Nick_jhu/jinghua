<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BannerCrudRequest as StoreRequest;
use App\Http\Requests\BannerCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class BannerCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Banner');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modBanner');
        $this->crud->setEntityNameStrings('Banner', 'Banners');

        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => "標題",
                'type'  => 'text'
            ],
            [
                'name' => 'descp',
                'label' => "摘要",
                'type'  => 'text'
            ],
            [
                'name' => 'banner_order',
                'label' => "順序",
                'type'  => 'text'
            ],
            [
                'name' => 'created_by',
                'label' => "建立者",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addField([
            'name' => 'title',
            'label' => '標題',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([  // Select
            'label' => "類別",
            'type' => 'select_from_array',
            'name' => 'cate_id', // the db column for the foreign key
            'attribute' => 'name', // foreign key attribute that is shown to user
            'options' => DB::table('mod_cate')->where('parent_id', 72)->pluck('descp', 'id'),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'banner_order',
            'label' => '順序',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([  // Select
            'label' => "文字對齊",
            'type' => 'select_from_array',
            'name' => 'position', // the db column for the foreign key
            'attribute' => 'name', // foreign key attribute that is shown to user
            'options' => ['left' => '靠左', 'center' => '置中', 'right' => '靠右'],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'link',
            'label' => '連結',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-8',
            ],
        ]);

        $this->crud->addField([
            'name' => 'btn_text',
            'label' => '按鈕文字',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-8',
            ],
        ]);

        $this->crud->addField([
            'name' => 'descp',
            'label' => '摘要',
            'type' => 'ckeditor',
            // optional:
            'extra_plugins' => ['oembed', 'widget', 'justify']
        ]);

        $this->crud->addField([   // Upload
            'name' => 'image',
            'label' => '圖片上傳',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'public' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
        ]);

        $this->crud->addField([   // URL
            'name' => 'video',
            'label' => '影片url',
            'type' => 'url'
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "廣告商品",
            'type'      => 'select2_from_array',
            'name'      => 'prod_id', // the method that defines the relationship in your Model
            'options'     => DB::table('mod_product')->pluck('title', 'id'), // foreign key model
            'allows_multiple' => true,
            // 'select_all' => true, // show Select All and Clear buttons?
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }

}