<?php

namespace App\Http\Controllers\Admin;

use Allpay;
use App\Http\Requests\OrderMgmtCrudRequest as StoreRequest;
use App\Http\Requests\OrderMgmtCrudRequest as UpdateRequest;
use App\Models\BaseModel;
use App\Models\CommonModel;
use App\Models\OrderDetailModel;
use App\Models\OrderMgmtModel;
use App\Models\OrderRentDetailModel;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;

class OrderMgmtCrudController extends CrudController
{
    public function setup()
    {
        $user = Auth::user();
        $this->crud->setModel("App\Models\OrderMgmtModel");
        $this->crud->setEntityNameStrings('訂單總覽', '訂單總覽');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/OrderMgmt');

        $this->crud->setColumns(['name']);

        $this->crud->setCreateView('OrderMgmt.edit');
        $this->crud->setEditView('OrderMgmt.edit');
        $this->crud->setListView('OrderMgmt.index');
        $this->crud->enableAjaxTable();

        $array_of_fields_definition_arrays = array(
            [
                'name' => 'ord_no',
                'type' => 'text',
            ],
            [
                'name' => 'status',
                'type' => 'select',
            ],
            [
                'name' => 'pay_way',
                'type' => 'select',
                'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'PAYWAY')->where('c_key', $user->c_key)->get(),
            ],
            [
                'name' => 'pick_way',
                'type' => 'select',
                'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'SHIPWAY')->where('c_key', $user->c_key)->get(),
            ],
            [
                'name' => 'number_of_periods',
                'type' => 'select2_multiple',
                'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'PERIODS')->where('c_key', $user->c_key)->get()
            ],
            [
                'name' => 'shipper_cd',
                'type' => 'select',
                'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'SHIPPER')->where('c_key', $user->c_key)->get(),
            ],
            [
                'name' => 'ord_nm',
                'type' => 'text',
            ],
            [
                'name' => 'dlv_nm',
                'type' => 'text',
            ],
            [
                'name' => 'dlv_phone',
                'type' => 'text',
            ],
            [
                'name' => 'dlv_zip',
                'type' => 'text',
            ],
            [
                'name' => 'dlv_city',
                'type' => 'text',
            ],
            [
                'name' => 'dlv_area',
                'type' => 'text',
            ],
            [
                'name' => 'dlv_addr',
                'type' => 'text',
            ],
            [
                'name' => 'r_code',
                'type' => 'text',
            ],
            [
                'name' => 's_code',
                'type' => 'text',
            ],
            [
                'name' => 'o_price',
                'type' => 'text',
            ],
            [
                'name' => 'd_price',
                'type' => 'text',
            ],
            [
                'name' => 'ship_fee',
                'type' => 'text',
            ],
            [
                'name' => 'pay_no',
                'type' => 'text',
            ],
            [
                'name' => 'delay_pay_no',
                'type' => 'text',
            ],
            [
                'name' => 'ship_no',
                'type' => 'text',
            ],
            [
                'name' => 'g_key',
                'type' => 'text',
            ],
            [
                'name' => 'c_key',
                'type' => 'text',
            ],
            [
                'name' => 's_key',
                'type' => 'text',
            ],
            [
                'name' => 'd_key',
                'type' => 'text',
            ],
            [
                'name' => 'created_by',
                'type' => 'text',
            ],
            [
                'name' => 'updated_by',
                'type' => 'text',
            ],
            [
                'name' => 'remark',
                'type' => 'textarea',
            ],
            [
                'name' => 'cust_remark',
                'type' => 'textarea',
            ],
            [
                'name' => 'sendprod_remark',
                'type' => 'textarea',
            ],
            [
                'name' => 'store_id',
                'type' => 'text',
            ],
            [
                'name' => 'store_type',
                'type' => 'text',
            ],
            [
                'name' => 'store_name',
                'type' => 'text',
            ],
            [
                'name' => 'store_addr',
                'type' => 'text',
            ],
            [
                'name' => 'store_phone',
                'type' => 'text',
            ],
            [
                'name' => 'ShipmentNo',
                'type' => 'text',
            ],
            [
                'name' => 'CVSPaymentNo',
                'type' => 'text',
            ],
            [
                'name' => 'CVSValidationNo',
                'type' => 'text',
            ],
            [
                'name' => 'email',
                'type' => 'text',
            ],
        );

        $this->crud->addFields($array_of_fields_definition_arrays);
    }

    public function index()
    {
        $user = Auth::user();

        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (!$this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        $BaseModel = new BaseModel();
        $obj = null;

        $layout = $BaseModel->getLayout('modOrderView');
        $obj = json_decode($layout);

        $fieldData = $BaseModel->getColumnInfo("mod_order_view");
        if ($obj != null) {
            array_push($fieldData, $obj);
        }

        $colModel = array('mod_order_view' => $fieldData);

        $this->crud->colModel = $fieldData;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    public function getData($table = null, $id)
    {
        $data = DB::table('mod_order')->where('id', $id)->first();

        return response()->json($data);
    }

    public function get($ord_id = null)
    {
        $prodDetail = [];
        if ($ord_id != null) {
            $this_query = DB::table('mod_order_detail');
            $this_query->where('ord_id', $ord_id);
            $prodDetail = $this_query->get();

        }

        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg" => "error", "errorLog" => $validator->messages()];
        } else {
            $ordDetail = new OrderDetailModel;
            foreach ($request->all() as $key => $val) {
                $ordDetail[$key] = request($key);
            }
            //dd($ordDetail);
            $ordDetail->save();
        }

        return ["msg" => "success", "data" => $ordDetail->where('id', $ordDetail->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg" => "error", "errorLog" => $validator->messages()];
        } else {
            $ordDetail = OrderDetailModel::find($request->id);
            foreach ($request->all() as $key => $val) {
                $ordDetail[$key] = request($key);
            }
            $ordDetail->save();
        }

        return ["msg" => "success", "data" => $ordDetail->where('id', $request->id)->get()];
    }

    public function detailDel($id)
    {
        $ordDetail = OrderDetailModel::find($id);

        $ordDetail->delete();

        return ["msg" => "success"];
    }

    public function getRent($ord_id = null)
    {
        $prodDetail = [];
        if ($ord_id != null) {
            $this_query = DB::table('mod_order_rent_detail');
            $this_query->where('ord_id', $ord_id);
            $prodDetail = $this_query->get();

        }

        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function detailRentStore(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg" => "error", "errorLog" => $validator->messages()];
        } else {
            $ordDetail = new OrderRentDetailModel;
            foreach ($request->all() as $key => $val) {
                $ordDetail[$key] = request($key);
            }
            //dd($ordDetail);
            $ordDetail->save();

            $detailData = $ordDetail->where('id', $ordDetail->id)->first();
            $orderData = DB::table('mod_order')
                            ->where('id', $detailData->ord_id)
                            ->update(['pick_up_date' => $detailData->f_day, 'return_date' => $detailData->e_day]);
            // $sample_arr = array();

            // if (isset($orderData)) {
            //     array_push($sample_arr, array(
            //         "ORDERNO" => $orderData->ord_no,
            //         "NAME" => $orderData->dlv_nm,
            //         "TEL" => $orderData->dlv_phone,
            //         "QTY" => $ordDetail->num,
            //         "DATE1" => substr($ordDetail->f_day, 0, 10),
            //         "TERMINAL1" => (isset($orderData->departure_terminal) || $orderData->departure_terminal != "") ? $orderData->departure_terminal : "", //T1 or T2
            //         "TIME1" => $orderData->pick_time,
            //         "DATE2" => substr($ordDetail->e_day, 0, 10),
            //         "TERMINAL2" => (isset($orderData->return_terminal) || $orderData->return_terminal != "") ? $orderData->return_terminal : "", //T1 or T2
            //         "TIME2" => $orderData->return_time,
            //         "EMAIL" => $orderData->email,
            //         "COUNTRY" => "",
            //         "REMARKS" => "訂單號：" . $orderData->ord_no,
            //     ));

            //     $result = app('App\Http\Controllers\SoapController')->createOrder($sample_arr);
            // }

        }

        return ["msg" => "success", "data" => $ordDetail->where('id', $ordDetail->id)->get()];
    }

    public function detailRentUpdate(Request $request)
    {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg" => "error", "errorLog" => $validator->messages()];
        } else {
            $ordDetail = OrderRentDetailModel::find($request->id);
            foreach ($request->all() as $key => $val) {
                $ordDetail[$key] = request($key);
            }
            $ordDetail->save();

            //$orderData = DB::table('mod_order')->where('id', $ordDetail->ord_id)->first();
            DB::table('mod_order')->where('id', $ordDetail->ord_id)->update(['pick_up_date' => $ordDetail->f_day, 'return_date' => $ordDetail->e_day]);
            // $sample_arr = array();

            // if (isset($orderData)) {
            //     if (isset($request->f_day) || isset($request->e_day)) {
            //         $result = app('App\Http\Controllers\SoapController')->deleteOrder(array($orderData->ord_no));
            //         if ($result[0]['ERRORCode'] == 0 || $result[0]['ERRORCode'] == 400) {
            //             array_push($sample_arr, array(
            //                 "ORDERNO" => $orderData->ord_no,
            //                 "NAME" => $orderData->dlv_nm,
            //                 "TEL" => $orderData->dlv_phone,
            //                 "QTY" => $ordDetail->num,
            //                 "DATE1" => substr($ordDetail->f_day, 0, 10),
            //                 "TERMINAL1" => (isset($orderData->departure_terminal) || $orderData->departure_terminal != "") ? $orderData->departure_terminal : "", //T1 or T2
            //                 "TIME1" => $orderData->pick_time,
            //                 "DATE2" => substr($ordDetail->e_day, 0, 10),
            //                 "TERMINAL2" => (isset($orderData->return_terminal) || $orderData->return_terminal != "") ? $orderData->return_terminal : "", //T1 or T2
            //                 "TIME2" => $orderData->return_time,
            //                 "EMAIL" => $orderData->email,
            //                 "COUNTRY" => "",
            //                 "REMARKS" => "訂單號：" . $orderData->ord_no,
            //             ));

            //             $result = app('App\Http\Controllers\SoapController')->createOrder($sample_arr);
            //         }

            //         DB::table('mod_order')->where('id', $ordDetail->ord_id)->update(['pick_up_date' => $orderData->f_day, 'return_date' => $orderData->e_day]);
            //     }
            // }
        }

        return ["msg" => "success", "data" => $ordDetail->where('id', $request->id)->get()];
    }

    public function detailRentDel($id)
    {
        $ordDetail = OrderRentDetailModel::find($id);

        $ordDetail->delete();

        return ["msg" => "success"];
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "", json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit') . ' ' . $this->crud->entity_name;

        $this->data['id'] = $id;
        //dd($this->data['entry']);

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        $user = Auth::user();
        $type = $request->cust_type;
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);
        } catch (\Exception $e) {

            \Log::error($e);

            return ["msg" => "error", "errorLog" => $e->getMessage()];
        }

        return ["msg" => "success", "response" => $response, "lastId" => $this->data['entry']->getKey()];
    }

    public function update(UpdateRequest $request)
    {
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        $oldData = OrderMgmtModel::find($request->id);
        $prodData = array();
        try {
            $response = parent::updateCrud($request);
            $orderData = OrderMgmtModel::find($request->id);

            if ($request->status == 'E') {
                $orderMgmtModel = new OrderMgmtModel();
                $orderMgmtModel->sendWifiReturnMail($orderData);
            }
        } catch (\Exception $e) {
            return ["msg" => "error", "errorLog" => $e->getMessage()];
        }

        return ["msg" => "success", "response" => $response, "data" => $orderData];
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $ordData = OrderMgmtModel::find($id);

        if (isset($ordData)) {
            if ($ordData->pick_way == "A") {
                $cnt = DB::table('mod_order_rent_detail')->where('ord_id', $id)->count();
                if ($cnt > 0) {
                    $sample_arr = [$ordData->ord_no];
                    $result = app('App\Http\Controllers\SoapController')->deleteOrder($sample_arr);
                }

            }

        }

        DB::table('mod_order_rent_detail')->where('ord_id', $id)->delete();
        DB::table('mod_order_detail')->where('ord_id', $id)->delete();

        return $this->crud->delete($id);
    }

    public function multiDel()
    {
        $ids = request('ids');
        $sample_arr = array();
        if (count($ids) > 0) {
            for ($i = 0; $i < count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);

                if (isset($order)) {
                    if ($order->pick_way == "A") {
                        $cnt = DB::table('mod_order_rent_detail')->where('ord_id', $id)->count();
                        if ($cnt > 0) {
                            array_push($sample_arr, $order->ord_no);
                        }
                    }
                }

                $order->delete();
            }

            if (count($sample_arr) > 0) {
                //$result = app('App\Http\Controllers\SoapController')->deleteOrder($sample_arr);
            }

        }

        return response()->json(array('msg' => 'success'));
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function confirmOrderCancel()
    {
        $ids = request('ids');
        for ($i = 0; $i < count($ids); $i++) {
            $id = $ids[$i];
            $order = OrderMgmtModel::find($id);

            if(isset($order) && $order->status != 'G') {
                $model = new OrderMgmtModel();
                $model->processStockReturn($order);
            }
            
            $order->status = 'G';
            $order->save();
        }
        

        return response()->json(['message' => 'success']);
    }

    public function createdInv()
    {
        $ids = request('ids');
        for ($i = 0; $i < count($ids); $i++) {
            $id = $ids[$i];
            $order = OrderMgmtModel::find($id);

            if(isset($order)) {
                $model = new OrderMgmtModel();
                $result = $model->createInvoice($order);

                if(isset($result)) {
                    return response()->json(['message' => 'error', "log" => $result]);
                }
            }
        }
        

        return response()->json(['message' => 'success']);
    }

    public function exportDetail()
    {
        $ids = request('ids');

        $now = date('Ymd');
        Excel::create('訂單-' . $now, function ($excel) use ($ids, $now) {
            $idsArray = explode(';', $ids);

            $excel->sheet('訂單-' . $now, function ($sheet) use ($idsArray) {
                $user = Auth::user();
                $orderData = DB::table('mod_order')
                    ->whereIn('id', $idsArray)
                    ->get();

                $sheet->row(1, array(
                    '訂單編號', '姓名', '電話', '數量', '領機日期', 'Terminal(Pick-up)', '時間(pick-up)', '還機日期(Return)', 'Terminal(Return)', '時間(return)', '國家', '電子信箱', '備註(機號)',
                ));

                foreach ($orderData as $key => $row) {
                    $orderDetail = DB::table('mod_order_rent_detail')->where('ord_id', $row->id)->get();
                    if (isset($orderDetail)) {
                        foreach ($orderDetail as $k => $val) {
                            $sheet->row(($key + 2) + $k, array(
                                $row->ord_no, $row->dlv_nm, $row->dlv_phone, $val->num, substr($val->f_day, 0, 10), $row->departure_terminal, '00:00', substr($val->e_day, 0, 10), $row->return_terminal, '00:00', '', $row->email, "訂單號：" . $row->ord_no,
                            ));
                        }

                    }

                }

            });

        })->export('xls');

    }

    public function shipping()
    {
        ignore_user_abort(1); //忽略使用者中斷
        set_time_limit(0); //最大執行時間，0為無線

        $ids = request('ids');
        if (count($ids) > 0) {
            $this->updateOrderStatus($ids, 'C');
        }

        return response()->json(array('msg' => 'success'));
    }

    public function vendorUpdateShipmentNo() {
        $ids = request('ids');
        $shipper = request('shipper');
        $shipNo = request('shipNo');
        if (count($ids) > 0) {
            //$orderMgmtModel = new OrderMgmtModel();
            for ($i = 0; $i < count($ids); $i++) {
                
                $order = OrderMgmtModel::find($ids[$i]);
                $order->shipper_cd = $shipper;
                $order->CVSPaymentNo = $shipNo;
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function sendToZ() {
        $ids = request('ids');
        if (count($ids) > 0) {
            //$orderMgmtModel = new OrderMgmtModel();
            for ($i = 0; $i < count($ids); $i++) {
                
                $order = OrderMgmtModel::find($ids[$i]);
                $order->ship_flag = 'F';
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function prodShipping()
    {
        ignore_user_abort(1); //忽略使用者中斷
        set_time_limit(0); //最大執行時間，0為無線

        $ids = request('ids');
        if (count($ids) > 0) {
            $this->updateOrderStatus($ids, 'I');
            $orderMgmtModel = new OrderMgmtModel();
            for ($i = 0; $i < count($ids); $i++) {
                $orderMgmtModel->shippingNotice($ids[$i]);

                $order = OrderMgmtModel::find($ids[$i]);
                $order->ship_date = \Carbon::now();
                $order->save();
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function returnConfirm()
    {
        $ids = request('ids');
        if (count($ids) > 0) {
            $this->updateOrderStatus($ids, 'H');
        }

        return response()->json(array('msg' => 'success'));
    }

    public function paymentConfirm()
    {
        $sn_no = array();
        $ids = request('ids');
        $id = "";
        \Log::info($ids);
        if (count($ids) > 0) {
            for($j = 0 ; $j < sizeof($ids);$j++) {
                $orddata = DB::table('mod_order')->where('id', $ids[$j])->first();
                $id = $ids[$j];
                $sn_no = array();
                    for($i = 0;$i < $orddata->num ;$i++){
                        $sn_no[$i]= $this->newsn_no();
                        DB::table('sys_sn')->insert([
                            'ord_no' => $orddata->ord_no,
                            'ord_nm' => $orddata->dlv_nm,
                            'ord_phone' =>$orddata->dlv_phone,
                            'prod_nm' =>$orddata->normal_detail_descp,
                            'sn_no' => $sn_no[$i],
                            'created_at' => \Carbon::now(),
                        ]);
                    }
                \Log::info($i);
                $this->updateOrderStatus_pay($id, 'B',$sn_no);
                $sn_no = array();
            }
        }

        return response()->json(array('msg' => 'success'));
    }
    private function newsn_no($length=8)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function updateOrderStatus($ids, $status)
    {
        DB::table('mod_order')->whereIn('id', $ids)->update([
            'status' => $status,
            'updated_at' => \Carbon::now(),
        ]);
        return;
    }
    private function updateOrderStatus_pay($ids, $status,$sn_no)
    {
        DB::table('mod_order')->where('id', $ids)->update([
            'remark' => json_encode($sn_no),
            'status' => $status,
            'updated_at' => \Carbon::now(),
        ]);
        return;
    }

    public function sendQrCode()
    {
        $ids = request('ids');
        $ordMgmt = new OrderMgmtModel();
        if (count($ids) > 0) {
            $order = DB::table('mod_order')->whereIn('id', $ids)->get();

            foreach ($order as $row) {
                $ordMgmt->sendQrCode($row->ord_no, $row->dlv_phone);
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function cancelPay($id)
    {
        $ordMgmt = new OrderMgmtModel();

        try {
            //$msg = $ordMgmt->cancelPay($id);
            $msg = $ordMgmt->tpaPayCancelPay($id);

            if ($msg == "SUCCESS") {
                $this->updateOrderStatus(array($id), 'H');
            } else {
                return response()->json(array('Status' => 'ERROR', 'msg' => $msg));
            }
        } catch (\Exception $e) {
            //dd($e);
            return response()->json(array('Status' => 'ERROR', 'msg' => $e->getMessage().'-->'.$e->getLine()));
        }

        return response()->json($msg);
    }

    public function PrintC2CBill($id)
    {

        $orderMgmtModel = new OrderMgmtModel();

        $orderData = $orderMgmtModel::find($id);

        $allPay = new Allpay();
        $AL = $allPay->l();
        $AL->HashKey = config('allpay.HashKey');
        $AL->HashIV = config('allpay.HashIV');

        if (isset($orderData)) {
            if ($orderData->pick_way == 'UNIMARTC2C') {
                $AL->Send = array(
                    'MerchantID' => config('allpay.MerchantID'),
                    'AllPayLogisticsID' => $orderData->AllPayLogisticsID,
                    'CVSPaymentNo' => $orderData->CVSPaymentNo,
                    'CVSValidationNo' => $orderData->CVSValidationNo,
                );

                $result = $AL->PrintUnimartC2CBill();

                $orderData->is_print = 'Y';
                $orderData->save();

                echo $result;
            } else if ($orderData->pick_way == 'FAMIC2C') {

                $AL->Send = array(
                    'MerchantID' => config('allpay.MerchantID'),
                    'AllPayLogisticsID' => $orderData->AllPayLogisticsID,
                    'CVSPaymentNo' => $orderData->CVSPaymentNo,
                );

                $result = $AL->PrintFamilyC2CBill();

                $orderData->is_print = 'Y';
                $orderData->save();

                echo $result;

            } else if ($orderData->pick_way == 'HILIFEC2C') {
                $AL->Send = array(
                    'MerchantID' => config('allpay.MerchantID'),
                    'AllPayLogisticsID' => $orderData->AllPayLogisticsID,
                    'CVSPaymentNo' => $orderData->CVSPaymentNo,
                );

                $result = $AL->PrintHiLifeC2CBill();

                $orderData->is_print = 'Y';
                $orderData->save();

                echo $result;
            }
        }
    }

    public function cancelWifi()
    {
        $ids = request('ids');
        $sample_arr = array();
        if (count($ids) > 0) {
            for ($i = 0; $i < count($ids); $i++) {
                $order = OrderMgmtModel::find($ids[$i]);
                $order->status = 'G';

                if (isset($order)) {
                    if ($order->pick_way == "A" || $order->return_way == "A") {
                        $cnt = DB::table('mod_order_rent_detail')->where('ord_id', $id)->count();
                        if ($cnt > 0) {
                            array_push($sample_arr, $order->ord_no);
                        }
                    }
                }

                $order->save();
            }

            if (count($sample_arr) > 0) {
                $result = app('App\Http\Controllers\SoapController')->deleteOrder($sample_arr);
            }
        }

        return response()->json(array('msg' => 'success'));
    }
    public function exportPelican(Request $request) {

        $now = date('YmdHis');
        $ids = request('ids');
        $idArray = explode(',', $ids);
        Excel::create('宅配通-'.$now, function($excel) use($ids,$idArray) {
            $excel->sheet("商品", function($sheet) use($ids,$idArray) {
                $prod = DB::select( 
                    DB::raw("SELECT
                    @rowid:=@rowid+1 as rowid,
                    mod_order.dlv_nm,
                    mod_order.dlv_phone,
                    mod_order.dlv_zip,
                    CONCAT(ifnull(mod_order.dlv_city,''),ifnull(mod_order.dlv_area,''),ifnull(mod_order.dlv_addr,'')) as dlv_addr,
                    
                    CASE 
                    WHEN ifnull((select sum(num) from mod_order_detail where mod_order.id=ord_id),0)>0 and ifnull((select sum(num) from mod_order_rent_detail where mod_order.id=ord_id),0)>0
                    then ''
                    when ifnull((select sum(num) from mod_order_detail where mod_order.id=ord_id),0)>0
                    then (select sum(num) from beautyz.mod_order_detail where mod_order.id=ord_id)
                    else (select sum(num) from beautyz.mod_order_rent_detail where mod_order.id=ord_id)
                    END  as NUM,
                    
                    CASE WHEN length(normal_detail_descp) >0 and length(detail_descp) >0
                    THEN ''
                    when length(normal_detail_descp)>12
                    THEN CONCAT( SUBSTRING(mod_order.normal_detail_descp,1,12),'...')
                    else CONCAT( SUBSTRING(mod_order.detail_descp,1,12),'...')
                    END  as normal_detail_descp,
                    CASE WHEN length(remark) >15
                    THEN CONCAT( SUBSTRING(mod_order.remark,1,12),'...')
                    else remark 
                    END AS remark,
                    mod_order.status,
                    mod_order.ord_no
                    FROM mod_order ,
                    (SELECT @rowid:=0) as init
                    WHERE mod_order.status='B'and mod_order.pick_way='B'and mod_order.id in($ids)"
                    ));
                
                $sheet->loadView('exports.Pelican')->with('prod', $prod);
            });      
            DB::table('mod_order')
            ->where('mod_order.status', 'B')
            ->where('mod_order.pick_way', 'B')
            ->whereIn('mod_order.id', $idArray)
            ->update([
                'mod_order.is_print'       => "Y",
            ]);          
        })->export('xls');
    }

    public function uploadExcel() {
        $user = Auth::user();
        setlocale(LC_ALL, 'zh_TW');
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            if (($handle = fopen($path, "r")) !== FALSE) {
                $flag = true;
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if($flag) { $flag = false; continue; }
                    $updateData = [
                        'ord_no'                => mb_convert_encoding(str_replace("=","",str_replace("\"","",$data[3])),"UTF-8"),
                        'CVSPaymentNo'          => mb_convert_encoding(str_replace("=","",str_replace("\"","",$data[2])),"UTF-8"),
                        'updated_by'            =>$user->email,
                        'updated_at'            => \Carbon::now(),
                    ];
                    $orderData = DB::table('mod_order')->where('ord_no',$updateData['ord_no'])->count();
                    if($orderData>0){
                        DB::table('mod_order')
                        ->where('ord_no', $updateData['ord_no'])
                        ->update(['CVSPaymentNo' => $updateData['CVSPaymentNo']]);
                    }        
                }
                fclose($handle);
            }    
        }
        return ["msg"=>"success"];
    }

    public function CreateLogisticsList(Request $request){
        $prodName = '';
        $ids = request('ids');
        $allPay = new Allpay();
        for($i = 0; $i < count($ids); $i++) {
            $order = new OrderMgmtModel();       
            $orderData = $order->where('id', $ids[$i])->first();
            $orderDetail = DB::table('mod_order_detail')->where('ord_id',$orderData->id)->get();
            $orderRentDetail = DB::table('mod_order_rent_detail')->where('ord_id', $orderData->id)->get();
            
            foreach($orderDetail as $row) {
                $prodName .= $row->prod_nm. ', ';
            }

            foreach($orderRentDetail as $row) {
                $prodName .= $row->prod_nm. ', ';
            }

            if(mb_strlen($prodName, 'UTF-8') > 20) {
                $prodName = mb_substr($prodName, 0, 20, 'UTF-8');
                $prodName = $prodName.'...';
            }

            $prodName = str_replace('^', '', $prodName);
            $prodName = str_replace('\'', '', $prodName);
            $prodName = str_replace('\`', '', $prodName);
            $prodName = str_replace('!', '', $prodName);
            $prodName = str_replace('@', '', $prodName);
            $prodName = str_replace('#', '', $prodName);
            $prodName = str_replace('%', '', $prodName);
            $prodName = str_replace('&', '', $prodName);
            $prodName = str_replace('*', '', $prodName);
            $prodName = str_replace('+', '', $prodName);
            $prodName = str_replace('\\', '', $prodName);
            $prodName = str_replace('\"', '', $prodName);
            $prodName = str_replace('<', '', $prodName);
            $prodName = str_replace('>', '', $prodName);
            $prodName = str_replace('|', '', $prodName);
            $prodName = str_replace('_', '', $prodName);
            $prodName = str_replace('[', '', $prodName);
            $prodName = str_replace(']', '', $prodName);
            $prodName = str_replace('-', '', $prodName);
            $prodName = str_replace('(', '', $prodName);
            $prodName = str_replace(')', '', $prodName);
            $prodName = str_replace(' ', '', $prodName);
            try {
                
                $AL = $allPay->l();
                $AL->HashKey = config('allpay.HashKey');
                $AL->HashIV = config('allpay.HashIV');
                $AL->Send = array(
                        'MerchantID' => config('allpay.MerchantID'),
                        'MerchantTradeNo' => $orderData->ord_no,
                        'MerchantTradeDate' => date('Y/m/d H:i:s'),
                        'LogisticsType' => 'CVS',
                        'LogisticsSubType' => $orderData->pick_way,
                        'GoodsAmount' => (int)$orderData->d_price,
                        'CollectionAmount' => (int)$orderData->d_price,
                        'IsCollection' => 'N',    //是否代收貨款
                        'GoodsName' => $prodName,
                        'SenderName' => '美z人生',
                        'SenderPhone' => '0973186006',
                        'SenderCellPhone' => '0973186006',
                        'ReceiverName' => $orderData->dlv_nm,
                        'ReceiverPhone' => $orderData->dlv_phone,
                        'ReceiverCellPhone' => $orderData->dlv_phone,
                        'ReceiverEmail' => $orderData->email,
                        'TradeDesc' => '交易敘述',
                        'ServerReplyURL' => url('trackingReply'),        //物流狀態回覆網址
                        'LogisticsC2CReplyURL' => url('logistics_order_C2C_reply'),    //到付店若有異動訊息回覆網址
                        'Remark' => $orderData->remark,
                        'PlatformID' => '',
                    );
                        $AL->SendExtend = array(
                        'ReceiverStoreID' => $orderData->store_id,     //到付店id
                        'ReturnStoreID' => $orderData->store_id       //回退店id,一般與寄件店id同
                );
                    $Result = $AL->BGCreateShippingOrder();   //超商系統回覆內容
                    //echo '<pre>' . print_r($Result, true) . '</pre>';  
                    if(isset($Result['RtnCode']) && ($Result['RtnCode'] == 300 || $Result['RtnCode'] == 2001) ){
                        
                        DB::table('mod_order')
                        ->where('ord_no', $Result['MerchantTradeNo'])
                        ->update(['AllPayLogisticsID' => $Result['AllPayLogisticsID'], 'CVSPaymentNo' => $Result['CVSPaymentNo'], 'CVSValidationNo' => $Result['CVSValidationNo']]);

                        DB::table('mod_order')
                        ->where('r_ord_no', $Result['MerchantTradeNo'])
                        ->update(['AllPayLogisticsID' => $Result['AllPayLogisticsID'], 'CVSPaymentNo' => $Result['CVSPaymentNo'], 'CVSValidationNo' => $Result['CVSValidationNo']]);

                        $order->delCart();
                    }
                    else {
                        \Log::error($prodName);
                        \Log::error(json_encode($Result));
                        return response()->json(array('msg' => '訂購發生問題，請聯絡客服。'));
                    }
                } 
                catch(\Exception $e) {
                    $Result = $e->getMessage();
                    \Log::error($e->getMessage());
                    return response()->json(array('msg' => '訂購發生問題，請聯絡客服')); 
                }
        }
        return response()->json(array('msg' => 'success'));
    }
}
