<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Response;

use App\Http\Requests\ProdMgmtCrudRequest as StoreRequest;
use App\Http\Requests\ProdMgmtCrudRequest as UpdateRequest;
use App\Models\CommonModel;
use App\Models\BaseModel;
use App\Models\ProdMgmtModel;
use App\Models\ProdDetailModel;
use App\Models\ModSnModel;
use App\Models\SysLogModel;
use App\Models\ProdGiftModel;
use App\Exports\ProdExport;
use Illuminate\Support\Facades\Auth;
use Excel;

class ProdMgmtCrudController extends CrudController
{
    public function setup() {
        $user = Auth::user();
        $this->crud->setModel("App\Models\ProdMgmtModel");
        $this->crud->setEntityNameStrings('商品總覽', '商品總覽');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/ProdMgmt');
    
        $this->crud->setColumns(['name']);
        
        $this->crud->setCreateView('ProdMgmt.edit');
        $this->crud->setEditView('ProdMgmt.edit');
        $this->crud->setListView('ProdMgmt.index');
        $this->crud->enableAjaxTable();

        $menuId = array();
        $menuData = Db::table('mod_cate')->where('parent_id', 77)->pluck('id');

        do {
            if(empty($menuId)) {
                foreach($menuData as $row) {
                    array_push($menuId, $row);
                }
            }

            $menuData = Db::table('mod_cate')->whereIn('parent_id', $menuData)->pluck('id');
            $paraentArray = array();
            if(!empty($menuData)) {
                foreach($menuData as $row) {
                    array_push($paraentArray, $row);
                    array_push($menuId, $row);
                }
                $menuData = $paraentArray;
            }
            

        } while ( empty($menuData) );

        $array_of_fields_definition_arrays = array(
            [
                'name' => 'sell_type',
                'type' => 'select'
            ],
            [
                'name' => 'cate_id',
                'type' => 'select2_from_array',
                'options' => DB::table('mod_cate')->select('id as code', 'name as descp')->where('id', '>', 77)->orderBy('parent_id', 'asc')->get()
            ],
            [
                'name' => 'cate_name',
                'type' => 'text'
            ],
            [
                'name' => 'country',
                'type' => 'select2_multiple',
                'options' => DB::table('mod_cate')->select('id as code', 'name as descp')->where('parent_id', 6)->orderBy('id', 'asc')->get()
            ],
            [
                'name' => 'title',
                'type' => 'text'
            ],
            [
                'name' => 'sub_title',
                'type' => 'text'
            ],
            [
                'name' => 'f_price',
                'type' => 'text'
            ],
            [
                'name' => 't_price',
                'type' => 'text'
            ],
            [
                'name' => 'pay_way',
                'type' => 'select2_multiple',
                'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'PAYWAY')->where('c_key', $user->c_key)->get()
            ],
            [
                'name' => 'brand_nm',
                'type' => 'lookup',
                'title' => '品牌查詢',
                'info1' => Crypt::encrypt('bscode'), //table
                'info2' => Crypt::encrypt("cd+cd_descp,cd,cd_descp"), //column
                'info3' => Crypt::encrypt(" cd_type='BRAND' "), //condition
                'info4' => "cd=brand;cd_descp=brand_nm" //field mapping
            ],
            // [
            //     'name' => 'brand',
            //     'type' => 'select2_from_array',
            //     'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'BRAND')->where('c_key', $user->c_key)->get()
            // ],
            [
                'name' => 'number_of_periods',
                'type' => 'select2_multiple',
                'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'PERIODS')->where('c_key', $user->c_key)->get()
            ],
            [
                'name' => 'ship_way',
                'type' => 'select2_multiple',
                'options' => DB::table('bscode')->select('cd as code', 'cd_descp as descp')->where('cd_type', 'SHIPWAY')->where('c_key', $user->c_key)->get()
            ],
            [
                'name' => 'descp',
                'type' => 'ckeditor',
                'extra_plugins' => ['oembed', 'widget', 'justify']
            ],
            [
                'name' => 'ttl_sell_amt',
                'type' => 'text'
            ],
            [
                'name' => 'is_added',
                'type' => 'text'
            ],
            [
                'name' => 'g_key',
                'type' => 'text'
            ],
            [
                'name' => 'c_key',
                'type' => 'text'
            ],
            [
                'name' => 's_key',
                'type' => 'text'
            ],
            [
                'name' => 'd_key',
                'type' => 'text'
            ],
            [
                'name' => 'created_by',
                'type' => 'text'
            ],
            [
                'name' => 'updated_by',
                'type' => 'text'
            ],
            [
                'name' => 'added_on',
                'type' => 'datetime_picker'
            ],
            [
                'name' => 'img_descp1',
                'type' => 'text'
            ],
            [
                'name' => 'img_descp2',
                'type' => 'text'
            ],
            [
                'name' => 'img_descp3',
                'type' => 'text'
            ],
            [
                'name' => 'img_descp4',
                'type' => 'text'
            ],
            [
                'name' => 'img_descp5',
                'type' => 'text'
            ],
            [
                'name' => 'slogan',
                'type' => 'ckeditor',
                'extra_plugins' => ['oembed', 'widget', 'justify']
            ],
            [
                'name' => 'other_content',
                'type' => 'ckeditor',
                'extra_plugins' => ['oembed', 'widget', 'justify']
            ],
            [
                'name' => 'prod_no',
                'type' => 'text'
            ],
            [
                'name' => 'prod_type',
                'type' => 'select'
            ],
            [
                'name' => 'd_percent',
                'type' => 'text'
            ],
            [
                'name' => 'action_date',
                'type' => 'datetime_picker'
            ],
            [
                'name' => 'is_hot',
                'type' => 'text'
            ],
            [
                'name' => 'sort',
                'type' => 'text'
            ],
            [
                'name' => 'is_focus',
                'type' => 'text'
            ]
        );


        $this->crud->addFields($array_of_fields_definition_arrays);
    }

    public function index() {
        $user = Auth::user();

        $this->crud->hasAccessOrFail('list');
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }
        
        $BaseModel = new BaseModel();
        $obj = null;

        $layout = $BaseModel->getLayout('modProduct');
        $obj = json_decode($layout);

        $fieldData = $BaseModel->getColumnInfo("mod_product_view");
        if($obj != null) {
            array_push($fieldData, $obj);
        }

        $colModel = array('mod_product_view' => $fieldData);

        $this->crud->colModel = $fieldData;
        $sqlQuery = '';

        if($user->identity == 'A') {
            $sqlQuery = " prod_type='N' ";
        }
        else {
            $sqlQuery = "created_by='".$user->email."' and prod_type='N' ";
        }
        $this->data['basecondition'] = Crypt::encrypt($sqlQuery);
        $this->data['nUrl'] = Crypt::encrypt($sqlQuery);

        if($user->identity == 'A') {
            $sqlQuery = " prod_type='W' ";
        }
        else {
            $sqlQuery = "created_by='".$user->email."' and prod_type='W' ";
        }
        $this->data['wUrl'] = Crypt::encrypt($sqlQuery);

        if($user->identity == 'A') {
            $sqlQuery = " prod_type='S' ";
        }
        else {
            $sqlQuery = "created_by='".$user->email."' and prod_type='S' ";
        }
        $this->data['sUrl'] = Crypt::encrypt($sqlQuery);

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    public function create()
    {
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['prodType'] = request('prodType');
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function getData($table=null,$id) {
        $data = DB::table('mod_product')->where('id', $id)->first();

        return response()->json($data);
    }

    public function get($prod_no=null) {
        $prodDetail = [];
        if($prod_no != null) {
            $this_query = DB::table('mod_product_detail');
            $this_query->where('prod_no', $prod_no);
            $this_query->orderBy('seq', 'asc');
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function giftget($prod_id=null) {
        $giftDetail = [];
        if($prod_id != null) {
            $this_query = DB::table('mod_gift');
            $this_query->where('prod_id', $prod_id);
            $giftDetail = $this_query->get();
        }
        $data[] = array(
            'Rows' => $giftDetail,
        );
        return response()->json($data);
    }

    public function snGet($prodNo=null) {
        $prodDetail = [];

        if($prodNo != null) {
            $this_query = DB::table('mod_sn');
            $this_query->where('prod_no', $prodNo);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            
            $prodDetail = new ProdDetailModel;
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            //dd($prodDetail);
            $prodDetail->save();

            $prodDetailData = ProdDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=5; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/product', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
                
            }
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function giftStore(Request $request)
    {
        $user = Auth::user();   
        $request->merge(array('created_by' => $user->email));
        $request->merge(array('updated_by' => $user->email));
        $gift = new ProdGiftModel;
        foreach($request->all() as $key=>$val) {
            $gift[$key] = request($key);
        }
        $gift->save();
        $giftData = ProdGiftModel::find($gift->id);
        $data = $request->all();
        if(isset($data['img1'])) {
            $img = $data['img1'];
            $path = Storage::putFile('public/uploads/product', $img);
            $giftData['img1'] = $path;
            $giftData->save();
        }           
        return ["msg"=>"success", "data"=>$gift->where('id', $giftData->id)->get()];
    }

    public function snStore(Request $request)
    {
        $validator = $this->detailValidator($request);        
        $prodDetail = new ModSnModel;
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            foreach($request->only(['sn', 'is_sell', 'prod_no']) as $key=>$val) {
                $prodDetail[$key] = request($key);
            }

            $prodDetail->save();
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetail->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $log =  new SysLogModel;
        $now = date('YmdHis');
        $prodDetail_INFO = ProdDetailModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = ProdDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = ProdDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=5; $i++) {

                if(isset($data['img'.$i])) {
                    $oldPath = $prodDetail['img'.$i];
                    
                    Storage::delete($oldPath);
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/product', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
                if($request["stock"] !=$prodDetail_INFO["stock"] ){
                    $log["remark"] = "stock:".$request["stock"];
                    if($prodDetail["prod_no"] == null or $prodDetail["prod_no"] == ""){
                        $log["ref_no"] = "id:".$request["id"];
                    }else{
                        $log["ref_no"] = "prod_no:".$prodDetail["prod_no"];
                    }
                    $log["updated_at"] = $now;
                    $log["created_at"] = $now;
                    $log["created_BY"] = $user->email;
                    $log->save();
                }
            }
        }


        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }

    public function giftUpdate(Request $request)
    {
        $user = Auth::user();
        $now = date('YmdHis');
        $gift_INFO = ProdGiftModel::find($request->id);
        foreach($request->all() as $key=>$val) {
            $gift_INFO[$key] = request($key);
        }
        $gift_INFO->save();

        if(isset($data['img1'])) {
            $oldPath = $prodDetail['img1'];
            Storage::delete($oldPath);
            $img = $data['img1'];
            $path = Storage::putFile('public/uploads/product', $img);
            $gift_INFO['img1'] = $path;
            $gift_INFO->save();
        }
        return ["msg"=>"success", "data"=>$gift_INFO->where('id', $request->id)->get()];
    }

    public function snUpdate(Request $request) {
        $validator = $this->detailValidator($request);

        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = ModSnModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();
        }


        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }

    public function detailDel($id)
    {
        $prodDetail = ProdDetailModel::find($id);
        Storage::delete($prodDetail->img1);
        Storage::delete($prodDetail->img2);
        Storage::delete($prodDetail->img3);
        Storage::delete($prodDetail->img4);
        Storage::delete($prodDetail->img5);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }
    public function giftDel($id)
    {
        $prodGift = ProdGiftModel::find($id);
        Storage::delete($prodGift->img1);
        $prodGift->delete();
        return ["msg"=>"success"];
    }

    public function snDel($id)
    {
        $prodDetail = ModSnModel::find($id);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }


    public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);

            $prodData = ProdMgmtModel::find($this->data['entry']->getKey());
            $data = $request->all();
            for($i=1; $i<=5; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/product', $img);
                    $prodData['img'.$i] = $path;
                    $prodData->save();
                }
                
            }

            if($prodData->prod_type == "S") {
                $softwareProdDetail = [];
                array_push($softwareProdDetail, [
                    'prod_no' => (isset($prodData->prod_no)) ? $prodData->prod_no: null,
                    'title'   => (isset($prodData->sub_title)) ? $prodData->sub_title    : null,
                    'seq'     => 999,
                    'stock'   => 9999999,
                    'o_price' => (isset($prodData->f_price)) ? $prodData->f_price: null,
                    'd_price' => (isset($prodData->f_price)) ? $prodData->f_price: null
                ]);

                DB::table('mod_product_detail')->insert($softwareProdDetail);
            }
            
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
	}

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        $oldData = ProdMgmtModel::find($request->id);
        $prodData = array();
        \Log::info(json_encode($request->all()));
        unset($request['created_by']);
        try {
            $response = parent::updateCrud($request);
            $prodData = ProdMgmtModel::find($request->id);
            $data = $request->all();
            for($i=1; $i<=5; $i++) {

                if(isset($data['img'.$i])) {
                    $oldPath = $oldData['img'.$i];
                    
                    Storage::delete($oldPath);
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/product', $img);
                    $prodData['img'.$i] = $path;
                    $prodData->save();
                }
                
            }

            if(isset($request->prod_no)) {

                if($oldData->prod_type == "S") {
                    DB::table('mod_sn')
                    ->where('prod_no', $oldData->prod_no)
                    ->update([
                        'prod_no' => $prodData->prod_no,
                        
                    ]);

                    if(isset($request->f_price)) {
                        DB::table('mod_product_detail')
                            ->where('prod_no', $prodData->prod_no)
                            ->update([
                                'o_price' => $prodData->f_price, 
                                'd_price' => $prodData->f_price,
                                'title'   => (isset($prodData->sub_title)) ? $prodData->sub_title : null
                        ]);
                    }
                }
                else {
                    DB::table('mod_product_detail')
                    ->where('prod_no', $oldData->prod_no)
                    ->update(['prod_no' => $request->prod_no]);
                }
                
            }

            
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        if(isset($prodData)) {
            if(isset($prodData->img1))
                $prodData->img1 = Storage::url($prodData->img1);

            if(isset($prodData->img2))
                $prodData->img2 = Storage::url($prodData->img2);
            
            if(isset($prodData->img3))
                $prodData->img3 = Storage::url($prodData->img3);

            if(isset($prodData->img4))
                $prodData->img4 = Storage::url($prodData->img4);

            if(isset($prodData->img5))
                $prodData->img5 = Storage::url($prodData->img5);
        }
        
        return ["msg"=>"success", "response"=>$response, "data" => $prodData];
    }
    
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $prodDetail = ProdDetailModel::where('prod_id', $id)->get();

        if(isset($prodDetail)) {
            foreach($prodDetail as $row) {
                Storage::delete($row->img1);
                Storage::delete($row->img2);
                Storage::delete($row->img3);
                Storage::delete($row->img4);
                Storage::delete($row->img5);

                DB::table('mod_product_detail')->where('id', $row->id)->delete();
            }
            
        }

        $prodData = ProdMgmtModel::find($id);
        DB::table('mod_product_detail')->where('prod_no', $prodData->prod_no)->delete();

        Storage::delete($prodData->img1);
        Storage::delete($prodData->img2);
        Storage::delete($prodData->img3);
        Storage::delete($prodData->img4);
        Storage::delete($prodData->img5);

        return $this->crud->delete($id);
    }

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $prodMgmtModel = ProdMgmtModel::find($ids[$i]);
                DB::table('mod_product_detail')->where('prod_no', $prodMgmtModel->prod_no)->delete();
                $prodMgmtModel->delete();
                
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function multiadded() {
        $ids = request('ids');
        $now = date('YmdHis');
        $user = Auth::user();
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                DB::table('mod_product')
                ->where('id', $ids[$i])
                ->update(['is_added' => 'Y',
                          'updated_by' =>$user->email,
                          'updated_at' =>$now,
                          'added_on' =>$now,
                         ]);
            }
        }

        return response()->json(array('msg' => 'success'));
    }

    public function multiremove() {
        $ids = request('ids');
        $now = date('YmdHis');
        $user = Auth::user();
        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                DB::table('mod_product')
                ->where('id', $ids[$i])
                ->update(['is_added' => 'N',
                          'updated_by' =>$user->email,
                          'updated_at' =>$now,
                          'added_on' =>$now,
                         ]);
            }
        }

        return response()->json(array('msg' => 'success'));
    }



    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function guid(){
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8)
            .substr($charid, 8, 4)
            .substr($charid,12, 4)
            .substr($charid,16, 4)
            .substr($charid,20,12);
        return $uuid;
    }

    public function exportWifi() {       
        $now = date('YmdHis');
        $ids = request('ids');
        $idArray = explode(',', $ids);
        Excel::create('WIFI商品-'.$now, function($excel) use($idArray) {
            $excel->sheet("商品", function($sheet) use($idArray) {
                //$sheet->row(1, array('主檔資料', '', '', '', '', '', '', '', '', '', '', '', '', '明細資料'));
                //$sheet->row(2, array('商品編號', '銷售方式', '商品類別', '商品名稱', '副標題', '價格區間(起)', '價格區間(迄)', '付款方式', '送貨方式', '上架時間', '商品簡述', '商品描述', '支援地區', '型號編號', '序號', '已使用', '上架'));
                
                $sheet->setAutoSize(array(
                    'D', 'E'
                ));


                //$prod = DB::select( DB::raw('SELECT m.id, m.sell_type,m.cate_id,m.title AS m_title,m.sub_title,m.f_price,m.t_price,m.pay_way,m.ship_way,m.added_on,m.descp,m.slogan,m.other_content, d.id as d_id, d.country, d.title,d.o_price,d.d_price,d.seq,d.stock FROM mod_product m, mod_product_detail d WHERE m.id=d.prod_id and m.cate_id=3') );
                $prod = DB::table('mod_product')
                            ->leftJoin('mod_product_detail', 'mod_product_detail.prod_no', '=', 'mod_product.prod_no')
                            ->select('mod_product.id', 
                                    'mod_product.prod_no',
                                    'mod_product.sell_type', 
                                    'mod_product.cate_id',
                                    DB::raw('(select name from mod_cate where mod_cate.id=mod_product.country and parent_id=6) as m_country'), 
                                    DB::raw('mod_product.title as m_title'), 
                                    'mod_product.sub_title', 
                                    'mod_product.f_price', 
                                    'mod_product.t_price', 
                                    'mod_product.pay_way', 
                                    'mod_product.ship_way', 
                                    'mod_product.added_on',
                                    'mod_product.descp',
                                    'mod_product.slogan',
                                    'mod_product.other_content',
                                    DB::raw('mod_product_detail.id as d_id'),
                                    'mod_product_detail.title',
                                    'mod_product_detail.o_price',
                                    'mod_product_detail.d_price',
                                    'mod_product_detail.seq',
                                    'mod_product_detail.stock'
                                    )
                            ->where('mod_product.prod_type', 'W')
                            ->whereIn('mod_product.id', $idArray)
                            ->orderBy('mod_product.id', 'asc')
                            ->get();
                
                $sheet->loadView('exports.WifiProd')->with('prod', $prod);
        
            });
            
        })->export('xls');
        
    }

    public function exportNormal() {       
        $now = date('YmdHis');
        $ids = request('ids');
        $idArray = explode(',', $ids);
        Excel::create('一般商品-'.$now, function($excel) use($idArray) {
            $excel->sheet("商品", function($sheet) use($idArray) {
                $sheet->setAutoSize(array(
                    'D', 'E', 'P'
                ));
                
                $prod = DB::table('mod_product')
                            ->leftJoin('mod_product_detail', 'mod_product_detail.prod_no', '=', 'mod_product.prod_no')
                            ->select('mod_product.id', 
                                    'mod_product.prod_no',
                                    'mod_product.sell_type', 
                                    'mod_product.cate_id',
                                    DB::raw('mod_product.title as m_title'), 
                                    'mod_product.sub_title', 
                                    'mod_product.f_price', 
                                    'mod_product.t_price', 
                                    'mod_product.pay_way', 
                                    'mod_product.ship_way', 
                                    'mod_product.added_on',
                                    'mod_product.descp',
                                    'mod_product.slogan',
                                    'mod_product.other_content',
                                    'mod_product.d_percent',
                                    'mod_product.action_date',
                                    'mod_product.is_hot',
                                    'mod_product.sort',
                                    'mod_product.brand',
                                    'mod_product.img1',
                                    DB::raw('mod_product_detail.id as d_id'), 
                                    'mod_product_detail.title',
                                    'mod_product_detail.o_price',
                                    'mod_product_detail.d_price',
                                    'mod_product_detail.seq',
                                    'mod_product_detail.stock',
                                    DB::raw('mod_product_detail.img1 as d_img1')
                                    )
                            ->where('mod_product.prod_type', 'N')
                            ->whereIn('mod_product.id', $idArray)
                            ->orderBy('mod_product.id', 'asc')
                            ->get();
                
                $sheet->loadView('exports.NormalProd')->with('prod', $prod);
            });            
        })->export('xls');
    }

    public function exportSoftware() {
        $now = date('YmdHis');
        $ids = request('ids');

        $idArray = [];

        if(isset($ids)) {
            $idArray = explode(',', $ids);
        }
        
        Excel::create('軟體商品-'.$now, function($excel) use($idArray) {
            $excel->sheet("商品", function($sheet) use($idArray) {
                $sheet->setAutoSize(array(
                    'D', 'E', 'P'
                ));
                
                $prod = DB::table('mod_product')
                            ->leftJoin('mod_sn', 'mod_sn.prod_no', '=', 'mod_product.prod_no')
                            ->select('mod_product.id', 
                                    'mod_product.prod_no',
                                    'mod_product.sell_type', 
                                    'mod_product.cate_id',
                                    DB::raw('mod_product.title as m_title'), 
                                    'mod_product.sub_title', 
                                    'mod_product.f_price', 
                                    'mod_product.t_price', 
                                    'mod_product.pay_way', 
                                    'mod_product.ship_way', 
                                    'mod_product.added_on',
                                    'mod_product.descp',
                                    'mod_product.slogan',
                                    'mod_product.other_content',
                                    DB::raw('mod_sn.id as d_id'), 
                                    'mod_sn.sn',
                                    'mod_sn.is_sell'
                                    )
                            ->where('mod_product.prod_type', 'S')
                            ->whereIn('mod_product.id', $idArray)
                            ->orderBy('mod_product.id', 'asc')
                            ->get();
                
                $sheet->setColumnFormat(array(
                    'N' =>  \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                ));
                
                $sheet->loadView('exports.SoftwareProd')->with('prod', $prod);
            });            
        })->export('xls');
    }

    public function uploadExcel($mode, $mainCol) {
        $user = Auth::user();
        config(['excel.import.startRow' => 2 ]);
        $prodModel = new ProdMgmtModel();

        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();

            try {
                $data = Excel::load($path, function($reader) {
                
                })->get();
    
                $dataFirst = $data[0];
                
                return $prodModel->processExcelData($dataFirst, $mode, $mainCol, $user);
            }
            catch (\Exception $e) {
                \Log::error($e);                
                return ["msg"=>"error", "errorLog"=>$e->getMessage()];
            }
            
        }

        return ["msg"=>"success"];
    }

    public function test() {
        $html = view('ProdMgmt.test')->render();

        print_r($html);
    }

    public function chkProdNoExist($prodNo) {
        $cnt = DB::table('mod_product')->where('prod_no', $prodNo)->count();

        if($cnt > 0) {
            return ['status' => 'error', 'msg' => '該編號已存在'];
        }

        return ['status' => 'success'];
    }
}
