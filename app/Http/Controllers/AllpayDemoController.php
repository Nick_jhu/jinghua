<?php

namespace App\Http\Controllers;

//use Illuminate\Routing\Controller;
use Storage;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Allpay;

class AllpayDemoController extends Controller
{
    

    public function demo(){
    Allpay::i()->Send['ReturnURL']         = "http://www.allpay.com.tw/receive.php" ;
    Allpay::i()->Send['MerchantTradeNo']   = "Test".time() ;           //訂單編號
    Allpay::i()->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');      //交易時間
    Allpay::i()->Send['TotalAmount']       = 2000;                     //交易金額
    Allpay::i()->Send['TradeDesc']         = "good to drink" ;         //交易描述
    Allpay::i()->Send['ChoosePayment']     = \PaymentMethod::ALL ;     //付款方式

    //訂單的商品資料
    array_push(Allpay::i()->Send['Items'], array('Name' => "歐付寶黑芝麻豆漿", 'Price' => (int)"2000",
               'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));

    //Go to AllPay
    echo "歐付寶頁面導向中...";
    echo Allpay::i()->CheckOut();
        
    }
}
