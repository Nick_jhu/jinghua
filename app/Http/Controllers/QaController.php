<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;


use App\Models\ModQaModel;


class QaController extends Controller
{
    public function index(Request $request) {

        $cateData = DB::table('mod_cate')->where('parent_id', 2)->get();
        $qaData = DB::table('mod_qa')->get();
        

        $viewData = array(
            'viewName' => 'qa',
            'cateData' => $cateData,
            'qaData'   => $qaData
        );

        return view('FrontEnd.qa')->with($viewData);
    }
}
