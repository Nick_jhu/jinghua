<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderMgmtModel;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;
use Redirect;

class MemberLoginController extends Controller
{
    public function __construct() {
        $this->middleware('guest:member', ['except' => 'logout']);
    }
    public function showLoginForm() {
        return view('FrontEnd.auth.newLogin')->with(['viewName' => 'login']);
    }
    public function showForgetForm() {
        return view('FrontEnd.auth.forget')->with(['viewName' => 'forget']);
    }
    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'phone'   => 'required',
            'password' => 'required'
        ]);

        $urlPrevious = url()->previous();
        $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;
        
        if(env('APP_ENV') == 'production') {
            $isBoot = $this->chkBoot();

            if($isBoot === false) {
                return redirect()->route('member.login')->with(['viewName' => 'login'])->withInput($request->only('phone', 'remember'))->withErrors(['message' => '登入失敗']);
            }
        }
        
        $o = new OrdermgmtModel();
        // Attempt to log the user in
        if (\Auth::guard('member')->attempt(['phone' => $request->phone, 'password' => $request->password, 'is_actived' => 'Y'], $request->remember)) {
            // if successful, then redirect to their intended location
            $user = \Auth::guard('member')->getLastAttempted();

            $o->mappingCart($cartId, $user->id);
            $o->processCart($user);
            unset($_COOKIE['cartId']);
            setcookie('cartId', 0, -1, '/');

            if($urlPrevious != url('member/login')) {
                return Redirect::to($urlPrevious);
            }

            return redirect()->route('FrontEnd.index');
        }
        else if(\Auth::guard('member')->attempt(['email' => $request->phone, 'password' => $request->password, 'is_actived' => 'Y'], $request->remember)) {
            $user = \Auth::guard('member')->getLastAttempted();

            $o->mappingCart($cartId, $user->id);
            $o->processCart($user);
            unset($_COOKIE['cartId']);
            setcookie('cartId', 0, -1, '/');

            if($urlPrevious != url('member/login')) {
                return Redirect::to($urlPrevious);
            }

            return redirect()->route('FrontEnd.index');
        }

        $errorMsg = "帳號密碼錯誤";

        $user = \App\ModMember::where('phone', $request->phone)->first();

        if(isset($user)) {
            if($user->is_actived === "N") {
                $errorMsg = "您的帳號尚未啟用，請透過手機驗證啟用您的帳號";
            }
        }

        

        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->route('member.login')->with(['viewName' => 'login'])->withInput($request->only('phone', 'remember'))->withErrors(['message' => $errorMsg]);
    }

    public function forget(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'phone'   => 'required'
        ]);
        
        if(env('APP_ENV') == 'production') {
            $isBoot = $this->chkBoot();

            if($isBoot === false) {
                return redirect()->route('member.forget')->with(['viewName' => 'forget'])->withInput($request->only('phone'))->withErrors(['message' => '系統發現不正常操作行為']);
            }
        }

        $today = date('Y-m-d');

        $cnt = DB::table('forget_log')
                ->where('phone', $request->phone)
                ->where('created_at', '>=', $today.' 00:00:00')
                ->where('created_at', '<=', $today.' 23:59:59')
                ->count();

        if($cnt >= 5) {
            return response()->json(['msg' => 'success', 'log' => '一天只有五次忘記密碼的機會']);
        }


        $newPwd = rand(10000, 99999);

        $hasPhone = \App\ModMember::where('phone', $request->phone)->count();
        if($hasPhone > 0) {
            \App\ModMember::where('phone', $request->phone)->update(['password' => \Hash::make($newPwd)]);

            DB::table('forget_log')->insert(['phone' => $request->phone, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

            if(env('APP_ENV') == 'production') {
                $mitake = app(\Mitake\Client::class);

                $message = (new \Mitake\Message\Message())
                    ->setDstaddr($request->phone)
                    ->setSmbody('美麗點人生網路購物會員密碼修改為：'.$newPwd.'，請盡快登入修改密碼');
                $result = $mitake->send($message);

                Mail::raw($request->phone.'美麗點人生網路購物會員密碼修改為：'.$newPwd.'，請盡快登入修改密碼', function ($message) {
                    $message->to('sales@standard-info.com')
                            ->subject('變更密碼通知');
                });

                // if unsuccessful, then redirect back to the login with the form data
                //return redirect()->route('member.login')->with(['viewName' => 'login'])->withInput($request->only('phone'))->withErrors(['message' => '訊息已發送，請盡快登入修改密碼']);
                return response()->json(['msg' => 'success', 'log' => '密碼已發送，請盡快登入修改密碼']);
            }
            

            // if unsuccessful, then redirect back to the login with the form data
            //return redirect()->route('member.login')->with(['viewName' => 'login'])->withInput($request->only('phone'))->withErrors(['message' => '訊息已發送，請盡快登入修改密碼'.$newPwd]);
            return response()->json(['msg' => 'success', 'log' => '密碼已發送，請盡快登入修改密碼'.$newPwd]);
        }
        else {
            $hasEmail = \App\ModMember::where('email', $request->phone)->count();

            if($hasEmail > 0) {
                \App\ModMember::where('email', $request->phone)->update(['password' => \Hash::make($newPwd)]);

                DB::table('forget_log')->insert(['phone' => $request->phone, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

                Mail::raw('美麗點人生網路購物會員密碼修改為：'.$newPwd.'，請盡快登入修改密碼', function ($message) use ($request) {
                    $message->to($request->phone)
                            ->bcc('sales@standard-info.com')
                            ->subject('變更密碼通知');
                });
                return response()->json(['msg' => 'success', 'log' => '密碼已發送，請盡快登入修改密碼']);
            }
        }

        return response()->json(['msg' => 'success', 'log' => '系統查無此帳號']);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::guard('member')->logout();
        
        $request->session()->flush();
        $request->session()->regenerate();

        // unset($_COOKIE['cartNum']);
        // setcookie('cartNum', 0, -1, '/');
        return redirect()->guest(route( 'FrontEnd.index' ));
    }

    public function chkBoot() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $params = array();
            //$params['secret'] = '6LfaRlIUAAAAAGyxPvEh12idhR8ZX9CFIU1XpQbg'; // Secret key
            $params['secret'] = '6Ldkv5oUAAAAALIyQvIrPHau-1O2hUQtX7K1z5QP'; // Secret key

            if (!empty($_POST) && isset($_POST['g-recaptcha-response'])) {
                $params['response'] = urlencode($_POST['g-recaptcha-response']);
            }
            $params['remoteip'] = $_SERVER['REMOTE_ADDR'];

            $params_string = http_build_query($params);
            $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $params_string;

            // Get cURL resource
            $curl = curl_init();

            // Set some options
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $requestURL,
            ));

            // Send the request
            $response = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            $response = @json_decode($response, true);

            if ($response["success"] == true) {
                return true;
            } else {
                return false;
            }
        } else {
            // get request.
        }

        return false;
    }
}
