<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MemberMgmtModel;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Redirect;
use Mail;
use Auth;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function index() {
        return view('FrontEnd.auth.register');
    }

    public function do_register(Request $request) {

        $customMessages = [
            'unique' => '此email已被註冊',
            'same'   => '密碼不一致',
            'min'    => '密碼至少六個字元'
        ];

        $validator = \Validator::make($request->all(), [
            'email' => 'required|unique:mod_member',
            'password' => 'required|min:6|max:16',
            'confirm_password' => 'required|same:password',
        ],$customMessages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput($request->only('phone'));
        }

        $member = new MemberMgmtModel;

        $member->email = $request->email;
        $member->password = \Hash::make($request->password);
        $member->validation_code = $this->gen_code();
        $member->validation_date = date('Y-m-d H:i:s');
        $member->g_key = 'STD';
        $member->c_key = 'STD';
        $member->s_key = 'STD';
        $member->d_key = 'STD';
        $member->save();


        $link = Url('register/mail/check').'/'.Crypt::encryptString($member->email);
        $sendData = [
            'link' => '<a href="'.$link.'">'.$link.'</a>',
        ];

        Mail::to($member->email)
            ->bcc('sales@standard-info.com')
            ->send(new RegisterMail($sendData));

        return Redirect::to('/')->with(['message' => '請到註冊信箱點選驗證連結完成註冊']);
    }

    //舊版註冊
    public function old_do_register(Request $request) {

        $customMessages = [
            'unique' => '此電話已被註冊'
        ];

        $validator = \Validator::make($request->all(), [
            'phone' => 'required|unique:mod_member',
            'password' => 'required|min:6|max:16',
            'confirm_password' => 'required|same:password',
        ],$customMessages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput($request->only('phone'));
        }

        $member = new MemberMgmtModel;

        $member->phone = $request->phone;
        $member->password = \Hash::make($request->password);
        $member->validation_code = $this->gen_code();
        $member->validation_date = date('Y-m-d H:i:s');
        $member->g_key = 'STD';
        $member->c_key = 'STD';
        $member->s_key = 'STD';
        $member->d_key = 'STD';
        $member->save();

        $mitake = app(\Mitake\Client::class);

		$message = (new \Mitake\Message\Message())
			->setDstaddr($request->phone)
			->setSmbody('美麗點人生網路購物會員註冊驗證碼：'.$member->validation_code.'，驗證碼將於十分鐘後失效');
		$result = $mitake->send($message);

        //$request->session()->flash('phone', $request->phone);
        //Session::put('cellPhone',$request->phone);
        //Session::save();

        return Redirect::to('register/confirm')->with('cellphone', $request->phone)->send();
    }

    public function registerConfirm(Request $request) {
        return view('FrontEnd.auth.confirm')->with(['viewName' => 'login']);
    }

    public function reSendCode(Request $request) {
        $phone = $request->phone;

        try {
            $code = $this->gen_code();
            DB::table('mod_member')
            ->where('phone', $phone)
            ->update([
                "validation_code" => $code,
                "validation_date" => date('Y-m-d H:i:s')
            ]);

            $mitake = app(\Mitake\Client::class);

            $message = (new \Mitake\Message\Message())
                ->setDstaddr($request->phone)
                ->setSmbody('美麗點人生網路購物會員註冊驗證碼：'.$code.'，驗證碼將於十分鐘後失效');
            $result = $mitake->send($message);

        }
        catch(\Exception $e)  {
            return response()->json(['msg' => 'error', 'error_log' => $e->getMessage()]);
        }

        
        return response()->json(['message' => 'success']);
    }


    public function confirm_validation(Request $request) {
        $errorMsg = "驗證失敗，請重新發送驗證碼";
        $validator = \Validator::make($request->all(), [
            //'phone' => 'required',
            'validation_code' => 'required|max:4|min:4'
        ]);
        
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator);
        }

        $member = MemberMgmtModel::where('phone', $request->phone)->first();

        if(isset($member)) {
            $now = date('Y-m-d H:i:s');
            $sec = strtotime($now) - strtotime($member->validation_date);

            if($sec <= 600 && $request->validation_code == $member->validation_code) {
                \Auth::guard('member')->loginUsingId($member->id);

                $updateUser = MemberMgmtModel::find($member->id);
                $updateUser->is_actived = "Y";
                $updateUser->save();

                return redirect('/');
            }

            if($request->validation_code != $member->validation_code) {
                $errorMsg = "驗證碼錯誤，請重新輸入";
            }

            if($sec > 600) {
                $errorMsg = "驗證碼超過時效，請重新發送驗證碼";
            }
        }

        

        return Redirect::to('register/confirm')->with(['cellphone' => $request->phone])->withErrors(['message' => $errorMsg]);
    }

    private function gen_code() {
        return mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
    }

    public function reSendVcode(Request $request) {
        $phone = $request->phone;

        $isBoot = $this->chkBoot();

        if($isBoot === false) {
            return redirect()->to('reSendVcode')->with(['viewName' => 'login'])->withErrors(['message' => '驗證失敗']);
        }

        if($phone != null) {
            $member = DB::table('mod_member')->where('phone', $phone)->where('is_actived', 'N')->first();

            if(isset($member)) {
                $code = $this->gen_code();
                DB::table('mod_member')
                ->where('phone', $phone)
                ->update([
                    "validation_code" => $code,
                    "validation_date" => date('Y-m-d H:i:s')
                ]);

                $mitake = app(\Mitake\Client::class);

                $message = (new \Mitake\Message\Message())
                    ->setDstaddr($request->phone)
                    ->setSmbody('美麗點人生網路購物會員註冊驗證碼：'.$code.'，驗證碼將於十分鐘後失效');
                $result = $mitake->send($message);
            }
            else {
                //return Redirect::back()->withErrors(['message', '您還沒註冊，請先註冊，或您已經註冊，不能再發送'])->send();
                return redirect()->to('reSendVcode')->with(['viewName' => 'login'])->withErrors(['message' => '您還沒註冊，請先註冊，或您已經註冊，不能再發送']);
            }
        }
        else{
            return redirect()->to('reSendVcode')->with(['viewName' => 'login'])->withErrors(['message' => '請輸入電話']);
        }

        return Redirect::to('register/confirm')->with('cellphone', $request->phone)->send();
    }

    public function chkBoot() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $params = array();
            $params['secret'] = '6LfaRlIUAAAAAGyxPvEh12idhR8ZX9CFIU1XpQbg'; // Secret key

            if (!empty($_POST) && isset($_POST['g-recaptcha-response'])) {
                $params['response'] = urlencode($_POST['g-recaptcha-response']);
            }
            $params['remoteip'] = $_SERVER['REMOTE_ADDR'];

            $params_string = http_build_query($params);
            $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $params_string;

            // Get cURL resource
            $curl = curl_init();

            // Set some options
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $requestURL,
            ));

            // Send the request
            $response = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            $response = @json_decode($response, true);

            if ($response["success"] == true) {
                return true;
            } else {
                return false;
            }
        } else {
            // get request.
        }

        return false;
    }

    public function registerMailCheck($code) {
        $mail = Crypt::decryptString($code);

        $user = DB::table('mod_member')->where('email', $mail)->first();

        if(isset($user)) {

            if($user->is_actived == 'Y') {
                return Redirect::to('/')->with(['message' => '您已經驗證過囉']);
            }
            
            DB::table('mod_member')->where('id', $user->id)->update(['is_actived' => 'Y']);

            Auth::guard('member')->loginUsingId($user->id, true);

            return Redirect::to('/')->with(['message' => '完成註冊，感謝您的加入']);
        }

        return Redirect::to('/')->with(['message' => '連結有誤，請恰管理人員']);
    }
}
