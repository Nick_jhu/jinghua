<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;

use App\Models\MemberMgmtModel;
use App\Models\OrderMgmtModel;
use App\Models\ModSnModel;
use App\Models\ProdDetailModel;
use Input;


class MemberCenterController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:member');
    }

    public function index(Request $request) {
        $user = Auth::guard('member')->user();
        $status = $request->s;
        $ordNo = $request->ordNo;

        if(empty($status)) {
            $status = 'B';
        }

        if(!isset($user)) {
            return redirect()->route('member.login')->with(['viewName' => 'login']);
        }
        
        $pickWay = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'SHIPWAY')
                    ->orderBy('cd', 'asc')
                    ->get();

        $returnWay = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'SHIPWAY')
                    ->orderBy('cd', 'asc')
                    ->get();

        //$userData = MemberMgmtModel::find($user->id);
        
        $orderDataCnt = OrderMgmtModel::Where(function($query) use ($user, $ordNo){
                                            $query->where('status', 'A')
                                            ->where('user_id', $user->id)
                                            ->whereRaw('(TIMESTAMPDIFF(SECOND, created_at, NOW())/3600/24)<=8');
                                            if(isset($ordNo)) {
                                                $query->where('ord_no', $ordNo);
                                            }
                                        })
                                        ->orderBy('created_at', 'desc')
                                        ->count();

        $orderData = array();
        
        if($status == 'A') {
            $orderData = OrderMgmtModel::Where(function($query) use ($user, $ordNo){
                $query->where('status', 'A')
                ->where('user_id', $user->id)
                ->whereRaw('(TIMESTAMPDIFF(SECOND, created_at, NOW())/3600/24)<=8');
                if(isset($ordNo)) {
                    $query->where('ord_no', $ordNo);
                }
            })
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        }
        else if($status == 'B') {
            $orderData = OrderMgmtModel::Where(function($query) use ($user, $ordNo){
                $query->whereIn('status', ['B', 'C', 'I'])
                ->where('user_id', $user->id);
                if(isset($ordNo)) {
                    $query->where('ord_no', $ordNo);
                }
            })
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        }
        else {
            $orderData = OrderMgmtModel::where('user_id', $user->id)
            ->whereIn('status', ['D', 'E', 'G','H','F'])
            ->where('user_id', $user->id)
            ->orWhere(function($query) use ($user, $ordNo)
            {
                $query->where('status', '=', 'A')
                    ->whereRaw('(TIMESTAMPDIFF(SECOND, created_at, NOW())/3600/24)>=8')
                    ->where('user_id', $user->id);

                if(isset($ordNo)) {
                    $query->where('ord_no', $ordNo);
                }

            })
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        }



        if(isset($ordNo)) {
            $orderData = OrderMgmtModel::where('ord_no', $ordNo)
                            ->where('user_id', $user->id)
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);

            foreach($orderData as $row) {
                
                if($row->status == 'A') {
                    $status = 'A';
                }
                else {
                    $status = 'B';
                }
            }
        }
        

        $processOrderData = OrderMgmtModel::Where(function($query) use ($user, $ordNo){
                                        $query->whereIn('status', ['B', 'C', 'I'])
                                        ->where('user_id', $user->id);
                                        if(isset($ordNo)) {
                                            $query->where('ord_no', $ordNo);
                                        }
                                    })
                                    ->orderBy('created_at', 'desc')
                                    ->count();
        $orderProcessRentDetail = array();
        $orderProcessDetail = array();

        // foreach($processOrderData as $key=>$row) {
        //     $data = DB::table('mod_order_rent_detail')->where('ord_id', $row->id)->get();
        //     $orderProcessRentDetail[$key] = $data;

        //     $data = DB::table('mod_order_detail')->where('ord_id', $row->id)->get();
        //     foreach($data as $val) {
        //         $prodType = DB::table('mod_product')
        //                         ->where('prod_no', $val->prod_no)
        //                         ->value('prod_type');
        //         $val->prod_type = $prodType;
        //     }

        //     $orderProcessDetail[$key] = $data;
        // }

        $orderRentDetail = array();
        $orderDetail = array();

        foreach($orderData as $key=>$row) {
            $data = DB::table('mod_order_rent_detail')->where('ord_id', $row->id)->get();
            $orderRentDetail[$key] = $data;

            $data = DB::table('mod_order_detail')->where('ord_id', $row->id)->get();
            foreach($data as $val) {
                $prodType = DB::table('mod_product')
                                ->where('prod_no', $val->prod_no)
                                ->value('prod_type');
                $val->prod_type = $prodType;
            }

            $orderDetail[$key] = $data;
        }

        $orderHistoryData = OrderMgmtModel::where('user_id', $user->id)
                                            ->whereIn('status', ['D', 'E', 'G','H', 'F'])
                                            ->where('user_id', $user->id)
                                            ->orWhere(function($query) use ($user, $ordNo)
                                            {
                                                $query->where('status', '=', 'A')
                                                    ->whereRaw('(TIMESTAMPDIFF(SECOND, created_at, NOW())/3600/24)>=8')
                                                    ->where('user_id', $user->id);
                                                    if(isset($ordNo)) {
                                                        $query->where('ord_no', $ordNo);
                                                    }

                                            })
                                            ->orderBy('created_at', 'desc')
                                            ->count();

        $orderHistoryRentDetail = array();
        $orderHistoryDetail = array();

        // foreach($orderHistoryData as $key=>$row) {
        //     $data = DB::table('mod_order_rent_detail')->where('ord_id', $row->id)->get();
        //     $orderHistoryRentDetail[$key] = $data;

        //     $data = DB::table('mod_order_detail')->where('ord_id', $row->id)->get();
        //     foreach($data as $val) {
        //         $prodType = DB::table('mod_product')
        //                         ->where('prod_no', $val->prod_no)
        //                         ->value('prod_type');
        //         $val->prod_type = $prodType;
        //     }
        //     $orderHistoryDetail[$key] = $data;
        // }

        // $cityData = DB::table('sys_area')->select('city_nm')->groupBy('city_nm')->orderBy('id', 'asc')->get();
        // $areaData = DB::table('sys_area')->select('dist_nm', 'zip_f')->where('city_nm', $userData->dlv_city)->orderBy('id', 'asc')->get();

        return view('FrontEnd.orderList')->with([
            'viewName'  => 'memberCenter',
            'pickWay'   => $pickWay,
            'returnWay' => $returnWay,
            //'userData'  => $userData,
            'orderDataCnt'  => $orderDataCnt,
            'orderData' => $orderData->appends(Input::except('page')),
            'orderRentData' => $orderRentDetail,
            'orderDetailData' => $orderDetail,
            'orderHistoryData' => $orderHistoryData,
            'orderHistoryRentData' => $orderHistoryRentDetail,
            'orderHistoryDetailData' => $orderHistoryDetail,
            // 'cityData' => $cityData,
            // 'areaData' => $areaData,
            'processOrderData' => $processOrderData,
            'orderProcessDetail' => $orderProcessDetail,
            'orderProcessRentDetail' => $orderProcessRentDetail,
            'status' => $status,
            'ordNo' => $ordNo
        ]);
    }

    public function memberInfo(Request $request) {
        $user = Auth::guard('member')->user();
        $status = $request->s;

        if(!isset($user)) {
            return redirect()->route('member.login')->with(['viewName' => 'login']);
        }

        $userData = MemberMgmtModel::find($user->id);
        $cityData = DB::table('sys_area')->select('city_nm')->groupBy('city_nm')->orderBy('id', 'asc')->get();
        $areaData = DB::table('sys_area')->select('dist_nm', 'zip_f')->where('city_nm', $userData->dlv_city)->orderBy('id', 'asc')->get();


        return view('FrontEnd.memberInfo')->with([
            'userData'  => $userData,
            'cityData' => $cityData,
            'areaData' => $areaData,
        ]);
    }

    public function updateMemberData(Request $request) {
        $user = Auth::guard('member')->user();

        $customMessages = [
            'cellphone.max'    => '電話最多只能輸入20碼',
            'confirm_password.same' => '密碼不一致'
        ];

        $validator = \Validator::make($request->all(), [
            'password' => 'nullable|min:6|max:16',
            'confirm_password' => 'nullable|same:password',
            'email' => 'nullable|email',
            'name' => 'nullable|max:191',
            'cellphone' => 'max:20'
        ],$customMessages);

        if ($validator->fails()) {
            return redirect('memberInfo')->withErrors($validator)
                ->withInput($request->only(['email', 'name', 'pick_way', 'return_way']));
        }

        $memberData = MemberMgmtModel::find($user->id);

        $memberData->cellphone  = $request->cellphone;
        //$memberData->email      = $request->email;
        $memberData->name       = $request->name;
        $memberData->pick_way   = $request->pick_way;
        $memberData->return_way = $request->return_way;
        $memberData->dlv_zip    = $request->dlv_zip;
        $memberData->dlv_city   = $request->dlv_city;
        $memberData->dlv_area   = $request->dlv_area;
        $memberData->dlv_addr   = $request->dlv_addr;
        $memberData->is_updated = 'Y';

        if(isset($request->password)) {
            $memberData->password = \Hash::make($request->password);
        }

        $memberData->save();

        if(isset($request->password)) {
            Auth::guard('member')->logout();
            return redirect()->route('member.login')->with(['viewName' => 'login'])->withErrors(['message' => '因為修改密碼所以請重新登入']);
        }

        return redirect()->back()->with(['message' => '修改成功']);
    }

    public function getOrderDetail($orderId) {
        $orderDetail = OrderMgmtModel::find($orderId);

        return respose()->json($orderDetail);
    }

    public function cancelOrder($ordId) {
        $orderData = OrderMgmtModel::find($ordId);
        $orderData->status = 'F';
        $orderData->save();

        $p = new ProdDetailModel();
        $p->cancelOrderForReturnStock($orderData);

        return response()->json(['status' => 'success', 'message' => '已通知客服人員，待客服人員確認取消']);

    }

    public function getAreaByCity(Request $request) {
        $areaData = DB::table('sys_area')->where('city_nm', $request->dlv_city)->orderBy('id', 'asc')->get();

        return response()->json(['status' => 'success', 'areaData' => $areaData]);
    }

    public function reSendSnMail($orderDetailId) {

        try {
            \Log::info("重發送序號email");
            $modSn = new ModSnModel;
            $modSn->reSendEmail($orderDetailId);
        }
        catch(\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json(['status' => 'error', 'msg' => '發生問題，請聯絡客服人員']);
        }
        

        return response()->json(['status' => 'success', 'msg' => '序號已重新發送']);
    }

    public function getSn($orderDetailId) {
        $sendData = array();
        try {
            $modSn = new ModSnModel;
            $sendData = $modSn->getSn($orderDetailId);
        }
        catch(\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json(['status' => 'error', 'msg' => '發生問題，請聯絡客服人員']);
        }
        

        return response()->json(['status' => 'success', 'sn' => $sendData]);
    }
}
