<?php

namespace App\Http\Controllers;

use Allpay;
use App\Models\BaseModel;
use App\Models\Discount;
use App\Models\MemberMgmtModel;
use App\Models\OrderMgmtModel;
use App\Models\ProdDetailModel;
use App\Models\ProdMgmtModel;
use App\Models\BscodeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;
use MPG;
use Redirect;

class CartController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:member');
    }

    public function index(Request $request)
    {
        $user = Auth::guard('member')->user();

        $cartData = DB::table('mod_cart')
            ->where('user_id', $user->id)
            ->first();
        //$cartContent = (isset($_COOKIE['cartContent'])) ? $_COOKIE['cartContent']: json_encode(array('rData'=>array(), 'sData' => array()));
        $cartContent = json_encode(array('rData' => array(), 'sData' => array()));
        if (isset($cartData)) {
            $cartContent = $cartData->content;
        }
        $cartData = json_decode($cartContent);

        $rSumAmt = 0;
        $sSumAmt = 0;

        $prodType = array();
        $prodType['W'] = 0;
        $prodType['S'] = 0;
        $prodType['N'] = 0;

        $canUseSellDiscount = true;
        $ProdMgmtModel = new ProdMgmtModel();
        $rentShipWay = array();
        foreach ($cartData->rData as $key => $row) {
            $day = (((strtotime($row->endDate) - strtotime($row->startDate)) / 3600) / 24) + 1;
            $amt = $row->dPrice * $day * $row->num;

            if ($day >= 5) {
                $canUseSellDiscount = false;
            }

            if ($row->insurance == "Y") {
                $amt = $amt + (50 * $day * $row->num);
            }

            $row->amt = $amt;

            $rSumAmt += $amt;

            $prodType['W']++;

            if($key == 0) {
                $rentShipWay = $ProdMgmtModel->getShipWayByProdIdToArray($row->prodId);
            }
        }

        $shipWayArray = array();
        $normalShipWay = array();
        foreach ($cartData->sData as $key => $row) {
            $amt = $row->dPrice * $row->num;

            $row->amt = $amt;

            $sSumAmt += $amt;

            if ($row->prodType == 'N') {
                $prodType['N']++;
            }

            if ($row->prodType == 'S') {
                $prodType['S']++;
            }

            $normalShipWay = $ProdMgmtModel->getShipWayByProdIdToArray($row->prodId);

            if(isset($shipWay)) {
                $shipWayArray = array_intersect($shipWayArray, $normalShipWay);
            }
        }

        $defaultShip = 1;
        //如果購物車只有軟體時，預設不帶宅配
        if ($prodType['S'] > 0 && $prodType['N'] == 0 && $prodType['W'] == 0) {
            $defaultShip = 0;
        }

        $showPickAir = 1;
        if ($prodType['N'] > 0) {
            $showPickAir = 0;
        }

        $cityData = DB::table('sys_area')->select('city_nm')->groupBy('city_nm')->orderBy('id', 'asc')->get();

        $viewData = [
            'viewName' => 'cart',
            'cartData' => $cartData,
            'rSumAmt' => $rSumAmt,
            'sSumAmt' => $sSumAmt,
            'defaultShip' => $defaultShip,
            'cartStr' => json_encode($cartData),
            'airlineData' => DB::table('bscode')->where('cd_type', 'AIRLINE')->orderBy('id', 'asc')->get(),
            'cityData' => $cityData,
            'canUseSellDiscount' => $canUseSellDiscount,
            'memberData' => $user,
            'showPickAir' => $showPickAir,
        ];

        return view('FrontEnd.cart')->with($viewData);
    }

    public function getUserData()
    {
        $user = Auth::guard('member')->user();
        $areaData = DB::table('sys_area')->where('city_nm', $user->dlv_city)->orderBy('id', 'asc')->get();
        $userData = array(
            'name' => $user->name,
            'cellphone' => $user->cellphone,
            'dlvZip' => $user->dlv_zip,
            'dlvCity' => $user->dlv_city,
            'dlvArea' => $user->dlv_area,
            'dlvAddr' => $user->dlv_addr,
            'email' => $user->email,
            'areaData' => $areaData,
        );

        return response()->json($userData);
    }

    public function getDiscountCode(Request $request)
    {
        $user = Auth::guard('member')->user();
        $code = $request->code;
        $sellType = $request->sellType;
        $uuid = $request->uuid;
        $prodDetailId = $request->prodDetailId;

        $d = new Discount;
        $obj = $d->useDiscount($code, $sellType, $user->id, $uuid, $prodDetailId);
        
        return response()->json(['num' => $obj['num'], 'discountWay' => $obj['discountWay']]);
    }

    public function payment(Request $request)
    {
        $payWay = $request->payWay;
        $num = $request->num;
        $ttlAmt = $request->ttlAmt;
        $dlvName = $request->dlv_nm;
        $userPhone = $request->dlv_phone;
        $dlv_addr = $request->dlv_addr;
        $email = $request->email;
        $shipFee = $request->shipfee;
        $dlvAddr = $request->dlv_addr;
        $prod_nm = $request->prod_nm;
        $cust_remark = $request->cust_remark;

            $pickTime = null;
            $returnTime = null;

            if($payWay != "Instalment") {
                $periods = 0;
                $periodsselect = '';
            }

            $bsModel = new BscodeModel;

            if(isset($returnWay)) {
                $shipFee += $bsModel->getShipFee($returnWay);
            }
            $today = new \DateTime();
            $str_date = $today->format('Ym');

        
            

            $detailItem = array();

            $isDiscount = false;
            $prodDetailModel = new ProdDetailModel;
            $orderMgmt = new OrderMgmtModel;
            $BaseModel = new BaseModel();

            // $visn_no = array();
            // for($i = 0 ; $i<$num;$i++){
            //     $visn_no[$i]=str_random(8);
            // }
            // dd($visn_no);
            $ordNo = 'JH'.time();
            $mainData = array(
                'ord_no'              => $ordNo,
                'o_price'             => $ttlAmt,
                'd_price'             => $ttlAmt,
                'ord_nm'              => $dlvName,
                'dlv_nm'              => $dlvName,
                'dlv_addr'            => $dlvAddr,
                'pay_way'             => $payWay,
                'dlv_phone'           => $userPhone,
                'email'               => $email,
                'ship_fee'            => $shipFee,
                'num'                 => $num,
                'prod_nm'             => $prod_nm,
                'cust_remark'         => $cust_remark,
            );

            $ordId = $orderMgmt->insertOrder($mainData);
            if ($ordId > 0) {
                $d = new Discount;

                $m = new MemberMgmtModel();
                // if ($payWay == "Credit" || $payWay == "Instalment") {
                //     $result = "";
                //     $remember = false;    

                //     $result = $orderMgmt->tapPayByPrime($ordId, null, $remember, $periods);
                //     dd($result);
                //     if($result != 'error') {
                //         //return Redirect::to($result);
                //         return redirect()->to('/')->with(['message' => '訂購完成']);
                //     }
                    
                //     return redirect()->to('/')->with(['message' =>'訂購發生問題']);
                    

                //     if($result == "success") {
                //         return redirect()->to('/')->with(['message' => '訂購完成']);
                //     }
                //     else {
                //         return redirect()->to('/')->with(['message' => $result]);
                //     }
                // }
                // else if($payWay == "GooglePay" || $payWay == "ApplePay" || $payWay == "LinePay" || $payWay == "SamsungPay") {
                //     $result = $orderMgmt->tapPayByPrime($ordId, $prime, false, $periods);

                //     if($payWay == 'LinePay' && $result != 'error') {
                //         return Redirect::to($result);
                //     }

                //     if($result == "success") {
                //         return redirect()->to('/')->with(['message' => '訂購完成']);
                //     }
                //     else {
                //         return redirect()->to('/')->with(['message' => $result]);
                //     }
                // } 
                // else {
                //     return $this->spgCheckout($ordNo, intval($ttlAmt), "jinghua", $payWay, null, null, null, null, $dlvAddr, $userPhone, $dlvName, $email, null);
                // }
                return $this->spgCheckout($ordNo, intval($ttlAmt), "jinghua", $payWay, null, null, null, null, $dlvAddr, $userPhone, $dlvName, $email, null);
            }
        

        return;
    }

    public function allPayCvsMap($ordNo, $pickWay, $isCollection)
    {
        $allPay = new Allpay();

        $allPay->l()->Send['MerchantTradeNo'] = $ordNo;
        $allPay->l()->Send['LogisticsType'] = 'CVS';
        $allPay->l()->Send['LogisticsSubType'] = $pickWay; //或FAMIC2C,全家
        $allPay->l()->Send['IsCollection'] = $isCollection; //是否代收貨款
        $allPay->l()->Send['ServerReplyURL'] = url('mapCallBack'); //超商系統回覆路徑post
        $allPay->l()->Send['ExtraData'] = ''; //附帶資料
        $allPay->l()->Send['Device'] = '0';
        $logisticsForm = $allPay->l()->CvsMap();

        echo $logisticsForm;
    }

    public function mapCallBack(Request $request)
    {

        $ordNo = $request->MerchantTradeNo;
        $MerchantID = $request->MerchantID;
        $LogisticsSubType = $request->LogisticsSubType;
        $CVSStoreID = $request->CVSStoreID;
        $CVSStoreName = $request->CVSStoreName;
        $CVSAddress = $request->CVSAddress;
        $CVSTelephone = $request->CVSTelephone;
        //dd($request->all());
        $order = new OrderMgmtModel();

        $orderData = $order->where('ord_no', $ordNo)->first();

        if (!isset($orderData)) {
            $orderData = $order->where('r_ord_no', $ordNo)->first();
        }

        $updateArray = array(
            'logistics_sub_type' => $LogisticsSubType,
            //'store_type'         => '7-11',
            'store_id' => $CVSStoreID,
            'store_name' => $CVSStoreName,
            'store_addr' => $CVSAddress,
            'store_phone' => $CVSTelephone,
        );

        $order->where('id', $orderData->id)->update($updateArray);

        return redirect()->to('logiPay/'.$orderData->id);
    }

    public function logiPay($ordId)
    {
        $user = Auth::guard('member')->user();
        $orderModel = new orderMgmtModel();

        try {
            $result = $orderModel->processLogiPay($ordId);

            if (!empty($result)) {
                //$this->allPayCheckout($result['ordNo'], $result['ttlAmt'], "goods payment", $result['payWay'], $result['detailItem'], $result['invoiceType'], $result['identifier'], $result['customerTitle'], $result['addr'], $result['dlvPhone'], $result['dlvName'], $result['email']);
                return $this->spgCheckout($result['ordNo'], $result['ttlAmt'], "美z人生旅遊", $result['payWay'], $result['detailItem'], $result['invoiceType'], $result['identifier'], $result['customerTitle'], $result['addr'], $result['dlvPhone'], $result['dlvName'], $result['email'], $result['pickWay']);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            //dd($e->getMessage());
            return redirect()->to('/')->with(['message' => '訂購發生問題，請聯絡客服']);
        }

        //return redirect()->to('/')->with(['message' => '訂購發生問題，請聯絡客服']);
    }

    public function rePayment(Request $request)
    {
        $user = Auth::guard('member')->user();
        $payWay = $request->payWay;
        $ordId = $request->ordId;
        $invoiceType = $request->invoiceType;
        $customerIdentifier = $request->customerIdentifier;
        $customerTitle = $request->customerTitle;
        $prime = $request->prime;
        $agreePayRemeber = $request->agreePayRemeber;

        $ordData = OrderMgmtModel::find($ordId);
        $ordRentDetailData = DB::table('mod_order_rent_detail')->where('ord_id', $ordId)->get();
        $ordDetailData = DB::table('mod_order_detail')->where('ord_id', $ordId)->get();

        $ttlAmt = 0;
        $detailItem = array();

        
        if($payWay == "Credit" && !$this->chkPrime($prime, $user->id)) {
            return redirect()->back()->withErrors(['message' => '信用卡資訊有誤，請確認']);
        }
        else if(($payWay == "GooglePay" || $payWay == "ApplePay" || $payWay == "LinePay" || $payWay == "SamsungPay") && $prime == null) {
            return redirect()->back()->withErrors(['message' => '第三方支付發生問題，請確認付款方式']);
        }
        
        $orderMgmt = new OrderMgmtModel;
        if (isset($ordData)) {
            $ordNo = $ordData->ord_no;
            $addr = $ordData->dlv_zip . ' ' . $ordData->dlv_city . $ordData->dlv_area . $ordData->dlv_addr;
            $dlvPhone = $ordData->dlv_phone;
            $dlvName = $ordData->dlv_nm;
            $email = $ordData->email;
            $shipFee = $ordData->ship_fee;
            $rCode = $ordData->r_code;
            $sCode = $ordData->s_code;
            $pickWay = $ordData->pick_way;

            foreach ($ordRentDetailData as $row) {
                $amt = $row->amt;
                $ttlAmt += $amt;

                $day = $row->use_day;
                $itemPrice = $row->amt;
                $prodName = $row->prod_nm . "(" . $day . "天)";

                $item = array(
                    'Name' => $prodName,
                    'Price' => (int) $itemPrice,
                    'Currency' => "元",
                    'Quantity' => (int) $row->num,
                    'URL' => "dedwed",
                );
                array_push($detailItem, $item);
            }

            if (isset($rCode)) {
                $d = new Discount;
                $dObj = $d->useDiscountForRePay($rCode);
                $num = $this->processDiscount($itemPrice, $dObj);
                $rentDnt = 0;
                if ($num != 0) {
                    //$rentDnt = $rAmt - round($rAmt * $num);
                    $rentDnt = $num;
                    $item = array(
                        'Name' => '優惠碼：' . $rCode . '折扣',
                        'Price' => 0 - $rentDnt,
                        'Currency' => "元",
                        'Quantity' => 1,
                        'URL' => "dedwed",
                    );
                    array_push($detailItem, $item);

                    $ttlAmt = $ttlAmt - $rentDnt;
                }
            }

            foreach ($ordDetailData as $row) {
                $amt = $row->amt;
                $ttlAmt += $amt;

                $item = array(
                    'Name' => $row->prod_nm,
                    'Price' => (int) $row->amt,
                    'Currency' => "元",
                    'Quantity' => (int) $row->num,
                    'URL' => "dedwed",
                );
                array_push($detailItem, $item);
            }

            if (isset($sCode)) {
                $d = new Discount;
                //$num = $d->useDiscountForRePay($sCode);
                $dObj = $d->useDiscountForRePay($sCode);
                $num = $this->processDiscount($row->amt, $dObj);
                $sellDnt = 0;
                if ($num != 0) {
                    //$sellDnt = $rAmt - round($rAmt * $num);
                    $sellDnt = $num;
                    $item = array(
                        'Name' => '優惠碼：' . $sCode . '折扣',
                        'Price' => 0 - $sellDnt,
                        'Currency' => "元",
                        'Quantity' => 1,
                        'URL' => "dedwed",
                    );
                    array_push($detailItem, $item);

                    $ttlAmt = $ttlAmt - $sellDnt;
                }
            }

            if ($shipFee > 0) {
                $item = array(
                    'Name' => '運費',
                    'Price' => (int) $shipFee,
                    'Currency' => "元",
                    'Quantity' => 1,
                    'URL' => "dedwed",
                );
                array_push($detailItem, $item);

                $ttlAmt += $shipFee;
            }

            //$ordNo = $ordNo . 'R' . rand(1000, 9999);

            //$ordData->r_ord_no = $ordNo;
            $ordData->pay_way = $payWay;
            $ordData->save();
            /*
            if ($pickWay == "UNIMARTC2C" || $pickWay == "FAMIC2C" || $pickWay == "HILIFEC2C") {
                return $this->allPayCvsMap($ordNo, $pickWay, 'N');
            } else {
                return $this->spgCheckout($ordNo, $ttlAmt, "美z人生旅遊", $payWay, $detailItem, $invoiceType, $customerIdentifier, $customerTitle, $addr, $dlvPhone, $dlvName, $email, $pickWay);
            }*/
            
            if ($payWay == "Credit") {
                $result = "";
                $remember = false;
                if($agreePayRemeber == "YES") {
                    $remember = true;
                }

                if($prime != null) {
                    $result = $orderMgmt->tapPayByPrime($ordId, $prime, $remember);
                }
                else {
                    $result = $orderMgmt->tapPayByToken($ordId);
                    if($result != 'error') {
                        //return Redirect::to($result);
                        return redirect()->to('/')->with(['message' => '訂購完成']);
                    }
                    
                    return redirect()->to('/')->with(['message' =>'訂購發生問題']);
                }
                

                if($result == "success") {
                    return redirect()->to('/')->with(['message' => '訂購完成']);
                }
                else {
                    return redirect()->to('/')->with(['message' => $result]);
                }
            }
            else if($payWay == "GooglePay" || $payWay == "ApplePay" || $payWay == "LinePay" || $payWay == "SamsungPay") {
                $result = $orderMgmt->tapPayByPrime($ordId, $prime, false);

                if($payWay == 'LinePay' && $result != 'error') {
                    return Redirect::to($result);
                }

                if($result == "success") {
                    return redirect()->to('/')->with(['message' => '訂購完成']);
                }
                else {
                    return redirect()->to('/')->with(['message' => $result]);
                }
            } 
            else {
                $ordNo = $ordNo . 'R' . rand(1000, 9999);
                $ordData->r_ord_no = $ordNo;
                $ordData->save();
                return $this->spgCheckout($ordNo, $ttlAmt, "美z人生旅遊", $payWay, $detailItem, $invoiceType, $customerIdentifier, $customerTitle, $addr, $dlvPhone, $dlvName, $email, $pickWay);
            }

            //return $this->spgCheckout($ordNo, $ttlAmt, "美z人生旅遊", $payWay, $detailItem, $invoiceType, $customerIdentifier, $customerTitle, $addr, $dlvPhone, $dlvName, $email, $pickWay);

        }
    }

    public function allPayCheckout($ordNo, $totalAmount, $TradeDesc, $ChoosePayment, $detailItem, $invoiceType, $customerIdentifier, $customerTitle, $addr, $dlvPhone, $dlvName, $email)
    {
        $allpay = new Allpay();

        //基本參數(請依系統規劃自行調整)
        $allpay->i()->Send['ReturnURL'] = url('allPayReplySchedule');
        $allpay->i()->Send['ClientRedirectURL'] = url('ecpCallback');
        $allpay->i()->Send['OrderResultURL'] = url('allPayReply');
        $allpay->i()->Send['ClientBackURL'] = url('product');
        $allpay->i()->Send['MerchantTradeNo'] = $ordNo; //訂單編號

        $allpay->i()->Send['MerchantTradeDate'] = date('Y/m/d H:i:s'); //交易時間
        $allpay->i()->Send['TotalAmount'] = $totalAmount; //交易金額
        $allpay->i()->Send['TradeDesc'] = $TradeDesc; //交易描述
        $allpay->i()->Send['ChoosePayment'] = $this->GetPaymentWay($ChoosePayment); //付款方式
        //dd($allpay->i()->Send['ChoosePayment']);
        //訂單的商品資料
        for ($i = 0; $i < count($detailItem); $i++) {
            array_push($allpay->i()->Send['Items'], $detailItem[$i]);
        }

        //Go to AllPay

        echo "線上刷卡頁面導向中...";
        # 電子發票參數
        $allpay->i()->Send['InvoiceMark'] = "N"; //是否開立發票
        if (env('APP_ENV') == 'production') {
            $allpay->i()->Send['InvoiceMark'] = "Y"; //是否開立發票
        }

        $allpay->i()->SendExtend['RelateNumber'] = $ordNo; //串連訂單號碼
        if (isset($customerTitle)) {
            $allpay->i()->SendExtend['CustomerName'] = $customerTitle;
        } else {
            $allpay->i()->SendExtend['CustomerName'] = $dlvName;
        }

        $allpay->i()->SendExtend['CustomerEmail'] = $email;
        $allpay->i()->SendExtend['CustomerPhone'] = $dlvPhone;
        if (isset($customerIdentifier)) {
            $allpay->i()->SendExtend['CustomerIdentifier'] = $customerIdentifier;
            $allpay->i()->SendExtend['Print'] = 1;
        }
        $allpay->i()->SendExtend['TaxType'] = '1'; //應稅 1,零稅率 2,免稅 3
        if (isset($addr)) {
            $allpay->i()->SendExtend['CustomerAddr'] = $addr;
        }

        $allpay->i()->SendExtend['InvoiceItems'] = array();
        // 將商品加入電子發票商品列表陣列
        foreach ($allpay->i()->Send['Items'] as $info) {
            array_push($allpay->i()->SendExtend['InvoiceItems'], array('Name' => $info['Name'], 'Count' =>
                $info['Quantity'], 'Word' => '個', 'Price' => $info['Price'], 'TaxType' => '1'));
        }

        //$allpay->i()->SendExtend['InvoiceRemark'] = '測試發票備註';
        $allpay->i()->SendExtend['DelayDay'] = '0'; //延遲N日開啟
        $allpay->i()->SendExtend['InvType'] = '07'; // 一般稅額 07 特種稅額 08

        if ($invoiceType == 'a1') {
            $allpay->i()->SendExtend['Donation'] = '1';
            $allpay->i()->SendExtend['LoveCode'] = '168001';
        }

        //dd($allpay->i()->SendExtend);

        //產生訂單(auto submit至ECPay)

        echo $allpay->i()->CheckOutString();
    }

    private function GetPaymentWay($p)
    {
        $val = "";

        switch ($p) {
            case 'ALL':
                $val = \PaymentMethod::ALL;
                break;
            case 'Credit':
                $val = \PaymentMethod::Credit;
                break;
            case 'CVS':
                $val = \PaymentMethod::CVS;
                break;
            default:
                $val = \PaymentMethod::ALL;
                break;
        }

        return $val;
    }

    public function payinmart(Request $request)
    {

        Allpay::l()->HashKey = '5294y06JbISpM5x9';
        Allpay::l()->HashIV = 'v77hoKGq4kWxNNIS';
        Allpay::l()->Send = array(
            'MerchantID' => '2000132',
            'MerchantTradeNo' => 'no' . date('YmdHis'),
            'MerchantTradeDate' => date('Y/m/d H:i:s'),
            'LogisticsType' => 'CVS',
            'LogisticsSubType' => 'UNIMARTC2C',
            'GoodsAmount' => 1500,
            'CollectionAmount' => 10,
            'IsCollection' => 'Y',
            'GoodsName' => '測試商品',
            'SenderName' => '測試寄件者',
            'SenderPhone' => '0226550115',
            'SenderCellPhone' => '0911222333',
            'ReceiverName' => '測試收件者',
            'ReceiverPhone' => '0226550115',
            'ReceiverCellPhone' => '0933222111',
            'ReceiverEmail' => 'test_emjhdAJr@test.com.tw',
            'TradeDesc' => '測試交易敘述',
            'ServerReplyURL' => 'http://www.yourwebsites.com.tw/ReturnURL',
            'LogisticsC2CReplyURL' => 'http://www.yourwebsites.com.tw/ReturnURL',
            'Remark' => '測試備註',
            'PlatformID' => '',
        );
        Allpay::l()->SendExtend = array(
            'ReceiverStoreID' => '991182',
            'ReturnStoreID' => '991182',
        );
        // BGCreateShippingOrder()
        $this_Result = Allpay::l()->BGCreateShippingOrder();
        echo '<pre>' . print_r($this_Result, true) . '</pre>';
    }
    public function payinmart_reply(Request $request)
    {
        $data = array();
        $data['merchant_trade_no'] = $request->input('MerchantTradeNo'); //訂單編號
        $data['LogisticsSubType'] = $request->input('LogisticsSubType'); //物流通路代碼,如統一:UNIMART
        $data['CVSStoreID'] = $request->input('CVSStoreID'); //商店代碼
        $data['CVSStoreName'] = $request->input('CVSStoreName');
        $data['CVSAddress'] = $request->input('CVSAddress'); //User 所選之超商店舖地址
        $data['CVSTelephone'] = $request->input('CVSTelephone'); //User 所選之超商店舖電話
        $data['ExtraData'] = $request->input('ExtraData'); //額外資訊,原資料回傳
    }

    public function oldcalCartAmt($cartObj, $rentDiscountCode, $sellDiscountCode, $userId)
    {

        $rAmt = 0;
        $sAmt = 0;
        $ttlAmt = 0;

        for ($i = 0; $i < count($cartObj->rData); $i++) {
            $num = $cartObj->rData[$i]->num;
            $startDate = $cartObj->rData[$i]->startDate;
            $endDate = $cartObj->rData[$i]->endDate;
            $dPrice = (int) $cartObj->rData[$i]->dPrice;
            $prodDetailId = $cartObj->rData[$i]->prodDetailId;

            $prodData = DB::table('mod_product_detail')->where('id', $prodDetailId)->first();

            if (isset($prodData)) {
                $dPrice = $prodData->d_price;
            } else {
                return 999999999;
            }

            $day = (((strtotime($cartObj->rData[$i]->endDate) - strtotime($cartObj->rData[$i]->startDate)) / 3600) / 24) + 1;
            $insurance = $cartObj->rData[$i]->insurance;
            $uuid = $cartObj->rData[$i]->uuid;

            $rAmt = $rAmt + ($num * $day * $dPrice);

            if ($insurance == "Y") {
                $rAmt = $rAmt + (50 * $day * $num);
            }

        }

        $rentDiscount = 0;
        if (isset($rentDiscountCode)) {
            $d = new Discount;
            $num = $d->useDiscount($rentDiscountCode, "R", $userId);
            if ($num != 0) {
                $rentDiscount = $num;
                //$rAmt = ceil($rAmt * $num);
            }

            $rAmt = $rAmt - $rentDiscount;
        }

        for ($i = 0; $i < count($cartObj->sData); $i++) {
            $num = $cartObj->sData[$i]->num;
            $dPrice = $cartObj->sData[$i]->dPrice;
            $prodDetailId = $cartObj->sData[$i]->prodDetailId;

            $prodData = DB::table('mod_product_detail')->where('id', $prodDetailId)->first();

            if (isset($prodData)) {
                $dPrice = $prodData->d_price;
            } else {
                return 999999999;
            }

            $sAmt = $sAmt + ($num * $dPrice);
        }

        $sellDiscount = 0;
        if (isset($sellDiscountCode)) {
            $d = new Discount;
            $num = $d->useDiscount($sellDiscountCode, "S", $userId);
            if ($num != 0) {
                $sellDiscount = $num;
                //$sAmt = ceil($sAmt * $num);
            }

            $sAmt = $sAmt - $sellDiscount;
        }

        $ttlAmt = $rAmt + $sAmt;

        return (int) $ttlAmt;
    }

    public function calCartAmt($cartObj, $userId, $periodsselect="")
    {

        $rAmt = 0;
        $sAmt = 0;
        $ttlAmt = 0;
        $d = new Discount;
        for ($i = 0; $i < count($cartObj->rData); $i++) {
            $num = abs($cartObj->rData[$i]->num);
            $startDate = $cartObj->rData[$i]->startDate;
            $endDate = $cartObj->rData[$i]->endDate;
            $dPrice = (int) $cartObj->rData[$i]->dPrice;
            $prodDetailId = $cartObj->rData[$i]->prodDetailId;

            $prodData = DB::table('mod_product_detail')->where('id', $prodDetailId)->first();

            if (isset($prodData)) {
                $dPrice = $prodData->d_price;
            } else {
                return 999999999;
            }

            $day = (((strtotime($cartObj->rData[$i]->endDate) - strtotime($cartObj->rData[$i]->startDate)) / 3600) / 24) + 1;
            $insurance = $cartObj->rData[$i]->insurance;
            $uuid = $cartObj->rData[$i]->uuid;

            $rAmt = $rAmt + ($num * $day * $dPrice);

            if ($insurance == "Y") {
                $rAmt = $rAmt + (50 * $day * $num);
            }

            if(isset($cartObj->rData[$i]->discountCode)) {
                $dObj = $d->useDiscount($cartObj->rData[$i]->discountCode, "R", $userId, $cartObj->rData[$i]->uuid, $cartObj->rData[$i]->prodDetailId);
                $damt = $this->processDiscount($rAmt, $dObj);
                $rAmt = $rAmt - $damt;
            }

        }


        for ($i = 0; $i < count($cartObj->sData); $i++) {
            $num = abs($cartObj->sData[$i]->num);
            $dPrice = $cartObj->sData[$i]->dPrice;
            $prodDetailId = $cartObj->sData[$i]->prodDetailId;

            $prodData = DB::table('mod_product_detail')->where('id', $prodDetailId)->first();

            if (isset($prodData)) {
                $dPrice = $prodData->d_price;
            } else {
                return 999999999;
            }

            $sAmt = $sAmt + ($num * $dPrice);
            
            if(isset($cartObj->sData[$i]->discountCode)) {
                $dObj = $d->useDiscount($cartObj->sData[$i]->discountCode, "S", $userId, $cartObj->sData[$i]->uuid, $cartObj->sData[$i]->prodDetailId);
                $damt = $this->processDiscount($cartObj->sData[$i]->dPrice * $num, $dObj);

                $sAmt = $sAmt - $damt;
            }
        }


        $ttlAmt = $rAmt + $sAmt;
        if($periodsselect != "") {
            $ProdMgmtModel = new ProdMgmtModel();
            $rate = $ProdMgmtModel->getInstalmentRate($periodsselect);

            $ttlAmt = $ttlAmt + round($ttlAmt * $rate);
        }
        
        return (int) $ttlAmt;
    }

    public function spgCheckout($ordNo, $totalAmount, $TradeDesc, $ChoosePayment, $detailItem, $invoiceType, $customerIdentifier, $customerTitle, $addr, $dlvPhone, $dlvName, $email, $pickWay)
    {
        $credit = 0;
        $cvs = 0;
        $cvscom = 0;
        $webatm = 0;
        $vacc = 0;
        $ExpireDate = new \DateTime();
        $ExpireDate->modify('+3 day');

        switch ($ChoosePayment) {
            case 'Credit':
                $credit = 1;
                $cvs = 0;
                $cvscom = 0;
                break;
            case 'CVS':
                $credit = 0;
                $cvs = 1;
                $cvscom = 0;
                break;
            case 'WEBATM':
                $credit = 0;
                $cvs = 0;
                $cvscom = 0;
                $webatm = 1;
                break;
            case 'VACC':
                $credit = 0;
                $cvs = 0;
                $cvscom = 0;
                $webatm = 0;
                $vacc = 1;
                break;
            default:
                $credit = 1;
                $cvs = 0;
                $cvscom = 0;
                $webatm = 0;
                $vacc = 0;
                break;
        }

        $tradArray = [
            'MerchantOrderNo' => $ordNo,
            'CREDIT' => $credit,
            'CVS' => $cvs,
            'CVSCOM' => 0,
            'WEBATM' => $webatm,
            'VACC' => 0,
            'ReturnURL' => url('spgCallBack'),
            'NotifyURL' => url('spgCallBackSchedule'),
            //'CustomerURL' => url('spgCustomerCallBack')
        ];

        if ($ChoosePayment == 'CVS') {
            $tradArray = [
                'MerchantOrderNo' => $ordNo,
                'CREDIT' => 0,
                'CVS' => 1,
                'CVSCOM' => 0,
                'ReturnURL' => url('spgCallBack'),
                'NotifyURL' => url('spgCallBackSchedule'),
                'ExpireDate' => $ExpireDate->format('Ymd'),
                //'CustomerURL' => url('spgCustomerCallBack')
            ];
        }

        \Log::info('CVS Pay');
        \Log::info($tradArray);

        // dd($tradArray);
        $order = MPG::generate(
            $totalAmount,
            $email,
            $TradeDesc,
            $tradArray
        );

        return $order->send();
    }

    public function chkship($pickWay)
    {
        $user = Auth::user();

        $onlyHomeDlv = false;

        $cartData = DB::table('mod_cart')
            ->where('user_id', $user->id)
            ->first();

        $cartContent = json_encode(array('rData' => array(), 'sData' => array()));
        if (isset($cartData)) {
            $cartContent = $cartData->content;
        }

        $cartData = json_decode($cartContent);

        foreach ($cartData->sData as $key => $row) {
            $shipWay = DB::table('mod_product')->where('id', $row->prodId)->value('ship_way');

            if (isset($shipWay)) {
                $shipWayArray = explode(',', $shipWay);

                if (count($shipWayArray) == 1 && in_array('B', $shipWayArray)) {
                    $onlyHomeDlv = true;
                    break;
                }
            }
        }

        return response()->json(['msg' => 'success', 'onlyHomeDlv' => $onlyHomeDlv]);
    }

    public function thirdPartyPayCallBack(Request $request)
    {
        \Log::info('front end url call back');
        \Log::info($request->all());

        if($request->status == 0) {
            $o = new OrderMgmtModel();
            try {
                $ordData = $o::where('ord_no', $request->order_number)->first();
                $o->afterTapPaySuccess($request, $ordData);

                return redirect()->to('/')->with(['message' => '付款完成']);
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
                return redirect()->to('/')->with(['message' => '訂購發生問題，請聯絡客服']);
            }
        }

        return redirect()->to('/')->with(['message' => '訂購發生問題，請聯絡客服']);
    }

    public function thirdPartyPayCallBack1(Request $request)
    {
        \Log::info('Back end url call back');
        \Log::info($request->all());
    }

    public function testCvsMap($isCollection, $pickWay)
    {
        // $allPay = new Allpay();

        // //$allPay->l()->Send['MerchantTradeNo'] = $ordNo;
        // $allPay->l()->Send['LogisticsType'] = 'CVS';
        // $allPay->l()->Send['LogisticsSubType'] = $pickWay; //或FAMIC2C,全家
        // $allPay->l()->Send['IsCollection'] = $isCollection;//是否代收貨款
        // $allPay->l()->Send['ServerReplyURL'] = url('mapCallBack'); //超商系統回覆路徑post
        // $allPay->l()->Send['ExtraData'] = ''; //附帶資料
        // $allPay->l()->Send['Device'] = '0';
        // $logisticsForm = $allPay->l()->CvsMap();

        // echo $logisticsForm;

        $viewData = array(
            'MerchantID' => config('ecpay.MerchantID'),
            'LogisticsType' => 'CVS',
            'LogisticsSubType' => $pickWay,
            'IsCollection' => $isCollection,
            'ServerReplyURL' => url('testMapCallBack'),
        );

        return view('FrontEnd.cvsMap')->with($viewData);
    }

    public function testMapCallBack(Request $request)
    {

        return view('FrontEnd.mapCallBack')->with($request->all());

    }

    public function chkPrime($prime, $userId) {
        $m = new MemberMgmtModel();
        $member = $m->getMemberData($userId);

        if(isset($member)) {
            if($prime == null && $member->card_token != null) {
                return true;
            }
            else if($prime != null && $member->card_token == null) {
                return true;
            }
        }

        return;
    }

    public function getShipFee($shipper) {
        $user = Auth::user();
        $data = DB::table('bscode')
                    ->where('cd_type', 'SHIP_FEE')
                    ->where('cd', $shipper)
                    ->first();

        $amt = 0;

        if(isset($data)) {
            $amt = (int)$data->value1;
        }

        $cartData = DB::table('mod_cart')->where('user_id', $user->id)->first();
        if(isset($cartData)) {
            $cartObj = json_decode($cartData->content);
            $serverAmt = $this->calCartAmt($cartObj, $user->id);
            $bscodeModel = new BscodeModel();
            $freeAmt = $bscodeModel->getShipFree();

            if($serverAmt > $freeAmt) {
                $amt = 0;
            }            
        }
        


        return response()->json(['amt' => $amt]);
    }

    public function processDiscount($amt, $obj) {
        $dway = $obj['discountWay'];
        $dnum = $obj['num'];

        if($dway == "C") {
            return $dnum;
        }
        else if($dway == "P") {
            return $amt - round($amt * ($dnum/100));
        }

        return 0;
    }

    public function getCart() {
        $user = Auth::user();
        $cartData = DB::table('mod_cart')->where('user_id', $user->id)->first();
        $obj = null;

        if(isset($cartData)) {
            $obj = json_decode($cartData->content);
        }

        return response()->json($obj);
    }

}
