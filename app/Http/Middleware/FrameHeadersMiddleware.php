<?php

namespace App\Http\Middleware;

use Closure;

class FrameHeadersMiddleware
{

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('X-Frame-Options', 'ALLOW FROM https://www.facebook.com/');
        return $response;
    }
}
