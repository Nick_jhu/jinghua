<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    protected $mobileMenu = array();
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            Schema::defaultStringLength(191);
            DB::listen(function ($query) {
                //\Log::info($query->sql);
                //\Log::info($query->time);
                //\Log::info($query->bindings);
                //dd($query->sql);
                // $query->bindings
                // $query->time
            });
            
            if(env('APP_ENV') == 'production') {
                \URL::forceScheme('https');
            }
            

            $footerProdData = DB::table('mod_product')->where('is_added', 'Y')->whereNotIn('prod_type', ['S','W'])->orderBy('created_at', 'desc')->limit(6)->get();
            $baseData = DB::table('mod_base')->where('id', '1')->first();

            $menuData = DB::table('mod_cate')->where('parent_id', 77)->get();
            $d = DB::table('mod_cate')->select('id', 'name', 'parent_id')->where('parent_id', '>', 77)->orderBy('id', 'asc')->get()->toArray();
            $mobileMenu = array();
            foreach($menuData as $row) {
                $mobileMenu[$row->id] = $this->buildTree($d, $row->id);
            }

            //$this->categoryTree(77);
            //dd($this->mobileMenu);
            //exit;

            $marqueeData = DB::table('mod_Marquee')->get();

            View::share(['footerProdData' => $footerProdData, 'baseData' => $baseData, 'menuData' => $menuData, 'marqueeData' => $marqueeData, 'mobileMenu' => $mobileMenu]);
    }

    public function buildTree(array $elements, $parentId = 0) {
    
        $branch = array();
    
        foreach ($elements as $element) 
        {
            if ($element->parent_id == $parentId) 
            {
                $children = $this->buildTree($elements, $element->id);
                
                if ($children) 
                {
                    $element->children = $children;
                }
                
                $branch[] = $element;
            }
        }
    
        return $branch;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    
}
