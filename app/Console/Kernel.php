<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use Crypt;
use Shorty;
use App\Models\OrderMgmtModel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            \Log::info("schedule start");

            $today = date('Y-m-d H:i:s');

            $ordData = DB::table('mod_order')->where('status','B')->where('pick_way', 'A')->get(); 
            $ordMgmt = new OrderMgmtModel();
            foreach($ordData as $row) {
                $pickUpDate = $row->pick_up_date;
    
                $day = strtotime($pickUpDate) - strtotime($today);
    
                $day = rand($day / 3600 / 24);
    
                if($day == 2) {
                    $ordMgmt->sendQrCode($row->ord_no, $row->dlv_phone);
                }
            }
            

            \Log::info("schedule end");

        })->dailyAt('09:00');

        /*
        $schedule->call(function () {
            \Log::info("wifi return schedule start");

            $orderMgmtModel = new OrderMgmtModel();

            try {
                $orderMgmtModel->updateWifiStatus();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }

            

            \Log::info("wifi return schedule end");

        })->dailyAt('00:00');*/


        $schedule->call(function () {
            \Log::info("Payment Reminder schedule start");

            $orderMgmtModel = new OrderMgmtModel();

            try {
                $orderMgmtModel->paymentReminderNotice();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }

            \Log::info("Payment Reminder schedule end");

        })->dailyAt('08:30');

        $schedule->call(function () {
            \Log::info("Auto Cancel Order schedule start");

            $orderMgmtModel = new OrderMgmtModel();

            try {
                $orderMgmtModel->autoCancelOrder();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }

            \Log::info("Auto Cancel Order schedule end");

        })->dailyAt('01:00');

        $schedule->call(function () {
            \Log::info("Auto change status for home ship schedule start");

            $orderMgmtModel = new OrderMgmtModel();

            try {
                $orderMgmtModel->changeStatusForHomeShip();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }

            \Log::info("Auto change status for home ship schedule end");

        })->dailyAt('11:00');

        $schedule->call(function () {
            \Log::info("Auto createdInv start");
            $orderMgmtModel = new OrderMgmtModel();
            try {
                $orderMgmtModel->autocreateInvoice();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }
            
            \Log::info("Auto createdInv end");
    
        })->dailyAt('12:00');

        $schedule->call(function () {
            $orderMgmtModel = new OrderMgmtModel();
            \Log::info("Send mail schedule_hour start");
            try {
                $orderMgmtModel->sendmail_hour();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }
            \Log::info("Send mail schedule_hour end");

        })->hourly();


        $schedule->call(function () {
            $PordMgmtModel = new PordMgmtModel();
            \Log::info("Update action price start");
            try {
                $PordMgmtModel->updateactionprice();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }
            \Log::info("Update action price end");

        })->daily();
        
        $schedule->call(function () {
            $orderMgmtModel = new OrderMgmtModel();
            \Log::info("Send mail schedule_five start");
            try {
                $orderMgmtModel->sendmail_five();
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
            }
            \Log::info("Send mail schedule_five end");
    
        })->everyFiveMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
