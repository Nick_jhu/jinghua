<?php

namespace App\Models;

use App\Mail\OrderShipped;
use Backpack\CRUD\CrudTrait;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class ModSnModel extends Model
{

    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */

    protected $table = 'mod_sn';
    // protected $primaryKey = 'cust_no';
    // protected $guarded = [];
    // protected $hidden = ['id'];
    protected $guarded = array('id');
    public $timestamps = true;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    public function sendEmail($orderData, $detailData)
    {

        $sendData = array();

        $sendData = [
            'name' => $orderData->dlv_nm,
            'prodName' => $detailData->prod_nm,
            'prodNo' => $detailData->prod_no,
            'num' => $detailData->num,
            'sn' => array(),
        ];

        $sn = DB::table('mod_sn')
            ->where('ord_detail_id', $detailData->id)
            ->get();

        $snIdStr = "";
        $snNoStr = "";
        foreach ($sn as $row) {
            $snIdStr .= $row->id . ',';
            $snNoStr .= $row->sn . ',';
            array_push($sendData['sn'], $row->sn);
        }

        $snIdStr = mb_substr($snIdStr, 0, -1);
        $snNoStr = mb_substr($snNoStr, 0, -1);

        if (!empty($orderData->email) && count($sn) > 0) {
            Mail::to($orderData->email)
                ->bcc('sales@standard-info.com')
                ->send(new OrderShipped($sendData));

            foreach ($sn as $row) {
                $this->where('id', $row->id)
                    ->update([
                        'is_sell' => 'Y',
                    ]);
            }

            DB::table('mod_order_detail')
                ->where('id', $detailData->id)
                ->update([
                    'sn_id' => $snIdStr,
                    'sn_no' => $snNoStr,
                ]);
        }

    }

    public function reSendEmail($orderDetailId)
    {
        $detailData = DB::table('mod_order_detail')
            ->where('id', $orderDetailId)
            ->first();

        if (isset($detailData)) {

            $orderData = DB::table('mod_order')
                ->select('ord_nm', 'email')
                ->where('id', $detailData->ord_id)
                ->first();

            $snIdStr = $detailData->sn_id;

            $snArray = explode(',', $snIdStr);

            $sendData = array();

            $sendData = [
                'name' => $orderData->dlv_nm,
                'prodName' => $detailData->prod_nm,
                'prodNo' => $detailData->prod_no,
                'num' => $detailData->num,
                'sn' => array(),
            ];

            $sn = DB::table('mod_sn')
                ->whereIn('id', $snArray)
                ->get();

            foreach ($sn as $row) {
                array_push($sendData['sn'], $row->sn);
            }

            if (!empty($orderData->email) && count($sn) > 0) {
                DB::table('sys_email')->insert([
                    'tpl' => 'OrderShipped',
                    'status' => 'N',
                    'schedule' =>'5',
                    'content' =>json_encode($sendData),
                    'to' => $orderData->email,
                    'bcc' => 'sales@standard-info.com',
                    'created_by' => $user->email,
                    'updated_by' => $user->email,
                    'created_at' => \Carbon::now(),
                    'updated_at' => \Carbon::now(),
                ]);
                // Mail::to($orderData->email)
                //     ->bcc('sales@standard-info.com')
                //     ->send(new OrderShipped($sendData));
            }
        }

    }

    public function getSn($orderDetailId)
    {
        $snData = DB::table('mod_sn')
            ->where('ord_detail_id', $orderDetailId)
            ->get();

        $sendData = array();
        if (isset($snData)) {

            foreach ($snData as $row) {
                array_push($sendData, $row->sn);
            }
		} 
		
		if(count($sendData) == 0) {
			$detailData = DB::table('mod_order_detail')
                ->where('id', $orderDetailId)
                ->first();

            if (isset($detailData)) {

                $snIdStr = $detailData->sn_id;

                $snArray = explode(',', $snIdStr);

                $sn = DB::table('mod_sn')
                    ->whereIn('id', $snArray)
                    ->get();

                foreach ($sn as $row) {
                    array_push($sendData, $row->sn);
                }
            }
		}

        return $sendData;
    }

    public function tempSell($orderData, $detailData)
    {

        $sn = DB::table('mod_sn')
            ->where('prod_no', $detailData->prod_no)
            ->where('is_sell', 'N')
            ->limit($detailData->num)
            ->get();

        foreach ($sn as $row) {
            $this->where('id', $row->id)
                ->update([
                    'ord_detail_id' => $detailData->id,
                    'is_sell' => 'T',
                    'member_id' => $orderData->user_id,
                    'email' => $orderData->email,
                ]);
        }

        return;

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
     */

    /*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
 */
}
