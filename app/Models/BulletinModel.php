<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class BulletinModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'bulletin';
	// protected $primaryKey = 'id';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $fillable = ['title','public_date','description','created_by','updated_by','g_key','c_key','s_key','d_key'];
	public $timestamps = true;


	public function parent()
    {
        return $this->belongsTo('Backpack\MenuCRUD\app\Models\MenuItem', 'city');
	}
	public function tags()
    {
        return $this->belongsToMany('Backpack\MenuCRUD\app\Models\MenuItem', 'city');
	}
	public function setCustDateAttribute($value) {
        $this->attributes['cust_date'] = \Date::parse($value);
    }
	public function setCustFileAttribute($value)
    {
        $attribute_name = "cust_file";
		$disk = "uploadslocal";
        $destination_path = "uploads";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
	}
	
	protected $casts = [
        'cust_file' => 'array'
    ];
	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	public function articles()
    {
        return $this->hasMany('App\Models\Article', 'article_tag');
    }

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}