<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use DB;
use Carbon\Carbon;

class ProdGiftModel extends Model 
{

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_gift';
	protected $guarded = array('id');
	public $timestamps = true;

}