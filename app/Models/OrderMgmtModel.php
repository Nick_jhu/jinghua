<?php

namespace App\Models;

use Allpay;
use App\Mail\NormalOrderShipped;
use App\Mail\PaymentReminderNotice;
use App\Mail\WifiReturnNotice;
use App\Models\CommonModel;
use App\Models\Discount;
use App\Models\ModSnModel;
use App\Models\MemberMgmtModel;
use App\Models\ProdDetailModel;
use App\Models\OrderDetailModel;
use Backpack\CRUD\CrudTrait;
use Crypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use LeoChien\Spgateway\Facades\MPG;
use Shorty;

class OrderMgmtModel extends Model
{

    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */

    protected $table = 'mod_order';
    // protected $primaryKey = 'cust_no';
    // protected $guarded = [];
    // protected $hidden = ['id'];
    protected $guarded = array('id');
    public $timestamps = true;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    public function getOrderByMemberId($userId) {
        return $this->where('user_id', $userId)
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                    ->first();
    }

    public function getCartId($userId) {
        return DB::table('mod_cart')->where('user_id', $userId)->value('id');
    }

    public function insertOrder($mainData)
    {
        $this->user_id = 0;
        $this->ord_no = $mainData['ord_no'];
        $this->status = 'A';
        $this->o_price = $mainData['o_price'];
        $this->d_price = $mainData['d_price'];
        $this->ord_nm = $mainData['ord_nm'];
        $this->dlv_nm = $mainData['dlv_nm'];
        $this->dlv_phone = $mainData['dlv_phone'];
        $this->num = $mainData['num'];
        $this->normal_detail_descp = $mainData['prod_nm'];
        $this->dlv_addr = $mainData['dlv_addr'];
        $this->pay_way = $mainData['pay_way'];
        $this->email = $mainData['email'];
        $this->ship_fee = $mainData['ship_fee'];
        $this->cust_remark = $mainData['cust_remark'];
        $this->g_key = 'STD';
        $this->c_key = 'STD';
        $this->s_key = 'STD';
        $this->d_key = 'STD';
        $this->save();

        $mainRentDate = null;
        $mainReturnDate = null;
        $mainRentDetailProds = '';
        $mainDetailProds = '';
        $ordId = $this->id;
        $this->save();
        return $this->id;
    }

    public function chkAmt()
    {

    }

    public function updateOrderTmp()
    {
        $orderData = DB::table('mod_order')->get();
        foreach ($orderData as $row) {
            $d = null;
            $r = null;
            $dd = '';
            $r = DB::table('mod_order_rent_detail')->where('ord_id', $row->id)->get();

            foreach ($r as $key => $v) {
                if ($key == 0) {
                    $d = $v->f_day;
                    $dd = $v->prod_nm;
                    $r = $v->e_day;
                } else {
                    if (strtotime($v->f_day) < strtotime($d)) {
                        $d = $v->f_day;
                    }

                    if (strtotime($v->e_day) > strtotime($r)) {
                        $r = $v->e_day;
                    }

                    $dd = $dd . ',' . $v->prod_nm;
                }
            }

            DB::table('mod_order')->where('id', $row->id)->update([
                'pick_up_date' => $d,
                'return_date' => $r,
                'detail_descp' => $dd,
            ]);
        }
    }

    public function test()
    {
        $today = date('Y-m-d H:i:s');

        $ordData = DB::table('mod_order')->where('status', 'B')->get();

        foreach ($ordData as $row) {
            $pickUpDate = $row->pick_up_date;

            $day = strtotime($pickUpDate) - strtotime($today);

            $day = floor($day / 3600 / 24);

            if ($day == 2) {
                $url = 'http://test.z-trip.com/orderQrCode/' . Crypt::encrypt($row->ord_no);
                echo $url;
                $url = Shorty::shorten($url);

                $mitake = app(\Mitake\Client::class);
                $message = (new \Mitake\Message\Message())
                    ->setDstaddr($row->dlv_phone)
                    ->setSmbody('感謝您選擇Z-TRIP.COM出國商品，請於兩天後到機場櫃位出示連結內的訂購資訊領取機器:' . $url);
                $result = $mitake->send($message);
                echo $row->ord_no . '\n';
            }
        }
    }

    public function proccessEcpCallBak($data)
    {
        $ordNo = explode('R', $data['MerchantTradeNo']);
        DB::table('mod_order')
            ->where('ord_no', $ordNo[0])
            ->update([
                'pay_no' => $data['TradeNo'],
                'pay_date' => $data['PaymentDate'],
                'status' => 'B',
            ]);

        $orderData = DB::table('mod_order')
            ->where('ord_no', $ordNo[0])
            ->first();
        $user = null;
        if (isset($orderData)) {
            $user = DB::table('mod_member')->where('id', $orderData->user_id)->first();
            if (isset($user)) {
                DB::table('mod_cart')
                    ->where('user_id', $user->id)
                    ->delete();
            }
        }

        unset($_COOKIE['cartNum']);
        setcookie('cartNum', 0, -1, '/');
        $sample_arr = array();
        if (isset($orderData)) {

            if (isset($user) && env('APP_ENV') == "production") {
                $smsMsg = "感謝您選購Z-TRIP.COM商品，我們已收到您款項" . number_format($orderData->d_price) . "，以下連結可查看訂單:" . url('center');
                CommonModel::sendSmsMsg($smsMsg, $user->cellphone);
            }

            $orderRentData = DB::table('mod_order_rent_detail')
                ->where('ord_id', $orderData->id)
                ->get();

            foreach ($orderRentData as $key => $row) {
                array_push($sample_arr, array(
                    "ORDERNO" => $orderData->ord_no,
                    "NAME" => $orderData->dlv_nm,
                    "TEL" => $orderData->dlv_phone,
                    "QTY" => $row->num,
                    "DATE1" => substr($row->f_day, 0, 10),
                    "TERMINAL1" => (isset($orderData->departure_terminal) || $orderData->departure_terminal != "") ? $orderData->departure_terminal : "", //T1 or T2
                    "TIME1" => $orderData->pick_time,
                    "DATE2" => substr($row->e_day, 0, 10),
                    "TERMINAL2" => (isset($orderData->return_terminal) || $orderData->return_terminal != "") ? $orderData->return_terminal : "", //T1 or T2
                    "TIME2" => $orderData->return_time,
                    "EMAIL" => $orderData->email,
                    "COUNTRY" => "",
                    "REMARKS" => "訂單號：" . $orderData->ord_no,
                ));
            }

            if (count($orderRentData) > 0 && $orderData->pick_way == "A") {
                //dd($sample_arr);
                /*$result = app('App\Http\Controllers\SoapController')->createOrder($sample_arr);

                if (!isset($result)) {
                    \Log::error("機場api沒成功");
                    \Log::error($result);
                }*/
            }

            $orderSellData = DB::table('mod_order_detail')
                ->where('ord_id', $orderData->id)
                ->get();
            foreach ($orderSellData as $key => $row) {
                $prodType = DB::table('mod_product')->where('prod_no', $row->prod_no)->value('prod_type');

                if ($prodType == 'S') {
                    \Log::info("發送序號email");
                    $modSn = new ModSnModel;
                    $modSn->sendEmail($orderData, $row);
                }
            }
        }
    }

    public static function spg()
    {
        $order = MPG::generate(
            100,
            'leo@hourmasters.com',
            '測試商品',
            [
                'MerchantOrderNo' => 'ord123456',
                'CREDIT' => 1,

            ]
        );

        dd($order);

        $order->send();
    }

    public function spgCallBack($tradeInfo)
    {
        $msg = 'success';
        // dd($tradeInfo);
        if ($tradeInfo->Status == 'SUCCESS') {
            $ordNo = explode('R', $tradeInfo->Result->MerchantOrderNo);
            $updateArray = array();
            $sn_no = array();
			$ordStatus = DB::table('mod_order')->where('ord_no', $ordNo[0])->value('status');

			unset($_COOKIE['cartNum']);
			setcookie('cartNum', 0, -1, '/');

            $orddata = DB::table('mod_order')->where('ord_no', $ordNo[0])->first();

            for($i = 0; $i < $orddata->num;$i++){
                $sn_no[$i]=strtoupper(str_random(8));
            }
            if ($ordStatus == "A") {

                $updateArray = [
                    'pay_no' => $tradeInfo->Result->TradeNo,
                    'status' => 'B',
                    'pay_date' => $tradeInfo->Result->PayTime,
                    'remark' => json_encode($sn_no),
                    //'store_type' => (isset($tradeInfo->Result->StoreType))?$tradeInfo->Result->StoreType:null,
                    //'store_id'   => (isset($tradeInfo->Result->StoreCode))?$tradeInfo->Result->StoreCode:null,
                    //'store_name' => (isset($tradeInfo->Result->StoreName))?$tradeInfo->Result->StoreName:null,
                    //'store_addr' => (isset($tradeInfo->Result->StoreAddr))?$tradeInfo->Result->StoreAddr:null,
                ];

                if ($tradeInfo->Result->PaymentType == "CREDIT") {
                    $updateArray['status'] = 'B';
                }
                else if($tradeInfo->Result->PaymentType == 'WEBATM' || $tradeInfo->Result->PaymentType == 'VACC') {
                    $updateArray = [
                        'pay_no'              => $tradeInfo->Result->TradeNo,
                        'status'              => 'B',
                        'pay_date'            => $tradeInfo->Result->PayTime,
                        'pay_bank_code'       => $tradeInfo->Result->PayBankCode,
                        'payer_account_5code' => $tradeInfo->Result->PayerAccount5Code,
                        'remark' => json_encode($sn_no),
                    ];
                }
                // dd($updateArray);
                DB::table('mod_order')
                    ->where('ord_no', $ordNo[0])
                    ->update($updateArray);
                for($i = 0; $i < sizeof($sn_no);$i++){
                    DB::table('sys_sn')->insert([
                        'ord_no' => $orddata->ord_no,
                        'ord_nm' => $orddata->dlv_nm,
                        'ord_phone' =>$orddata->dlv_phone,
                        'prod_nm' =>$orddata->normal_detail_descp,
                        'sn_no' => $sn_no[$i],
                        'created_at' => \Carbon::now(),
                    ]);
                }
                $orderData = DB::table('mod_order')
                    ->where('ord_no', $ordNo[0])
                    ->first();           
                $user = null;
                // if (isset($orderData)) {
                //     $user = DB::table('mod_member')->where('id', $orderData->user_id)->first();
                //     if (isset($user)) {
                //         DB::table('mod_cart')
                //             ->where('user_id', $user->id)
                //             ->delete();
                //     }

                // }

                $sample_arr = array();
                if (isset($orderData)) {

                    // if (isset($user) && env('APP_ENV') == 'production') {
                    //     $smsMsg = "感謝您選購Z-TRIP.COM商品，我們已收到您款項" . number_format($orderData->d_price) . "，以下連結可查看訂單:" . url('center');
                    //     CommonModel::sendSmsMsg($smsMsg, $user->cellphone);
                    // }

                    // $orderRentData = DB::table('mod_order_rent_detail')
                    //     ->where('ord_id', $orderData->id)
                    //     ->get();

                    // foreach ($orderRentData as $key => $row) {
                    //     array_push($sample_arr, array(
                    //         "ORDERNO" => $orderData->ord_no,
                    //         "NAME" => $orderData->dlv_nm,
                    //         "TEL" => $orderData->dlv_phone,
                    //         "QTY" => $row->num,
                    //         "DATE1" => substr($row->f_day, 0, 10),
                    //         "TERMINAL1" => (isset($orderData->departure_terminal) || $orderData->departure_terminal != "") ? $orderData->departure_terminal : "", //T1 or T2
                    //         "TIME1" => $orderData->pick_time,
                    //         "DATE2" => substr($row->e_day, 0, 10),
                    //         "TERMINAL2" => (isset($orderData->return_terminal) || $orderData->return_terminal != "") ? $orderData->return_terminal : "", //T1 or T2
                    //         "TIME2" => $orderData->return_time,
                    //         "EMAIL" => $orderData->email,
                    //         "COUNTRY" => "",
                    //         "REMARKS" => "訂單號：" . $orderData->ord_no,
                    //     ));
                    // }

                    if (count($orderRentData) > 0 && ($orderData->pick_way == "A" || $orderData->return_way == "A") && env('APP_ENV') == 'production') {

                        /*$result = app('App\Http\Controllers\SoapController')->createOrder($sample_arr);

                        if (!isset($result)) {
                            \Log::error("機場api沒成功");
                            \Log::error($result);
                        }*/
                    }

                    // $orderSellData = DB::table('mod_order_detail')
                    //     ->where('ord_id', $orderData->id)
                    //     ->get();

                    // $softCnt = 0;
                    // $normalCnt = 0;
                    // foreach ($orderSellData as $key => $row) {
                    //     $prodType = DB::table('mod_product')->where('prod_no', $row->prod_no)->value('prod_type');

                    //     if ($prodType == 'S') {
                    //         \Log::info("發送序號email");
                    //         $modSn = new ModSnModel;
                    //         $modSn->sendEmail($orderData, $row);
                    //         $softCnt++;
                    //     } else {
                    //         $normalCnt++;
                    //     }
                    // }

                    //訂單內如果只有存購買軟體序號時，在付完款後Email序後發送完畢後，訂單狀態改成已送達
                    // if ($softCnt > 0 && $normalCnt == 0 && count($orderRentData) == 0) {
                    //     DB::table('mod_order')
                    //         ->where('ord_no', $ordNo[0])
                    //         ->update(['status' => 'D']);
                    // }

                    if (isset($orderData->AllPayLogisticsID)) {
                        $shipmentNo = $this->QueryLogisticsInfo($orderData->id, $orderData->ord_no, $orderData->AllPayLogisticsID);
                        $this->updateOrdShipmentNo($orderData->id, $shipmentNo);
                    }

                    //$this->createInvoice($orderData);
                }
            }
        } else {
            $msg = "fail";
        }

        return $msg;
    }

    public function delCart()
    {
        $user = Auth::guard('member')->user();

        if (isset($user)) {
            DB::table('mod_cart')
                ->where('user_id', $user->id)
                ->delete();
        }

        unset($_COOKIE['cartNum']);
        setcookie('cartNum', 0, -1, '/');

        return;
    }

    public function processLogiPay($ordId)
    {
        $msg = 'success';
        $orderData = $this::find($ordId);

        $ordRentDetailData = DB::table('mod_order_rent_detail')->where('ord_id', $ordId)->get();
        $ordDetailData = DB::table('mod_order_detail')->where('ord_id', $ordId)->get();
        $ttlAmt = 0;
        $detailItem = array();
        if (isset($orderData)) {
            $payWay = $orderData->pay_way;
            $ordNo = (isset($orderData->r_ord_no)) ? $orderData->r_ord_no : $orderData->ord_no;
            $addr = $orderData->dlv_zip . ' ' . $orderData->dlv_city . $orderData->dlv_area . $orderData->dlv_addr;
            $dlvPhone = $orderData->dlv_phone;
            $dlvName = $orderData->dlv_nm;
            $email = $orderData->email;
            $shipFee = $orderData->ship_fee;
            $rCode = $orderData->r_code;
            $sCode = $orderData->s_code;
            $identifier = $orderData->identifier;
            $customerTitle = $orderData->customer_title;
            $invoiceType = $orderData->invoice_type;
            $pickWay = $orderData->pick_way;

            foreach ($ordRentDetailData as $row) {
                $amt = $row->amt;
                $ttlAmt += $amt;

                $day = $row->use_day;
                $itemPrice = $row->amt;
                $prodName = $row->prod_nm . "(" . $day . "天)";

                $item = array(
                    'Name' => $prodName,
                    'Price' => (int) $itemPrice,
                    'Currency' => "元",
                    'Quantity' => (int) $row->num,
                    'URL' => "dedwed",
                );
                array_push($detailItem, $item);
            }

            if (isset($rCode)) {
                $d = new Discount;
                $num = $d->useDiscountForRePay($rCode);
                $rentDnt = 0;
                if ($num != 0) {
                    //$rentDnt = $rAmt - round($rAmt * $num);
                    $rentDnt = $num;
                    $item = array(
                        'Name' => '優惠碼：' . $rCode . '折扣',
                        'Price' => 0 - $rentDnt,
                        'Currency' => "元",
                        'Quantity' => 1,
                        'URL' => "dedwed",
                    );
                    array_push($detailItem, $item);

                    $ttlAmt = $ttlAmt - $rentDnt;
                }
            }

            foreach ($ordDetailData as $row) {
                $amt = $row->amt;
                $ttlAmt += $amt;

                $item = array(
                    'Name' => $row->prod_nm,
                    'Price' => (int) $row->amt,
                    'Currency' => "元",
                    'Quantity' => (int) $row->num,
                    'URL' => "dedwed",
                );
                array_push($detailItem, $item);
            }

            if (isset($sCode)) {
                $d = new Discount;
                $num = $d->useDiscountForRePay($sCode);
                $sellDnt = 0;
                if ($num != 0) {
                    //$sellDnt = $rAmt - round($rAmt * $num);
                    $sellDnt = $num;
                    $item = array(
                        'Name' => '優惠碼：' . $sCode . '折扣',
                        'Price' => 0 - $sellDnt,
                        'Currency' => "元",
                        'Quantity' => 1,
                        'URL' => "dedwed",
                    );
                    array_push($detailItem, $item);

                    $ttlAmt = $ttlAmt - $sellDnt;
                }
            }

            if ($shipFee > 0) {
                $item = array(
                    'Name' => '運費',
                    'Price' => (int) $shipFee,
                    'Currency' => "元",
                    'Quantity' => 1,
                    'URL' => "dedwed",
                );
                array_push($detailItem, $item);

                $ttlAmt += $shipFee;
            }

            return [
                'ordNo' => $ordNo,
                'ttlAmt' => (int) $orderData->d_price,
                'payWay' => $payWay,
                'detailItem' => $detailItem,
                'invoiceType' => $invoiceType,
                'identifier' => $identifier,
                'customerTitle' => $customerTitle,
                'addr' => $addr,
                'dlvPhone' => $dlvPhone,
                'dlvName' => $dlvName,
                'email' => $email,
                'pickWay' => $pickWay,
            ];
        }

        return array();
    }

    public function sendQrCode($ordNo, $phone)
    {
        $url = url('orderQrCode') . '/' . Crypt::encrypt($ordNo);
        $url = Shorty::shorten($url);

        $mitake = app(\Mitake\Client::class);
        $message = (new \Mitake\Message\Message())
            ->setDstaddr($phone)
            ->setSmbody('感謝您選擇Z-TRIP.COM旅遊商品，請於機場櫃位出示連結內的訂購資訊領取機器:' . $url);
        $result = $mitake->send($message);
    }

    public function cancelPay($id)
    {
        $msg = array('Status' => 'ERROR');
        $ordMgmt = $this::find($id);

        $merchantId = config('spgateway.mpg.MerchantID');
        $hashKey = config('spgateway.mpg.HashKey');
        $hashIv = config('spgateway.mpg.HashIV');

        if (isset($ordMgmt)) {

            if ($ordMgmt->pay_way != 'Credit') {
                return array(
                    'Status' => 'ERROR',
                    'Message' => '信用卡付款的才能申請退款',
                );
            }

            $checkValue = $this->getChkValue($ordMgmt, $merchantId, $hashKey, $hashIv);

            $result = $this->queryOrderForSp($ordMgmt, $merchantId, $hashKey, $hashIv, $checkValue);

            if ($result->Status == 'SUCCESS') {
                $isAuth = true;
                if ($result->Result->CloseStatus == 0) {
                    $isAuth = false;
                }

                $msg = $this->applyReturnPay($ordMgmt, $merchantId, $hashKey, $hashIv, $isAuth);
            } else {
                \Log::error($result);
                return $result;
            }
        }

        return $msg;
    }

    public function tpaPayCancelPay($id) {
        $msg = $this->tapPayReturnPay($id);

        return $msg;
    }

    public function queryPay($id)
    {
        $msg = array('Status' => 'ERROR');
        $ordMgmt = $this::find($id);

        $merchantId = config('spgateway.mpg.MerchantID');
        $hashKey = config('spgateway.mpg.HashKey');
        $hashIv = config('spgateway.mpg.HashIV');

        if (isset($ordMgmt)) {

            $checkValue = $this->getChkValue($ordMgmt, $merchantId, $hashKey, $hashIv);

            $result = $this->queryOrderForSp($ordMgmt, $merchantId, $hashKey, $hashIv, $checkValue);

            dd($result);
        }

        return $msg;
    }

    private function getChkValue($ordMgmt, $merchantId, $hashKey, $hashIv)
    {
        $check_code = array(
            "MerchantID" => $merchantId,
            "Amt" => (int) $ordMgmt->d_price,
            "MerchantOrderNo" => (isset($ordMgmt->r_ord_no)) ? $ordMgmt->r_ord_no : $ordMgmt->ord_no,
        );
        ksort($check_code);
        $check_str = http_build_query($check_code);
        $CheckCode_str = "IV=$hashIv&$check_str&Key=$hashKey";

        $CheckCode = strtoupper(hash("sha256", $CheckCode_str));

        return $CheckCode;
    }

    private function applyReturnPay($ordMgmt, $merchantId, $hashKey, $hashIv, $isAuth)
    {
        \Log::info('Cancel Pay');
        $orderNo = (isset($ordMgmt->r_ord_no)) ? $ordMgmt->r_ord_no : $ordMgmt->ord_no;
        $post_data_str = 'RespondType=JSON&Version=1.0&Amt=' . (int) $ordMgmt->d_price . '&MerchantOrderNo=' . $orderNo . '&TimeStamp=' . time() . '&IndexType=1&TradeNo=' . $ordMgmt->pay_no . '&CloseType=2';
        $post_data = trim(bin2hex(openssl_encrypt($this->addpadding($post_data_str), "aes-256-cbc", $hashKey, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $hashIv))); //加密

        $ch = curl_init();
        $postUrl = config('spgateway.mpg.CloseURL');
        //$postUrl = config('spgateway.mpg.CancelURL');
        if ($isAuth == false) {
            $postUrl = config('spgateway.mpg.CancelURL');
            \Log::info('Card Auth:' . $isAuth);
        }

        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'MerchantID_=' . $merchantId . '&PostData_=' . $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($server_output);

        if ($result->Status == 'SUCCESS') {
            $this->invalidInvoice($ordMgmt->inv_no);
        }

        return $result;

    }

    private function tapPayReturnPay($ordId)
    {
        $ordData = $this::find($ordId);

        if(isset($ordData)) {
            $m = new MemberMgmtModel();
            $member = $m->getMemberData($ordData->user_id);

            $ch = curl_init();
            $postUrl = env('TAPPAY_SANBOX_REFUND_URL');
            if(env('APP_ENV') == 'production') {
                $postUrl = env('TAPPAY_REFUND_URL');
            }
            $headers = [
                'x-api-key: '.env('TAPPAY_PARTNER_KEY'),
                'content-type: application/json'
            ];

            $postData = array(
                'partner_key'  => env('TAPPAY_PARTNER_KEY'),
                'rec_trade_id'     => $ordData->pay_no
            );

            $dataStr = json_encode($postData);
            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);

            $res = curl_exec($ch);
            curl_close($ch);   

            try{
                $dRes = json_decode($res);

                if($dRes->status == 0) {
                    $this->invalidInvoice($ordData->inv_no);
                    return "SUCCESS";
                }

                \Log::error(json_encode($dRes));
                \Log::error($postUrl);
                \Log::error($dataStr);
                
                return $dRes->msg;
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
                return 'error';
            }            
        }

        return 'error';

    }

    private function queryOrderForSp($ordMgmt, $merchantId, $hashKey, $hashIv, $checkValue)
    {
        $ch = curl_init();
        $orderNo = (isset($ordMgmt->r_ord_no)) ? $ordMgmt->r_ord_no : $ordMgmt->ord_no;
        curl_setopt($ch, CURLOPT_URL, config('spgateway.mpg.QueryURL'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            'MerchantID=' . $merchantId . '&Version=1.1&RespondType=JSON&CheckValue=' . $checkValue . '&TimeStamp=' . time() . '&MerchantOrderNo=' . $orderNo . '&Amt=' . $ordMgmt->d_price);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($server_output);

        if ($result->Status == 'SUCCESS') {
            $this->invalidInvoice($ordMgmt->inv_no);
        }

        return $result;
    }

    private function addpadding($string, $blocksize = 32)
    {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;
    }

    public function QueryLogisticsInfo($ordId, $ordNo, $AllPayLogisticsID)
    {

        $allPay = new Allpay();
        $AL = $allPay->l();
        $AL->HashKey = config('allpay.HashKey');
        $AL->HashIV = config('allpay.HashIV');
        $AL->Send = array(
            'MerchantID' => config('allpay.MerchantID'),
            'MerchantTradeNo' => $ordNo,
            'AllPayLogisticsID' => $AllPayLogisticsID,
        );
        $result = $AL->QueryLogisticsInfo();

        //dd($result);
        if (isset($result['ShipmentNo'])) {
            return $result['ShipmentNo'];
            //$this->updateOrdShipmentNo($ordId);
        } else {
            \Log::error("Update Shipment No fail:" . $ordNo);
        }

        return null;
    }

    public function updateOrdShipmentNo($ordId, $shipmentNo)
    {
        $this::find($ordId)->update(['ShipmentNo' => $shipmentNo]);

        return;
    }

    public function createInvoice($orderData)
    {
        //$orderData = $this::find($ordId);

        //$taxAmt = round($orderData->d_price * 0.05, 0);
        $post_data_array = $this->proccessInvData($orderData);

        if($post_data_array['TotalAmt'] > 0) {
            $post_data_str = http_build_query($post_data_array); //轉成字串排列

            $key = 'A5KsKzsOS1FZd3uiRQ6niFw9UE7PD7gH'; //商店專屬串接金鑰
            $iv = '6L6hK3gOHt4f0z5Q'; //商店專屬串接金鑰 HashIV 值
            $MerchantID = '31244169'; //商店代號
            $url = 'https://cinv.ezpay.com.tw/Api/invoice_issue';

            if (env('APP_ENV') == 'production') {
                $key = 'ayU7LCW3OP5By5FeHrYg8AxkfxbsBAPA'; //商店專屬串接金鑰
                $iv = 's3UicMuT70LwzIQY'; //商店專屬串接金鑰 HashIV 值
                $MerchantID = '33072258'; //商店代號
                $url = 'https://inv.ezpay.com.tw/Api/invoice_issue';
            }

            $post_data = trim(bin2hex(openssl_encrypt($this->addpadding($post_data_str), 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv)));

            $transaction_data_array = array( //送出欄位
                'MerchantID_' => $MerchantID,
                'PostData_' => $post_data,
            );
            $transaction_data_str = http_build_query($transaction_data_array);

            $result = $this->curl_work($url, $transaction_data_str); //背景送出

            $res = json_decode($result['web_info']);

            if ($res->Status != 'SUCCESS') {
                \Log::error('code:' . $res->Status . ' ===> ' . $res->Message);
                return $res->Message;
            } else {
                \Log::info('create invoice success');
                $inv = json_decode($res->Result);
                DB::table('mod_order')->where('id', $orderData->id)->update(['inv_no' => $inv->InvoiceNumber]);
                \Log::info('invoice number: ' . $inv->InvoiceNumber);
            }
        }

        return;
    }

    public function invalidInvoice($invNo)
    {
        $post_data_array = array(
            'RespondType' => 'JSON',
            'Version' => '1.0',
            'TimeStamp' => time(),
            'InvoiceNumber' => $invNo,
            'InvalidReason' => '退貨或取消',
        );

        $post_data_str = http_build_query($post_data_array); //轉成字串排列
        $key = 'A5KsKzsOS1FZd3uiRQ6niFw9UE7PD7gH'; //商店專屬串接金鑰
        $iv = '6L6hK3gOHt4f0z5Q'; //商店專屬串接金鑰 HashIV 值
        $MerchantID = '31244169'; //商店代號
        $url = 'https://cinv.ezpay.com.tw/Api/invoice_invalid';

        if (env('APP_ENV') == 'production') {
            $key = 'ayU7LCW3OP5By5FeHrYg8AxkfxbsBAPA'; //商店專屬串接金鑰
            $iv = 's3UicMuT70LwzIQY'; //商店專屬串接金鑰 HashIV 值
            $MerchantID = '33072258'; //商店代號
            $url = 'https://inv.ezpay.com.tw/Api/invoice_invalid';
        }

        $post_data = trim(bin2hex(openssl_encrypt($this->addpadding($post_data_str), 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv)));

        $transaction_data_array = array( //送出欄位
            'MerchantID_' => $MerchantID,
            'PostData_' => $post_data,
        );
        $transaction_data_str = http_build_query($transaction_data_array);

        $result = $this->curl_work($url, $transaction_data_str); //背景送出

        $res = json_decode($result['web_info']);

        if (isset($res) && $res->Status == 'SUCCESS') {
            \Log::info('invalid invoice success');
        } else {
            \Log::error('invalid invoice fail');
            if (isset($res)) {
                \Log::error('code:' . $res->Status . ' ===> ' . $res->Message);
            } else {
                \Log::error('res is null');
            }

        }

        return;
    }

    private function curl_work($url = '', $parameter = '')
    {
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'Google Bot',
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POST => '1',
            CURLOPT_POSTFIELDS => $parameter,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_error = curl_errno($ch);
        curl_close($ch);
        $return_info = array(
            'url' => $url,
            'sent_parameter' => $parameter,
            'http_status' => $retcode,
            'curl_error_no' => $curl_error,
            'web_info' => $result,
        );

        return $return_info;
    }

    private function proccessInvData($orderData)
    {
        $BuyerUBN = (isset($orderData->identifier)) ? $orderData->identifier : '';
        $detailData = DB::table('mod_order_detail')->where('ord_id', $orderData->id)->whereNull('sn_id')->get();
        $ttlAmt = 0;
        foreach ($detailData as $row) {
            $ttlAmt += $row->amt;
        }

        $ttlAmt += $orderData->ship_fee;

        if(isset($orderData->s_code)) {
            $discount = DB::table('mod_discount_member')->where('cd', $orderData->s_code)->first();

            if(isset($discount)) {
                $ttlAmt = $ttlAmt - $discount->discount;
            }
            else {
                return;
            }
        }

        $taxAmt = round($ttlAmt * 0.05, 0);

        $BuyerName = "";

        if (isset($orderData->customer_title)) {
            $BuyerName = str_replace(' ', '', $orderData->customer_title);
        } else {
            $BuyerName = str_replace(' ', '', $orderData->ord_nm);
        }

        if (empty($orderData->customer_title) && empty($orderData->ord_nm)) {
            $BuyerName = str_replace(' ', '', $orderData->dlv_nm);
        }

        $result = array(
            'RespondType' => 'JSON',
            'Version' => '1.4',
            'Amt' => (int) $ttlAmt - $taxAmt,
            'MerchantOrderNo' => (isset($orderData->r_ord_no)) ? $orderData->r_ord_no : $orderData->ord_no,
            'TimeStamp' => time(),
            'Status' => 1,
            'Category' => ($orderData->invoice_type == 'a3') ? 'B2B' : 'B2C',
            'BuyerName' => $BuyerName,
            'BuyerUBN' => str_replace(' ', '', $BuyerUBN),
            'PrintFlag' => 'Y',
            'BuyerAddress' => $orderData->dlv_addr,
            'BuyerEmail' => $orderData->email,
            'CarrierType' => '',
            'TaxType' => 1,
            'TaxRate' => 5,
            'TotalAmt' => (int) $ttlAmt,
            'ItemName' => '美z人生3C商品',
            'ItemCount' => 1,
            'ItemUnit' => '個',
            'ItemPrice' => (int) $ttlAmt,
            'ItemAmt' => (int) $ttlAmt,
            'TaxAmt' => $taxAmt,
        );

        \Log::info($result);

        return $result;
    }

    public function shippingNotice($ordId)
    {
        $orderData = $this::find($ordId);

        if (isset($orderData)) {


            $shipUrl = "";
            $bscodeData = DB::table('bscode')
                ->where('cd_type', 'SHIPPER')
                ->where('cd', $orderData->shipper_cd)
                ->value('value1');

            $shipUrl = $bscodeData;
            $sendData = [
                'name' => $orderData->dlv_nm,
                'ordNo' => $orderData->ord_no,
                'ShipmentNo' => $orderData->CVSPaymentNo.$orderData->CVSValidationNo,
                'ShipmentUrl' => $shipUrl,
                'prodDetail' => $normalDetail,
                'wifiDetail' => $wifiDetail,
                'remark' => $orderData->remark,
                'sendprod_remark' => $orderData->sendprod_remark,
            ];
            $user = Auth::user();
            
            DB::table('sys_email')->insert([
                'tpl' => 'NormalOrder',
                'status' => 'N',
                'schedule' =>'60',
                'content' =>json_encode($sendData),
                'to' => $orderData->email,
                'bcc' => 'sales@standard-info.com',
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => \Carbon::now(),
                'updated_at' => \Carbon::now(),
            ]);

            // Mail::to($orderData->email)
            //     ->bcc('sales@standard-info.com')
            //     ->send(new NormalOrderShipped($sendData));
        }
    }

    public function updateWifiStatus()
    {
        $today = date('Y-m-d');
        $yesterday = date("Y-m-d", strtotime('-1 days'));

        $today = $today . ' 23:59:59';
        $yesterday = $yesterday . ' 00:00:00';

        $ordData = DB::table('mod_order')
            ->select('mod_order.ord_no')
            ->join('mod_order_rent_detail', 'mod_order_rent_detail.ord_id', '=', 'mod_order.id')
            ->whereNotIn('mod_order.status', ['E', 'F', 'G', 'H'])
            ->where('mod_order_rent_detail.e_day', '<=', $today)
            ->groupBy('mod_order.ord_no')
            ->get();

        $ordArray = array();
        foreach ($ordData as $row) {
            array_push($ordArray, $row->ord_no);
        }

        $result = app('App\Http\Controllers\SoapController')->getOrderStatus($ordArray);

        if (isset($result)) {
            foreach ($result as $row) {
                if ($row['STATUS'] == '已歸還') {
                    DB::table('mod_order')->where('ord_no', $row['ORDERNO'])->update(['status' => 'E']);

                    $orderData = DB::table('mod_order')->where('ord_no', $row['ORDERNO'])->first();

                    if (isset($orderData)) {
                        $this->sendWifiReturnMail($orderData);
                    }
                }
            }
        }
    }

    public function sendWifiReturnMail($orderData)
    {
        $sendData = array(
            'ordNo' => $orderData->ord_no,
            'name' => $orderData->dlv_nm,
        );
        Mail::to($orderData->email)
            ->bcc('sales@standard-info.com')
            ->send(new WifiReturnNotice($sendData));
    }

    public function paymentReminderNotice()
    {
        $today = date('Y-m-d H:i:s');

        $orderData = $this::where('status', 'A')->where('send_mail', '<>', 'Y')->get();

        foreach ($orderData as $row) {
            $createdDate = $row->created_at;

            $orderSellData = DB::table('mod_order_detail')
                ->where('ord_id', $row->id)
                ->get();

            $normalDetail = array();

            foreach ($orderSellData as $drow) {
                array_push($normalDetail, array(
                    'prod_nm' => $drow->prod_nm,
                    'num' => $drow->num,
                ));
            }

            $orderWifiData = DB::table('mod_order_rent_detail')
                ->where('ord_id', $row->id)
                ->get();

            $wifiDetail = array();

            foreach ($orderWifiData as $drow) {
                array_push($wifiDetail, array(
                    'prod_nm' => $drow->prod_nm,
                    'num' => $drow->num,
                ));
            }

            $day = strtotime($today) - strtotime($createdDate);

            $day = floor($day / 3600 / 24);

            if ($day >= 1) {
                $sendData = array(
                    'ordNo' => $row->ord_no,
                    'name' => $row->ord_nm,
                    'prodDetail' => $normalDetail,
                    'wifiDetail' => $wifiDetail,
                );
                Mail::to($row->email)
                    ->bcc('sales@standard-info.com')
                    ->send(new PaymentReminderNotice($sendData));

                DB::table('mod_order')->where('ord_no', $row->ord_no)->update(['send_mail' => 'Y']);
            }
        }

    }

    public function autoCancelOrder()
    {
        $today = date('Y-m-d H:i:s');

		$orderData = $this::where('status', 'A')
							->where('created_at', '>', '2018-11-01 00:00:00')
							->get();

        foreach ($orderData as $row) {
            $day = strtotime($today) - strtotime($row->created_at);
            $day = floor($day / 3600 / 24);

            if ($day >= 3) {
				$this->processStockReturn($row);
            }
		}
		
		return;
    }

    public function processStockReturn($orderData)
    {
        $this::where('id', $orderData->id)->update(['status' => 'G']);

        $detailData = DB::table('mod_order_detail')->where('ord_id', $orderData->id)->get();

        foreach ($detailData as $val) {
            $prodType = DB::table('mod_product')->where('prod_no', $val->prod_no)->value('prod_type');

            if ($prodType == 'S') {
                $snData = ModSnModel::where('ord_detail_id', $val->id)->get();

                foreach ($snData as $sn) {
                    ModSnModel::where('id', $sn->id)->update([
                        'ord_detail_id' => null,
                        'member_id' => null,
                        'email' => null,
                        'is_sell' => 'N',
                    ]);
                }
            } else if ($prodType == 'N') {
                $stock = DB::table('mod_product_detail')->where('id', $val->prod_detail_id)->value('stock');

                $stock = (int) $stock + (int) $val->num;

                DB::table('mod_product_detail')
                    ->where('id', $val->prod_detail_id)
                    ->update(['stock' => $stock]);
            }
		}
		
		return;
    }

    public function changeStatusForHomeShip()
    {
        $today = date('Y-m-d H:i:s');

        $orderData = DB::table('mod_order')
                            ->where('pick_way', 'B')
                            ->whereIn('status', ['C','I'])
							->where('created_at', '>', '2018-12-01 00:00:00')
                            ->get();

        foreach ($orderData as $row) {
            $day = strtotime($today) - strtotime($row->ship_date);
            $day = floor($day / 3600 / 24);

            if ($day > 2) {
                $e = $this::find($row->id);
                $e->status = 'D';
                $e->save();
            }
		}
		
		return;
    }

    public function processErrorOrder() {
        
        $ordData = DB::table('mod_order')
                    ->whereNotNull('inv_no')
                    ->where('created_at', '<', '2018-10-15 00:00:00')
                    ->get();
        

        
        foreach($ordData as $row) {
            $invNo = $row->inv_no;

            $this->invalidInvoice($invNo);
        }

        echo "success";
    }

    public function tempcreateInvoice($orderData)
    {
        //$orderData = $this::find($ordId);

        //$taxAmt = round($orderData->d_price * 0.05, 0);
        $post_data_array = $this->tempproccessInvData($orderData);

        if($post_data_array['TotalAmt'] > 0) {
            $post_data_str = http_build_query($post_data_array); //轉成字串排列

            $key = 'A5KsKzsOS1FZd3uiRQ6niFw9UE7PD7gH'; //商店專屬串接金鑰
            $iv = '6L6hK3gOHt4f0z5Q'; //商店專屬串接金鑰 HashIV 值
            $MerchantID = '31244169'; //商店代號
            $url = 'https://cinv.ezpay.com.tw/Api/invoice_issue';

            if (env('APP_ENV') == 'production') {
                $key = 'ayU7LCW3OP5By5FeHrYg8AxkfxbsBAPA'; //商店專屬串接金鑰
                $iv = 's3UicMuT70LwzIQY'; //商店專屬串接金鑰 HashIV 值
                $MerchantID = '33072258'; //商店代號
                $url = 'https://inv.ezpay.com.tw/Api/invoice_issue';
            }

            $post_data = trim(bin2hex(openssl_encrypt($this->addpadding($post_data_str), 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv)));

            $transaction_data_array = array( //送出欄位
                'MerchantID_' => $MerchantID,
                'PostData_' => $post_data,
            );
            $transaction_data_str = http_build_query($transaction_data_array);

            $result = $this->curl_work($url, $transaction_data_str); //背景送出

            $res = json_decode($result['web_info']);

            if ($res->Status != 'SUCCESS') {
                \Log::error('code:' . $res->Status . ' ===> ' . $res->Message);
            } else {
                \Log::info('create invoice success');
                $inv = json_decode($res->Result);
                DB::table('mod_order')->where('id', $orderData->id)->update(['inv_no' => $inv->InvoiceNumber]);
                \Log::info('invoice number: ' . $inv->InvoiceNumber);
            }
        }

        return;
    }

    private function tempproccessInvData($orderData)
    {
        $BuyerUBN = (isset($orderData->identifier)) ? $orderData->identifier : '';
        $detailData = DB::table('mod_order_detail')->where('ord_id', $orderData->id)->whereNull('sn_id')->get();
        $ttlAmt = 0;
        foreach ($detailData as $row) {
            $ttlAmt += $row->amt;
        }

        $ttlAmt += $orderData->ship_fee;

        if(isset($orderData->s_code)) {
            $discount = DB::table('mod_discount_member')->where('cd', $orderData->s_code)->first();

            if(isset($discount)) {
                $ttlAmt = $ttlAmt - $discount->discount;
            }
            else {
                $ttlAmt = $orderData->d_price;
            }
        }

        $taxAmt = round($ttlAmt * 0.05, 0);

        $BuyerName = "";

        if (isset($orderData->customer_title)) {
            $BuyerName = str_replace(' ', '', $orderData->customer_title);
        } else {
            $BuyerName = str_replace(' ', '', $orderData->ord_nm);
        }

        if (empty($orderData->customer_title) && empty($orderData->ord_nm)) {
            $BuyerName = str_replace(' ', '', $orderData->dlv_nm);
        }

        $result = array(
            'RespondType' => 'JSON',
            'Version' => '1.4',
            'Amt' => (int) $ttlAmt - $taxAmt,
            'MerchantOrderNo' => (isset($orderData->r_ord_no)) ? $orderData->r_ord_no : $orderData->ord_no,
            'TimeStamp' => time(),
            'Status' => 1,
            'Category' => ($orderData->invoice_type == 'a3') ? 'B2B' : 'B2C',
            'BuyerName' => $BuyerName,
            'BuyerUBN' => str_replace(' ', '', $BuyerUBN),
            'PrintFlag' => 'Y',
            'BuyerAddress' => $orderData->dlv_addr,
            'BuyerEmail' => $orderData->email,
            'CarrierType' => '',
            'TaxType' => 1,
            'TaxRate' => 5,
            'TotalAmt' => (int) $ttlAmt,
            'ItemName' => '美z人生3C商品',
            'ItemCount' => 1,
            'ItemUnit' => '個',
            'ItemPrice' => (int) $ttlAmt,
            'ItemAmt' => (int) $ttlAmt,
            'TaxAmt' => $taxAmt,
        );

        \Log::info($result);

        return $result;
    }

    public function mappingCart($cartId, $userId) {

        $cartCnt = DB::table('mod_cart')->where('user_id', $userId)->count();
        $cartByCartIdCnt = DB::table('mod_cart')->where('id', $cartId)->count();

        if($cartByCartIdCnt > 0) {
            DB::table('mod_cart')->where('user_id', $userId)->delete();
            DB::table('mod_cart')
            ->where('id', $cartId)
            ->update(['user_id' => $userId]);

            unset($_COOKIE['cartId']);
            setcookie('cartId', 0, -1, '/');
        }

        if($cartCnt > 0) {
            unset($_COOKIE['cartId']);
            setcookie('cartId', 0, -1, '/');
        }

        
        

        return;
    }

    public function autocreateInvoice() {

        $orderMgmtModel = new OrderMgmtModel();
        $today = date('Y-m-d H:i:s');
        $temp = "";
        $ordData = DB::table('mod_order')
            ->whereNull('inv_no')
            ->whereNotNull('pay_date')
            ->whereIn('status', ['B','C','D','I'])
            ->where('created_at', '>', '2019-03-04 00:00:00')
            ->get();
        foreach($ordData as $row) {
            $day = strtotime($today) - strtotime($row->pay_date);
            $day = floor($day / 3600 / 24);
            if ($day > 7) {
                try {
                    $orderMgmtModel->createInvoice($row);
                }
                catch(\Exception $e) {
                    \Log::error($e->getMessage());
                }
            }
        }
    }

    public function sendmail_hour() {
        $orderMgmtModel = new OrderMgmtModel();
        $sendData = DB::table('sys_email')
        ->whereIn('status', ['N','F'])
        ->where('schedule','60')
        ->where('f_count','<',3)
        ->get();
        foreach($sendData as $row) {
            try {
                if($row->tpl=="NormalOrder"){
                    Mail::to($row->to)
                        ->bcc($row->bcc)
                        ->send(new NormalOrderShipped(json_decode($row->content,true)));
                    $orderMgmtModel->updatesendmail($row,'Y');
                }else if($row->tpl == "OrderShipped"){
                    Mail::to($row->to)
                    ->bcc('$row->bcc')
                    ->send(new OrderShipped(json_decode($row->content,true)));
                    $orderMgmtModel->updatesendmail($row,'Y');
                }else if($row->tpl == "adminEmail"){
                    $tmpsubject = $row->subject;
                    $tmpcontent = $row->content;
                    $tmpto = $row->to;
                    Mail::send([], [], function ($message) use ($tmpsubject, $tmpto,$tmpcontent) {
                        $message->to($row->to)
                            ->subject('消費通知：' .$row->subject)
                            ->setBody($row->content, 'text/html');
                    $orderMgmtModel->updatesendmail($row,'Y');
                    });
                }
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
                $orderMgmtModel->updatesendmail($row,'F');
            }
        }
    }

    public function sendmail_five() {
        $orderMgmtModel = new OrderMgmtModel();
        $sendData = DB::table('sys_email')
        ->whereIn('status', ['N','F'])
        ->where('schedule','5')
        ->where('f_count','<',3)
        ->get();
        foreach($sendData as $row) {
            try {
                if($row->tpl=="NormalOrder"){
                    Mail::to($row->to)
                        ->bcc($row->bcc)
                        ->send(new NormalOrderShipped(json_decode($row->content,true)));
                    $orderMgmtModel->updatesendmail($row,'Y');
                }else if($row->tpl == "OrderShipped"){
                    Mail::to($row->to)
                    ->bcc('$row->bcc')
                    ->send(new OrderShipped(json_decode($row->content,true)));
                    $orderMgmtModel->updatesendmail($row,'Y');
                }else if($row->tpl == "adminEmail"){
                    // dd($row);
                    $tmpsubject = $row->subject;
                    $tmpcontent = $row->content;
                    $tmpto = $row->to;
                    $rowtmp = $row;
                    Mail::send([], [], function ($message) use ($tmpsubject, $tmpto,$tmpcontent,$rowtmp) {
                        $message->to($tmpto)
                            ->subject('消費通知：' .$tmpsubject)
                            ->setBody($tmpcontent, 'text/html');
                        $this->updatesendmail($rowtmp,'Y');
                    });
                }
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
                $orderMgmtModel->updatesendmail($row,'F');
            }
        }
    }
    public function updatesendmail($row,$status) {
        DB::table('sys_email')
        ->where('id', $row->id)
        ->update([
            'status' => $status,
            'updated_at' => \Carbon::now(),
            'f_count'=> $row->f_count + 1 ]);
    }
    public function processCart($user) {
        $cartData = DB::table('mod_cart')->where('user_id', $user->id)->first();

        if(isset($cartData)) {
            $cartContent = json_decode($cartData->content);

            $cartNum = count($cartContent->rData) + count($cartContent->sData);

            setcookie('cartNum', $cartNum, time()+60*60*24, '/');
        }
    }

    public function tapPayByPrime($ordId, $prime, $remember, $periods=0) {
        
        $ordData = $this::find($ordId);

        if(isset($ordData)) {
            $ch = curl_init();
            $postUrl = 'https://sandbox.tappaysdk.com/tpc/payment/pay-by-prime';

            if(env('APP_ENV') == 'production') {
                $postUrl = 'https://prod.tappaysdk.com/tpc/payment/pay-by-prime';
            }

            $headers = [
                'x-api-key: '.env('TAPPAY_PARTNER_KEY'),
                'content-type: application/json'
            ];

            $postData = array(
                'partner_key'      => env('TAPPAY_PARTNER_KEY'),
                'prime'            => $prime,
                'amount'           => (int)$ordData->d_price,
                'merchant_id'      => env('TAPPAY_MERCHANT_ID'),
                'details'          => "晶華",
                'order_number'     => $ordData->ord_no,
                'remember'         => $remember,
                'instalment'      => (int)$periods,
                'cardholder'       =>  array(
                    'phone_number' => $ordData->dlv_phone,
                    'name'         => $ordData->dlv_nm,
                    'email'        => $ordData->email
                ),
                'result_url'   => null
            );

            if($ordData->pay_way == "LinePay") {
                $postData['result_url'] = array(
                    'frontend_redirect_url' => env('APP_URL').'/thirdPartyPayCallBack',
                    'backend_notify_url' => env('APP_URL').'/thirdPartyPayCallBack1'
                );

                $postData['merchant_id'] = 'z_LINEPAY';
            }

            $dataStr = json_encode($postData);
            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);

            $res = curl_exec($ch);
            curl_close($ch);   

            try{
                $dRes = json_decode($res);
                
                if($dRes->status == 0) {
                    if($ordData->pay_way == 'LinePay') {
                        return $dRes->payment_url;
                    }

                    $this->afterTapPaySuccess($dRes, $ordData);

                    if($remember == true) {
                        $m = new MemberMgmtModel();
                        $m->cardRemember($ordData->user_id, $dRes->card_secret->card_token, $dRes->card_secret->card_key, $dRes->card_info->last_four);
                    }

                    if($ordData->pay_way == 'LinePay') {
                        \Log::error($res);
                        return 'error';
                    }

                    return "success";
                }

                \Log::error($res);
                return 'error';
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
                return 'error';
            }            
        }

        return '查無訂單';
    }

    public function tapPayByToken($ordId, $periods) {
        $ordData = $this::find($ordId);

        if(isset($ordData)) {
            $m = new MemberMgmtModel();
            $member = $m->getMemberData($ordData->user_id);

            $ch = curl_init();
            $postUrl = 'https://sandbox.tappaysdk.com/tpc/payment/pay-by-token';

            if(env('APP_ENV') == 'production') {
                $postUrl = 'https://prod.tappaysdk.com/tpc/payment/pay-by-token';
            }

            $headers = [
                'x-api-key: '.env('TAPPAY_PARTNER_KEY'),
                'content-type: application/json'
            ];

            $postData = array(
                'partner_key'  => env('TAPPAY_PARTNER_KEY'),
                'card_key'     => $member->card_key,
                'card_token'   => $member->card_token,
                'currency'     => 'TWD',
                'amount'       => (int)$ordData->d_price,
                'merchant_id'  => env('TAPPAY_MERCHANT_ID'),
                'details'      => "晶華",
                'instalment'   => (int)$periods,
                'order_number' => $ordData->ord_no,
            );

            $dataStr = json_encode($postData);
            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);

            $res = curl_exec($ch);
            curl_close($ch);   

            try{
                $dRes = json_decode($res);

                if($dRes->status == 0) {
                    $this->afterTapPaySuccess($dRes, $ordData);

                    return "success";
                }

                return $dRes->msg;
            }
            catch(\Exception $e) {
                \Log::error($e->getMessage());
                return 'error';
            }            
        }

        return '查無訂單';
    }

    public function afterTapPaySuccess($res, $orderData)
    {
        $orderData->pay_no = $res->rec_trade_id;
        $orderData->status = 'B';
        if(isset($res->transaction_time_millis)) {
            $orderData->pay_date = date('Y-m-d H:i:s', ((int)round($res->transaction_time_millis/1000)));
        }
        else {
            $orderData->pay_date =  \Carbon::now();
        }
        
        $orderData->save();

        $user = null;
        if (isset($orderData)) {
            $user = DB::table('mod_member')->where('id', $orderData->user_id)->first();
            if (isset($user)) {
                DB::table('mod_cart')
                    ->where('user_id', $user->id)
                    ->delete();
            }

        }

        if (isset($orderData)) {

            if (isset($user) && env('APP_ENV') == 'production') {
                $smsMsg = "感謝您選購Z-TRIP.COM商品，我們已收到您款項" . number_format($orderData->d_price) . "，以下連結可查看訂單:" . url('center');
                CommonModel::sendSmsMsg($smsMsg, $user->cellphone);
            }

            $orderRentData = DB::table('mod_order_rent_detail')
                ->where('ord_id', $orderData->id)
                ->get();


            $orderSellData = DB::table('mod_order_detail')
                ->where('ord_id', $orderData->id)
                ->get();

            $softCnt = 0;
            $normalCnt = 0;
            foreach ($orderSellData as $key => $row) {
                $prodType = DB::table('mod_product')->where('prod_no', $row->prod_no)->value('prod_type');

                if ($prodType == 'S') {
                    \Log::info("發送序號email");
                    $modSn = new ModSnModel;
                    $modSn->sendEmail($orderData, $row);
                    $softCnt++;
                } else {
                    $normalCnt++;
                }
            }

            //訂單內如果只有存購買軟體序號時，在付完款後Email序後發送完畢後，訂單狀態改成已送達
            if ($softCnt > 0 && $normalCnt == 0 && count($orderRentData) == 0) {
                DB::table('mod_order')
                    ->where('ord_no', $orderData->ord_no)
                    ->update(['status' => 'D']);
            }

            if (isset($orderData->AllPayLogisticsID)) {
                $shipmentNo = $this->QueryLogisticsInfo($orderData->id, $orderData->ord_no, $orderData->AllPayLogisticsID);
                $this->updateOrdShipmentNo($orderData->id, $shipmentNo);
            }

            //$this->createInvoice($orderData); //七天後再開發票
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
     */

    /*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
 */
}
