<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use DB;

class ProdDetailModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_product_detail';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	public function hasStock($id, $num, $prodType, $prodNo) {
		$prodData = DB::table('mod_product_detail')->where('id', $id)->first();
		$stock = 0;
		if($prodType == 'S') {
			$stock = DB::table('mod_sn')->where('prod_no', $prodNo)->where('is_sell', 'N')->count();
		}
		else {
			if(isset($prodData)) {
				$stock = $prodData->stock;
			}
		}

		if($stock >= (int)$num) {
			return true;
		}

		return false;
	}

	public function cancelOrderForReturnStock($orderData) {
		
		$ordDetailData = DB::table('mod_order_detail')
						->where('ord_id', $orderData->id)
						->whereNull('sn_id')
						->get();
						
		foreach($ordDetailData as $row) {
			$prodData = $this::find($row->prod_detail_id);
			if(isset($prodData)) {
				$stock = $prodData->stock + $row->num;

				$prodData->stock = $stock;
				$prodData->save();
			}
		}
	}

	public function getVendorById($id) {
		$prodDetail = $this::find($id);

		return $prodDetail->created_by;
	}

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------


	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}