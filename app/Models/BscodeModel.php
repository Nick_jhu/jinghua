<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use DB;

class BscodeModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'bscode';
	protected $primaryKey = 'id';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $fillable = ['cust_no','name','phone','address'];
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
    */
    public function customer()
    {
        return $this->belongsTo('App\Models\CustomerModel', 'cust_no');
	}
	
	public function getShipFee($shipper) {
		$data = DB::table('bscode')
                    ->where('cd_type', 'SHIP_FEE')
                    ->where('cd', $shipper)
                    ->first();

        $amt = 0;

        if(isset($data)) {
            $amt = (int)$data->value1;
		}
		
		return $amt;
	}

	public function getShipWayToArray() {
		$data = array();
		$shipWay = $this->where('cd_type', 'SHIPWAY')->get();

		if(isset($shipWay)) {
			foreach($shipWay as $row) {
				array_push($data, $row->cd);
			}
		}

		return $data;
	}

	public function getShipWayKeyValueArray($shipData) {
		$data = array();
		$shipWay = $this->where('cd_type', 'SHIPWAY')->whereIn('cd', $shipData)->get();

		if(isset($shipWay)) {
			foreach($shipWay as $row) {
				array_push($data, array(
					'key' => $row->cd,
					'value' => $row->cd_descp
				));
			}
		}

		return $data;
	}

	public function getShipFree() {
		return (int)$this->where('cd_type', 'SHIP_FREE')->where('cd', 'AMT')->value('value1');
	}

	public function getShipFeeToArray() {
		$data = array();
		$bsdata = DB::table('bscode')
                    ->where('cd_type', 'SHIP_FEE')
                    ->get();

		foreach($bsdata as $row) {
			$data[$row->cd] = (int)$row->value1;
		}

		return $data;

	}


	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}