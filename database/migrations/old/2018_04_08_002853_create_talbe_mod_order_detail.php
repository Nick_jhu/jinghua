<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalbeModOrderDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_order_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ord_id');
            $table->integer('prod_id');
            $table->integer('prod_detail_id');
            $table->string('prod_nm')->nullable();
            $table->integer('num')->default(0);
            $table->decimal('unit_price', 18,2)->nullable();
            $table->decimal('amt', 18,2)->nullable();
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('g_key', 10);
            $table->string('c_key', 10);
            $table->string('s_key', 10);
            $table->string('d_key', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_order_detail');
    }
}
