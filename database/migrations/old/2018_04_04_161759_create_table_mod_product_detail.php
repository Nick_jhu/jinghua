<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModProductDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_product_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prod_id');
            $table->string('country');
            $table->string('title');
            $table->string('descp', 150);
            $table->integer('stock');
            $table->integer('sell_amt');
            $table->decimal('o_price', 18,2)->nullable();
            $table->decimal('d_price', 18,2)->nullable();
            $table->decimal('c_price', 18,2)->nullable();
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
            $table->string('g_key', 10);
            $table->string('c_key', 10);
            $table->string('s_key', 10);
            $table->string('d_key', 10);
            $table->string('img1', 100)->nullable();
            $table->string('img2', 100)->nullable();
            $table->string('img3', 100)->nullable();
            $table->string('img4', 100)->nullable();
            $table->string('img5', 100)->nullable();
            $table->string('img_descp1', 255)->nullable();
            $table->string('img_descp2', 255)->nullable();
            $table->string('img_descp3', 255)->nullable();
            $table->string('img_descp4', 255)->nullable();
            $table->string('img_descp5', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_product_detail');
    }
}
