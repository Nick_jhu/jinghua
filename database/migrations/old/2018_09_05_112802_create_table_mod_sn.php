<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModSn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_sn', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->nullable();
            $table->string('email')->nullable();
            $table->string('prod_no')->nullable();
            $table->string('sn')->nullable();
            $table->string('is_sell', 1)->default('N');
            
            $table->timestamps();
        });

        Schema::table('mod_product', function (Blueprint $table) {
            $table->string('prod_no')->nullable();
        });

        Schema::table('mod_product_detail', function (Blueprint $table) {
            $table->string('prod_no')->nullable();
        });

        Schema::table('mod_order_detail', function (Blueprint $table) {
            $table->string('prod_no')->nullable();
            $table->string('sn_id')->nullable();
        });

        Schema::table('mod_order_rent_detail', function (Blueprint $table) {
            $table->string('prod_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_sn');

        Schema::table('mod_product', function (Blueprint $table) {
            $table->drop('prod_no');
        });

        Schema::table('mod_product_detail', function (Blueprint $table) {
            $table->drop('prod_no');
        });
    }
}
