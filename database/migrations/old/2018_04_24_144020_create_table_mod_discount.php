<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModDiscount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_discount', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('descp')->nullable();
            $table->integer('discount')->nullable();
            $table->timestamp('f_date')->nullable();
            $table->timestamp('t_date')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('discount_type', 1)->default('A');
            $table->string('sell_type', 1)->default('R');
            $table->string('is_send', 1)->default('N');
            $table->string('cd');
            $table->integer('amt');
            $table->timestamps();
        });

        Schema::create('mod_discount_member', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discount_id');
            $table->integer('member_id');
            $table->string('phone');
            $table->string('status', 1)->default('N');
            $table->string('cd')->nullable();
            $table->string('title')->nullable();
            $table->text('descp')->nullable();
            $table->integer('discount')->nullable();
            $table->timestamp('f_date')->nullable();
            $table->timestamp('t_date')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('discount_type', 1)->default('B');
            $table->string('sell_type', 1)->default('R');
            $table->timestamps();
            $table->string('g_key', 10)->nullable();
            $table->string('c_key', 10)->nullable();
            $table->string('s_key', 10)->nullable();
            $table->string('d_key', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_discount');
        Schema::dropIfExists('mod_discount_member');
    }
}
