<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalbeModOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('ord_no');
            $table->string('status', 20);
            $table->decimal('o_price', 18,2)->nullable();
            $table->decimal('d_price', 18,2)->nullable();
            $table->string('ord_nm')->nullable();
            $table->string('dlv_nm')->nullable();
            $table->string('dlv_zip', 5)->nullable();
            $table->string('dlv_city')->nullable();
            $table->string('dlv_area')->nullable();
            $table->string('dlv_addr')->nullable();
            $table->decimal('ship_fee', 18,2)->nullable();
            $table->string('pay_way')->nullable();
            $table->string('ship_way')->nullable();
            $table->string('ship_no')->nullable();
            $table->string('pay_no')->nullable();
            $table->string('delay_pay_no')->nullable();
            $table->string('discount_no')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('g_key', 10)->nullable();
            $table->string('c_key', 10)->nullable();
            $table->string('s_key', 10)->nullable();
            $table->string('d_key', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_order');
    }
}
