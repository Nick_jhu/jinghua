<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_member', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('dlv_zip', 5)->nullable();
            $table->string('dlv_city', 10)->nullable();
            $table->string('dlv_area', 10)->nullable();
            $table->string('dlv_addr', 50)->nullable();
            $table->string('pick_way')->nullable();
            $table->string('return_way')->nullable();
            $table->string('g_key', 10);
            $table->string('c_key', 10);
            $table->string('s_key', 10);
            $table->string('d_key', 10);
            $table->rememberToken();
            $table->string('created_by', 30);
            $table->string('updated_by', 30);
            $table->string('is_actived', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_member');
    }
}
