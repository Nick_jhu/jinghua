<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModDiscount201805125 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_discount', function (Blueprint $table) {
            $table->integer('limit_amt')->nullabe();
            $table->string('prod_id', 200)->nullabe();
            $table->string('discount_mode', 1)->default('A');
        });

        Schema::table('mod_discount_member', function (Blueprint $table) {
            $table->integer('limit_amt')->nullabe();
            $table->string('prod_id', 200)->nullabe();
            $table->string('discount_mode', 1)->default('A');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_discount', function (Blueprint $table) {
            $table->drop('limit_amt');
            $table->drop('prod_id');
            $table->drop('discount_mode');
        });
    }
}
