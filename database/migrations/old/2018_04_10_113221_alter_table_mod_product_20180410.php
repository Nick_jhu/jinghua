<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModProduct20180410 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_product', function (Blueprint $table) {
            $table->text('slogan')->nullable();
        });

        Schema::table('mod_product_detail', function (Blueprint $table) {
            $table->text('slogan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_product', function (Blueprint $table) {
            $table->drop('slogan');
        });

        Schema::table('mod_product_detail', function (Blueprint $table) {
            $table->drop('slogan');
        });
    }
}
