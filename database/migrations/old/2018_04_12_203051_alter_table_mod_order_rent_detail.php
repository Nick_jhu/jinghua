<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModOrderRentDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_order_rent_detail', function (Blueprint $table) {
            $table->string('departure_airline')->nullable();
            $table->string('departure_flight_no')->nullable();

            $table->string('return_airline')->nullable();
            $table->string('return_flight_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_order_rent_detail', function (Blueprint $table) {
            $table->drop('departure_airline');
            $table->drop('departure_flight_no');
            $table->drop('return_airline');
            $table->drop('return_flight_no');
        });
    }
}
