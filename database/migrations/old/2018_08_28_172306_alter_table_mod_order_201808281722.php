<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModOrder201808281722 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_order', function (Blueprint $table) {
            $table->string('logistics_sub_type')->nullable();
            $table->string('store_id')->nullable();
            $table->string('store_name')->nullable();
            $table->string('store_addr')->nullable();
            $table->string('store_phone')->nullable();
            $table->string('is_collection', 1)->nullable();
            $table->string('identifier')->nullable();
            $table->string('customer_title')->nullable();
            $table->string('invoice_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_order', function (Blueprint $table) {
            $table->drop('logistics_sub_type');
            $table->drop('store_id');
            $table->drop('store_name');
            $table->drop('store_addr');
            $table->drop('store_phone');
            $table->drop('is_collection');
            $table->drop('identifier');
            $table->drop('customer_title');
            $table->drop('invoice_type');
        });
    }
}
