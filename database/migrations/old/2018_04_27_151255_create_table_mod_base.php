<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_base', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country_img1')->nullable();
            $table->string('country_img2')->nullable();
            $table->string('country_img3')->nullable();
            $table->string('country_img4')->nullable();
            $table->string('country_img5')->nullable();
            $table->string('country_img6')->nullable();
            $table->string('country_img7')->nullable();
            $table->string('country_img8')->nullable();
            $table->string('title1')->nullable();
            $table->text('descp1')->nullable();
            $table->string('img1')->nullable();
            $table->string('title2')->nullable();
            $table->text('descp2')->nullable();
            $table->string('img2')->nullable();
            $table->string('title3')->nullable();
            $table->text('descp3')->nullable();
            $table->string('img3')->nullable();
            $table->string('title4')->nullable();
            $table->text('descp4')->nullable();
            $table->string('img4')->nullable();
            $table->string('about_us')->nullable();
            $table->string('about_img')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_desc')->nullable();
            $table->string('seo_img')->nullable();
            $table->text('contract_us')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_base');
    }
}
