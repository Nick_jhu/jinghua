<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSysEmail20190211 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_email', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tpl');
            $table->text('descp');
            $table->text('content');
            $table->string('status');
            $table->text('to');
            $table->text('bcc');
            $table->integer('f_count');
            $table->string('schedule');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_email');
    }
}
