<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModProduct20190305 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_product', function (Blueprint $table) {
            $table->text('number_of_periods',10)->nullable();
        });
        Schema::table('mod_order', function (Blueprint $table) {
            $table->text('number_of_periods',10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_product', function (Blueprint $table) {
            $table->drop('number_of_periods');
        });
        Schema::table('mod_order', function (Blueprint $table) {
            $table->drop('number_of_periods');
        });
    }
}
