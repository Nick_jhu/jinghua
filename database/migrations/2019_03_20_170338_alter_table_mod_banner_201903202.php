<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModBanner201903202 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_banner', function (Blueprint $table) {
            $table->string('position', 20)->nullable()->default('left');
            $table->string('link', 300)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_banner', function (Blueprint $table) {
            $table->drop('position');
            $table->drop('link');
        });
    }
}
