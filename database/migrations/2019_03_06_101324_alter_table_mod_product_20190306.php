<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModProduct20190306 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_product', function (Blueprint $table) {
            $table->integer('d_percent')->nullable();
            $table->date('action_date')->nullable();
            $table->string('is_hot')->default('N');
            $table->integer('sort')->default(99);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_product', function (Blueprint $table) {
            $table->drop('d_percent');
            $table->drop('action_date');
            $table->drop('is_hot');
            $table->drop('sort');
        });
    }
}
