<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSysLog20180116 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_log', function (Blueprint $table) {
            $table->string('remark', 20)->nullable();
            $table->string('ref_no', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_log', function (Blueprint $table) {
            $table->drop('remark');
            $table->drop('ref_no');
        });
    }
}
