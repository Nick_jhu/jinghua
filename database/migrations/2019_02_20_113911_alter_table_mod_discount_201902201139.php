<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableModDiscount201902201139 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mod_discount', function (Blueprint $table) {
            $table->string('discount_way', 1)->nullable();
        });

        Schema::table('mod_discount_member', function (Blueprint $table) {
            $table->string('discount_way', 1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mod_discount', function (Blueprint $table) {
            $table->string('discount_way', 1)->nullable();
        });

        Schema::table('mod_discount_member', function (Blueprint $table) {
            $table->string('discount_way', 1)->nullable();
        });
    }
}
