<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun'            => [
        'domain'         => env('MAILGUN_DOMAIN'),
        'secret'         => env('MAILGUN_SECRET'),
    ],

    'ses'                => [
        'key'            => env('SES_KEY'),
        'secret'         => env('SES_SECRET'),
        'region'         => 'us-east-1',
    ],

    'sparkpost'          => [
        'secret'         => env('SPARKPOST_SECRET'),
    ],

    'stripe'             => [
        'model'          => App\User::class,
        'key'            => env('STRIPE_KEY'),
        'secret'         => env('STRIPE_SECRET'),
    ],

    'firebase'           => [
        'api_key'        => 'AIzaSyAoga1_Qddj-AROeeFqZJN67d_gWg4Mcaw', // Only used for JS integration
        'auth_domain'    => 'tmsapp-22e76.firebaseapp.com', // Only used for JS integration
        'database_url'   => 'https  ://tmsapp-22e76.firebaseio.com/',
        'secret'         => 'DATABASE_SECRET',
        'storage_bucket' => 'tmsapp-22e76.appspot.com', // Only used for JS integration
    ],

    'facebook'           => [
        'client_id'      => '360829844703164',
        'client_secret'  => '571fc54c9a3ffd862b84e315d8af7244',
        'redirect'       => env('APP_URL').'/login/facebook/callback',
    ],


    'google'             => [
        'client_id'      => '984233105128-ul301n8sgbkfc1gakvgs5esk74k0m2fo.apps.googleusercontent.com',
        'client_secret'  => 'cNdsOVZdbtaHs_JY2FmiFnns',
        'redirect'       => env('APP_URL').'/login/google/callback',
    ],

];
