<?php
Auth::routes();
// Backpack\CRUD: Define the resources for the entities you want to CRUD.
CRUD::resource('tag', 'TagCrudController');
Route::get('/', 'Auth\LoginController@login');
Route::group([
        'middleware' => ['admin', 'auth', 'vendor']
], function () {
    

    CRUD::resource('modBase', 'BaseCrudController');
    CRUD::resource('modNews', 'NewsCrudController');
    CRUD::resource('modQa', 'ModQaCrudController');
    CRUD::resource('modBanner', 'BannerCrudController');
    CRUD::resource('modMarquee', 'MarqueeCrudController');
    CRUD::resource('modMarquee', 'MarqueeCrudController');
    CRUD::resource('modUnboxing', 'UnboxingCrudController');
    CRUD::resource('modDiscount', 'DiscountCrudController');
    Route::get('discount', 'DiscountCrudController@discountView');
    Route::post('discount/multi/del', 'DiscountCrudController@multiDel'); 
    Route::get('sendUserDiscountCode/{id}', 'DiscountCrudController@sendUserDiscountCode');
    
    
    CRUD::resource('user', 'UserController');
    CRUD::resource('customer', 'CustomerCrudController');

    CRUD::resource('customerProfile', 'CustomerProfileCrudController');
    Route::post('custProfile/multi/del', 'CustomerProfileCrudController@multiDel'); 

    CRUD::resource('companyProfile', 'CompanyProfileCrudController');

    CRUD::resource('bulletin', 'BulletinCrudController');


    CRUD::resource('bscodeKind', 'BscodeKindCrudController');
    Route::get('bscode/{cd_type?}', 'BscodeKindCrudController@get');
    Route::post('bscode/store', 'BscodeKindCrudController@detailStore');
    Route::post('bscode/update/{cd_type}', 'BscodeKindCrudController@detailUpdate');
    Route::get('bscode/delete/{id}', 'BscodeKindCrudController@detailDel');
    Route::get('bscodeKind/detail/{cd_type}', 'BscodeKindCrudController@get');

    CRUD::resource('mailFormat', 'MailFormatCrudController');

    CRUD::resource('MemberMgmt', 'MemberMgmtCrudController');   //會員管理
    Route::post('activeMember', 'MemberMgmtCrudController@activeMember'); 


    CRUD::resource('CateMgmt', 'CateMgmtCrudController');   //類別管理

    CRUD::resource('OrderMgmt', 'OrderMgmtCrudController');   //訂單管理
    Route::get('orderDetail/{ord_id?}', 'OrderMgmtCrudController@get');
    Route::post('orderDetail/store', 'OrderMgmtCrudController@detailStore');
    Route::post('orderDetail/update/{cd_type}', 'OrderMgmtCrudController@detailUpdate');
    Route::get('orderDetail/delete/{id}', 'OrderMgmtCrudController@detailDel');
    Route::get('orderRentDetail/{ord_id?}', 'OrderMgmtCrudController@getRent');
    Route::post('orderRentDetail/store', 'OrderMgmtCrudController@detailRentStore');
    Route::post('orderRentDetail/update/{cd_type}', 'OrderMgmtCrudController@detailRentUpdate');
    Route::get('orderRentDetail/delete/{id}', 'OrderMgmtCrudController@detailRentDel');
    Route::post('order/multi/del', 'OrderMgmtCrudController@multiDel'); 
    Route::post('shipping', 'OrderMgmtCrudController@shipping'); 
    
    Route::post('returnConfirm', 'OrderMgmtCrudController@returnConfirm'); 
    Route::post('paymentConfirm', 'OrderMgmtCrudController@paymentConfirm'); 
    Route::post('sendQrCode', 'OrderMgmtCrudController@sendQrCode'); 
    Route::post('cancelPay/{id}', 'OrderMgmtCrudController@cancelPay'); 
    Route::get('PrintC2CBill/{id}', 'OrderMgmtCrudController@PrintC2CBill'); 
    Route::post('cancelWifi', 'OrderMgmtCrudController@cancelWifi'); 
    Route::post('CreateLogisticsList', 'OrderMgmtCrudController@CreateLogisticsList');
    



    //CRUD::resource('TranPlanMgmt/SendCar', 'DlvCrudController');   //運輸計劃

    CRUD::resource('QuotMgmt', 'QuotMgmtCrudController');   //報價管理

    CRUD::resource('carProfile', 'CarProfileCrudController');   //卡車建檔
    Route::post('carProfile/multi/del', 'CarProfileCrudController@multiDel'); 

    CRUD::resource('sysCountry', 'SysCountryCrudController');   //國家代碼建檔
    CRUD::resource('sysArea', 'SysAreaCrudController');   //區域建檔
    CRUD::resource('modTransStatus', 'ModTransStatusCrudController');   //貨況建檔

    CRUD::resource('goodsProfile', 'GoodsCrudController');   //料號建檔
    Route::post('goodsProfile/multi/del', 'GoodsCrudController@multiDel'); 


    

    Route::get('export/detail', 'OrderMgmtCrudController@exportDetail');  
    Route::post('confirmOrderCancel', 'OrderMgmtCrudController@confirmOrderCancel');  
    Route::post('createdInv', 'OrderMgmtCrudController@createdInv');  

    

    Route::get('exportPelican', 'OrderMgmtCrudController@exportPelican');

    Route::post('OrderMgmt/importExcel', 'OrderMgmtCrudController@uploadExcel');

    Route::get('rentDetailView', function(){
        return view('OrderMgmt.rentDetail');
    });

    Route::get('orderDetailView', function(){
        $user = Auth::user();
        $sqlQuery = "vendor='".$user->email."'";
        $url = Crypt::encrypt($sqlQuery);

        $shipper = DB::table('bscode')->where('cd_type', 'SHIPPER')->get();
        return view('OrderMgmt.orderDetail')->with(array('qUrl' => $url, 'shipper' => $shipper));
    });

    Route::get('wifiOrderView', function(){
        return view('OrderMgmt.wifiOrderView');
    });

    Route::get('normalOrderView', function(){
        return view('OrderMgmt.normalOrderView');
    });
});

Route::group([
    'middleware' => ['admin', 'auth']
], function () {    

    Route::get('searchList/get/{key}', 'CommonController@getSearchLayoutList');
    Route::get('searchHtml/get/{id?}', 'CommonController@getSearchHtml');
    Route::post('saveSearchLayout', 'CommonController@saveSearchLayout');
    Route::put('setSearchDefault/{key}/{id}', 'CommonController@setSearchDefault');
    Route::delete('delSearchLayout/{id}', 'CommonController@delSearchLayout');

    Route::post('userPwd/update', 'CommonController@updatePwd');

    Route::get('get/{table?}/{id?}', 'CommonController@getData'); 
    Route::get('gcsd/get', 'CommonController@getCompnayList'); 
        
    Route::get('board', 'DashboardController@dashboard')->name('admin.board');
    Route::get('getFieldsLangSepc/{table}', 'BaseApiController@getFieldsLangSepc');

    CRUD::resource('ProdMgmt', 'ProdMgmtCrudController');   //商品管理
    Route::post('product/multi/del', 'ProdMgmtCrudController@multiDel'); 
    Route::get('prodDetail/{prod_id?}', 'ProdMgmtCrudController@get');
    Route::post('prodDetail/store', 'ProdMgmtCrudController@detailStore');
    Route::post('prodDetail/update/{cd_type}', 'ProdMgmtCrudController@detailUpdate');
    Route::get('prodDetail/delete/{id}', 'ProdMgmtCrudController@detailDel');
    Route::get('prodGift/{prod_id?}', 'ProdMgmtCrudController@giftget');
    Route::post('prodGift/store', 'ProdMgmtCrudController@giftStore');
    Route::post('prodGift/update/{cd_type}', 'ProdMgmtCrudController@giftUpdate');
    Route::get('prodGift/delete/{id}', 'ProdMgmtCrudController@giftDel');
    Route::post('product/multi/added', 'ProdMgmtCrudController@multiadded'); 
    Route::post('product/multi/remove', 'ProdMgmtCrudController@multiremove'); 

    Route::get('snDetail/{prod_id?}', 'ProdMgmtCrudController@snGet');
    Route::post('snDetail/store', 'ProdMgmtCrudController@snStore');
    Route::post('snDetail/update/{cd_type}', 'ProdMgmtCrudController@snUpdate');
    Route::get('snDetail/delete/{id}', 'ProdMgmtCrudController@snDel');
    Route::get('chkProdNoExist/{prodNo}', 'ProdMgmtCrudController@chkProdNoExist');

    //商品excel匯出
    Route::get('exportSoftware', 'ProdMgmtCrudController@exportSoftware');
    Route::get('exportNormal', 'ProdMgmtCrudController@exportNormal');
    Route::get('exportWifi', 'ProdMgmtCrudController@exportWifi');
    Route::post('product/importExcel/{mode}/{mainCol}', 'ProdMgmtCrudController@uploadExcel');
    Route::get('product/importExcel/{mode}/{mainCol}', 'ProdMgmtCrudController@uploadExcel');


    CRUD::resource('OrderMgmt', 'OrderMgmtCrudController');   //訂單管理
    Route::post('shipping', 'OrderMgmtCrudController@shipping'); 
    Route::post('prodShipping', 'OrderMgmtCrudController@prodShipping'); 
    Route::post('vendorUpdateShipmentNo', 'OrderMgmtCrudController@vendorUpdateShipmentNo'); 
    Route::post('returnConfirm', 'OrderMgmtCrudController@returnConfirm'); 
    Route::post('paymentConfirm', 'OrderMgmtCrudController@paymentConfirm'); 
    Route::post('sendQrCode', 'OrderMgmtCrudController@sendQrCode'); 
    Route::post('cancelPay/{id}', 'OrderMgmtCrudController@cancelPay'); 
    Route::get('PrintC2CBill/{id}', 'OrderMgmtCrudController@PrintC2CBill'); 
    Route::post('cancelWifi', 'OrderMgmtCrudController@cancelWifi'); 
    Route::post('CreateLogisticsList', 'OrderMgmtCrudController@CreateLogisticsList');
    Route::post('sendToZ', 'OrderMgmtCrudController@sendToZ');


    Route::get('export/detail', 'OrderMgmtCrudController@exportDetail');  
    Route::post('confirmOrderCancel', 'OrderMgmtCrudController@confirmOrderCancel');  
    Route::post('createdInv', 'OrderMgmtCrudController@createdInv');  

    //商品excel匯出
    Route::get('exportSoftware', 'ProdMgmtCrudController@exportSoftware');
    Route::get('exportNormal', 'ProdMgmtCrudController@exportNormal');
    Route::get('exportWifi', 'ProdMgmtCrudController@exportWifi');
    Route::post('product/importExcel/{mode}/{mainCol}', 'ProdMgmtCrudController@uploadExcel');
    Route::get('product/importExcel/{mode}/{mainCol}', 'ProdMgmtCrudController@uploadExcel');
    Route::get('exportPelican', 'OrderMgmtCrudController@exportPelican');
    Route::post('OrderMgmt/importExcel', 'OrderMgmtCrudController@uploadExcel');


    Route::get('vendorOrderDetail', function(){
        $user = Auth::user();
        $sqlQuery = "(vendor='".$user->email."' and `status` in ('出貨', '已刷退', '已付款', '已送達', '確認取消', '已刷退')) ";
        $url = Crypt::encrypt($sqlQuery);

        $shipper = DB::table('bscode')->where('cd_type', 'SHIPPER')->whereNotIn('cd', ['UNIMARTC2C','FAMIC2C','HILIFEC2C'])->get();
        return view('OrderMgmt.vendorOrderDetail')->with(array('qUrl' => $url, 'shipper' => $shipper));
    });

    Route::get('normalOrderView', function(){
        return view('OrderMgmt.normalOrderView');
    });
});

Route::auth();
Route::get('logout', 'Auth\LoginController@logout');

