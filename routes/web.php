<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('FrontEnd.index');
Route::get('about', 'HomeController@about')->name('FrontEnd.about');
// Route::post('/member/logout', 'Auth\MemberLoginController@logout')->name('member.logout');

// Route::get('member/forget', 'Auth\MemberLoginController@showForgetForm')->name('member.forget');
// Route::post('member/forget', 'Auth\MemberLoginController@forget')->name('member.forget.submit');
// Route::get('member/login', 'Auth\MemberLoginController@showLoginForm')->name('member.login');
// Route::post('member/login', 'Auth\MemberLoginController@login')->name('member.login.submit');
// Route::get('member/logout', 'Auth\MemberLoginController@logout')->name('member.logout');

Route::post('reSendCode', 'RegisterController@reSendCode');


Route::get('member/register', 'RegisterController@index');
Route::post('do/register', 'RegisterController@do_register');
Route::get('register/confirm', 'RegisterController@registerConfirm');
Route::get('register/mail/check/{code}', 'RegisterController@registerMailCheck');
Route::post('do/confirm', 'RegisterController@confirm_validation')->name('member.do.confirm');

Route::get('c', 'MemberCenterController@index');
Route::get('ce', 'MemberCenterController@index');
Route::get('cen', 'MemberCenterController@index');
Route::get('cent', 'MemberCenterController@index');
Route::get('cente', 'MemberCenterController@index');
Route::get('center', 'MemberCenterController@index')->name('member.center');
Route::get('getAreaByCity', 'MemberCenterController@getAreaByCity');
Route::get('reSendSnMail/{orderDetailId}', 'MemberCenterController@reSendSnMail');
Route::get('getSn/{orderDetailId}', 'MemberCenterController@getSn');
Route::get('memberInfo', 'MemberCenterController@memberInfo');

Route::post('member/update', 'MemberCenterController@updateMemberData');

Route::get('product', 'ProductController@index');

Route::get('productDetail/{id}', 'ProductController@productDetail');

Route::get('qa', 'QaController@index');
Route::get('news', 'NewsController@index');
Route::get('newsDetail/{id}', 'NewsController@newsDetail');

Route::get('process', function(){
    return view('FrontEnd.process')->with(['viewName'=>'process']);
});

//Route::get('cart', 'CartController@index');
Route::get('search', 'GuestCartController@searchView');
Route::get('search_list', 'GuestCartController@searchView');
Route::post('search_list', 'GuestCartController@searchlistView');
Route::get('cart', 'GuestCartController@cartView');
Route::post('cartsub', 'GuestCartController@cartsub');
Route::get('cartsub', 'GuestCartController@cartsub');
Route::get('soapDemo', 'SoapController@show');
Route::get('soapDemo/createOrder', 'SoapController@createOrder');
Route::get('soapDemo/getOrderStatus', 'SoapController@getOrderStatus');
Route::get('soapDemo/getStock', 'SoapController@getStock');
Route::get('soapDemo/deleteOrder', 'SoapController@deleteOrder');
Route::get('chkship/{pickWay}', 'CartController@chkship');
Route::get('get/shipway', 'GuestCartController@getShipWayFromCart');


Route::get('getUserData', 'CartController@getUserData');
Route::post('pay', 'CartController@payment');
Route::post('rePay', 'CartController@rePayment');
Route::get('logiPay/{ordId}', 'CartController@logiPay');



Route::get('lang/{locale}', ['as'=>'lang.change', 'uses'=>'LanguageController@setLocale']);

Route::post('allPayReply', 'HomeController@allPayReply');
Route::get('allPayReply', 'HomeController@allPayReply');

Route::post('allPayReplySchedule', 'HomeController@allPayReplySchedule');
Route::get('allPayReplySchedule', 'HomeController@allPayReplySchedule');

Route::post('trackingReply', 'HomeController@trackingReply');
Route::get('trackingReply', 'HomeController@trackingReply');

Route::get('chkAuth', 'GuestCartController@chkAuth');

Route::get('cancelOrder/{ordId}', 'MemberCenterController@cancelOrder');
Route::post('addCart', 'GuestCartController@addCart');
Route::post('updateInsuranceToCart', 'GuestCartController@updateInsuranceToCart');
Route::post('updateNumToCart', 'GuestCartController@updateNumToCart');
Route::post('delCart', 'GuestCartController@delCart');

Route::get('ecpCallback', 'HomeController@ecpCallback');
Route::post('ecpCallback', 'HomeController@ecpCallback');

Route::get('reSendVcode', function(){
    return view('FrontEnd.auth.reSendVcode')->with(['viewName' => 'login']);
});

Route::post('reSendVcode', 'RegisterController@reSendVcode');
Route::post('getDiscountCode', 'CartController@getDiscountCode');


Route::get('orderQrCode/{ordNo?}', 'HomeController@orderQrCode');

Route::get('genShorty', 'HomeController@genShorty');


Route::group([
    'namespace' => '\Admin',
    'prefix'    => 'allpay'],
    function () {
        Route::get('/', 'AllPayController@index');
        Route::get('/checkout', 'AllPayController@checkout');
        Route::get('/payinmart', 'AllPayController@payinmart');
        Route::get('/payinmart_reply', 'AllPayController@payinmart_reply');
        Route::get('/send_msg', 'AllPayController@send_msg');
        
    }
);


Route::get('spg', 'CartController@spg');
Route::post('spgCallBack', 'HomeController@spgCallBack');
Route::get('spgCallBack', 'HomeController@spgCallBack');
Route::post('spgCallBackSchedule', 'HomeController@spgCallBackSchedule');
Route::get('spgCallBackSchedule', 'HomeController@spgCallBackSchedule');
Route::get('spgCustomerCallBack', 'HomeController@spgCustomerCallBack');
Route::post('spgCustomerCallBack', 'HomeController@spgCustomerCallBack');


Route::get('mapCallBack', 'CartController@mapCallBack');
Route::post('mapCallBack', 'CartController@mapCallBack');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('payment', function(){
    $viewData = [
        'viewName'    => 'payment'
    ];

    return View('FrontEnd.payment')->with($viewData);
});

Route::get('tappay/{prime}', 'CartController@tapPay');
Route::get('map/choose/{isCollection}/{pickWay}', 'CartController@testCvsMap');
Route::get('testMapCallBack', 'CartController@testMapCallBack');
Route::post('testMapCallBack', 'CartController@testMapCallBack');

Route::get('thirdPartyPayCallBack', 'CartController@thirdPartyPayCallBack');
Route::get('thirdPartyPayCallBack1', 'CartController@thirdPartyPayCallBack1');
Route::get('linepay', 'CartController@linepay');
Route::get('shipfee/get/{shipper}', 'CartController@getShipFee');
Route::get('get/cart', 'GuestCartController@getCart');
Route::get('updateInstalmentList', 'GuestCartController@updateInstalmentList');

Route::get('getHotProductForMenu/{cateId?}', 'ProductController@getHotProductForMenu');
Route::get('getNextMenu/{cateId?}', 'ProductController@getNextMenu');

Auth::routes();

Route::get('newcart', function() {
    $viewData = array(
        'viewName' => 'cart'
    );

    return view('FrontEnd.newCart')->with($viewData);
});
