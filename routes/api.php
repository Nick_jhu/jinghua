<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    //'middleware' => ['web', 'admin'],
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
], function () {
// Route::post('login', 
    //Route::get('example/getData', 'ExampleController@getData');
    //Route::get('example/getFieldsJson', 'ExampleController@getFieldsJson');
    Route::get('baseApi/getGridJson/{table}/{status?}', 'Admin\BaseApiController@getGridJson');
    Route::get('baseApi/getFieldsJson/{table}', 'Admin\BaseApiController@getFieldsJson');
    Route::get('baseApi/getLookupGridJson/{info1}/{info2}/{info3}', 'Admin\BaseApiController@getLookupGridJson');
    Route::get('baseApi/getLookupFieldsJson/{info1}/{info2}', 'Admin\BaseApiController@getLookupFieldsJson');
    Route::get('baseApi/getAutocompleteJson/{info1}/{info2}/{info3}/{input}', 'Admin\BaseApiController@getAutocompleteJson');
    Route::get('baseApi/getAutoNumber/{model}', 'Admin\BaseApiController@getAutoNumber');
    
    Route::get('baseApi/getLayoutJson', 'Admin\BaseApiController@getLayoutJson');
    Route::post('baseApi/saveLayoutJson', 'Admin\BaseApiController@saveLayoutJson');
    Route::post('baseApi/clearLayout', 'Admin\BaseApiController@clearLayout');

    //貨況相關
    //Route::get('trackingApi/get/{ord_no?}', 'TrackingApiController@getTrackingByOrder');
    //Route::get('tracking/get/{ord_no?}', 'TrackingApiController@getTsRecord');
    

    //Route::get('dlvApi/getDlvData', 'DlvCrudController@getDlvData');
    //Route::get('getOrderData/{dlv_no?}', 'OrderMgmtCrudController@getOrderData');
    Route::get('getOrderDetail/{ord_no?}', 'OrderMgmtCrudController@getOrderDetail');
    Route::post('uploadChk', 'OrderMgmtCrudController@orderUpload');
    Route::post('sendMessage', 'FcmController@sendMessage');
    Route::post('export/data', 'CommonController@exportData');

    Route::get('category/{id}', 'Api\CategoryController@show');
});







    
