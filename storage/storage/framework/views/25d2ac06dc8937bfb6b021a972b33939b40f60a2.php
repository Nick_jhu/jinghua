


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo e(asset('vendor/adminlte/')); ?>/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('vendor/adminlte/')); ?>/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(asset('vendor/adminlte/')); ?>/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page" style="height:100vh;background-image: url('../img/login_bg.jpg');background-repeat: no-repeat;background-size:cover;overflow:hidden;background-position:center center;">
    <style>
        .login-box-body {
            /* Safari 3-4, iOS 1-3.2, Android 1.6- */
            -webkit-border-radius: 20px;
            /* Firefox 1-3.6 */
            -moz-border-radius: 20px;
            /* Opera 10.5, IE 9, Safari 5, Chrome, Firefox 4, iOS 4, Android 2.1+ */
            border-radius: 20px;
        }
    </style>
    <div class="login-box" style="margin: 5% auto">

        <!-- /.login-logo -->
        <div class="login-box-body" style="
    background-color: rgba(255, 255, 255, 0);
    border: 1px solid rgba(102, 102, 102, 0.26);">
            <div class="login-logo">
                <a href="./" style="color:#000; font-size:45px">
                    <b>Standard</b>TMS </a>
            </div>


            <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url(config('backpack.base.route_prefix').'/login')); ?>">
                <?php echo csrf_field(); ?>

                <div class="form-group has-feedback <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                    <input type="email" class="form-control" placeholder="Email" name="email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <?php if($errors->has('email')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                    <?php endif; ?>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <?php if($errors->has('password')): ?>
                    <span class="help-block">
                   
                        <strong><?php echo e($errors->first('password')); ?></strong>
                    </span>
                    <?php endif; ?>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Company" name="c_key">
                    <span class="fa fa-building-o form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <select class="form-control" name="lang" id="mutiLang">
                        <?php $__currentLoopData = Config::get('app.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang => $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($lang); ?>"><?php echo e($language); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <span class="fa fa-language form-control-feedback"></span>
                </div>
                <?php if($loginStatus): ?>
                        
                <div class="form-group has-feedback">
                    <span style="color:red">
                        <?php echo e($loginStatus); ?>

                    </span>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <div class="social-auth-links text-center"></div>
            <!-- /.social-auth-links -->

            
            <p>版本：<?php echo e(Config::get('app.version')); ?></p>
            <br />
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery 3 -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo e(asset('vendor/adminlte')); ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="<?php echo e(asset('vendor/adminlte')); ?>/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
            var lang = window.navigator.userLanguage || window.navigator.language;
            switch (lang) {
                case 'zh-CN' :                        
                        $('#mutiLang option[value=zh-CN]').prop('selected', true);
                        break;
                case 'zh-TW' :
                        $('#mutiLang option[value=zh-TW]').prop('selected', true);
                        break;
                case 'en' :
                        $('#mutiLang option[value=en]').prop('selected', true);
                        break;
                default:
                        $('#mutiLang option[value=zh-TW]').prop('selected', true);
                        break;
            }
            //alert(lang);
        });
    </script>
</body>

</html>