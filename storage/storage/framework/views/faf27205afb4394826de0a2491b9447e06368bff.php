 <?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		<?php echo e(trans('modDlv.titleName')); ?>

		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?php echo e(trans('modDlv.titleName')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> <?php $__env->startSection('before_scripts'); ?>


<script>
	var gridOpt = {};
gridOpt.enabledStatus = true;
gridOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_dlv')); ?>";
gridOpt.dataUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_dlv')); ?>";
gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
gridOpt.createUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar/create')); ?>";
gridOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar')); ?>" + "/{id}/edit";
gridOpt.height = 800;

var btnGroup = [
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "<?php echo e(trans('common.exportExcel')); ?>",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '<?php echo e(trans("modDlv.titleName")); ?>');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "<?php echo e(trans('common.gridOption')); ?>",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"<?php echo e(trans('common.add')); ?>",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  },
  {
    btnId:"btnSendApp",
    btnIcon:"fa fa-mobile",
    btnText:"<?php echo e(trans('modDlv.sendApp')); ?>",
    btnFunc:function(){
      swal("<?php echo e(trans('modDlv.msg1')); ?>", "<?php echo e(trans('modDlv.msg2')); ?>", "success");
    }
  },
  {
    btnId:"btnAutoRoute",
    btnIcon:"fa fa-road",
    btnText:"<?php echo e(trans('modDlv.routingPlan')); ?>",
    btnFunc:function(){
      swal("<?php echo e(trans('modDlv.msg3')); ?>", "", "success");
    }
  },
  {
    btnId:"btnCancelCar",
    btnIcon:"fa fa-ban",
    btnText:"<?php echo e(trans('modDlv.cancelCar')); ?>",
    btnFunc:function(){
      swal("<?php echo e(trans('modDlv.msg4')); ?>", "", "success");
    }
  },
];

</script>
<?php $__env->stopSection(); ?> <?php echo $__env->make('backpack::template.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>