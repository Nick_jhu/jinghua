 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
		<?php echo e(trans('bscodeKind.titleAddName')); ?>

		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo e(url(config('backpack.base.route_prefix'),'bscodeKind')); ?>"><?php echo e(trans('bscodeKind.titleName')); ?></a>
		</li>
		<li class="active"><?php echo e(trans('bscodeKind.titleAddName')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?> 
<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">

	<div class="col-md-12">
		<div class="callout callout-danger" id="errorMsg" style="display:none">
			<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
			<ul>

			</ul>
		</div>
		<form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
			<input type="hidden" name="g_key" class="form-control">
			<input type="hidden" name="c_key" class="form-control">
			<input type="hidden" name="s_key" class="form-control">
			<input type="hidden" name="d_key" class="form-control">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('bscodeKind.baseInfo')); ?></a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<form role="form">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="cd_type"><?php echo e(trans('bscodeKind.cdType')); ?></label>
										<input type="text" class="form-control" id="cd_type" name="cd_type" placeholder="Enter No.">
									</div>
									<div class="form-group col-md-4">
										<label for="cd_descp"><?php echo e(trans('bscodeKind.cdDescp')); ?></label>
										<input type="text" class="form-control" id="cd_descp" name="cd_descp" placeholder="Enter Description">
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-3">
										<label for="created_by"><?php echo e(trans('bscodeKind.createdBy')); ?></label>
										<input type="text" class="form-control" id="created_by" name="created_by">
									</div>
									<div class="form-group col-md-3">
										<label for="created_at"><?php echo e(trans('bscodeKind.createdAt')); ?></label>
										<input type="text" class="form-control" id="created_at" name="created_at">
									</div>
									<div class="form-group col-md-3">
										<label for="updated_by"><?php echo e(trans('bscodeKind.updatedBy')); ?></label>
										<input type="text" class="form-control" id="updated_by" name="updated_by">
									</div>
									<div class="form-group col-md-3">
										<label for="updated_at"><?php echo e(trans('bscodeKind.updatedAt')); ?></label>
										<input type="text" class="form-control" id="updated_at" name="updated_at">
									</div>
								</div>


								<?php if(isset($id)): ?>
								<input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> <?php endif; ?>

							</div>
						</form>
					</div>
					<!-- /.tab-pane -->

				</div>
				<!-- /.tab-content -->
			</div>
        </form>
	</div>
	
</div>

<div class="row">

	<div class="col-md-12">
        <div class="nav-tabs-custom" <?php if(!isset($id)): ?> style="display:none" <?php endif; ?> id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_2" data-toggle="tab" aria-expanded="false"><?php echo e(trans('bscodeKind.detail')); ?></a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_2">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                        <h3 class="box-title"><?php echo e(trans('bscode.titleName')); ?></h3>
                        </div>
                        <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="cd"><?php echo e(trans('bscode.cd')); ?></label>
                                    <input type="text" class="form-control input-sm" name="cd" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cd_descp"><?php echo e(trans('bscode.cdDescp')); ?></label>
                                    <input type="text" class="form-control input-sm" name="cd_descp" grid="true" >
                                </div>                                    
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="value1"><?php echo e(trans('bscode.value1')); ?></label>
                                    <input type="text" class="form-control input-sm" name="value1" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value2"><?php echo e(trans('bscode.value2')); ?></label>
                                    <input type="text" class="form-control input-sm" name="value2" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value3"><?php echo e(trans('bscode.value3')); ?></label>
                                    <input type="text" class="form-control input-sm" name="value3" grid="true" >
                                </div>                                      
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                            <button type="button" class="btn btn-sm btn-primary" id="Save"><?php echo e(trans('common.save')); ?></button>
                            <button type="button" class="btn btn-sm btn-danger" id="Cancel"><?php echo e(trans('common.cancel')); ?></button>
                        </div>
                        </button>
                    </div>
                    
                    <div id="jqxGrid"></div>
                </div>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
	</div>
	
</div>


<?php $__env->stopSection(); ?> 
<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $__env->startSection('after_scripts'); ?>

<script type="text/javascript" src="<?php echo e(asset('vendor/jqwidgets')); ?>/jqxwindow.js"></script>


<script>
	var mainId = "";
    var cd_type = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind')); ?>";

    var fieldData = null;
    var fieldObj = null;

    <?php if(isset($crud->create_fields)): ?>
        fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    <?php endif; ?>

    
    
    <?php if(isset($id)): ?>
        mainId = "<?php echo e($id); ?>";
        editData = '{<?php echo $entry; ?>}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cd_type= editObj.cd_type;
        console.log(editObj);
    <?php endif; ?>

    
   

    $(function(){

        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind')); ?>";
        formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') .'/get/bscode_kind')); ?>/" + mainId;
        formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind')); ?>";
        formOpt.addFunc = function(){
            $('#jqxGrid').jqxGrid('clear');
            $("#subPanel").hide();
        };
        formOpt.initFieldCustomFunc = function (){
            $("select[name='state[]']").select2({tags: true});
        };
        formOpt.afterDel = function() {
            $('#jqxGrid').jqxGrid('clear');
            $('#packDetailGrid').jqxGrid('clear');
        }

        formOpt.editFunc = function() {
            $("#subPanel").show();
        }

        formOpt.copyFunc = function() {
            $("#subPanel").hide();
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

        $.get( "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/bscode')); ?>", function( fieldData ) {
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = fieldData;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/bscode')); ?>" + '/' + cd_type;
            opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscode')); ?>" + "/store";
            opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscode')); ?>" + "/update/"+ cd_type;
            opt.delUrl  = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/bscode')); ?>" + "/delete/";
            opt.defaultKey = {'cd_type': cd_type};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                
            }

            genDetailGrid(opt);
        });
    })

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>