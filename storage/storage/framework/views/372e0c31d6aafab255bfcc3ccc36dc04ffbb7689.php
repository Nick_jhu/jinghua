

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      Excel匯入<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'dashboard')); ?>"><?php echo e(trans('backpack::crud.admin')); ?></a></li>
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/log')); ?>"><?php echo e(trans('backpack::logmanager.log_manager')); ?></a></li>
        <li class="active"><?php echo e(trans('backpack::logmanager.existing_logs')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
   
    <div class="col-md-10">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
        
                    
                    
                      
						<?php echo e(csrf_field()); ?>

						<input type="file" name="import_file" id="importFile"/>
						<button type="submit" class="btn btn-primary" id="btnImport">Excel匯入</button>
						<button type="button" class="btn btn-primary" id="btnDel">刪除</button>
						<button type="button" class="btn btn-primary" id="btnSave">保存</button>
                    
                    <!-- /.tab-pane -->
                   
                        
                        
                        <div id="jqxGrid"></div>
          
                    <!-- /.tab-pane -->

                    
                    <!-- /.tab-content -->
        
            </div>
        </form>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startSection('after_scripts'); ?>

<script type="text/javascript" src="<?php echo e(asset('vendor/jqwidgets')); ?>/jqxwindow.js"></script>


<script>
   

    $(function(){

	
			var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'dlv_cust_nm', type: 'string' },
                        { name: 'etd', type: 'date' },
                        { name: 'driver', type: 'string' },
                        { name: 'remark', type: 'string' },
                        { name: 'ord_no', type: 'string' },
						{ name: 'dlv_attn', type: 'string' },
						{ name: 'pkg_num', type: 'float' },
						{ name: 'total_cbm', type: 'float' },
						{ name: 'dlv_addr', type: 'string' },
						{ name: 'dlv_tel', type: 'number' }
					],
					root:"Rows",
					pagenum: 1,					
                    beforeprocessing: function (data) {
                        source.totalrecords = data[0].TotalRows;
                    },
                    filter: function () {
                    // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
                    },
                    sort: function () {
                        // update the grid and send a request to the server.
                        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
                    },
                    pagesize: 20,
					url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/ExcelImport/get')); ?>",					
				};
				var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                    // get data records.
                    // var records = dataAdapter.records;
					// var length = records.length;
					// if(length>0){
					// 	swal(
					// 		'提示',
					// 		'您有上一次資料未保存!!',
					// 		'warning'
					// 	  )
					// }
				}
				});
                //var dataAdapter = new $.jqx.dataAdapter(source);
                $("#jqxGrid").jqxGrid(
                {
                    width: '100%',
                    height: '100%',
					source: dataAdapter,
					selectionmode: 'checkbox',
					columnsresize: true,
                    columns: [
                        { text: '客戶名稱', datafield: 'dlv_cust_nm', width: 250 },
                        { text: '到貨日期', datafield: 'etd', width: 150,cellsformat: 'yyyy-MM-dd' },
                        { text: '司機', datafield: 'driver', width: 180 },
                        { text: '備註', datafield: 'remark', width: 120 },
						{ text: '客戶貨單編號', datafield: 'ord_no' },
						{ text: '收貨人', datafield: 'dlv_attn', width: 120 },
						{ text: '件數', datafield: 'pkg_num', width: 120, cellsalign: 'right' },
						{ text: '總材積', datafield: 'total_cbm', width: 120, cellsalign: 'right' },
						{ text: '收貨人地址', datafield: 'dlv_addr', width: 120 },
						{ text: '收貨人電話', datafield: 'dlv_tel', width: 120, cellsalign: 'right' }
                    ]
                });
				
        
			$("#myForm").submit(function () {
				if ($('#importFile').get(0).files.length === 0) {
    				swal('請上傳檔案', "", "warning");
					return false;
				}
				var postData = new FormData($(this)[0]);
				$.ajax({
                url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/ExcelImport/importExcel')); ?>",
                type: 'POST',
                data: postData,
                async: false,
                beforeSend: function () {
                    
                },
                error: function () {
                   
                },
                success: function (data) {
                   //alert(data);
				   //console.log(data);
				// var source =
                // {
                //     datatype: "json",
                //     datafields: [
                //         { name: 'dlv_cust_nm', type: 'string' },
                //         { name: 'etd', type: 'date' },
                //         { name: 'driver', type: 'string' },
                //         { name: 'remark', type: 'string' },
                //         { name: 'ord_no', type: 'string' },
				// 		{ name: 'dlv_attn', type: 'string' },
				// 		{ name: 'pkg_num', type: 'float' },
				// 		{ name: 'total_cbm', type: 'float' },
				// 		{ name: 'dlv_addr', type: 'string' },
				// 		{ name: 'dlv_tel', type: 'number' }
                //     ],
                //     localdata: data
                // };
                // var dataAdapter = new $.jqx.dataAdapter(source);
                // $("#jqxGrid").jqxGrid(
                // {
                //     width: '100%',
                //     height: '100%',
				// 	source: dataAdapter,
				// 	selectionmode: 'checkbox',
                //     columns: [
                //         { text: 'dlv_cust_nm', datafield: 'dlv_cust_nm', width: 250 },
                //         { text: 'etd', datafield: 'etd', width: 150,cellsformat: 'yyyy-MM-dd' },
                //         { text: 'driver', datafield: 'driver', width: 180 },
                //         { text: 'remark', datafield: 'remark', width: 120 },
				// 		{ text: 'ord_no', datafield: 'ord_no' },
				// 		{ text: 'dlv_attn', datafield: 'dlv_attn', width: 120 },
				// 		{ text: 'pkg_num', datafield: 'pkg_num', width: 120, cellsalign: 'right' },
				// 		{ text: 'total_cbm', datafield: 'total_cbm', width: 120, cellsalign: 'right' },
				// 		{ text: 'dlv_addr', datafield: 'dlv_addr', width: 120 },
				// 		{ text: 'dlv_tel', datafield: 'dlv_tel', width: 120, cellsalign: 'right' }
                //     ]
				// });
				$('#jqxGrid').jqxGrid('updatebounddata');
				// var dataAdapter = new $.jqx.dataAdapter(data.data);
                // $("#jqxGrid").jqxGrid({ source: dataAdapter });

                },
                cache: false,
                contentType: false,
                processData: false
            });
						// $.post("<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/ExcelImport/importExcel')); ?>", {postData}, function (data) {
						// 	if(data.length == 0) {
						// 		swal("查無結果", "", "warning");
						// 		return;
						// 	}
							
						// });
			return false;
		});
		
		$("#btnDel").on("click",function(event){
            //event.stopPropagation()
			var rowIndexes = $('#jqxGrid').jqxGrid('getselectedrowindexes');
			if(rowIndexes.length===0){
				swal('請至少選一筆記錄', "", "warning");
				return;
			}
			var rowIds = new Array();
			for (var i = 0; i < rowIndexes.length; i++) {
				var currentId = $('#jqxGrid').jqxGrid('getrowid', rowIndexes[i]);
				rowIds.push(currentId);
			};
			$('#jqxGrid').jqxGrid('deleterow', rowIds);
			$('#jqxGrid').jqxGrid('clearselection');

		});    
		
		$("#btnSave").on("click",function(event){
			var rows = $('#jqxGrid').jqxGrid('getrows');
			// if(rows.length===0){
			// 	swal('至少需要有一筆資料才能保存', "", "warning");
			// 	return;
			// }
            //event.stopPropagation()
			swal({
				title: "確定保存?",
				text: "一旦保存資料將匯入至資料庫!",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				})
				.then((willDelete) => {
				if (willDelete) {
				//var postData = new FormData($('#myForm')[0]);
				// $.post("<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/ExcelImport/store')); ?>",{'obj':JSON.stringify(rows)}, function(data){

				// }, 'JSON');
				$.ajax({
				url: "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/ExcelImport/store')); ?>",
				type: "POST",				
				data: {'obj':JSON.stringify(rows)},
				async: false,
				dataType:"json",
				beforeSend: function () {
					loadingFunc.show();
				},
				error: function (jqXHR, exception) {
					loadingFunc.hide();
					return false;
				},
				success: function (data) {
					if(data.msg == "success") {
						swal("保存成功", {
						icon: "success",
					});
					}
					$('#jqxGrid').jqxGrid('updatebounddata');
					loadingFunc.hide();
					return false;
				}

					});	
				
				} else {
					//swal("Your imaginary file is safe!");
				}
				});
        });    
    });
</script>    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>