 
<?php $__env->startSection('header'); ?>
<section class="content-header">
	<h1>
	<?php echo e(trans('modOrder.orderInfo')); ?><small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'OrderMgmt')); ?>"><?php echo e(trans('modOrder.titleName')); ?></a></li>
		<li class="active"><?php echo e(trans('modOrder.titleAddName')); ?></li>
	</ol>
</section>
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('modOrder.basicInfo')); ?></a></li>
						<li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><?php echo e(trans('modOrder.deliveryInfo')); ?></a></li>
						<li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false"><?php echo e(trans('modOrder.pickupInfo')); ?></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">

							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="ord_no"><?php echo e(trans('modOrder.ordNo')); ?></label>
										<input type="text" class="form-control" id="ord_no" name="ord_no">
									</div>
									<div class="form-group col-md-4">
										<label for="status"><?php echo e(trans('modOrder.status')); ?></label>
										<select class="form-control" id="status" name="status">
                                            <option value="UNTREATED"><?php echo e(trans('modOrder.STATUS_UNTREATED')); ?></option>
                                            <option value="SEND"><?php echo e(trans('modOrder.STATUS_SEND')); ?></option>
                                            <option value="LOADING"><?php echo e(trans('modOrder.STATUS_LOADING')); ?></option>
                                            <option value="SETOFF"><?php echo e(trans('modOrder.STATUS_SETOFF')); ?></option>
                                            <option value="NORMAL"><?php echo e(trans('modOrder.STATUS_NORMAL')); ?></option>
                                            <option value="ERROR"><?php echo e(trans('modOrder.STATUS_ERROR')); ?></option>
                                            <option value="DELAY"><?php echo e(trans('modOrder.STATUS_DELAY')); ?></option>
                                            <option value="FINISHED"><?php echo e(trans('modOrder.STATUS_FINISHED')); ?></option>
                                        </select>
									</div>
									<div class="form-group col-md-4">
										<label for="trs_mode"><?php echo e(trans('modOrder.trsMode')); ?></label>
										<select class="form-control" id="trs_mode" name="trs_mode">
                                            <option value="C">專車</option>
                                            <option value="N">非專車</option>
                                            <option value="D">快遞</option>
                                        </select>
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-4">
										<label for="truck_cmp_no"><?php echo e(trans('modOrder.truckCmpNo')); ?></label>
										<input type="text" class="form-control" id="truck_cmp_no" name="truck_cmp_no">
									</div>
									<div class="form-group col-md-4">
										<label for="truck_cmp_nm"><?php echo e(trans('modOrder.truckCmpNm')); ?></label>
										<input type="text" class="form-control" id="truck_cmp_nm" name="truck_cmp_nm">
									</div>
									<div class="form-group col-md-4">
										<label for="driver"><?php echo e(trans('modOrder.driver')); ?></label>
										<input type="text" class="form-control" id="driver" name="driver">
									</div>
								</div>

								<div class="row">
									<div class="form-group col-md-4">
										<label for="car_no"><?php echo e(trans('modOrder.truckNo')); ?></label>
										<input type="text" class="form-control" id="car_no" name="car_no">
									</div>
									<div class="form-group col-md-8">
										<label for="remark"><?php echo e(trans('modOrder.remark')); ?></label>
										<input type="text" class="form-control" id="remark" name="remark">
									</div>
								</div>


								<?php if(isset($id)): ?>
								<input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								<?php endif; ?>

							</div>
						</div>

						<div class="tab-pane" id="tab_2">
							<div class="row">
								<div class="form-group col-md-4">
									<label for="dlv_cust_no"><?php echo e(trans('modOrder.dlvCustNo')); ?></label>
									<input type="text" class="form-control" id="dlv_cust_no" name="dlv_cust_no">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_cust_nm"><?php echo e(trans('modOrder.dlvCustNm')); ?></label>
									<input type="text" class="form-control" id="dlv_cust_nm" name="dlv_cust_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_attn"><?php echo e(trans('modOrder.dlvAttn')); ?></label>
									<input type="text" class="form-control" id="dlv_attn" name="dlv_attn">
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-md-4">
									<label for="dlv_tel"><?php echo e(trans('modOrder.dlvTel')); ?></label>
									<input type="text" class="form-control" id="dlv_tel" name="dlv_tel">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_email"><?php echo e(trans('modOrder.dlvEmail')); ?></label>
									<input type="text" class="form-control" id="dlv_email" name="dlv_email">
								</div>
							</div>

							<div class="row">
							    <div class="form-group col-md-4">
									<label for="dlv_zip"><?php echo e(trans('modOrder.dlvZip')); ?></label>
									<input type="text" class="form-control" id="dlv_zip" name="dlv_zip">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_city_nm"><?php echo e(trans('modOrder.dlvCityNm')); ?></label>
									<input type="text" class="form-control" id="dlv_city_nm" name="dlv_city_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="dlv_area_nm"><?php echo e(trans('modOrder.dlvAreaNm')); ?></label>
									<input type="hidden" class="form-control" id="dlv_area_id" name="dlv_area_id">
									<input type="text" class="form-control" id="dlv_area_nm" name="dlv_area_nm">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label for="dlv_addr"><?php echo e(trans('modOrder.dlvAddr')); ?></label>
									<input type="text" class="form-control" id="dlv_addr" name="dlv_addr">
									<input type="hidden" class="form-control" id="dlv_lat" name="dlv_lat">
									<input type="hidden" class="form-control" id="dlv_lng" name="dlv_lng">
								</div>
							</div>
						</div>

						<div class="tab-pane" id="tab_3">
							<div class="row">
								<div class="form-group col-md-4">
									<label for="pick_cust_no"><?php echo e(trans('modOrder.pickCustNo')); ?></label>
									<input type="text" class="form-control" id="pick_cust_no" name="pick_cust_no">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_cust_nm"><?php echo e(trans('modOrder.pickCustNm')); ?></label>
									<input type="text" class="form-control" id="pick_cust_nm" name="pick_cust_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_attn"><?php echo e(trans('modOrder.pickAttn')); ?></label>
									<input type="text" class="form-control" id="pick_attn" name="pick_attn">
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-md-4">
									<label for="pick_tel"><?php echo e(trans('modOrder.pickTel')); ?></label>
									<input type="text" class="form-control" id="pick_tel" name="pick_tel">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_email"><?php echo e(trans('modOrder.pickEmail')); ?></label>
									<input type="text" class="form-control" id="pick_email" name="pick_email">
								</div>
							</div>

							<div class="row">
							    <div class="form-group col-md-4">
									<label for="pick_zip"><?php echo e(trans('modOrder.pickZip')); ?></label>
									<input type="text" class="form-control" id="pick_zip" name="pick_zip">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_city_nm"><?php echo e(trans('modOrder.pickCityNm')); ?></label>
									<input type="text" class="form-control" id="pick_city_nm" name="pick_city_nm">
								</div>
								<div class="form-group col-md-4">
									<label for="pick_area_nm"><?php echo e(trans('modOrder.pickAreaNm')); ?></label>
									<input type="hidden" class="form-control" id="pick_area_id" name="pick_area_id">
									<input type="text" class="form-control" id="pick_area_nm" name="pick_area_nm">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-12">
									<label for="pick_addr"><?php echo e(trans('modOrder.pickAddr')); ?></label>
									<input type="text" class="form-control" id="pick_addr" name="pick_addr">
									<input type="hidden" class="form-control" id="pick_lat" name="pick_lat">
									<input type="hidden" class="form-control" id="pick_lng" name="pick_lng">
								</div>
							</div>
						</div>

					</div>

				</div>
			</form>

			<div class="nav-tabs-custom" <?php if(!isset($id)): ?> style="display:none" <?php endif; ?> id="subPanel">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="false"><?php echo e(trans('modOrder.orderDetail')); ?></a></li>
					<li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false"><?php echo e(trans('modOrder.loadingPlan')); ?></a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_3">
						<div class="box box-primary" id="subBox" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title"><?php echo e(trans('modOrder.orderDetail')); ?></h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-3">
											<label for="goods_no"><?php echo e(trans('modOrderDetail.goodsNo')); ?></label>
											<input type="text" class="form-control input-sm" name="goods_no" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="goods_nm"><?php echo e(trans('modOrderDetail.goodsNm')); ?></label>
											<input type="text" class="form-control input-sm" name="goods_nm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="pkg_num"><?php echo e(trans('modOrderDetail.pkgNum')); ?></label>
											<input type="text" class="form-control input-sm" name="pkg_num" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="pkg_unit"><?php echo e(trans('modOrderDetail.pkgUnit')); ?></label>
											<input type="text" class="form-control input-sm" name="pkg_unit" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="gw"><?php echo e(trans('modOrderDetail.gw')); ?></label>
											<input type="text" class="form-control input-sm" name="gw" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="gwu"><?php echo e(trans('modOrderDetail.gwu')); ?></label>
											<input type="text" class="form-control input-sm" name="gwu" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbm"><?php echo e(trans('modOrderDetail.cbm')); ?></label>
											<input type="text" class="form-control input-sm" name="cbm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbmu"><?php echo e(trans('modOrderDetail.cbmu')); ?></label>
											<input type="text" class="form-control input-sm" name="cbmu" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="length"><?php echo e(trans('modOrderDetail.length')); ?></label>
											<input type="text" class="form-control input-sm" name="length" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="weight"><?php echo e(trans('modOrderDetail.weight')); ?></label>
											<input type="text" class="form-control input-sm" name="weight" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="height"><?php echo e(trans('modOrderDetail.height')); ?></label>
											<input type="text" class="form-control input-sm" name="height" grid="true" >
										</div>
									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="Save"><?php echo e(trans('common.save')); ?></button>
									<button type="button" class="btn btn-sm btn-danger" id="Cancel"><?php echo e(trans('common.cancel')); ?></button>
								</div>
							</form>
						</div>

						<div id="jqxGrid"></div>
					</div>

					<div class="tab-pane" id="tab_4">
						<div class="box box-primary" id="sub1Box" style="display:none">
							<div class="box-header with-border">
								<h3 class="box-title"><?php echo e(trans('order.loadingPlan')); ?></h3>
							</div>
							<form method="POST" accept-charset="UTF-8" id="sub1Form" enctype="multipart/form-data">
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="form-group col-md-3">
											<label for="goods_tnm"><?php echo e(trans('modOrderPack.goodsTnm')); ?></label>
											<input type="text" class="form-control input-sm" name="goods_tnm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="pack_no"><?php echo e(trans('modOrderPack.packNo')); ?></label>
											<input type="text" class="form-control input-sm" name="pack_no" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbm"><?php echo e(trans('modOrderPack.cbm')); ?></label>
											<input type="text" class="form-control input-sm" name="cbm" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cbmu"><?php echo e(trans('modOrderPack.cbmu')); ?></label>
											<input type="text" class="form-control input-sm" name="cbmu" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="cntr_no"><?php echo e(trans('modOrderPack.cntrNo')); ?></label>
											<input type="text" class="form-control input-sm" name="cntr_no" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="cntr_max_weight"><?php echo e(trans('modOrderPack.cntrMaxWeight')); ?></label>
											<input type="text" class="form-control input-sm" name="cntr_max_weight" grid="true" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label for="length"><?php echo e(trans('modOrderPack.length')); ?></label>
											<input type="text" class="form-control input-sm" name="length" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="weight"><?php echo e(trans('modOrderPack.weight')); ?></label>
											<input type="text" class="form-control input-sm" name="weight" grid="true" >
										</div>
										<div class="form-group col-md-3">
											<label for="height"><?php echo e(trans('modOrderPack.height')); ?></label>
											<input type="text" class="form-control input-sm" name="height" grid="true" >
										</div>
									</div>
								</div>
								<!-- /.box-body -->

								<div class="box-footer">
									<input type="hidden" class="form-control input-sm" name="id" grid="true">
									<button type="button" class="btn btn-sm btn-primary" id="packSave"><?php echo e(trans('common.save')); ?></button>
									<button type="button" class="btn btn-sm btn-danger" id="packCancel"><?php echo e(trans('common.cancel')); ?></button>
								</div>
							</form>
						</div>
						<div id="packGrid"></div>
					</div>
				</div>
				<!-- /.tab-content -->
			</div>
		</div>	
	</div>

	<!-- Modal -->
	<div class="modal fade" id="packModal" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title"><?php echo e(trans('modOrderPackDetail.packingContent')); ?></h5>
		</div>
		<div class="modal-body">
			<div id="packDetailGrid"></div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(trans('common.close')); ?></button>
		</div>
		</div>
	</div>
	</div>


 <?php $__env->stopSection(); ?> 
 <?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
 <?php $__env->startSection('after_scripts'); ?>
	<script>
		var mainId = "";
		var ord_no = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";

		var fieldData = null;
		var fieldObj = null;

		<?php if(isset($crud -> create_fields)): ?>
		fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		console.log(fieldObj);
		<?php endif; ?>



		<?php if(isset($id)): ?>
		mainId = "<?php echo e($id); ?>";
		editData = '{<?php echo $entry; ?>}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
		editObj = JSON.parse(editData);
		ord_no = editObj.ord_no;
		console.log(editObj);
		<?php endif; ?>




		$(function () {

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";
			formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') .'/get/mod_order')); ?>/" + mainId;
			formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt')); ?>";

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {
				$('#jqxGrid').jqxGrid('clear');
				$('#packGrid').jqxGrid('clear');
				$('#packDetailGrid').jqxGrid('clear');
			}

			formOpt.addFunc = function() {
				$('#jqxGrid').jqxGrid('clear');
				$('#packGrid').jqxGrid('clear');
				$("#subPanel").hide();
			}

			formOpt.editFunc = function() {
				$("#subPanel").show();
				$("#jqxGrid").jqxGrid({'showtoolbar': false});
				$("#packGrid").jqxGrid({'showtoolbar': false});
			}

			formOpt.copyFunc = function() {
				$("#subPanel").hide();
			}

			formOpt.saveSuccessFunc = function() {
				$("#jqxGrid").jqxGrid({'showtoolbar': true});
				$("#packGrid").jqxGrid({'showtoolbar': true});
			}

			var btnGroup = [];

			initBtn(btnGroup);

			$.ajax({
				url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_detail')); ?>",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {
					console.log(fieldData);
					var opt = {};
					opt.gridId = "jqxGrid";
					opt.fieldData = fieldData;
					opt.formId = "subForm";
					opt.saveId = "Save";
					opt.cancelId = "Cancel";
					opt.showBoxId = "subBox";
					opt.height = 300;
					opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderDetail/get')); ?>" + '/' + ord_no;
					opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/store";
					opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/update";
					opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderDetail')); ?>" + "/delete/";
					opt.commonBtn = true;
					opt.showtoolbar = true;
					opt.defaultKey = {
						'ord_no': ord_no
					};
					opt.beforeSave = function (formData) {

					}

					opt.afterSave = function (data) {

					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url: "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_pack')); ?>",
				type: 'GET',
				async: false,
				beforeSend: function () {
				},
				error: function (jqXHR, exception) {

				},
				success: function (fieldData) {
					$.each(fieldData[1], function(i, v){
						if(fieldData[1][i]['text'] == 'cbm') {
							fieldData[1][i]['aggregates'] = ['sum'];
							console.log(fieldData[1][i]['aggregates']);
						}
					});

					var opt = {};
					opt.gridId = "packGrid";
					opt.fieldData = fieldData;
					opt.formId = "sub1Form";
					opt.saveId = "packSave";
					opt.cancelId = "packCancel";
					opt.showBoxId = "sub1Box";
					opt.height = 300;
					opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderPack/get')); ?>" + '/' + ord_no;
					opt.addUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/store";
					opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/update";
					opt.delUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPack')); ?>" + "/delete/";
					opt.showaggregates = true;
					opt.commonBtn = true;
					opt.inlineEdit = false;
					opt.showtoolbar = true;
					opt.custBtn = [
						{
							title: "<?php echo e(trans('modOrderPackDetail.packingContent')); ?>",
							func: function() {
								var selectedrowindex = $("#packGrid").jqxGrid('getselectedrowindex');
								if(selectedrowindex == -1) {
									swal("<?php echo e(trans('modOrder.msg1')); ?>", "", "warning");
									return;
								}
								$('#packDetailGrid').jqxGrid('updatebounddata');
								$('#packModal').modal('show');
							}
						}
					];
					opt.defaultKey = {
						'ord_no': ord_no
					};
					opt.beforeSave = function (formData) {

					}

					opt.afterSave = function (data) {

					}

					genDetailGrid(opt);
				},
				cache: false,
				contentType: false,
				processData: false
			});


			var opt = {};
			var custFieldData = [];
			custFieldData[0] = [
				{name: 'id', type: 'integer'},
				{name: 'goods_no', type: 'string'},
				{name: 'goods_nm', type: 'string'},
				{name: 'num', type: 'integer'},
				{name: 'last_num', type: 'integer'},
			];
			custFieldData[1] = [
				{text:"ID", datafield: 'id', filtertype:"number", width:100, editable: false},
				{text:"<?php echo e(trans('modOrderDetail.goodsNo')); ?>", datafield: 'goods_no', filtertype:"textbox", width:100, editable: false},
				{text:"<?php echo e(trans('modOrderDetail.goodsNm')); ?>", datafield: 'goods_nm', filtertype:"textbox", width:100, editable: false},
				{text:"<?php echo e(trans('modOrderPackDetail.num')); ?>", datafield: 'num', filtertype:"number", width:100},
				{text:"<?php echo e(trans('modOrder.lastNum')); ?>", datafield: 'last_num', filtertype:"number", width:100, editable: false}
			];
			opt.gridId = "packDetailGrid";
			opt.fieldData = custFieldData;
			opt.inlineEdit = true;
			opt.height = 300;
			opt.getUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/orderPackDetail/get')); ?>" + '/' + ord_no;
			opt.updateUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/orderPackDetail')); ?>" + "/update";
			opt.defaultKey = {
				'ord_no': ord_no
			};
			opt.commonBtn = false;
			opt.beforeSave = function (rowData) {
				var returnMsg = [];
				returnMsg[0] = true;
				returnMsg[1] = rowData;
				var num = rowData.num;
				var last_num = rowData.last_num;
				if(num == 0) {
					returnMsg[0] = false;
					return returnMsg;
				}
				if(num > last_num) {
					swal("<?php echo e(trans('modOrder.msg2')); ?>", "", "warning");
					returnMsg[0] = false;
					return returnMsg;
				}

				var selectedrowindex = $("#packGrid").jqxGrid('getselectedrowindex');
				var packData = $("#packGrid").jqxGrid('getrowdata', selectedrowindex);
				rowData = {pack_no: packData.pack_no, ord_no: packData.ord_no, ord_detail_id: rowData.ord_detail_id, num: rowData.num, id: rowData.id, goods_no: rowData.goods_no};
				returnMsg[1] = rowData;
				return returnMsg;
			}

			opt.afterSave = function (data, rowid, newdata) {
				
				newdata.last_num = data.data;
				return newdata;
			}

			genDetailGrid(opt);
		})
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>