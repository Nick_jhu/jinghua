 <?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
				
                <p></p>
				<div style="width:100%;" id="statusList">
                    
                </div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		 
		<!-- /. box -->
	</div>
	<div class="col-md-12">
		<div class="box box-primary">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="button-group">
					<div class="row" id="btnArea">

					</div>
					<div id="jqxGrid"></div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('common.close')); ?></button>
				<button type="button" class="btn btn-primary" id="saveGrid"><?php echo e(trans('common.saveChange')); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php $__env->stopSection(); ?> <?php $__env->startSection('after_scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/core')); ?>/grid-core.js?v=<?php echo e(Config::get('app.version')); ?>"></script>
<script>
	$(function(){

       
    var theme = 'bootstrap';
    $.jqx.theme = theme;
    $('#q-edit').on('click', function(){
        
    });

    if(gridOpt.enabledStatus===false){
        $("#statusDiv").remove();
    }
    
    gridOpt.gridId = "jqxGrid";
    gridOpt.getState = function(){
        loadState();
    };
    //gridOpt.getState = function(){};

    var fieldData = null;
    <?php if(count($crud -> colModel) > 0): ?>
		fieldData = '{<?php echo json_encode($crud->colModel); ?>}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		console.log(fieldObj);
        initGrid(gridOpt.gridId,fieldObj, gridOpt.dataUrl+"/"+gridOpt.enabledStatus,gridOpt);
    <?php else: ?>
    $.get( gridOpt.fieldsUrl, function( fieldData ) {
        console.log(fieldData);
        //alert(thisUrl);
        initGrid(gridOpt.gridId,fieldData, gridOpt.dataUrl+"/"+gridOpt.enabledStatus,gridOpt);
        /*$("#"+gridOpt.gridId).on("bindingcomplete", 
        function (event) {
            alert("bindingcomplete");
            loadState();
        }); */           
        

        // Create a jqxMenu
        //$("#statusList").jqxMenu();
        //$("#statusList").css('visibility', 'visible');
       
    });

    <?php endif; ?>

    


    $('#'+gridOpt.gridId).on('rowdoubleclick', function (event) 
    { 
        var args = event.args;
        // row's bound index.
        var boundIndex = args.rowindex;
        // row's visible index.
        var visibleIndex = args.visibleindex;
        // right click.
        var rightclick = args.rightclick; 
        // original event.
        var ev = args.originalEvent;

        var datarow = $("#"+gridOpt.gridId).jqxGrid('getrowdata', boundIndex);
        console.log(datarow);

        var editUrl = gridOpt.editUrl.replace("{id}", datarow.id);
        location.href = editUrl;
    });

    $("#saveGrid").on("click", function(){
        var items = $("#jqxlistbox").jqxListBox('getItems');
        //$('#jqxGrid').jqxGrid('clear');
        $("#"+gridOpt.gridId).jqxGrid('beginupdate');

        $.each(items, function(i, item) {
            var thisIndex = $('#'+gridOpt.gridId).jqxGrid('getcolumnindex', item.value)-1;
            if(thisIndex != item.index){
                //console.log(item.value+":"+thisIndex+"="+item.index);
                $('#'+gridOpt.gridId).jqxGrid('setcolumnindex', item.value,  item.index);
            }
            if (item.checked) {
                $("#"+gridOpt.gridId).jqxGrid('showcolumn', item.value);
            }
            else {
                $("#"+gridOpt.gridId).jqxGrid('hidecolumn', item.value);
            }
        })
        
        $("#"+gridOpt.gridId).jqxGrid('endupdate');
        state = $("#"+gridOpt.gridId).jqxGrid('getstate');

        var saveUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/saveLayoutJson')); ?>"; 	  	  	
        var stateToSave = JSON.stringify(state);

        $.ajax({
            type: "POST",										
            url: saveUrl,		
            data: { data: stateToSave,key: gridOpt.dataUrl },		 
            success: function(response) {
                if(response == "true"){
                    alert("save successful");
                    $('#gridOptModal').modal('hide');
                }else{
                    alert("save failded");
                }
                
            }
        });	
    });

    function loadState() {  	
        var loadURL = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api'). '/admin/baseApi/getLayoutJson')); ?>"; 	  	  	
                    
        $.ajax({
            type: "GET", //  OR POST WHATEVER...
            url: loadURL,
            data: { key: gridOpt.dataUrl },		 
            success: function(response) {										
                if (response != "") {	
                    response = JSON.parse(response);
                    
                    //$("#"+gridOpt.gridId).jqxGrid('loadstate', response);
                }
                
                var listSource = [];
        
                state = $("#"+gridOpt.gridId).jqxGrid('getstate');

        
                $.each(state.columns, function(i, item) {
                    if(item.text != "" && item.text != "undefined"){
                        listSource.push({ 
                        label: item.text, 
                        value: i, 
                        checked: !item.hidden });
                    }
                })
                $("#jqxlistbox").jqxListBox({ 
                    allowDrop: true, 
                    allowDrag: true,
                    source: listSource,
                    width: "99%",
                    height: 200,
                    checkboxes: true,
                    filterable: true,
                    searchMode: 'contains'
                });
            }
        });			  	  	  	  	
    }	

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
});

</script>

<?php $__env->stopSection(); ?>