

<?php $__env->startSection('header'); ?>
    <section class="content-header">
      <h1>
      <?php echo e(trans('sysArea.titleName')); ?><small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix'),'dashboard')); ?>"><?php echo e(trans('backpack::crud.admin')); ?></a></li>
        <li><a href="<?php echo e(url(config('backpack.base.route_prefix', 'admin').'/log')); ?>"><?php echo e(trans('backpack::logmanager.log_manager')); ?></a></li>
        <li class="active"><?php echo e(trans('backpack::logmanager.existing_logs')); ?></li>
      </ol>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
    <?php echo $__env->make('backpack::template.toolbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-md-10">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4><?php echo e(trans('backpack::crud.please_fix')); ?></h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?php echo e(trans('sysArea.titleName')); ?></a></li>                    
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="cntry_cd"><?php echo e(trans('sysArea.cntryCd')); ?></label>
                                        <input type="text" class="form-control" id="cntry_cd" name="cntry_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cntry_nm"><?php echo e(trans('sysArea.cntryNm')); ?></label>
                                        <input type="text" class="form-control" id="cntry_nm" name="cntry_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>                                
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="state_cd"><?php echo e(trans('sysArea.stateCd')); ?></label>
                                        <input type="text" class="form-control" id="state_cd" name="state_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="state_nm"><?php echo e(trans('sysArea.stateNm')); ?></label>
                                        <input type="text" class="form-control" id="state_nm" name="state_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>       
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="city_cd"><?php echo e(trans('sysArea.cityCd')); ?></label>
                                        <input type="text" class="form-control" id="city_cd" name="city_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="city_nm"><?php echo e(trans('sysArea.cityNm')); ?></label>
                                        <input type="text" class="form-control" id="city_nm" name="city_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>       
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="dist_cd"><?php echo e(trans('sysArea.distCd')); ?></label>
                                        <input type="text" class="form-control" id="dist_cd" name="dist_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="dist_nm"><?php echo e(trans('sysArea.distNm')); ?></label>
                                        <input type="text" class="form-control" id="dist_nm" name="dist_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="zip_f"><?php echo e(trans('sysArea.zipF')); ?>)</label>
                                        <input type="text" class="form-control" id="zip_f" name="zip_f" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="zip_e"><?php echo e(trans('sysArea.zipE')); ?></label>
                                        <input type="text" class="form-control" id="zip_e" name="zip_e" placeholder="Enter No.">
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><?php echo e(trans('sysArea.remark')); ?></label>
                                            <textarea class="form-control" rows="3" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>              
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by"><?php echo e(trans('sysArea.createdBy')); ?></label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at"><?php echo e(trans('sysArea.createdAt')); ?></label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by"><?php echo e(trans('sysArea.updatedBy')); ?></label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at"><?php echo e(trans('sysArea.updatedAt')); ?></label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                <?php if(isset($id)): ?>
                                    <input type="hidden" name="id" value="<?php echo e($id); ?>" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                <?php endif; ?>

                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->                    
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div>    


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::template.lookup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startSection('after_scripts'); ?>


<script>
    var mainId = "";
    var cust_no = "";
    var editData = null;
    var editObj = null;
    var SAVE_URL = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysArea')); ?>";

    var fieldData = null;
    var fieldObj = null;

    <?php if(isset($crud->create_fields)): ?>
        fieldData = '{<?php echo json_encode($crud->create_fields); ?>}';
        fieldData = fieldData.substring(1);
        fieldData = fieldData.substring(0, fieldData.length - 1);
        fieldObj = JSON.parse(fieldData);
        console.log(fieldObj);
    <?php endif; ?>

    
    
    <?php if(isset($id)): ?>
        mainId = "<?php echo e($id); ?>";
        editData = '{<?php echo $entry; ?>}';
        editData = editData.substring(1);
        editData = editData.substring(0, editData.length - 1);
        editObj = JSON.parse(editData);
        cust_no= editObj.cust_no;
        console.log(editObj);
    <?php endif; ?>

  
   

    $(function(){

        $( "#lookupEvent" ).on( "custContactFunc", function(event,indexs) {
            /*$.each(indexs,function(k,v){
                alert(k+v);
            });*/
            alert(1);
        });
        //var formOpt = {};
        formOpt.formId = "myForm";
        formOpt.editObj = editObj;
        formOpt.fieldObj = fieldObj;
        formOpt.editUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysArea')); ?>";
        formOpt.fieldsUrl = "<?php echo e(url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_area')); ?>";
        formOpt.saveUrl = "<?php echo e(url(config('backpack.base.route_prefix', 'admin') . '/sysArea')); ?>";
        
        formOpt.initFieldCustomFunc = function (){
            
        };                
    })
</script>    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>