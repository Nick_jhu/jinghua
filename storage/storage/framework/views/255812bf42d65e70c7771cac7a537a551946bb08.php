

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body">
                <a class="btn btn-app" id="iBack" style="margin-right: 35px !important;" href="<?php echo e(url($crud->route)); ?>"><i class="fa fa-arrow-left"></i> Back</a>
                
                <a class="btn btn-app" id="iAdd"><i class="fa fa-plus"></i> <?php echo e(trans('common.add')); ?></a>
                
                <a class="btn btn-app" id="iCopy"><i class="fa fa-files-o"></i> <?php echo e(trans('common.copy')); ?></a>
                
                <a class="btn btn-app" id="iEdit"><i class="fa fa-pencil"></i> <?php echo e(trans('common.edit')); ?></a>
                
                <a class="btn btn-app" id="iDel"><i class="fa fa-trash-o"></i> <?php echo e(trans('common.delete')); ?></a>
                
                <a class="btn btn-app" id="iSave"><i class="fa fa-floppy-o"></i> <?php echo e(trans('common.save')); ?></a>
                <a class="btn btn-app" id="iCancel"><i class="fa fa-ban"></i> <?php echo e(trans('common.cancel')); ?></a>
            </div>
        </div>
    </div>
</div>

<?php $__env->startSection('toolbar_scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/core')); ?>/edit-var.js"></script>
<script>

$(function(){
            formOpt.initFieldCustomFunc();
            
            //初始化欄位
            $.get( formOpt.fieldsUrl , function( data ) {
                if(mainId == "") {
                    //initField(data);
                    menuBtnFunc.addFunc();
                    formOpt.addFunc();
                    setField.init(formOpt.formId,formOpt.fieldObj, true);
                }
                else {
                    //initField(data);
                    setField.init(formOpt.formId,formOpt.fieldObj, true);

                    //若是編輯進來的，要把資料塞入Form中
                    if(data !== null) {
                        setFormData(formOpt.formId,data);
                        formOpt.setFormDataFunc();
                    }
                }
            });
    
            $('#iAdd').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[switch="off"]').prop('disabled', false);
                $('select').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                $(':input:not(:checkbox, :radio)').not('[switch="off"]').val('');
                $('input[name="_method"]').val('POST');
                SAVE_URL = formOpt.saveUrl;
                if(typeof OLD_DATA != "undefined") {
                    OLD_DATA = [];
                }
                menuBtnFunc.addFunc();
                
                formOpt.addFunc();
            });
    
            $('#iEdit').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[grid="true"]').not('[switch="off"]').prop('disabled', false);
                $('select').not('[grid="true"]').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                
                SAVE_URL = formOpt.saveUrl + '/' + mainId;
                menuBtnFunc.editFunc();
                
                formOpt.editFunc();
            });
    
            $('#iCopy').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[switch="off"]').prop('disabled', false);
                $('select').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                $('input[name="_method"]').val('POST');
                menuBtnFunc.addFunc();
                formOpt.addFunc();
                
                formOpt.copyFunc();
            });
    
            $('#iCancel').on('click', function(){
                location.reload();
                formOpt.cancelFunc();
            });
            
            $('#iSave').on('click', function(){
                var changeData = getChangeData(formOpt.formId, new FormData($('#'+formOpt.formId)[0]));
                $.ajax({
                    url: SAVE_URL,
                    type: 'POST',
                    data: changeData,
                    async: false,
                    beforeSend: function () {
                        loadingFunc.show();
                    },
                    error: function (jqXHR, exception) {
                        try{
                            var msgObj = JSON.parse(jqXHR.responseText);
                            $('#errorMsg').find('ul').html('');
                            $.each(msgObj, function(i, v){
                                $('#errorMsg').find('ul').append('<li>' + v + '</li>');
                                $('#errorMsg').show();
                            });
                            
                        }
                        catch(err){
                            new PNotify(PNotifyOpt.saveError);
                        }
                        formOpt.saveErrorFunc();
                        loadingFunc.hide();
                        return false;
                    },
                    success: function (data) {
                        if(data.msg == "success") {
                            $('#errorMsg').hide();
                            new PNotify(PNotifyOpt.saveSuccess);
    
                            if(typeof data.lastId != "undefined") {
                                location.href = formOpt.editUrl  + "/" + data.lastId + "/edit";
                            }
    
                            setField.disabledAll(formOpt.formId,fieldObj, true);
                            formOpt.saveSuccessFunc();
                            
                        }
                        loadingFunc.hide();
                        return false;
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
    
            $('#iDel').on('click', function(e){
                e.preventDefault();
                var delete_button = $(this);
                var delete_url = SAVE_URL + '/' + mainId;

                

                if (formOpt.beforeDelFunc() && confirm("<?php echo e(trans('common.msg2')); ?>?") == true) {
                    $.ajax({
                        url: delete_url,
                        type: 'DELETE',
                        success: function(result) {
                            // Show an alert with the result
                            new PNotify(PNotifyOpt.delSuccess);
                            $('#iAdd').click();
                            menuBtnFunc.addFunc();
                            formOpt.addFunc();
                            location.href = formOpt.editUrl + '/create';
                        },
                        error: function(result) {
                            // Show an alert with the result
                            new PNotify(PNotifyOpt.delError);
                        }
                    });
                } else {
                    new PNotify(PNotifyOpt.delError);
                }
    
            });
          
        });

        function initBtn(btnGroup){
            $.each(btnGroup, function(i, item) {
                var btnHtml = '<li><a class="MenuButton" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a></li>';
                btnHtml = btnHtml.replace("{btnId}",item.btnId);
                btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
                btnHtml = btnHtml.replace("{btnText}",item.btnText);
                $("#btnArea").append(btnHtml);
                $("#"+item.btnId).on("click",function(){
                    item.btnFunc();
                });
            })
        }
</script>
<?php $__env->stopSection(); ?>